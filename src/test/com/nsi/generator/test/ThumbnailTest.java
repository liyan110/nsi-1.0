package com.nsi.generator.test;


import com.nsi.util.CompositeImageUtil;
import com.nsi.util.ImageUtil;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Coordinate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import java.net.URL;

@Slf4j
public class ThumbnailTest {

    public static void main(String[] args) throws Exception {
        URL url = new URL("http://nsi.oss-cn-zhangjiakou.aliyuncs.com/nsi-event/vis2020/VIP.jpg");
        URL qrUrl = new URL("http://qr.topscan.com/api.php?text=123123&w=260&h=260");


        String filePath = "C:/upImage/event/eventTickets/" + System.currentTimeMillis() + ".jpg";

        long currentTime = System.currentTimeMillis();

        BufferedImage bufferedImage = Thumbnails.of(url)
                .watermark(new Coordinate(230, 490), ImageIO.read(qrUrl), 1f)
                .scale(1f)
                .asBufferedImage();


        String username = "罗 震";
        int x = 0;
        if (ThumbnailTest.isChinese(username)) {
            x = -(50 * username.length() / 2);
        } else {
            x = -(23 * username.length() / 2);
        }

        ImageUtil imageUtil = new ImageUtil();
        bufferedImage = imageUtil.modifyImage(bufferedImage, username, x, 290);
        imageUtil.writeImageLocal(filePath, bufferedImage);

        long endTime = System.currentTimeMillis();

        System.out.println("文件路径:" + filePath + ",耗时:" + (endTime - currentTime) / 1000 + "秒");
    }

    private static boolean isChinese(String str) {
        if (str == null) {
            return false;
        }
        for (char c : str.toCharArray()) {
            // 如果是中文
            if (CompositeImageUtil.isChinese(c)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isChinese(char c) {
        return c >= 0x4E00 && c <= 0x9FA5;
    }


}
