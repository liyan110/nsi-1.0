package com.nsi.generator.test;

public class TestCompareAndSwap {

    public static void main(String[] args) {

        final CompareAndSawp cas = new CompareAndSawp();

        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    int vlaue = cas.get();
                    boolean a = cas.compareAndSet(vlaue, (int) (Math.random() * 100));
                    System.out.println(a);

                }
            }).start();
        }

    }

}

class CompareAndSawp {

    private int value;

    // 获取
    public synchronized int get() {
        return value;
    }

    // 比较
    public synchronized int compareAndSwap(int expecteValue, int newValue) {
        int oldValue = value;
        if (oldValue == expecteValue) {
            this.value = newValue;
        }
        return oldValue;
    }

    // 设置
    public synchronized boolean compareAndSet(int expectedValue, int newValue) {
        return expectedValue == compareAndSwap(expectedValue, newValue);
    }
}