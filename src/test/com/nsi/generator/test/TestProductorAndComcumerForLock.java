package com.nsi.generator.test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestProductorAndComcumerForLock {
    public static void main(String[] args) {

        Clerk clerk = new Clerk();

        Product product = new Product(clerk);
        Comsumer comsumer = new Comsumer(clerk);

        new Thread(product, "生产者——A").start();
        new Thread(comsumer, "消费者——B").start();

        new Thread(product, "生产者——C").start();
        new Thread(comsumer, "消费者——D").start();
    }

}

// 店员
class Clerk {

    private int product = 0;

    private ReentrantLock lock = new ReentrantLock();

    private Condition condition = lock.newCondition();

    public void get() {
        lock.lock();
        try {
            while (product >= 1) {
                System.out.println("产品已满，无法添加");
                try {
                    condition.await();
                } catch (InterruptedException e) {
                }
            }
            System.out.println(Thread.currentThread() + ":" + ++product);
            condition.signalAll();
        } finally {
            lock.unlock();
        }

    }

    public void sale() {
        lock.lock();
        try {
            while (product <= 0) {
                System.out.println("缺货...");
                try {
                    condition.await();
                } catch (InterruptedException e) {
                }
            }
            System.out.println(Thread.currentThread() + ":" + --product);
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}

// 生产者
class Product implements Runnable {

    private Clerk clerk;

    public Product(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.get();
        }
    }
}

// 消费者
class Comsumer implements Runnable {

    private Clerk clerk;

    public Comsumer(Clerk clerk) {
        this.clerk = clerk;
    }


    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            clerk.sale();
        }
    }
}

