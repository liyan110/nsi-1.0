package com.nsi.generator.test;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestLock {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();

        ExecutorService executors = Executors.newFixedThreadPool(5);
        executors.submit(new Thread(ticket, "1号窗口"));
        executors.submit(new Thread(ticket, "2号窗口"));
        executors.submit(new Thread(ticket, "3号窗口"));

        executors.shutdown();
    }

}


class Ticket implements Runnable {

    private int ticket = 50;

    private Lock lock = new ReentrantLock();

    @Override
    public void run() {

        while (true) {

            lock.lock();
            if (ticket > 0) {
                try {
                    Thread.sleep(200);
                    System.out.println(Thread.currentThread().getName() + "：" + --ticket);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }

        }

    }
}