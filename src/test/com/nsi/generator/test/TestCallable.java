package com.nsi.generator.test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TestCallable {

    public static void main(String[] args) {

        ThreadDemo threadDemo = new ThreadDemo();

        FutureTask<Integer> task = new FutureTask<>(threadDemo);
        long start = System.currentTimeMillis();
        new Thread(task).start();

        long end = System.currentTimeMillis();
        try {
            System.out.println("---------------------------");
            System.out.println(task.get());
            System.out.println("结束时间:" + (end - start));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

}

class ThreadDemo implements Callable<Integer> {


    @Override
    public Integer call() throws Exception {
        int sum = 0;

        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            sum += i;
        }
        return sum;
    }
}