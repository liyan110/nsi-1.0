<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>${school.schoolName}</title>
</head>
<body style="overflow-y: hidden ">
    <div style="display: none">
        <h1>${school.schoolName}</h1>
        <img src="${school.schoolShow}"/>
        <p>${school.schoolEnglishname}</p>
        <p>${school.schoolProperties}</p>
        <p>${school.areas03}</p>
    </div>
    <iframe id="iframeId" src="http://data.xinxueshuo.cn/nsi/school/detail.html?School_name=${school.id}&whereFrom=search" width="100%" height="900px"></iframe>
    <script type="text/javascript">
        // function reinitIframe(){
        //     var iframe = document.getElementById("iframeId");
        //     try{
        //         var bHeight = iframe.contentWindow.document.body.scrollHeight;
        //         var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
        //         var height = Math.max(bHeight, dHeight);
        //         iframe.height = height;
        //         console.log(height);
        //     }catch (ex){}
        // }
        // window.setInterval("reinitIframe()", 200);
    </script>
</body>

</html>