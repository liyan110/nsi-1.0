package com.old.nsi_class.DB;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间操作的类
 * ()
 * @author 12984
 *
 */
public class DateUtils {

	
    /**
     * @return 当前日期 标准格式
     */
	public static String onToday() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
    /**
     * @return 当前日期 全数字格式
     */
	public static String NowTimeNum() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}
	
    /**
     * @return 当前日期（精确到天）
     */
	public static String NowDay() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	
	/**
	 * @return 当前日期前一天
	 */
	public static String yesterday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	}
	
	/**
	 * @return 根据当前时间计算上周一
	 */
	public static String lastMondy() {
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)-1;
		int offset = 1-dayOfWeek;
		cal.add(Calendar.DATE, offset-7);
		return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	}
	
	/**
	 * @return 根据当前时间计算上周日
	 */
	public static String lastSunday() {
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)-1;
		int offset = 7-dayOfWeek;
		cal.add(Calendar.DATE, offset-7);
		return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	}
	
	/**
	 * @return 根据当前时间计算本周一
	 */
	public static String onMondy() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
		//判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出现问题
		if(1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		//设置一个星期的第一天应该是星期一
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		//获得当前日期是这个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		//给当前日期减去星期几与 一个星期第一天的差值
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek()-day);
		return sdf.format(cal.getTime());
	}
}
