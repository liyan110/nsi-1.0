package com.old.nsi_class.DB;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 通过此对象封装控制层返回的JSON结果
 * 便于对控制层返回数据进行统一格式化,
 * 友好性管理*/
public class JSONResult2 {
    public static final int SUCCESS=0;
    public static final int ERROR=-2;
    public static final int FAULT = -3;
	/**服务端的响应状态*/
	private int code;
	/**信息(给用户的提示)*/
	private String message; 
	/**前端返回格式*/
	private String msg;
	/**数据库查询条数(有必要的时候给一下)*/
	private int count;
	/**具体业务数据1*/
	private Object data;
	/**具体业务数据2*/
	private Object data2;
	
	
	public JSONResult2() {
		this.code=SUCCESS;
		this.msg="";
	}
	public JSONResult2(Object data) {
		this();
		this.data = data;
	}
	
	public JSONResult2(int count,Object data) {
		this();
		this.count = count;
		this.data = data;
	}
	public JSONResult2(Object data,Object data2) {
		this();
		this.data = data;
		this.data2 = data2;
	}
	
	public JSONResult2(String msg) {
		this.code = ERROR;
		this.msg = msg;
	}
	

	
	public String getMessage() {
		return message;
	}
	public Object getData() {
		return data;
	}
	public int getState() {
		return code;
	}
	public int getCount() {
		return count;
	}
	public Object getData2() {
		return data2;
	}
	
    /**
     * 返回结果集0(基于前端框架的返回格式)
     * @param req
     * @param res
     * @param data
     * @throws IOException
     */
	public static void toResult(HttpServletRequest req, HttpServletResponse res, String data) throws IOException {
		res.setContentType("application/json;charset=UTF-8");
		res.getWriter().write(data);
	}
	
	   /**
     * 返回结果集0
     * @param req
     * @param res
     * @throws IOException
     */
	public static void toResult(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String back="{\"code\":"+SUCCESS+"}";
		res.setContentType("application/json;charset=UTF-8");  
		res.getWriter().write(back);
	}
	
    /**
     * 返回结果集-2
     * @param req
     * @param res
     * @throws IOException
     */
	public static void toResultError(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String back="{\"code\":"+ERROR+"}";
		res.setContentType("application/json;charset=UTF-8");  
		res.getWriter().write(back);
	}
	
	/**
	 * 返回结果集-3
	 * @param req
	 * @param res
	 * @return -3
	 */
	public static void toResultFault(HttpServletRequest req,HttpServletResponse res) throws IOException {
		String back="{\"code\":"+FAULT+"}";
		res.setContentType("application/json;charset=UTF-8");  
		res.getWriter().write(back);
	}
}
