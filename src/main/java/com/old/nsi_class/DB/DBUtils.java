package com.old.nsi_class.DB;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * C3P0 连接池
 * 
 * @author 
 *
 */
public class DBUtils {

	private static DBUtils dbUtils;
	private ComboPooledDataSource ds;
	
	static {
		dbUtils = new DBUtils();
	}
	
	public DBUtils() {
		try {
			ds = new ComboPooledDataSource();
			ds.setUser("root");
			ds.setPassword("123456");
			ds.setJdbcUrl(""
					+ "jdbc:mysql://localhost:3306/nsi_class_database?characterEncoding=UTF8&useSSL=true");
			ds.setDriverClass("com.mysql.jdbc.Driver");
			ds.setInitialPoolSize(10);
			ds.setMinPoolSize(5);
			ds.setMaxStatements(30);
			ds.setMaxPoolSize(15);
			ds.setMaxIdleTime(30);

//			System.out.println("-!-!-!-:调试：创建c3p0连接池，配置步骤！！！");
		} catch (PropertyVetoException e) {
			throw new RuntimeException(e);
		}
	}
	
	public final static DBUtils getInstance() {
		return dbUtils;
	}
	
	public final Connection getConnection() {
		try {
			return ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("无法连接数据库...");
		}
		return null;
	}
	
	public static void getClose(Connection conn, PreparedStatement ps, ResultSet rs) {


        if(rs!=null){  
            try {  
                rs.close();  
            } catch (SQLException e) {  
                e.printStackTrace();  
            }  
        }  
        if(ps!=null){  
            try {  
            	ps.close();  
            } catch (SQLException e) {  
                e.printStackTrace();  
            }  
        }  
  
        if(conn!=null){  
            try {  
                conn.close();  
            } catch (SQLException e) {  
                e.printStackTrace();  
            }  
        }    
	}
}	
