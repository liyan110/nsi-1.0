package com.old.nsi_class.model;

public class Properties_model {

	private int id;
	private String teacherId;
	private String courseId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	@Override
	public String toString() {
		return "[id=" + id + ", teacherId=" + teacherId + ", courseId=" + courseId + "]";
	}
	
	
	
}
