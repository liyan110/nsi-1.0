package com.old.nsi_class.model;

public class Teacher_model {
	
	//编号
	private int Id;
	//教师姓名
	private String TeacherName;
	//教师简介
	private String TeacherDescription;
	//教师课程
	private String TeacherCourse;
	//教师图片地址
	private String TeacherImage;
	//富文本编辑内容
	private String Html01;
	//提交人姓名
	private String Load_people;
	//提交时间
	private String Load_time;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTeacherName() {
		return TeacherName;
	}
	public void setTeacherName(String teacherName) {
		TeacherName = teacherName;
	}
	public String getTeacherDescription() {
		return TeacherDescription;
	}
	public void setTeacherDescription(String teacherDescription) {
		TeacherDescription = teacherDescription;
	}
	public String getTeacherCourse() {
		return TeacherCourse;
	}
	public void setTeacherCourse(String teacherCourse) {
		TeacherCourse = teacherCourse;
	}
	public String getTeacherImage() {
		return TeacherImage;
	}
	public void setTeacherImage(String teacherImage) {
		TeacherImage = teacherImage;
	}
	public String getHtml01() {
		return Html01;
	}
	public void setHtml01(String html01) {
		Html01 = html01;
	}
	public String getLoad_people() {
		return Load_people;
	}
	public void setLoad_people(String load_people) {
		Load_people = load_people;
	}
	public String getLoad_time() {
		return Load_time;
	}
	public void setLoad_time(String load_time) {
		Load_time = load_time;
	}
	@Override
	public String toString() {
		return "[Id=" + Id + ", TeacherName=" + TeacherName + ", TeacherDescription=" + TeacherDescription
				+ ", TeacherCourse=" + TeacherCourse + ", TeacherImage=" + TeacherImage + ", Html01=" + Html01
				+ ", Load_people=" + Load_people + ", Load_time=" + Load_time + "]";
	}
	
	
	
	
	
	
	
}
