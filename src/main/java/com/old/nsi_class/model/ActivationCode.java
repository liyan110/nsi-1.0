package com.old.nsi_class.model;

/**
 * 激活码实体类
 * @author 12984
 *
 */
public class ActivationCode {

	/**编号*/
	private int id;
	/**激活码*/
	private String activationCode;
	/**提交时间*/
	private String loadTime;
	/**使用时间*/
	private String userTime;
	/**状态*/
	private String valid;
	/**备注*/
	private String text;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public String getLoadTime() {
		return loadTime;
	}
	public void setLoadTime(String loadTime) {
		this.loadTime = loadTime;
	}
	public String getUserTime() {
		return userTime;
	}
	public void setUserTime(String userTime) {
		this.userTime = userTime;
	}
	public String getValid() {
		return valid;
	}
	public void setValid(String valid) {
		this.valid = valid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		return "[id=" + id + ", activationCode=" + activationCode + ", loadTime=" + loadTime
				+ ", userTime=" + userTime + ", valid=" + valid + ", text=" + text + "]";
	}
	
	
	
}
