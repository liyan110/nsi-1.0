package com.old.nsi_class.model;


public class Image_model {

	private int id;
	private String lowFile;
	private String bigFile;
	private String createTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLowFile() {
		return lowFile;
	}
	public void setLowFile(String lowFile) {
		this.lowFile = lowFile;
	}
	public String getBigFile() {
		return bigFile;
	}
	public void setBigFile(String bigFile) {
		this.bigFile = bigFile;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "[id=" + id + ", lowFile=" + lowFile + ", bigFile=" + bigFile + ", createTime=" + createTime
				+ "]";
	}
	
	
}
