package com.old.nsi_class.model;

public class JsonTransferObject {
	String object01;
	String object02;
	String object03;
	String object04;
	String object05;
	String object06;
	String object07;
	String object08;
	String object09;
	String object10;
	
	public String getObject01() {
		return object01;
	}
	public void setObject01(String object01) {
		this.object01 = object01;
	}
	public String getObject02() {
		return object02;
	}
	public void setObject02(String object02) {
		this.object02 = object02;
	}
	public String getObject03() {
		return object03;
	}
	public void setObject03(String object03) {
		this.object03 = object03;
	}
	public String getObject04() {
		return object04;
	}
	public void setObject04(String object04) {
		this.object04 = object04;
	}
	public String getObject05() {
		return object05;
	}
	public void setObject05(String object05) {
		this.object05 = object05;
	}
	public String getObject06() {
		return object06;
	}
	public void setObject06(String object06) {
		this.object06 = object06;
	}
	public String getObject07() {
		return object07;
	}
	public void setObject07(String object07) {
		this.object07 = object07;
	}
	public String getObject08() {
		return object08;
	}
	public void setObject08(String object08) {
		this.object08 = object08;
	}
	public String getObject09() {
		return object09;
	}
	public void setObject09(String object09) {
		this.object09 = object09;
	}
	public String getObject10() {
		return object10;
	}
	public void setObject10(String object10) {
		this.object10 = object10;
	}
	
}
