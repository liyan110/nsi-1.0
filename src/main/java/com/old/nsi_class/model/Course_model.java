package com.old.nsi_class.model;

public class Course_model {
	
	//编号ID
	private int Id;
	//频道号
	private String ChannelNumber;
	//频道名
	private String ChannelName;
	//固定SecretKey
	private String Secretkey;
	//教师ID
	private String TeacherId;	
	private String Html01;
	private String Html02;
	private String Html03;
	private String Html04;	
	//课程主题
	private String CourseSubject;
	//课程名字
	private String CourseName;
	//课程状态
	private String CourseState;
	//课程介绍
	private String CourseDescription;
	//开课时间
	private String ClassBegins;
	//课程评价
	private String CoursePraise;
	//课程单价
	private String CoursePrice;
	//课程发布时间
	private String CourseRelease;
	//课程发布人
	private String CoursePeople;
	//封面图片
	private String CoverImage;
	//课程图片
	private String CourseImage;
	
	


	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getChannelNumber() {
		return ChannelNumber;
	}
	public void setChannelNumber(String channelNumber) {
		ChannelNumber = channelNumber;
	}
	public String getChannelName() {
		return ChannelName;
	}
	public void setChannelName(String channelName) {
		ChannelName = channelName;
	}
	public String getSecretkey() {
		return Secretkey;
	}
	public void setSecretkey(String secretkey) {
		Secretkey = secretkey;
	}
	public String getTeacherId() {
		return TeacherId;
	}
	public void setTeacherId(String teacherId) {
		TeacherId = teacherId;
	}
	public String getHtml01() {
		return Html01;
	}
	public void setHtml01(String html01) {
		Html01 = html01;
	}
	public String getHtml02() {
		return Html02;
	}
	public void setHtml02(String html02) {
		Html02 = html02;
	}
	public String getHtml03() {
		return Html03;
	}
	public void setHtml03(String html03) {
		Html03 = html03;
	}
	public String getHtml04() {
		return Html04;
	}
	public void setHtml04(String html04) {
		Html04 = html04;
	}	
	public String getCourseSubject() {
		return CourseSubject;
	}
	public void setCourseSubject(String courseSubject) {
		CourseSubject = courseSubject;
	}
	public String getCourseName() {
		return CourseName;
	}
	public void setCourseName(String courseName) {
		CourseName = courseName;
	}
	public String getCourseState() {
		return CourseState;
	}
	public void setCourseState(String courseState) {
		CourseState = courseState;
	}
	public String getCourseDescription() {
		return CourseDescription;
	}
	public void setCourseDescription(String courseDescription) {
		CourseDescription = courseDescription;
	}
	public String getClassBegins() {
		return ClassBegins;
	}
	public void setClassBegins(String classBegins) {
		ClassBegins = classBegins;
	}
	public String getCoursePraise() {
		return CoursePraise;
	}
	public void setCoursePraise(String coursePraise) {
		CoursePraise = coursePraise;
	}
	public String getCoursePrice() {
		return CoursePrice;
	}
	public void setCoursePrice(String coursePrice) {
		CoursePrice = coursePrice;
	}
	public String getCourseRelease() {
		return CourseRelease;
	}
	public void setCourseRelease(String courseRelease) {
		CourseRelease = courseRelease;
	}
	public String getCoursePeople() {
		return CoursePeople;
	}
	public void setCoursePeople(String coursePeople) {
		CoursePeople = coursePeople;
	}
	public String getCoverImage() {
		return CoverImage;
	}
	public void setCoverImage(String coverImage) {
		CoverImage = coverImage;
	}
	public String getCourseImage() {
		return CourseImage;
	}
	public void setCourseImage(String courseImage) {
		CourseImage = courseImage;
	}
	
	@Override
	public String toString() {
		return "Course_model [Id=" + Id + ", ChannelNumber=" + ChannelNumber + ", ChannelName=" + ChannelName
				+ ", Secretkey=" + Secretkey + ", TeacherId=" + TeacherId + ", Html01=" + Html01 + ", Html02=" + Html02
				+ ", Html03=" + Html03 + ", Html04=" + Html04 + ", CourseSubject=" + CourseSubject + ", CourseName="
				+ CourseName + ", CourseDescription=" + CourseDescription + ", ClassBegins=" + ClassBegins
				+ ", CoursePraise=" + CoursePraise + ", CoursePrice=" + CoursePrice + ", CourseRelease=" + CourseRelease
				+ ", CoursePeople=" + CoursePeople + ", CoverImage=" + CoverImage + ", CourseImage=" + CourseImage
				+ "]";
	}
	
}
