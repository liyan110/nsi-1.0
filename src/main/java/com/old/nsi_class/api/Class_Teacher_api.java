package com.old.nsi_class.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.old.nsi_class.DB.Course_DB;
import com.old.nsi_class.DB.DateUtils;
import com.old.nsi_class.DB.JsonResult;
import com.old.nsi_class.model.JsonTransferObject;
import com.old.nsi_class.model.Teacher_model;

@WebServlet("/Class_Teacher_api")
public class Class_Teacher_api extends HttpServlet {

	/**
	 * 序列化对象
	 */
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		this.doPost(req, res);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		String whereFrom = req.getParameter("whereFrom");
		// 所有参数
		String TeacherName = req.getParameter("TeacherName");
		String TeacherDescription = req.getParameter("TeacherDescription");
		String TeacherCourse = req.getParameter("TeacherCourse");
		String TeacherImage = req.getParameter("TeacherImage");
		String Html01 = req.getParameter("Html01");
		String Load_people = req.getParameter("Load_people");
		/**
		 * 添加方法
		 */
		if (whereFrom.equals("insert")) {
			System.out.println("Class_Teacher_api————————————insert");
			// 解析富文本编辑器传入的Html1参数
			String newStr = parseJSON(req);
			// 对于javabean直接给出class实例
			JsonTransferObject jsonTransferObject = gson.fromJson(newStr, JsonTransferObject.class);
			TeacherName = jsonTransferObject.getObject01();
			TeacherDescription = jsonTransferObject.getObject02();
			TeacherCourse = jsonTransferObject.getObject03();
			Html01 = jsonTransferObject.getObject04();
			TeacherImage = jsonTransferObject.getObject05();
			Load_people = jsonTransferObject.getObject06();
			String sql = "INSERT INTO nsi_teacher (TeacherName,TeacherDescription,TeacherCourse,TeacherImage,Html01,Load_people,Load_time)"
					+ "VALUES('" + TeacherName + "','" + TeacherDescription + "','" + TeacherCourse + "','"
					+ TeacherImage + "','" + Html01 + "','" + Load_people + "','" + DateUtils.onToday() + "');";
			Course_DB.commonMethod(sql);
			// 返回结果集
			JsonResult.toResult(req, res);

			/**
			 * 查询方法，根据Id返回指定的数据信息
			 */
		} else if (whereFrom.equals("searchTeacherMessage")) {
			System.out.println("Class_Teacher_api————————————searchTeacherMessage");
			String id = req.getParameter("Id");
			if (id != null && id.length() > 0) {
				String sql = "SELECT * FROM nsi_teacher WHERE Id='" + id + "';";
				List<Teacher_model> list = Course_DB.searchTeacher(sql);
				// 返回结果集
				JsonResult.toResult(req, res, gson, list);
			} else {
				// 返回结果集
				JsonResult.toResultError(req, res);
			}

			/*
			 * 查询方法
			 */
		} else if (whereFrom.equals("search")) {
			System.out.println("Class_Teacher_api————————————search");
			String sqlDate = "SELECT * FROM nsi_teacher ;";
			List<Teacher_model> list = Course_DB.searchTeacher(sqlDate);
			String data = gson.toJson(list);
			String sqlCount = "SELECT * FROM nsi_teacher;";
			int rowCount = Course_DB.count(sqlCount);
			JsonResult.toResult(req, res, rowCount, data);

			/**
			 * 删除方法
			 */
		} else if (whereFrom.equals("delete")) {
			System.out.println("Class_Teacher_api——————————search");
			String id = req.getParameter("Id");
			if (id != null && id.length() > 0) {
				String sql = "DELETE FROM nsi_teacher where Id  ='" + id + "';";
				Course_DB.commonMethod(sql);
				JsonResult.toResult(req, res);
			} else {
				// 返回结果集
				JsonResult.toResultError(req, res);
			}

			/**
			 * 修改方法
			 */
		} else if (whereFrom.equals("alter")) {
			System.out.println("Class_Teacher_api————————————alter");
			// 解析富文本编辑器传入的Html1参数
			String newStr = parseJSON(req);
			// 对于javabean直接给出class实例
			JsonTransferObject jsonTransferObject = gson.fromJson(newStr, JsonTransferObject.class);
			String id = jsonTransferObject.getObject01();
			if (id != null && id.length() > 0) {
				TeacherName = jsonTransferObject.getObject02();
				TeacherDescription = jsonTransferObject.getObject03();
				TeacherCourse = jsonTransferObject.getObject04();
				Html01 = jsonTransferObject.getObject05();
				TeacherImage = jsonTransferObject.getObject06();
				Load_people = jsonTransferObject.getObject07();

				String sql = "UPDATE nsi_teacher SET TeacherName='" + TeacherName + "',TeacherDescription='"
						+ TeacherDescription + "',TeacherCourse='" + TeacherCourse + "'," + "Html01='" + Html01
						+ "',TeacherImage='" + TeacherImage + "',Load_people='" + Load_people + "' WHERE Id='" + id
						+ "';";
				Course_DB.commonMethod(sql);
				// 返回结果集
				JsonResult.toResult(req, res);
			} else {
				// 返回结果集
				JsonResult.toResultError(req, res);
			}

			/**
			 * 无参数返回结果
			 */
		} else {
			// 无参数返回结果集
			JsonResult.toResultFault(req, res);
		}

	}

	/**
	 * 解析前端的富文本内容
	 * 
	 * @param req
	 *            参数
	 * @return 返回字符串变量对象
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private String parseJSON(HttpServletRequest req) throws UnsupportedEncodingException, IOException {
		StringBuilder builder = new StringBuilder();
		BufferedReader br = new BufferedReader(
				new InputStreamReader((ServletInputStream) req.getInputStream(), "utf-8"));
		String temp;
		while ((temp = br.readLine()) != null) {
			builder.append(temp);
		}
		br.close();
		return builder.toString();
	}

}
