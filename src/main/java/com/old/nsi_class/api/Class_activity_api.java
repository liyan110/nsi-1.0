package com.old.nsi_class.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.old.nsi_class.DB.Course_DB;
import com.old.nsi_class.DB.DateUtils;
import com.old.nsi_class.DB.JsonResult;
import com.old.nsi_class.model.Activity_model;

@WebServlet("/Class_activity")
public class Class_activity_api extends HttpServlet{
	/**
	 * 生成序列化对象
	 */
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		this.doPost(req, res);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String whereFrom = req.getParameter("whereFrom");
		String Title1 = req.getParameter("Title1");
		String Title2 = req.getParameter("Title2");
		String Title3 = req.getParameter("Title3");
		String Title4 = req.getParameter("Title4");
		String Title5 = req.getParameter("Title5");
		String Title6 = req.getParameter("Title6");
		String Title7 = req.getParameter("Title7");
		String Title8 = req.getParameter("Title8");
		String Title9 = req.getParameter("Title9");
		String Title10 = req.getParameter("Title10");
		String Title11 = req.getParameter("Title11");
		String Title12 = req.getParameter("Title12");
		String Title13 = req.getParameter("Title13");
		String Title14 = req.getParameter("Title14");
		String Title15 = req.getParameter("Title15");
		String Title16 = req.getParameter("Title16");
		String Title17 = req.getParameter("Title17");
		String Title18 = req.getParameter("Title18");
		String Title19 = req.getParameter("Title19");
		String Title20 = req.getParameter("Title20");
		String Content1 = req.getParameter("Content1");
		String Content2 = req.getParameter("Content2");
		String Content3 = req.getParameter("Content3");
		String Content4 = req.getParameter("Content4");
		String Content5 = req.getParameter("Content5");
		String Content6 = req.getParameter("Content6");
		String Content7 = req.getParameter("Content7");
		String Content8 = req.getParameter("Content8");
		String Content9 = req.getParameter("Content9");
		String Content10 = req.getParameter("Content10");
		String Content11 = req.getParameter("Content11");
		String Content12 = req.getParameter("Content12");
		String Content13 = req.getParameter("Content13");
		String Content14 = req.getParameter("Content14");
		String Content15 = req.getParameter("Content15");
		String Content16 = req.getParameter("Content16");
		String Content17 = req.getParameter("Content17");
		String Content18 = req.getParameter("Content18");
		String Content19 = req.getParameter("Content19");
		String Content20 = req.getParameter("Content20");
		String Deadline = req.getParameter("Deadline");
		
		/**
		 * 后台给的insert，接受参数是title，返回的是ID
		 */
		if(whereFrom.equals("insertTitle")) {
			System.out.println("Class_activity_api——————————insertTitle");
			String sqlInsert = "insert into nsi_activity ("
					+ "Title1,Title2,Title3,Title4,Title5,Title6,Title7,Title8,Title9,Title10,Title11,Title12,Title13,Title14,Title15,Title16,Title17,Title18,Title19,Title20,Deadline,Load_time) "
					+ "VALUES ('"+Title1+"','"+Title2+"','"+Title3+"','"+Title4+"','"+Title5+"','"+Title6+"','"+Title7+"','"+Title8+"','"+Title9+"','"+Title10+"','"+Title11+"',"
					+ "'"+Title12+"','"+Title13+"','"+Title14+"','"+Title15+"','"+Title16+"','"+Title17+"','"+Title18+"','"+Title19+"','"+Title20+"','"+Deadline+"','"+ DateUtils.onToday()+"');";
			//System.out.println(sqlInsert);
			//执行插入操作
			Course_DB.commonMethod(sqlInsert);
			//返回Id
			String sqlSearch = "select * FROM nsi_activity ORDER BY Load_time DESC limit 1;";
			List<Activity_model> list = Course_DB.searchActivity(sqlSearch);
			Integer Id = list.get(0).getId();
			String strId = String.valueOf(Id);
			//返回结果集
			JsonResult.toResultString(req, res, strId);
			
		/**
		 * 返回给前端Title			
		 */
		}else if(whereFrom.equals("toResultTitle")) {
			System.out.println("Class_activity_api——————————toResultTitle");
			String Id = req.getParameter("Id");
			if(Id!=null && Id.length()>0) {
				String sql = "SELECT * FROM nsi_activity where Id='"+Id+"';";
				List<Activity_model> list = Course_DB.searchActivity(sql);
				//返回结果集
				JsonResult.toResult(req, res, gson, list);
			}else {
				String sql= "SELECT * FROM nsi_activity ORDER BY Load_time DESC limit 1;";
				List<Activity_model> list = Course_DB.searchActivity(sql);
				//返回结果集
				JsonResult.toResult(req, res, gson,list);
			}

		/**
		 *  接受用户信息，获取表单成功返回1
		 */
		}else if(whereFrom.equals("insert")) {
			System.out.println("Class_activity_api——————————insert");
			String sqlInsert = "INSERT INTO nsi_activity ("
					+ "Title1,Title2,Title3,Title4,Title5,Title6,Title7,Title8,Title9,Title10,Title11,Title12,Title13,Title14,Title15,Title16,Title17,Title18,Title19,Title20,"
					+ "Content1,Content2,Content3,Content4,Content5,Content6,Content7,Content8,Content9,Content10,"
					+ "Content11,Content12,Content13,Content14,Content15,Content16,Content17,Content18,Content19,Content20,Deadline,Load_time) "
					+ "VALUES ('"+Title1+"','"+Title2+"','"+Title3+"','"+Title4+"','"+Title5+"','"+Title6+"','"+Title7+"','"+Title8+"','"+Title9+"','"+Title10+"','"+Title11+"',"
					+ "'"+Title12+"','"+Title13+"','"+Title14+"','"+Title15+"','"+Title16+"','"+Title17+"','"+Title18+"','"+Title19+"','"+Title20+"',"
					+ "'"+Content1+"','"+Content2+"','"+Content3+"','"+Content4+"','"+Content5+"','"+Content6+"','"+Content7+"','"+Content8+"','"+Content9+"','"+Content10+"','"+Content11+"','"+Content12+"',"
					+ "'"+Content13+"','"+Content14+"','"+Content15+"','"+Content16+"','"+Content17+"','"+Content18+"','"+Content19+"','"+Content20+"','"+Deadline+"','"+DateUtils.onToday()+"');";
			Course_DB.commonMethod(sqlInsert);
			//返回结果集
			JsonResult.toResult(req, res);
		
		/**
		 * 查询活动列表功能 
		 */
		}else if(whereFrom.equals("searchActivity")) {
			String sql = "SELECT * FROM nsi_activity WHERE Content3='0' ORDER BY Load_time DESC;";
			//返回活动列表结果集
			List<Activity_model> list = Course_DB.searchActivity(sql);
			//返回结果集
			JsonResult.toResult(req, res, gson, list);
		
		/**
		 * 查询信息列表功能	
		 */
		}else if(whereFrom.equals("searchInformation")) {	
			System.out.println("Class_activity_api——————————searchInformation");
			String Title01 = req.getParameter("Title1");
			if(Title01!=null && Title01.length()>0) {
				String sql = "SELECT * FROM nsi_activity WHERE Title1='"+Title01+"' AND Content3<>'0';";
				List<Activity_model> list = Course_DB.searchActivity(sql);
				JsonResult.toResult(req, res, gson, list);
			}else {
				JsonResult.toResultError(req, res);
			}
		
		/**
		 * 无参数返回结果集	
		 */
		}else{
			JsonResult.toResultFault(req, res);
		}			
	}
	
//	public static void main(String[] args) {
//		String Tutke01 = "HRC报名表02";
//		String sql = "SELECT * FROM nsi_activity WHERE Title1='"+Tutke01+"' AND Content3<>'0';";
//		List<Activity_model> list = Course_DB.searchActivity(sql);
//		System.out.println(list);
//	}
	
}
