package com.old.nsi_class.api;

import com.old.model.Model;
import com.old.nsi_class.DB.Course_DB;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Class_Polyv_api")
public class Class_Polyv_api extends HttpServlet {

    /**
     * 生成序列化对象
     */
    private static final long serialVersionUID = -2768731128545620094L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        this.doPost(req, res);
    }

    /**
     * 外部授权接口
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String userid = req.getParameter("userid");
        String ts = req.getParameter("ts");
        String token = req.getParameter("token");

//		从首页配置项 中取课程Id	
//        List<Properties_model> list = Properties.searchProperties();
//        String CourseId = list.get(0).getCourseId();
//        String CourseId ="20001";
//		返回值		
        System.out.println("Class_Polyv_api ：userid:" + userid + " ts:" + ts + " token:" + token);

//        String sql = "SELECT * from nsi_course_user WHERE ClassId= '" + CourseId + "' AND UserMail='" + userid + "';";
//        System.out.println("直播回调 Polyv_sql:"+userid+"::"+sql);

//        查询是否购买过课程
        String sql = "SELECT * from nsi_course_user WHERE  UserMail='" + userid + "';";
        System.out.println("直播回调 Polyv_sql:"+userid+"::"+sql);
        int i = Course_DB.count(sql);
//		成功，用户有购买过课程
        if (i >= 1) {

//			查询出用户信息
            Model model = new Model();
            String User_TureName = model.queryByName(userid).getUser_TureName();
            String User_portrait = model.queryByName(userid).getUser_portrait();

//			用户头像为空
            if (StringUtils.isBlank(User_portrait)||"0".equals(User_portrait)) {
                User_portrait = "http://data.xinxueshuo.cn/nsi/assets/img/userLogo.jpg";
            }

            String back = "{ \"status\":1,\"userid\":\"" + userid + "\",\"nickname\":\"" + User_TureName + "\",\"avatar\":\"" + User_portrait + "\" }";
            System.out.println("Class_Polyv_api-返回值:" + back);
            res.setContentType("application/json;charset=UTF-8");
            res.getWriter().write(back);


//		失败，用户未购买任何课程
        } else {

            String back = "{ \"status\":0,\"errorUrl\":\"http://data.xinxueshuo.cn/nsi-class\" }";
            System.out.println("Class_Polyv_api ：验证错误，返回值：" + back);
            res.setContentType("application/json;charset=UTF-8");
            res.getWriter().write(back);
        }
    }

    // MD5解密
//    public static String MD5_JM(String inStr) {
//        char[] a = inStr.toCharArray();
//        for (int i = 0; i < a.length; i++) {
//            a[i] = (char) (a[i] ^ 't');
//        }
//        String k = new String(a);
//        return k;
//    }

}
