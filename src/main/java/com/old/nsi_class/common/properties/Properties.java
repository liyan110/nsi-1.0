package com.old.nsi_class.common.properties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.old.nsi_class.DB.DBUtils;
import com.old.nsi_class.model.Properties_model;

/**
 * 直播配置信息类
 * @author 12984
 *
 */
//待删除
public class Properties {
	
	/**
	 * 增/删/改通用方法
	 * @param sql 数据库语句
	 * @return 返回数据结果集
	 */
	public static int commonMethod(String sql){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int rows = 0;
		try {
			conn = DBUtils.getInstance().getConnection();
			ps = conn.prepareStatement(sql);
			rows = ps.executeUpdate();
			if(rows>0) {
				//System.out.println("插入/修改/删除成功...");
			}else {
				System.out.println("插入/修改/删除失败...");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//释放资源
			DBUtils.getClose(conn, ps, rs);
		}
		return rows;
	}
	
	/**
	 * 查询方法
	 * @return list
	 */
	public static List<Properties_model> searchProperties(){
		List<Properties_model> list = new ArrayList<Properties_model>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBUtils.getInstance().getConnection();
			String sql = "SELECT * FROM nsi_properties;";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				Properties_model pm = new Properties_model();
				receiver(rs, pm);
				list.add(pm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtils.getClose(conn, ps, rs);
		}
		return list;
	}
	
	//封装置顶参数
	private static void receiver(ResultSet rs, Properties_model pm) throws SQLException {
		pm.setId(rs.getInt("Id"));
		pm.setTeacherId(rs.getString("teacherId"));
		pm.setCourseId(rs.getString("courseId"));
		
	}
}
