package com.old.people;

/**
 * 人员表
 * @author 12984
 *
 */
public class People_model {
	
	/**人员ID*/
	private int People_id;
	/**人员姓名*/
	private String People_name;
	/**人员会员类型*/
	private String People_member;
	/**人员会员到期时间*/
	private String People_dueTime;
	/**人员工作单位*/
	private String People_work;
	/**人员职位*/
	private String People_position;
	/**人员手机号码*/
	private String People_phone;
	/**人员邮箱*/
	private String People_mail;
	/**人员座机*/
	private String People_telephone;
	/**人员微信*/
	private String People_wechat;
	/**数据提交者*/
	private String People_loadPeople;
	/**提交时间*/
	private String People_loadTime;
	/**人员通讯地址*/
	private String People_address;
	/**人员简介*/
	private String People_introduction;
	/**人员备注*/
	private String People_remark;
	/*人员图片URL地址**/
	private String People_ImgUrl;

	public int getPeople_id() {
		return People_id;
	}
	public void setPeople_id(int people_id) {
		this.People_id = people_id;
	}
	public String getPeople_name() {
		return People_name;
	}
	public void setPeople_name(String people_name) {
		this.People_name = people_name;
	}		
	public String getPeople_member(){
		return People_member;
	}		
	public void setPeople_member(String People_member){
		this.People_member = People_member;
	}	
	public String getPeople_dueTime() {
		return People_dueTime;
	}
	public void setPeople_dueTime(String people_dueTime) {
		this.People_dueTime = people_dueTime;
	}
		
	public String getPeople_work() {
		return People_work;
	}
	public void setPeople_work(String people_work) {
		this.People_work = people_work;
	}
	public String getPeople_position() {
		return People_position;
	}
	public void setPeople_position(String people_position) {
		this.People_position = people_position;
	}
	public String getPeople_phone() {
		return People_phone;
	}
	public void setPeople_phone(String people_phone) {
		this.People_phone = people_phone;
	}	
	public String getPeople_mail() {
		return People_mail;
	}
	public void setPeople_mail(String people_mail) {
		this.People_mail = people_mail;
	}
	public String getPeople_telephone() {
		return People_telephone;
	}
	public void setPeople_telephone(String People_telephone) {
		this.People_telephone = People_telephone;
	}
	public String getPeople_wechat() {
		return People_wechat;
	}
	public void setPeople_wechat(String people_wechat) {
		this.People_wechat = people_wechat;
	}
	public String getPeople_loadPeople() {
		return People_loadPeople;
	}
	public void setPeople_loadPeople(String people_loadPeople) {
		this.People_loadPeople = people_loadPeople;
	}
	public String getPeople_loadTime() {
		return People_loadTime;
	}
	public void setPeople_loadTime(String people_loadTime) {
		this.People_loadTime = people_loadTime;
	}
	public String getPeople_address() {
		return People_address;
	}
	public void setPeople_address(String people_address) {
		this.People_address = people_address;
	}
	public String getPeople_introduction() {
		return People_introduction;
	}
	public void setPeople_introduction(String people_introduction) {
		this.People_introduction = people_introduction;
	}
	public String getPeople_remark() {
		return People_remark;
	}
	public void setPeople_remark(String people_remark) {
		this.People_remark = people_remark;
	}
	public String getPeople_ImgUrl() {
		return People_ImgUrl;
	}
	public void setPeople_ImgUrl(String people_ImgUrl) {
		this.People_ImgUrl = people_ImgUrl;
	}
	@Override
	public String toString() {
		return "[People_id=" + People_id + ", People_name=" + People_name + ", People_member="
				+ People_member + ", People_dueTime=" + People_dueTime + ", People_work=" + People_work
				+ ", People_position=" + People_position + ", People_phone=" + People_phone + ", People_mail="
				+ People_mail + ", People_telephone=" + People_telephone + ", People_wechat=" + People_wechat
				+ ", People_loadPeople=" + People_loadPeople + ", People_loadTime=" + People_loadTime
				+ ", People_address=" + People_address + ", People_introduction=" + People_introduction
				+ ", People_remark=" + People_remark + ", People_ImgUrl=" + People_ImgUrl + "]";
	}

		
		
}
