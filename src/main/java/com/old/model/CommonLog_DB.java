package com.old.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CommonLog_DB {

	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost:3306/NSI_DATABASE?characterEncoding=UTF8&useSSL=true";
	public static final String USER_NAME = "root";
	public static final String PASS_WORD = "123456";
	
	/**
	 * 查询方法
	 * @param sql
	 * @return
	 */
	public static List<CommonLog_model> Search(String sql){
		List<CommonLog_model> list = new ArrayList<CommonLog_model>();
		Connection conn = null;
		Statement state = null;
		ResultSet rs = null;
		try {
			Class.forName(DRIVER_CLASS);
			conn = DriverManager.getConnection(URL,USER_NAME,PASS_WORD);
			state = conn.createStatement();
			rs = state.executeQuery(sql);
			while(rs.next()){
				CommonLog_model rm = new CommonLog_model();
				receiver(rs, rm);
				list.add(rm);
			}
			rs.last();
			rs.close();
			state.close();
			conn.close();
		} catch (Exception e) {
			System.out.println("search 查询Sql方法异常...");
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 查询接受参数
	 * @param rs
	 * @param rm
	 * @throws SQLException
	 */
	private static void receiver(ResultSet rs, CommonLog_model cm) throws SQLException {
		
		cm.setId(rs.getInt("Id"));
		cm.setSign(rs.getString("sign"));
		cm.setIndex01(rs.getString("index01"));
		cm.setIndex02(rs.getString("index02"));
		cm.setIndex03(rs.getString("index03"));
		cm.setIndex04(rs.getString("index04"));
		cm.setIndex05(rs.getString("index05"));

	}
}
