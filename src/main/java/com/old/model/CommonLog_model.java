package com.old.model;

public class CommonLog_model {
	
	/**Log编号*/
	private int Id;
	/**模块统称*/
	private String sign;
	/**执行姓名*/
	private String index01;
	/**执行时间*/
	private String index02;
	/**执行功能*/
	private String index03;
	/**执行Id*/
	private String index04;
	/**用户邮箱*/
	private String index05;
	
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getIndex01() {
		return index01;
	}
	public void setIndex01(String index01) {
		this.index01 = index01;
	}
	public String getIndex02() {
		return index02;
	}
	public void setIndex02(String index02) {
		this.index02 = index02;
	}
	public String getIndex03() {
		return index03;
	}
	public void setIndex03(String index03) {
		this.index03 = index03;
	}
	public String getIndex04() {
		return index04;
	}
	public void setIndex04(String index04) {
		this.index04 = index04;
	}
	public String getIndex05() {
		return index05;
	}
	public void setIndex05(String index05) {
		this.index05 = index05;
	}
	
	

}
