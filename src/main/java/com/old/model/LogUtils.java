package com.old.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.old.model.dbutil.Dbconn;

/**
 * 该类用于打印log信息，便插入到数据库
 * @author 12984
 *
 */
public class LogUtils {

	private static Dbconn s=new Dbconn();	
	/**
	 * 插入log信息静态方法
	 * @param
	 */
	public static void insertLogInformation(CommonLog_model cm) {
		Connection conn = null;
		String sql = "insert into nsi_log (sign,index01,index02,index03,index04,index05)VALUES(?,?,?,?,?,?)";
		PreparedStatement ps = null;
		try {
			conn = s.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, cm.getSign());
			ps.setString(2, cm.getIndex01());
			ps.setString(3, cm.getIndex02());
			ps.setString(4, cm.getIndex03());
			ps.setString(5, cm.getIndex04());
			ps.setString(6, cm.getIndex05());
			int rows = ps.executeUpdate();
			if(rows>0){
//				System.out.println("commonLog插入成功...");
			}else {
				System.out.println("commonLog插入失败...");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
