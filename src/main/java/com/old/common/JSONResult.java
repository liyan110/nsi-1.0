package com.old.common;

/**
 * 通过此对象封装控制层返回的JSON结果
 * 便于对控制层返回数据进行统一格式化,
 * 友好性管理*/
public class JSONResult {
    public static final int SUCCESS=1;
    public static final int ERROR=-2;
	/**服务端的响应状态*/
	private int code;
	/**信息(给用户的提示)*/
	private String message;
	/**数据库查询条数(有必要的时候给一下)*/
	private int count;
	/**具体业务数据1*/
	private Object data1;
	/**具体业务数据2*/
	private Object data2;
	public JSONResult() {
		this.code=SUCCESS;
	}
	
	public JSONResult(Object data) {
		this();
		this.data1 = data;
	}
	
	public JSONResult(int count,Object data) {
		this();
		this.count = count;
		this.data1 = data;
	}
	public JSONResult(Object data1,Object data2) {
		this();
		this.data1 = data1;
		this.data2 = data2;
	}
	
	public JSONResult(String message,int count,Object data) {
		this();
		this.data1 = data;
		this.message = message;
		this.count = count;
		this.data1 = data;
	}
	
	public JSONResult(String message,int count,Object data,Object data2) {
		this();
		this.data1 = data;
		this.message = message;
		this.count = count;
		this.data1 = data;
		this.data2 = data2;
	}
	public String getMessage() {
		return message;
	}
	public Object getData() {
		return data1;
	}
	public int getState() {
		return code;
	}
	public int getCount() {
		return count;
	}
	public Object getData2() {
		return data2;
	}

	@Override
	public String toString() {
		return "JSONResult{" +
				"code=" + code +
				", message='" + message + '\'' +
				", count=" + count +
				", data1=" + data1 +
				", data2=" + data2 +
				'}';
	}
}
