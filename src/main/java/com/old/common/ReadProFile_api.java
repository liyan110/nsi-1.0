package com.old.common;

import com.old.nsi_class.DB.JsonResult;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 读取配置文件接口
 *
 * @author 12984
 */
@WebServlet("/ReadProFile_api")
@Slf4j 
public class ReadProFile_api extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        this.doPost(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //读取服务器地址
        String path = this.getServletContext().getRealPath("/WEB-INF/classes/WeeklyMail.properties");
        String newStr = ReadProFile_api.readFile(path);
        String updateStr = ReadProFile_api.updateStr(newStr);
        ReadProFile_api.writeFile(path, updateStr);
        //返回结果集
        JsonResult.toResult(req, res);
    }

    /**
     * 读文件f方法
     *
     * @param file 参数
     * @return 文件内的字符串
     */
    private static String readFile(String file) {
        FileInputStream fis = null;
        String str = null;
        try {
            fis = new FileInputStream(file);
            byte[] data = new byte[100];
            int len = fis.read(data);
            str = new String(data, 0, len, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.info("[文件读取不到/或目录结构出错...]");
        } catch (IOException e) {
            e.printStackTrace();
            log.info("[读取文件异常]");
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    /**
     * 对字符串的值进行操作方法(每次增1)
     *
     * @param data
     */
    private static String updateStr(String data) {
        String[] str = data.split("=");
        String split = str[1];
        Integer parseInt = Integer.valueOf(split);
        parseInt += 1;
        String newStr = "MailNum=" + parseInt;
        return newStr;
    }

    /**
     * 写文件方法
     *
     * @param file 文件名
     * @param data 需要写出的字符串
     */
    private static void writeFile(String file, String data) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(data.getBytes("UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
