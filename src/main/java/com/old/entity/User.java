package com.old.entity;

public class User {
    private int id;           //=1;
	private String name;      //
	private String password;  //
	private int Member_sign;	//用户级别标记位
	private String User_TureName;	//用户真实姓名
	private String User_phone;  //用户电话
	private int User_score;
	private String Load_time;
	private String WechatId;
	private String User_portrait;
	private String Register_Type;
	
	public String getName() {
		return name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getMember_sign() {
		return Member_sign;
	}
	public void setMember_sign(int member_sign) {
		this.Member_sign = member_sign;
	}
	public String getUser_TureName() {
		return User_TureName;
	}
	public void setUser_TureName(String user_TureName) {
		this.User_TureName = user_TureName;
	}
	public String getUser_phone() {
		return User_phone;
	}
	public void setUser_phone(String user_phone) {
		this.User_phone = user_phone;
	}
	
	public int getUser_score() {
		return User_score;
	}
	public void setUser_score(int user_score) {
		this.User_score = user_score;
	}
	public String getLoad_time() {
		return Load_time;
	}
	public void setLoad_time(String load_time) {
		this.Load_time = load_time;
	}
	public String getWechatId() {
		return WechatId;
	}
	public void setWechatId(String wechatId) {
		this.WechatId = wechatId;
	}
	public String getUser_portrait() {
		return User_portrait;
	}
	public void setUser_portrait(String user_portrait) {
		this.User_portrait = user_portrait;
	}
	public String getRegister_Type() {
		return Register_Type;
	}
	public void setRegister_Type(String register_Type) {
		this.Register_Type = register_Type;
	}
	
	
	
}
