package com.old.user;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class User_DB {
	
	private static final String CLASSDRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/NSI_DATABASE?useSSL=true&characterEncoding=UTF8";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "123456";
	
	public static List<User_model> Search(String sql)
	{
		List<User_model> list = new ArrayList<User_model>();
		try
		{	
			Class.forName(CLASSDRIVER);
			Connection conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){				
				User_model user = new User_model();				
				user.setName(rs.getString("username"));
				user.setMember_sign(rs.getInt("Member_sign"));
				user.setUser_TureName(rs.getString("User_TureName"));
				user.setUser_Organization(rs.getString("User_Organization"));
				user.setUser_position(rs.getString("User_position"));
				user.setUser_phone(rs.getString("User_phone"));
				user.setUser_score(rs.getString("User_score"));
				user.setLoad_time(rs.getString("Load_time"));
				user.setWechatId(rs.getString("WechatId"));
				user.setUser_portrait(rs.getString("User_portrait"));
				user.setRegister_Type(rs.getString("Register_Type"));
//				未完整

				list.add(user);
			}
			rs.last();
            //关闭结果集,语句
            rs.close();
            stmt.close();
            conn.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("User_DB.java:搜索sql异常");	
		}
		return list;
	}
}
