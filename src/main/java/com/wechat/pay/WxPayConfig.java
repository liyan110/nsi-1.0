package com.wechat.pay;

/**
 * 微信配置参数类
 *
 * @author Luo Zhen
 */
public class WxPayConfig {
    /**
     * 开放平台的appid
     */
    public static String appid = "wx37e5ddff7dc5282e";

    public static String appid_miniProgramShop = "wx239dd449dc72d145";

    /**
     * 商务号
     */
    public static String mch_id = "1498143042";
    /**
     * 设备号 PC网页支付可以传"WEB"
     */
    public static String device_info = "WEB";
    /**
     * h5二次回调
     */
    public static String sign_type = "MD5";
    /**
     * 终端IP
     */
    public static String spbill_create_ip = "192.168.0.198";
    /**
     * 回调地址
     */
    public static String notify_url = "http://data.xinxueshuo.cn/nsi-1.0/Pay/WxPay_callback.do";
    /**
     * 交易类型 原生扫码支付
     */
    public static String trade_type_qr = "NATIVE";
    /**
     * 交易类型 H5 支付
     */
    public static String trade_type_h5 = "MWEB";
    /**
     * 交易类型 APP 和 公众号
     */
    public static String trade_type2 = "JSAPI";
    /**
     * key 验签参数
     */
    public static String key = "20180131wxpaylee987654MD897575HM";
    /**
     * 公众号的appsecret
     */
    public static String secret = "b1650d18615b1de657519091e977e8cd";


}
