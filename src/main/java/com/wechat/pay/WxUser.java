package com.wechat.pay;

import lombok.Data;

/**
 * 微信用户信息
 */
@Data
public class WxUser {

    /**openid*/
    private String openid;
    /**微信昵称*/
    private String nickname;
    /**微信用户性别  1==男性  2==女性  0==未知*/
    private String sex;
    /**微信个人资料填写的省份*/
    private String province;
    /**微信个人资料填写的城市*/
    private String city;
    /**微信个人资料填写的国家*/
    private String country;
    /**微信个人头像*/
    private String headimgurl;
    /**unionid*/
    private String unionid;

}
