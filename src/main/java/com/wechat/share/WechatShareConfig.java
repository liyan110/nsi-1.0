package com.wechat.share;

import java.util.UUID;

/**
 * 微信分享配置类
 *
 * @author Luo Zhen
 * @create 2018-08-07
 **/
public class WechatShareConfig {

    /**APPID*/
    public static String APPID = "wx37e5ddff7dc5282e";
    /**时间戳*/
    public static String TIMESTAMP = Long.toString(System.currentTimeMillis() / 1000);
    /**
     * 随机字符串
     */
    public static String NONCE_STR = UUID.randomUUID().toString();

    /**微信分享私钥*/
    public static String SECRET = "b1650d18615b1de657519091e977e8cd";
    /**根据此地址生成 ACCESS_TOKEN*/
    public static String requestUrl_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+APPID+"&secret="+SECRET+"";

}
