package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Luo Zhen
 * @create 2018-08-23
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentVo {

    private Integer id;

    private Integer courseId;

    private String userMail;

    private String userName;

    private String userPortrait;

    private String assignmentContent;

    private String attachmentNameOne;

    private String attachmentUrlOne;

    private String attachmentNameTwo;

    private String attachmentUrlTwo;

    private String attachmentNameThree;

    private String attachmentUrlThree;

    private String teacherId;

    private Long createTime;

}
