package com.nsi.vo;

import lombok.Data;

/**
 * @author: Luo Zhen
 * @date: 2018/10/18 16:01
 * @description:
 */
@Data
public class SchoolVo {

    private Integer id;

    private String schoolName;

    private String schoolEnglishname;

    private String schoolProperties;

    private String areas;

    private String areas02;

    private String areas03;

    private Integer foundedTime;

    private String operationstate;

    private String schoolSystem;

    private String tuition01;

    private String tuition02;

    private String tuition03;

    private String tuition04;

    private String tuitionhigh;

    private String website;

    private String telephone;

    private Integer interCourseFoundedTime;

    private String course;

    private String authentication;

    private String courseEvaluation;

    private Integer studentNumAll;

    private Integer studentNum01;

    private Integer studentNum02;

    private Integer studentNum03;

    private Integer studentNum04;

    private Integer studentCapacity;

    private Integer graduatedStuNum;

    private String stuDominantNationality;

    private String stuYearInvestment;

    private String clubNum;

    private String presidentCountry;

    private Integer staffNum;

    private Integer teacherNum;

    private Integer foreignTeacherNum;

    private String teacherYearInvestment;

    private String teacherRetention;

    private String teacherSalary;

    private String teacherStuRatio;

    private String coveredArea;

    private String builtArea;

    private String hardware;

    private String investment;

    private String remark;

    private String recentModifier;

    private String loadPeople;

    private String loadTime;

    private String un09;

    private String un10;

    private String schoolLogo;

    private String schoolShow;

    private String img02;

    private String img03;

    private String img04;

    private String img05;

    private String batchinputSign;

    private String verifysign;

    private String evaluate;

    private String certificateAuthority;

    private String studentEvaluation;

    private String thirdOrganizations;

    private String courseAuthority;

    private String anyCertification;

}
