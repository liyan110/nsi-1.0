package com.nsi.vo;

import lombok.Data;

/**
 * 订单vo
 *
 * @author Luo Zhen
 * @create 2019-01-08 16:05
 */
@Data
public class OrderVo extends OrderListVo {

    private String wechatId;

    private Integer goodsId;

    private ShopVo shopVo;

    private Integer addressId;

}
