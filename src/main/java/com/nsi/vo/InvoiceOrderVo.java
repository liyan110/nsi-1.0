package com.nsi.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-12-20
 **/
@Data
public class InvoiceOrderVo {

    private Integer id;

    private String userInvoiceName;

    private String userInvoiceNum;

    private String userBillingType;

    private String usermail;

    private String username;

    private String userOrderNum;

    private String userBankAddr;

    private String userReservePhone;

    private String manageName;

    private String manageProjectName;

    private String managePaymentMethod;

    private String manageMoney;

    private String manageState;

    private String financeName;

    private String financeState;

    private Integer invoiceType;

    private Date createTime;

    private Date updateTime;

    private String remark;

    private Integer goodsId;

    private String productName;

    private String productType;

}
