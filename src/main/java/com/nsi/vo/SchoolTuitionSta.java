package com.nsi.vo;

import lombok.Data;

@Data
public class SchoolTuitionSta {

    private String province;

    private String properties;

    private Integer kindergartenTuition;

    private Integer elementartTuition;

    private Integer juniorTuition;

    private Integer seniorTuition;

}
