package com.nsi.vo;

import lombok.Data;

import java.util.Date;

/**
 * 活动订单-官网后台 Vo
 *
 * @author Luo Zhen
 * @create 2019-09-17 14:30
 */
@Data
public class ActiveReviewVo {

    private Integer id;

    private String name;

    private String company;

    private String position;

    private String mail;

    private String phone;

    private Integer totalPrice;

    private String option01;

    private String option02;

    private String ispublic;

    private String attendno;

    private String type;

    private Date createTime;
}
