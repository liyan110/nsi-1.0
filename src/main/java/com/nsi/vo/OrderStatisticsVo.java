package com.nsi.vo;

import lombok.Data;

/**
 * @author Luo Zhen
 * @create 2019-03-05 10:27
 */
@Data
public class OrderStatisticsVo {

    private String date;

    private int courseCount;

    private int bookCount;

    private int cartCount;

    private int totalPrice;

}
