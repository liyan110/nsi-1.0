package com.nsi.vo;

import lombok.Data;

@Data
public class SchoolInfoIntegrity {

    private Integer schoolId;

    private String schoolName;

    private Integer percent;

    private String percentDesc;

}
