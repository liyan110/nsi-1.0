package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class CheckInVo {

    private Integer id;

    private String name;

    private String type;

    private String company;

    private String position;

    private String phone;

    private String email;

    private String token;

    private String activity;

    private String createTime;

    private String checkinTime;
}
