package com.nsi.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
public class SchoolSubmitVo {

    /**
     * 逻辑ID
     */
    private Integer schoolId;

    /**
     * 学校名字
     */
    private String schoolName;

    /**
     * 学校英文名字
     */
    private String schoolEnglishName;

    /**
     * 学校性质
     */
    private String schoolProperties;

    /**
     * 运营状态
     */
    private String operationState;

    /**
     * 课程
     */
    private String course;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String town;

    /**
     * 地址
     */
    private String address;

    private String submitName;

    private String telphone;

    private String company;

    private Date createTime;


}
