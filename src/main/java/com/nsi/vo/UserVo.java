package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户高级模型
 *
 * @author Luo Zhen
 * @create 2019-02-18 15:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVo {

    private Integer id;
    /**登录名*/
    private String username;
    /**用户真实名*/
    private String userTurename;
    /**用户所属机构*/
    private String userOrganization;
    /**用户微信ID*/
    private String wechatid;

}
