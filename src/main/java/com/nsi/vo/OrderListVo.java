package com.nsi.vo;

import com.nsi.pojo.ShopGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Luo Zhen
 * @create 2019-01-16 13:47
 */
@Data
public class OrderListVo {

    private Long orderNo;

    private Integer quantity;

    private Integer total_price;

    private Integer status;

    private String statusDesc;

    private String productType;

    private String productName;

    private String buyerMessage;

    private String shippingCode;

    private Date paymentTime;

    private ShopGoods product;

    private Integer addressId;

    private Date createTime;

}
