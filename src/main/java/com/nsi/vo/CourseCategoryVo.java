package com.nsi.vo;

import com.nsi.pojo.CourseCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 录播课vo模型
 *
 * @author Luo Zhen
 * @create 2019-02-13 12:05
 */
@Data
public class CourseCategoryVo {

    private String description;

    private List<CourseCategory> courseLists;

}
