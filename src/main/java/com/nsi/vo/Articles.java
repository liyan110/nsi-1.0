package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Luo Zhen
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Articles {
    private Integer id;

    private String title;
}
