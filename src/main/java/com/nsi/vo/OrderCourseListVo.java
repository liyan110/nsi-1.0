package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单课程Vo
 *
 * @author Luo Zhen
 * @create 2019-02-20 10:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCourseListVo {

    private Long orderNo;

    private Integer status;

    private String statusDesc;

    private Integer totalPrice;

    private Integer listId;

    private String listTitle;

    private String listTheme;

    private String lecturer;

    private String listImgAddr;

    private String syllabus;

}
