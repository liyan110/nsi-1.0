package com.nsi.vo;

import lombok.Data;
import lombok.Setter;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-07-02
 **/
@Data
public class ActiveOrderVo {

    private Integer entryId;

    private Long orderNo;

    private Integer totalPrice;

    private String ticketType;

    private String name;

    private String company;

    private String position;

    private String option01;

    private String option02;

    private String phone;

    private String type;

    private Integer status;

    private String statusDesc;

    private Date createTime;

}
