package com.nsi.vo;

import lombok.Data;

@Data
public class SchoolCollectVo {

    private Integer id;
    private String schoolName;
    private String province;
    private String properties;
    private String foundTime;
    private String icon;
    private String system;
    private String status;
    private Integer collectId;


}
