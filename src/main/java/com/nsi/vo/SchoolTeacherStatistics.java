package com.nsi.vo;

import lombok.Data;

@Data
public class SchoolTeacherStatistics {

    private Integer teacherNum;

    private Integer foreignTeacherNum;

    private Integer schoolStudentNum;

    private Integer degreeNum;

}
