package com.nsi.vo;

import lombok.Data;

@Data
public class EventOrderVo {

    private Integer id;

    private Integer collectId;

    private String name;

    private String position;

    private String company;

    private String telphone;

    private Integer checkIn;

    private String productName;

    private String checkInDesc;

}

