package com.nsi.vo;

import lombok.Data;

@Data
public class InstitutionCollectVo {

    private Integer id;
    private String name;
    private String icon;
    private String areas;
    private String type;
    private Integer foundTime;
    private Integer collectId;

}
