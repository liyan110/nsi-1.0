package com.nsi.vo.nsi_shop_bar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsi.pojo.PostCategoryItem;
import lombok.Data;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-12-27 16:04
 */
@Data
public class PostDetailVO {

    @JsonProperty("postItem")
    private PostCategoryItem postCategoryItem;

    @JsonProperty("hotList")
    private List<PostCategoryItem> postCategoryItemList;

}
