package com.nsi.vo.nsi_shop_bar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsi.pojo.CommunityAnswer;
import lombok.Data;
import java.util.Date;
import java.util.List;


@Data
public class CommunityAskAnswerVo {
    private static final long serialVersionUID = 1L;


    private Integer id;

    /**
     * 消息人姓名
     */

    private String messageName;

    /**
     * 消息人头像
     */

    private String messagePortrait;

    /**
     * 消息人微信id
     */

    private String messageWechatId;

    /**
     * 消息 内容
     */

    private String messageContent;

    /**
     * 对象用户微信id
     */

    private String objectWechatId;

    /**
     * 是否被查看（0未查看 1已查看）
     */

    private Integer checkState;

    /**
     * 消息查看时间
     */

    private Date checkTime;

    /**
     * 创建时间
     */

    private Date createTime;

    @JsonProperty("AnswerList")
    private List<CommunityAnswer> answerList;
}
