package com.nsi.vo.nsi_shop_bar;

import lombok.Data;

import java.util.Date;

/**
 * 帖子收藏Vo
 *
 * @author Luo Zhen
 * @create 2020-01-08 17:32
 */
@Data
public class PostCollectVO {

    private Integer collectId;

    private Integer itemId;

    private String title;

    private String content;

    private String summaryDesc;

    private String postIcon;

    private Integer commentNum;

    private Integer shareNum;

    private Integer watchNum;

    private Integer collectNum;

    private String tag;

    private Date createTime;

    private String avatar;

    private String nickName;

    private Integer gradeSign;

}
