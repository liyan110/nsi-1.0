package com.nsi.vo.nsi_shop_bar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsi.pojo.PostCategory;
import lombok.Data;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-12-16 9:47
 */
@Data
public class PostVO {

    @JsonProperty("postId")
    private Integer id;
    @JsonProperty("postName")
    private String categoryName;
    @JsonProperty("categoryItem")
    private List<PostCategory> postCategory;

}
