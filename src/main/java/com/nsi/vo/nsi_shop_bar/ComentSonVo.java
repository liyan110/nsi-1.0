package com.nsi.vo.nsi_shop_bar;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.nsi.pojo.CommunityCommentSon;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2019-12-23
 **/
@Data
public class ComentSonVo {

    private Integer id;

    private String wechatId;

    private String portrait;

    private String nickname;

    private Integer objectId;

    private String content;

    private Integer verifySign;

    private Integer level;

    private Date createTime;

    private Date verifyTime;

    private Integer sonCommentsNum;

    private Integer score;

    @JsonProperty("sonList")
    private List<CommunityCommentSon> communityCommentSonList;

}
