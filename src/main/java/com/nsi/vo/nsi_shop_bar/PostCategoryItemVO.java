package com.nsi.vo.nsi_shop_bar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsi.pojo.PostCategoryItem;
import lombok.Data;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-12-16 15:34
 */
@Data
public class PostCategoryItemVO {

    @JsonProperty("categoryId")
    private Integer id;
    @JsonProperty("categoryName")
    private String categoryName;
    @JsonProperty("backImage")
    private String backgroundImg;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("itemList")
    private List<PostCategoryItem> postCategoryItems;
    @JsonProperty("itemListCount")
    private Integer postCategoryItemCount;

}
