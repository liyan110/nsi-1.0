package com.nsi.vo.nsi_shop_bar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsi.pojo.PostCategoryItem;
import lombok.Data;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-12-16 15:18
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostInfoVO {

    @JsonProperty("postVO")
    private List<PostVO> postVOList;
    @JsonProperty("lastList")
    private List<PostCategoryItem> lastItemList;
    @JsonProperty("lastListCount")
    private Integer lastListCount;
    @JsonProperty("hotList")
    private List<PostCategoryItem> hotItemList;
    @JsonProperty("topList")
    private List<PostCategoryItem> topItemList;

}
