package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Luo Zhen
 * @create 2019-01-24 13:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TracesBean {

    private String AcceptTime;
    private String AcceptStation;
    private Object Remark;
}
