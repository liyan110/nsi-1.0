package com.nsi.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CourseItemVo {

    private Integer listId;

    private String listImg;

    private String listTitle;

    private Integer listPrice;

    private String listTheme;

    private String lecturer;

    private Integer status;

    private Integer pattern;

    private Date createTime;

    private Date updateTime;

    /**
     * 0-父类  1-子类
     */
    private Integer parentId;

    private List<CourseItemVo> children;

}
