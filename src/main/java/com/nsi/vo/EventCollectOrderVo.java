package com.nsi.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EventCollectOrderVo implements Serializable {

    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 职位
     */
    private String position;

    /**
     *
     * 公司
     */
    private String company;

    /**
     * 电话
     */
    private String telphone;

    /**
     * 邮箱
     */
    private String userMail;

    /**
     * 备份选项-1
     */
    private String option1;

    /**
     * 备份选项-2
     */
    private String option2;

    /**
     * 备份选项-3
     */
    private String option3;

    /**
     * 备份选项-4
     */
    private String option4;

    /**
     * 备份选项-5
     */
    private String option5;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 审核标记（0 审核通过 1 审核中）
     */
    private Integer verify;

    /**
     * 签到标记（0 已签到 1 未签到）
     */
    private Integer checkIn;

    /**
     * 来源
     */
    private String inviter;

    /**
     * 活动id
     */
    private Integer activeId;

    private Long orderNo;

    private String wechatId;

    private String unionId;

    private String username;

    private Integer goodsId;

    private String productName;

    private String productType;

    private Integer totalPrice;

    private Integer status;

    private String buyerMessage;


}
