package com.nsi.vo;

import com.nsi.pojo.ShopCart;
import lombok.Data;

/**
 * @author Luo Zhen
 * @create 2019-03-11 14:15
 */
@Data
public class OrderCartVo {

    private Long orderNo;

    private Integer quantity;

    private Integer totalPrice;

    private Integer status;

    private String statusDesc;

    private ShopCart shopCart;

}
