package com.nsi.vo;

import lombok.Data;

@Data
public class SchoolStatistics {

    private String province;

    private Integer minban;

    private Integer gongban;

    private Integer waiji;

    private Integer other;

}
