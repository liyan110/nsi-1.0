package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 收获地址Vo
 *
 * @author Luo Zhen
 * @create 2019-01-08 16:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShopVo {

    private String usermail;

    private String receivename;

    private String receivearea01;

    private String receivearea02;

    private String receivearea03;

    private String receivephone;

    private String postcode;

}
