package com.nsi.vo;

/**
 * @author: Luo Zhen
 * @date: 2018/9/18 14:34
 * @description: 交易用户实体类
 */

public class UserPayInfoVo {

    private Integer id;
    private String payment;
    private String username;
    private String mail;
    private String phone;
    private String institution;
    private String position;
    private String activity;
    private String total_fee;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public UserPayInfoVo() {
    }

    public UserPayInfoVo(Integer id, String payment, String username, String mail, String phone, String institution, String position, String activity, String total_fee, String create_time) {
        this.id = id;
        this.payment = payment;
        this.username = username;
        this.mail = mail;
        this.phone = phone;
        this.institution = institution;
        this.position = position;
        this.activity = activity;
        this.total_fee = total_fee;
        this.create_time = create_time;
    }
}
