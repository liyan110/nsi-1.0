package com.nsi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 课程父类详情
 *
 * @author Luo Zhen
 * @create 2019-02-21 10:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseListVo {

    private Integer listId;

    private String listImg;

    private String listTitle;

    private String syllabus;

    private Integer listPrice;

    private String listTheme;

    private String lecturer;

    private String listDescription;

}
