package com.nsi.exception;

import com.nsi.common.ResponseCode;

/**
 * @author: Luo Zhen
 * @date: 2018/9/28 11:32
 * @description:
 */

public class NsiOperationException extends RuntimeException {

    private Integer code;

    public NsiOperationException(String msg) {
        super(msg);
    }

    public NsiOperationException(ResponseCode responseCode) {
        super(responseCode.getDesc());
        this.code = responseCode.getCode();
    }
}
