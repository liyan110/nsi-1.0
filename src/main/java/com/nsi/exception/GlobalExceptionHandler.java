package com.nsi.exception;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理
 *
 * @author Luo Zhen
 * @create 2019-01-29 14:25
 */
@ControllerAdvice
@Slf4j
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ServerResponse jsonErrorHandler(HttpServletRequest req, Exception e) {
        log.error("################################################################################################################");
        log.error("[全局异常信息] uri : {}", req.getRequestURI());
        e.printStackTrace();
        log.error("[全局异常信息] msg : {}", e.toString());
        log.error("################################################################################################################");
        return ServerResponse.createByErrorCodeMessage(ResponseCode.SERVER_ERROR.getCode(), "服务器异常");
    }

    @ExceptionHandler(NsiOperationException.class)
    public ServerResponse operationException(HttpServletRequest req, NsiOperationException e) {
        return ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), e.getMessage());
    }

}
