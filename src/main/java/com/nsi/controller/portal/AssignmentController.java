package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Assignment;
import com.nsi.service.IAssignmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2018-08-15
 * 课后作业提交功能
 **/
@Controller
@RequestMapping("/assignment")
@Api(value = "/assignment", description = "新学说课堂——作业(前台)接口")
public class AssignmentController {

    @Autowired
    private IAssignmentService iAssignmentService;

    /**
     * 添加方法
     *
     * @param assignment
     * @return
     */
    @RequestMapping(value = "add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加作业方法", httpMethod = "POST")
    public ServerResponse add(Assignment assignment) {
        return iAssignmentService.saveOn(assignment);
    }

    /**
     * 根据用户邮箱，返回详情
     *
     * @param usermail
     * @return
     */
    @RequestMapping(value = "get_info.do")
    @ResponseBody
    @ApiOperation(value = "根据用户邮箱，返回详情", httpMethod = "GET")
    public ServerResponse findInfoByUserMail(String usermail) {
        return iAssignmentService.detail(usermail);
    }

    /**
     * 根据用户邮箱，返回详情
     *
     * @param courseId
     * @return
     */
    @RequestMapping(value = "get_course_info.do")
    @ResponseBody
    @ApiOperation(value = "根据课程id，返回详情", httpMethod = "GET")
    public ServerResponse findInfoByCourseId(Integer courseId) {
        return iAssignmentService.getInfoByCourseId(courseId);
    }

    /**
     * 返回老师提交的作业
     *
     * @return
     */
    @RequestMapping(value = "get_teacher_assignment.do")
    @ResponseBody
    @ApiOperation(value = "返回老师提交的作业", httpMethod = "GET")
    public ServerResponse getAssignmentByTeacherId(Integer courseId) {
        return iAssignmentService.getTeacherAssignment(courseId);
    }

    /**
     * 修改方法
     *
     * @param assignment
     * @return
     */
    @RequestMapping(value = "update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse updateAssignment(Assignment assignment) {
        return iAssignmentService.setAssignment(assignment);
    }


    /**
     * 返回学生提交的作业
     *
     * @param courseId
     * @return
     */
    @RequestMapping(value = "get_student_assignment.do")
    @ResponseBody
    @ApiOperation(value = "返回学生提交的作业", httpMethod = "GET")
    public ServerResponse getAssignmentWithTeacherIdNull(Integer courseId) {
        return iAssignmentService.findAssignmentByCourseId(courseId);
    }


}
