package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.User;
import com.nsi.service.IUserService;
import com.nsi.service.IWxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Luo Zhen
 */
@Slf4j
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private IWxPayService iWxPayService;

    /**
     * 注册接口-旧接口
     *
     * @param user
     * @return
     */
    @RequestMapping("/register.do")
    @ResponseBody
    public ServerResponse<String> register(User user) {
        return iUserService.register(user);
    }


    /**
     * 手机注册接口
     *
     * @param user
     * @return
     */
    @RequestMapping("/phoneRegister.do")
    @ResponseBody
    public ServerResponse<String> phoneRegister(User user) {
        return iUserService.phoneRegister(user);
    }


    /**
     * 邮箱激活码验证
     *
     * @param
     * @return
     */
    @RequestMapping("/UsermailVerify.do")
    @ResponseBody
    public ServerResponse<String> userMailVerify(@RequestParam(value = "Usermail") String Usermail,
                                                 @RequestParam(value = "VerifyCode") String VerifyCode) {
        return iUserService.UsermailVerify(Usermail, VerifyCode);
    }


    /**
     * 登录接口
     *
     * @param userName
     * @param passWord
     * @return
     */
    @RequestMapping("/login.do")
    @ResponseBody
    public ServerResponse<String> login(@RequestParam(value = "userName") String userName,
                                        @RequestParam(value = "passWord") String passWord) {
        return iUserService.login(userName, passWord);
    }

    /**
     * 验证-用户cookie校验
     *
     * @param userName
     * @param memberSign
     * @return
     */
    @RequestMapping("/verify.do")
    @ResponseBody
    public ServerResponse<String> verify(@RequestParam(value = "userName", required = false) String userName,
                                         @RequestParam(value = "memberSign", required = false) String memberSign,
                                         @RequestParam(value = "UserVerifyCode", required = false) String UserVerifyCode) {
        return iUserService.verify(userName, memberSign, UserVerifyCode);
    }


    /**
     * 验证用户是否注册过
     *
     * @param UserMail
     * @return
     */
    @RequestMapping("/UserMailCheck.do")
    @ResponseBody

    public ServerResponse<String> userMailCheck(@RequestParam(value = "UserMail") String UserMail) {
        return iUserService.UserMailCheck(UserMail);
    }


    /**
     * 获取用户详细
     *
     * @param userName
     * @return
     */
    @RequestMapping("/get_user_infomation.do")
    @ResponseBody
    public ServerResponse getUserInfomation(String userName) {
        return iUserService.getUserInformation(userName);
    }


    /**
     * 根据邮箱获取详细信息
     *
     * @param userName
     * @return
     */
    @RequestMapping("/get_userInfo.do")
    @ResponseBody
    public ServerResponse getUserInfo(String userName) {
        return iUserService.getUserInfo(userName);
    }


    /**
     * 根据Id修改用户信息
     *
     * @param user
     * @return
     */
    @RequestMapping("/update_userInfo.do")
    @ResponseBody
    public ServerResponse updateInfomation(User user) {
        return iUserService.updateInfomation(user);
    }

    /**
     * 更新用户信息
     *
     * @return
     */
    @RequestMapping("/update_user_information.do")
    @ResponseBody
    public ServerResponse updateImageHead(User user) {
        return iUserService.updateUserInformation(user);
    }


//    忘记密码相关 3个接口

    /**
     * 忘记密码
     *
     * @return
     */
    @RequestMapping("/forgetPW.do")
    @ResponseBody
    public ServerResponse forgetPW(String UserMail) {
        return iUserService.forgetPW(UserMail);
    }

    /**
     * 忘记密码02-验证
     *
     * @return
     */
    @RequestMapping("/forgetPWverify.do")
    @ResponseBody
    public ServerResponse forgetPWverify(String UserMail, String Code) {
        return iUserService.forgetPWverify(UserMail, Code);
    }


    /**
     * 修改密码-忘记密码03（通用）
     *
     * @return
     */
    @RequestMapping("/PWAlter.do")
    @ResponseBody
    public ServerResponse PWAlter(String UserMail, String password) {
        return iUserService.PWAlter(UserMail, password);
    }


    /**
     * feedback
     *
     * @param UserName
     * @param Content
     * @return
     */
    @RequestMapping("/feedback.do")
    @ResponseBody
    public ServerResponse<String> feedback(@RequestParam(value = "UserName") String UserName,
                                           @RequestParam(value = "Content") String Content,
                                           @RequestParam(value = "Contact") String Contact) {
        return iUserService.feedback(UserName, Content, Contact);
    }

    /**
     * feedback
     *
     * @param UserMail
     * @param ScoreNum
     * @return
     */
    @RequestMapping("/Score.do")
    @ResponseBody
    public ServerResponse<String> Score(@RequestParam(value = "UserMail") String UserMail,
                                        @RequestParam(value = "ScoreNum") String ScoreNum
    ) {
        return iUserService.Score(UserMail, ScoreNum);
    }

    // --------------------------------------微信PC端扫码登录---------------------------------------------------------->>

    /**
     * 微信获取OpenId
     *
     * @return
     */
    @RequestMapping("/WechatGetOpenId.do")
    @ResponseBody
    public ServerResponse WechatGetOpenId(String code) {
        return iUserService.WechatGetOpenId(code);
    }

    /**
     * 微信扫码登录
     *
     * @return
     */
    @RequestMapping("/WechatLogin.do")
    @ResponseBody
    public ServerResponse WechatLogin(String OpenId) {
        return iUserService.WechatLogin(OpenId);
    }

    /**
     * 微信用户绑定(openId)
     *
     * @return
     */
    @RequestMapping("/WechatBinding.do")
    @ResponseBody
    public ServerResponse WechatBinding(String UserName, String Password, String OpenId) {
        return iUserService.WechatBinding(UserName, Password, OpenId);
    }

    // --------------------------------------微信公众号---------------------------------------------------------->>


    /**
     * 微信公众号 获取openId用户信息
     *
     * @param code
     * @return
     */
    @RequestMapping("/get_wx_info_public.do")
    @ResponseBody
    public ServerResponse getWXUserInfo(String code) {
        return iWxPayService.getUserInfo(code);
    }

    /**
     * 补全微信用户信息
     *
     * @param user
     * @return
     */
    @RequestMapping("/completionWechatUser.do")
    @ResponseBody
    public ServerResponse completionWechatUser(User user) {
        return iUserService.completionWechatUser(user);
    }


    /**
     * 微信扫码关注公众号登录
     *
     * @return
     */
    @RequestMapping("/get_qrcode.do")
    @ResponseBody
    public ServerResponse getQrcode() {
        return iUserService.getQrcodeStr();
    }

    /**
     * 微信公众号扫码登录回调
     *
     * @param sceneStr
     * @return
     */
    @RequestMapping("/get_check_login.do")
    @ResponseBody
    public ServerResponse wechatMpCheckLogin(String sceneStr) {
        return iUserService.checkLoginBySceneStr(sceneStr);
    }


    /**
     * 微信公众号扫码登录回调
     *
     * @param request
     */
    @RequestMapping(value = "/signature.do")
    @ResponseBody
    public void callback(HttpServletRequest request) {
        iUserService.getCallback(request);
    }

    // --------------------------------------微信小程序---------------------------------------------------------->>

    /**
     * 微信小程序 获取openId用户信息
     *
     * @param type
     * @param code
     * @param encryptedData
     * @param iv
     * @return
     */
    @RequestMapping(value = "/decodeUserInfo.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "微信小程序获取用户信息", httpMethod = "POST")
    public ServerResponse deCodeUserInfo(String type,
                                         @RequestParam(value = "code") String code,
                                         @RequestParam(value = "encryptedData") String encryptedData,
                                         @RequestParam(value = "iv") String iv) {
        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(encryptedData) || StringUtils.isEmpty(iv)) {
            return ServerResponse.createByErrorMessage("param not empty");
        }
        return iWxPayService.getUserByCodeAndIv(type, code, encryptedData, iv);
    }


    /**
     * 根据wechatId修改unionId
     *
     * @param wechatId
     * @param unionId
     * @return
     */
    @RequestMapping(value = "/add_union_id.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据wechatId修改unionId", httpMethod = "POST")
    public ServerResponse modifyUserByWechatId(@RequestParam(value = "wechatId") String wechatId,
                                               @RequestParam(value = "unionId") String unionId) {
        try {
            if (StringUtils.isBlank(wechatId) || StringUtils.isBlank(unionId)) {
                return ServerResponse.createByErrorMessage("参数不合法");
            }
            return iUserService.modifyUserByWechatId(wechatId, unionId);
        } catch (Exception e) {
            log.error("系统业务异常", e);
        }
        return ServerResponse.createByErrorCodeMessage(500, "修改失败");
    }

    /**
     * 四库全书小程序注册功能
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/register_by_siku.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "四库全书小程序注册功能", httpMethod = "POST")
    public ServerResponse registerBySiKu(User user) {
        return iUserService.registerBySiKu(user);
    }

    /**
     * 四库全书小程序根据unindId返回用户详情
     *
     * @param unionId
     * @return
     */
    @RequestMapping(value = "/find_siku_by_unionId.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "四库全书小程序根据unindId返回用户详情", httpMethod = "GET")
    public ServerResponse findSiKuByUnionId(String unionId) {
        return iUserService.findUserByUnIonId(unionId);
    }

    /**
     * 微信用户绑定unionId(四库全书-小程序)
     *
     * @param username
     * @param unionId
     * @return
     */
    @RequestMapping("/wechat_bind_unionid.do")
    @ResponseBody
    @ApiOperation(value = "微信用户绑定unionId", httpMethod = "POST")
    public ServerResponse wechatBindUnionId(@RequestParam("username") String username,
                                            @RequestParam("unionId") String unionId) {
        return iUserService.wechatBindUnionId(username, unionId);
    }


    // --------------------------------------微信服务号消息通知---------------------------------------------------------->>


    /**
     * 测试，前端不调用，
     */
    @RequestMapping(value = "/sendWechatPublic_msg.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "测试sendWechatPublic_msg", httpMethod = "POST")
    public ServerResponse sendWechatPublic_msg(String openId) {

        iUserService.sendWechatPublic_msg(openId);
        return ServerResponse.createByErrorCodeMessage(500, "修改失败");
    }

}
