package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Log;
import com.nsi.service.IVisWechatUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2018-10-13
 **/

//用于vis2018 微信分享砍价活动-用户信息记录


@Controller
@RequestMapping("/VisWechatUser")
@Api(value = "/VisWechatUser", description = "VIS活动：分享砍价活动 接口")
public class VisWechatUserController {

    @Autowired
    private IVisWechatUserService iVisWechatUserService;

    /**
     * 活动发起人-插入
     *
     * @param log
     * @return
     */
    @RequestMapping("/VisWechatUserLaunch_insert.do")
    @ResponseBody

    @ApiOperation(value = "活动发起人-插入", httpMethod = "POST")
    public ServerResponse<String> VisWechatUserLaunch_insert(Log log) {
        //DynamicDataSourceHolder.setDataSource("dataSource1");
        return iVisWechatUserService.VisWechatUserLaunch_insert(log);
    }

    /**
     * 活动发起人-检查是否报名过
     *
     * @param OpenId
     * @return
     */
    @RequestMapping("/VisWechatUserLaunch_check.do")
    @ResponseBody

    @ApiOperation(value = "活动发起人-检查是否报名过", httpMethod = "POST")
    public ServerResponse<String> VisWechatUserLaunch_check(String OpenId) {
        //DynamicDataSourceHolder.setDataSource("dataSource1");
        return iVisWechatUserService.VisWechatUserLaunch_check(OpenId);
    }


    /**
     * 活动参与人-检查是否砍过价
     *
     * @param OpenId
     * @return
     */
    @RequestMapping("/VisWechatUserShare_check.do")
    @ResponseBody

    @ApiOperation(value = "活动参与人-检查是否砍过价", httpMethod = "POST")
    public ServerResponse<String> VisWechatUserShare_check(String OpenId,String LaunchOpenId) {
        //DynamicDataSourceHolder.setDataSource("dataSource1");
        return iVisWechatUserService.VisWechatUserShare_check(OpenId,LaunchOpenId);
    }

    /**
     * 活动发起人-获取优惠金额和列表
     *
     * @param OpenId
     * @return
     */
    @RequestMapping("/VisWechatUserLaunch_list.do")
    @ResponseBody

    @ApiOperation(value = "活动发起人-获取优惠金额和列表", httpMethod = "POST")
    public ServerResponse<String> VisWechatUserLaunch_list(String OpenId) {
        //ynamicDataSourceHolder.setDataSource("dataSource1");
        return iVisWechatUserService.VisWechatUserLaunch_list(OpenId);
    }

    /**
     * 活动参与人-为好友助力砍价
     *
     * @param LaunchOpenId
     * @return
     */
    @RequestMapping("/VisWechatUserShare_insert.do")
    @ResponseBody

    @ApiOperation(value = "活动参与人-为好友助力砍价", httpMethod = "POST")
    public ServerResponse<String> VisWechatUserShare_insert(Log log,String LaunchOpenId) {
        //DynamicDataSourceHolder.setDataSource("dataSource1");
        return iVisWechatUserService.VisWechatUserShare_insert(log,LaunchOpenId);
    }


}
