package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityMessage;
import com.nsi.service.CommunityMessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 社区消息 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-26
 */
@Controller
@RequestMapping("/communityMessage")
public class CommunityMessageController {

    @Autowired
    private CommunityMessageService communityMessageService;


//    @RequestMapping("/insertMessage")
//    @ResponseBody
//    @ApiOperation(value = "插入方法", httpMethod = "POST")
//    public ServerResponse insertMessage(CommunityMessage communityMessage) {
//        int i=communityMessageService.insertMessage(communityMessage);
//        return ServerResponse.createBySuccess(i);
//    }

    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation(value = "列表方法", httpMethod = "POST")
    public ServerResponse list(String wechatId,
                               @RequestParam(defaultValue = "未查看") String state,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {
        //转换为数字状态
        int checkState=0;
        if(state.equals("已查看")){
            checkState=1;
        }
        List<CommunityMessage> list= communityMessageService.list(wechatId,checkState, pageNum, pageSize);

        return ServerResponse.createBySuccess(list);
    }

    @RequestMapping("/messageCount")
    @ResponseBody
    @ApiOperation(value = "未读消息数量", httpMethod = "POST")
    public ServerResponse messageCount(String wechatId) {
        EntityWrapper<CommunityMessage> wrapper=new EntityWrapper<>();
        wrapper.eq("receiver_wechat_id",wechatId)
            .eq("check_state",0);
        int i= communityMessageService.selectCount(wrapper);
        return ServerResponse.createBySuccess(i);
    }


    @RequestMapping("/checked")
    @ResponseBody
    @ApiOperation(value = "已查看方法", httpMethod = "POST")
    public ServerResponse checked(Integer messageId) {
        int i= communityMessageService.checked(messageId);

        return ServerResponse.createBySuccess(i);
    }

    @RequestMapping("/checkAll")
    @ResponseBody
    @ApiOperation(value = "全部查看，全部已读", httpMethod = "POST")
    public ServerResponse checkAll(String wechatId) {
        EntityWrapper<CommunityMessage> wrapper=new EntityWrapper<>();
        wrapper.eq("receiver_wechat_id",wechatId);
//      修改状态为 已查看
        CommunityMessage communityMessage=new CommunityMessage();
        communityMessage.setCheckState(1);
        boolean b= communityMessageService.update(communityMessage,wrapper);
        if(b){
            return ServerResponse.createBySuccess("success");
        }else {
            return ServerResponse.createByErrorMessage("fail");
        }
    }

    @RequestMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除消息", httpMethod = "POST")
    public ServerResponse delete(Integer messageId) {
        EntityWrapper<CommunityMessage> wrapper=new EntityWrapper<>();
        wrapper.eq("id",messageId);
        boolean b= communityMessageService.delete(wrapper);
        return b ? ServerResponse.createBySuccess("success") : ServerResponse.createByErrorMessage("fail");
    }

}
