package com.nsi.controller.portal.nsi_post_bar;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityCommentSon;
import com.nsi.service.CommunityCommentSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 社区-子评论表 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-17
 */
@Controller
@RequestMapping("/communityCommentSon")
@Api(value = "/communityCommentSon", description = "社区小程序——2级 子评论 管理接口")
public class CommunityCommentSonController {

    @Autowired
    private CommunityCommentSonService communityCommentSonService;


    @RequestMapping("/insertCommentSon")
    @ResponseBody
    @ApiOperation(value = "插入 子评论方法", httpMethod = "POST")
    public ServerResponse insertCommentSon(CommunityCommentSon communityCommentSon){
        return communityCommentSonService.insertCommentSon(communityCommentSon);
    }


    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation(value = "列表方法", httpMethod = "POST")
    public ServerResponse list(String id,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize){

        return communityCommentSonService.list(id,pageNum,pageSize);
    }

}
