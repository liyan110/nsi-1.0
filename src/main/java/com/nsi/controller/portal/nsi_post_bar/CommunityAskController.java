package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityAnswer;
import com.nsi.pojo.CommunityAsk;
import com.nsi.pojo.CommunityComment;
import com.nsi.service.CommunityAnswerService;
import com.nsi.service.CommunityAskService;
import com.nsi.service.CommunityMessageService;
import com.nsi.vo.nsi_shop_bar.ComentSonVo;
import com.nsi.vo.nsi_shop_bar.CommunityAskAnswerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 提问互动-提问表 前端控制器
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@Controller
@RequestMapping("/communityAsk")
@Api(value = "/communityAsk", description = "社区小程序——提问互动-提问接口")
public class CommunityAskController {

    @Autowired
    private CommunityAskService communityAskService;
    @Autowired
    private CommunityAnswerService communityAnswerService;
    @Autowired
    private CommunityMessageService communityMessageService;


    @RequestMapping("/insert")
    @ResponseBody
    @ApiOperation(value = "插入方法,不需要审核", httpMethod = "POST")
    public ServerResponse insert(CommunityAsk communityAsk) {
        communityAsk.setCheckState(1);
        communityAsk.setCreateTime(new Date());
        boolean b=communityAskService.insert(communityAsk);

        //添加消息
        communityMessageService.insertMessage(
                "互动被提问",
                communityAsk.getMessageWechatId(),
                communityAsk.getMessageContent(),
                communityAsk.getObjectWechatId(),
                communityAsk.getObjectWechatId()
        );

        return b?ServerResponse.createBySuccessMessage("success"):ServerResponse.createByErrorMessage("fail");
    }

    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation(value = "个人主页列表方法", httpMethod = "POST")
    public ServerResponse list(String wechatId,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {
        EntityWrapper<CommunityAsk> wrapper=new EntityWrapper<>();
        wrapper.eq("object_wechat_id",wechatId)
                .eq("check_state",1);
        List<CommunityAsk> list =communityAskService.selectPage(new Page(pageNum, pageSize),wrapper).getRecords();

        CommunityAskAnswerVo communityAskAnswerVo;
        List<CommunityAskAnswerVo> AskAnswerVoList=new ArrayList<>();
        for (CommunityAsk communityAsk:list) {
            communityAskAnswerVo = new CommunityAskAnswerVo();
            BeanUtils.copyProperties(communityAsk, communityAskAnswerVo);

            List<CommunityAnswer> answers_list= communityAnswerService.listByAsk_id(communityAsk.getId(),1,3);
            System.out.println("communityAskAnswerVo:"+communityAskAnswerVo);
            communityAskAnswerVo.setAnswerList(answers_list);
            System.out.println("communityAskAnswerVo::"+communityAskAnswerVo);
            AskAnswerVoList.add(communityAskAnswerVo);
        }
        return ServerResponse.createBySuccess("success",AskAnswerVoList);
    }

//    @RequestMapping("/verify_pass")
//    @ResponseBody
//    @ApiOperation(value = "审核方法", httpMethod = "POST")
//    public ServerResponse verify_pass(int id) {
//        CommunityAsk communityAsk=communityAskService.selectById(id);
//        communityAsk.setCheckState(1);
//        boolean b=communityAskService.updateById(communityAsk);
//        return b?ServerResponse.createBySuccessMessage("success"):ServerResponse.createByErrorMessage("fail");
//    }

}
