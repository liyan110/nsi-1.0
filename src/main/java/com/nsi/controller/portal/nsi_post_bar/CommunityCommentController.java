package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.CommunityCommentSon;
import com.nsi.service.CommunityCommentService;
import com.nsi.service.CommunityCommentSonService;
import com.nsi.vo.nsi_shop_bar.ComentSonVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 社区-标准评论表 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
@Controller
@RequestMapping("/communityComment")
@Api(value = "/communityComment", description = "社区小程序——1级评论管理接口")
public class CommunityCommentController {

    @Autowired
    CommunityCommentService communityCommentService;
    @Autowired
    CommunityCommentSonService communityCommentSonService;

    @RequestMapping("/insert")
    @ResponseBody
    @ApiOperation(value = "添加评论", httpMethod = "POST")
    public ServerResponse insert(CommunityComment communityComment) throws Exception {

        return communityCommentService.insertComment(communityComment);
    }


    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation(value = "列表方法", httpMethod = "POST")
    public ServerResponse list(String id,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {

        return communityCommentService.list(id, pageNum, pageSize);
    }

    @RequestMapping("/detail")
    @ResponseBody
    @ApiOperation(value = "详情方法", httpMethod = "POST")
    public ServerResponse detail(int id) {

        CommunityComment communityComment = communityCommentService.detail(id);
        return ServerResponse.createBySuccess(communityComment);
    }


    @RequestMapping("/CommentAndSonlist")
    @ResponseBody
    @ApiOperation(value = "列表方法(子评论数：3)", httpMethod = "POST")
    public ServerResponse CommentAndSonlist(String id,
                                            @RequestParam(defaultValue = "1") int pageNum,
                                            @RequestParam(defaultValue = "10") int pageSize) {
        // 返回父类列表
        List<CommunityComment> commentList = communityCommentService.CommentAndSonlist(id, pageNum, pageSize);
        //遍历子类列表
        ComentSonVo comentSonVo = null;
        List<ComentSonVo> comentSonVoList = new ArrayList<>();
        for (CommunityComment comment : commentList) {
            comentSonVo = new ComentSonVo();
            BeanUtils.copyProperties(comment, comentSonVo);
            List<CommunityCommentSon> sonList = communityCommentSonService.listPage3(comment.getId());
            comentSonVo.setCommunityCommentSonList(sonList);
            comentSonVoList.add(comentSonVo);
        }
        //查询父评论已审核 总数
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("object_id", id)
                .eq("verify_sign", 1);
        int CommentCount = communityCommentService.selectCount(wrapper);
        return ServerResponse.createBySuccess("success", CommentCount, comentSonVoList);
    }

    @RequestMapping("/myComment")
    @ResponseBody
    @ApiOperation(value = "我的评论", httpMethod = "POST")
    public ServerResponse myComment(String wechatId,
                                    @RequestParam(defaultValue = "1") int pageNum,
                                    @RequestParam(defaultValue = "10") int pageSize) {

        return communityCommentService.myComment(wechatId, pageNum, pageSize);
    }

}
