package com.nsi.controller.portal.nsi_post_bar;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityAnswer;
import com.nsi.pojo.CommunityAsk;
import com.nsi.service.CommunityAnswerService;
import com.nsi.service.CommunityAskService;
import com.nsi.service.CommunityMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 提问互动-回复表 前端控制器
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@Controller
@RequestMapping("/communityAnswer")
@Api(value = "/communityAnswer", description = "社区小程序——提问互动-回复接口")
public class CommunityAnswerController {

    @Autowired
    private CommunityAnswerService communityAnswerService;
    @Autowired
    private CommunityAskService communityAskService;
    @Autowired
    private CommunityMessageService communityMessageService;


    @RequestMapping("/insert")
    @ResponseBody
    @ApiOperation(value = "插入方法", httpMethod = "POST")
    public ServerResponse insert(CommunityAnswer communityAnswer) {
        communityAnswer.setCheckState(0);
        communityAnswer.setCreateTime(new Date());
        boolean b=communityAnswerService.insert(communityAnswer);
        //添加消息
        CommunityAsk communityAsk=communityAskService.selectById(communityAnswer.getAskId());
        communityMessageService.insertMessage(
                "互动被回复",
                communityAnswer.getMessageWechatId(),
                communityAnswer.getMessageContent(),
                communityAnswer.getAskId().toString(),
                communityAsk.getObjectWechatId()
        );
        return b?ServerResponse.createBySuccessMessage("success"):ServerResponse.createByErrorMessage("fail");
    }

}
