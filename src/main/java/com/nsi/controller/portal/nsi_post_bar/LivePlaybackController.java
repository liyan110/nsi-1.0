package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.LivePlayback;
import com.nsi.service.LivePlaybackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 火龙果小程序—直播回放
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
@Controller
@RequestMapping("/playback")
@Api(value = "/playback", description = "火龙果小程序(前台)—回放地址功能")
public class LivePlaybackController {

    @Autowired
    private LivePlaybackService livePlaybackService;

    /**
     * 回放课程列表
     *
     * @param pageNum
     * @param pageSize
     * @param courseType
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "回放课程列表", httpMethod = "GET")
    public ServerResponse findCourseList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                         String courseType) {
        PageHelper.startPage(pageNum, pageSize);
        List<LivePlayback> playbackList = livePlaybackService.selectList(
                new EntityWrapper<LivePlayback>()
                        .eq(StringUtils.isNotBlank(courseType), "course_type", courseType)
                        .orderBy("course_id", false)
        );

        PageInfo pageInfo = new PageInfo(playbackList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 根据课程id返回详情
     *
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据课程id返回详情", httpMethod = "GET")
    public ServerResponse findByCourseId(Integer courseId) {
        LivePlayback livePlayback = livePlaybackService.selectById(courseId);

        // 添加观看数
        livePlaybackService.addWatchNum(courseId);
        return ServerResponse.createBySuccess(livePlayback);
    }

}
