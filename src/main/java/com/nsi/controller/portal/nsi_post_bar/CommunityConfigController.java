package com.nsi.controller.portal.nsi_post_bar;

import com.nsi.common.ServerResponse;
import com.nsi.dao.ConfigureMapper;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.Configure;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Li Yan
 * @create 2020-01-16
 **/

@Controller
@RequestMapping("/CommunityConfig")
@Api(value = "/CommunityConfig", description = "社区小程序—首页配置相关接口")
public class CommunityConfigController {

    @Autowired
    private ConfigureMapper configureMapper;

    @RequestMapping("/insert")
    @ResponseBody
    @ApiOperation(value = "插入方法", httpMethod = "POST")
    public ServerResponse insert(Configure configure) {
        configure.setType("Community");
        int i = configureMapper.insert(configure);
        return ServerResponse.createBySuccess(i);
    }

    @RequestMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "POST")
    public ServerResponse delete(int id) {
        int i = configureMapper.deleteById(id);
        return ServerResponse.createBySuccess(i);
    }

    @RequestMapping("/update")
    @ResponseBody
    @ApiOperation(value = "更新方法", httpMethod = "POST")
    public ServerResponse update(Configure configure) {
        configure.setType("Community");
        int i = configureMapper.updateById(configure);
        return ServerResponse.createBySuccess(i);
    }


    @RequestMapping("/homeConfig")
    @ResponseBody
    @ApiOperation(value = "查找首页配置方法", httpMethod = "POST")
    public ServerResponse homeConfig() {
        String type = "Community";
        List<Configure> list = configureMapper.selectByType(type);
        return ServerResponse.createBySuccess(list);
    }

}
