package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.*;
import com.nsi.service.*;
import com.nsi.vo.nsi_shop_bar.PostCollectVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 社区用户 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-13
 */
@Controller
@RequestMapping("/communityUser")
@Api(value = "/communityUser", description = "社区小程序——用户管理接口")
public class CommunityUserController {

    @Autowired
    private CommunityUserService communityUserService;
    @Autowired
    private CommunityCommentService communityCommentService;
    @Autowired
    private CommunityCommentSonService communityCommentSonService;
    @Autowired
    private IWxPayService iWxPayService;
    @Autowired
    private PostCategoryItemService postCategoryItemService;
    @Autowired
    private PostCollectService postCollectService;
    @Autowired
    private CommunityFollowService communityFollowService;


    @RequestMapping("/login")
    @ResponseBody
    @ApiOperation(value = "登录方法", httpMethod = "POST")
    public ServerResponse login(@RequestParam("wechatId") String wechatId) {
        return communityUserService.login(wechatId);
    }


    @RequestMapping("/register")
    @ResponseBody
    @ApiOperation(value = "注册方法", httpMethod = "POST")
    public ServerResponse register(CommunityUser communityUser) {
        return communityUserService.register(communityUser);
    }


    @RequestMapping("/userLogin")
    @ResponseBody
    @ApiOperation(value = "用户&登录注册联合方法", httpMethod = "POST")
    public ServerResponse userLogin(CommunityUser communityUser) {
        return communityUserService.userLogin(communityUser);
    }


    @RequestMapping("/update")
    @ResponseBody
    @ApiOperation(value = "更新完善用户信息方法", httpMethod = "POST")
    public ServerResponse update(CommunityUser communityUser) {
        return communityUserService.update(communityUser);
    }


    @RequestMapping(value = "/decodeUserInfo.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "微信小程序获取用户信息", httpMethod = "POST")
    public ServerResponse deCodeUserInfo(@RequestParam(value = "code") String code,
                                         @RequestParam(value = "encryptedData") String encryptedData,
                                         @RequestParam(value = "iv") String iv) {
        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(encryptedData) || StringUtils.isEmpty(iv)) {
            return ServerResponse.createByErrorMessage("param not empty");
        }
        String type = "2";
        return iWxPayService.getUserByCodeAndIv(type, code, encryptedData, iv);
    }


    @RequestMapping(value = "/personalCenterPanel.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "个人页面-数据面板", httpMethod = "POST")
    public ServerResponse personalCenterPanel(@RequestParam(value = "wechatId") String wechatId) {

        // 我的帖子
        int myPostResult = postCategoryItemService.selectCount(
                new EntityWrapper<PostCategoryItem>().eq("open_id", wechatId)
        );
        // 我的关注
        int myFollowResult = communityFollowService.selectCount(
                new EntityWrapper<CommunityFollow>().eq("open_id", wechatId)
        );
        // 我的收藏
        int myCollectResult = postCollectService.findCollectCount(wechatId);

        // 我的粉丝
        int myFans = communityFollowService.selectCount(
                new EntityWrapper<CommunityFollow>().eq("follower_id", wechatId)
        );

        // 父评论统计
        int commentResult = communityCommentService.selectCount(
                new EntityWrapper<CommunityComment>()
                        .eq("wechat_id", wechatId)
        );
        // 子评论统计
        int CommentSonResult = communityCommentSonService.selectCount(
                new EntityWrapper<CommunityCommentSon>()
                        .eq("wechat_id", wechatId)
        );
        Map<String, Integer> map = new HashMap<>();
        map.put("myPostResult", myPostResult);
        map.put("myFollowResult", myFollowResult);
        map.put("myFans", myFans);
        map.put("myCollectResult", myCollectResult);
        map.put("CommentCount", commentResult + CommentSonResult);
        return ServerResponse.createBySuccess(map);
    }


    /**
     * 个人页面——删除帖子
     *
     * @param itemId
     * @param openId
     * @return
     */
    @RequestMapping(value = "my_delete.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "个人页面——删除帖子", httpMethod = "POST")
    public ServerResponse deletePostCategoryItem(@RequestParam("itemId") Integer itemId,
                                                 @RequestParam("openId") String openId) {
        if (StringUtils.isEmpty(openId) || itemId == null) {
            throw new NsiOperationException("参数不合法");
        }
        // 校验是否本人操作
        int result = postCategoryItemService.selectCount(
                new EntityWrapper<PostCategoryItem>()
                        .eq("item_id", itemId)
                        .eq("open_id", openId)
        );
        if (result == 0) {
            throw new NsiOperationException("警告,非法操作");
        }
        postCategoryItemService.deleteById(itemId);
        return ServerResponse.createBySuccessMessage("删除帖子成功");
    }


    /**
     * 个人页面——我的帖子
     *
     * @param pageNum
     * @param pageSize
     * @param openId
     * @return
     */
    @RequestMapping(value = "my_post_item.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "个人页面——我的帖子", httpMethod = "GET")
    public ServerResponse myPostItem(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                     @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                     @RequestParam("openId") String openId,
                                     @RequestParam(value = "isCheck", required = false, defaultValue = "") String isCheck) {
        if (StringUtils.isEmpty(openId)) {
            throw new NsiOperationException("参数不合法");
        }
        PageHelper.startPage(pageNum, pageSize);

        EntityWrapper<PostCategoryItem> wrapper = new EntityWrapper<>();
        wrapper.eq("open_id", openId);
        if (StringUtils.isNotBlank(isCheck)) {
            wrapper.eq("is_check", Integer.parseInt(isCheck));
        }
        wrapper.orderBy("item_id", false);
        List<PostCategoryItem> categoryItemList = postCategoryItemService.selectList(wrapper);

        PageInfo pageInfo = new PageInfo(categoryItemList);
        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     * 个人页面——我的收藏
     *
     * @param openId
     * @return
     */
    @RequestMapping(value = "my_collect.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "个人页面——我的收藏", httpMethod = "GET")
    public ServerResponse myCollect(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                    @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                    @RequestParam("openId") String openId) {
        if (StringUtils.isEmpty(openId)) {
            throw new NsiOperationException("参数不合法");
        }
        PageHelper.startPage(pageNum, pageSize);
        List<PostCollectVO> collectList = postCollectService.findCollectList(openId);
        PageInfo pageInfo = new PageInfo(collectList);
        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     * 个人页面——我的收藏删除
     *
     * @param openId
     * @return
     */
    @RequestMapping(value = "my_collect_delete.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "个人页面——我的收藏删除", httpMethod = "POST")
    public ServerResponse myCollect(@RequestParam("collectId") Integer collectId,
                                    @RequestParam("openId") String openId) {
        if (StringUtils.isEmpty(openId) || collectId == null) {
            throw new NsiOperationException("参数不合法");
        }
        postCollectService.delete(
                new EntityWrapper<PostCollect>()
                        .eq("id", collectId)
                        .eq("open_id", openId)
                        .orderBy("item_id", false)
        );
        return ServerResponse.createBySuccessMessage("删除成功");
    }


    @RequestMapping("/SendSubscribeMessage_verfiy.do")
    @ResponseBody
    @ApiOperation(value = "管理员 消息推送", httpMethod = "POST")
    public ServerResponse SendSubscribeMessage_verfiy(@RequestParam("id") int id) throws Exception {
        communityUserService.SendSubscribeMessage();
        return ServerResponse.createBySuccess("success");
    }


    //用户主页相关
    @RequestMapping("/getUserInfo")
    @ResponseBody
    @ApiOperation(value = "通过微信id 获取用户信息", httpMethod = "POST")
    public ServerResponse getUserInfo(@RequestParam("wechatId") String wechatId) {
        return ServerResponse.createBySuccess(communityUserService.getUserInfo(wechatId));
    }


    @RequestMapping("/myFans")
    @ResponseBody
    @ApiOperation(value = "我的粉丝列表方法(缺关注者昵称)", httpMethod = "POST")
    public ServerResponse myFollow(String wechatId,
                                   @RequestParam(defaultValue = "1") int pageNum,
                                   @RequestParam(defaultValue = "10") int pageSize) {
        Page<CommunityFollow> page = new Page<>(pageNum, pageSize);
        EntityWrapper<CommunityFollow> wrapper = new EntityWrapper<>();
        wrapper.eq("follower_id", wechatId);
        System.out.println(communityFollowService.selectPage(page, wrapper));
        List<CommunityFollow> list = communityFollowService.selectPage(page, wrapper).getRecords();
        return ServerResponse.createBySuccess(list);
    }


    //测试用户积分
    @RequestMapping("/testAddScore")
    @ResponseBody
    @ApiOperation(value = "测试用户积分", httpMethod = "POST")
    public ServerResponse testAddScore(@RequestParam("wechatId") String wechatId,
                                       int Num) {
        int i = communityUserService.AddScore(Num, wechatId);
        return ServerResponse.createBySuccess(i);
    }


}

