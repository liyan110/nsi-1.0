package com.nsi.controller.portal.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityFollow;
import com.nsi.pojo.CommunityUser;
import com.nsi.service.CommunityFollowService;
import com.nsi.service.CommunityUserService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 社区关注 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@Controller
@RequestMapping("/communityFollow")
public class CommunityFollowController {

    @Autowired
    private CommunityFollowService communityFollowService;

    @Autowired
    private CommunityUserService communityUserService;


    @RequestMapping("/insert")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse insert(CommunityFollow communityFollow){
        //关注对象信息
        String followerId=communityFollow.getFollowerId();
        CommunityUser Follower =communityUserService.detail(followerId);

        //发起关注-用户信息
        String openId=communityFollow.getOpenId();
        CommunityUser user =communityUserService.detail(openId);

        communityFollow.setNickname(user.getNickname());
        communityFollow.setWechatPortrait(user.getWechatPortrait());

        communityFollow.setFollowerNickname(Follower.getNickname());
        communityFollow.setFollowerPortrait(Follower.getWechatPortrait());
        communityFollow.setCreateTime(new Date());
        boolean b=communityFollowService.insert(communityFollow);
        if (b){
            return ServerResponse.createBySuccess("返回成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }


    @RequestMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "POST")
    public ServerResponse delete(int id){
        boolean b=communityFollowService.deleteById(id);
        if (b==true){
            return ServerResponse.createBySuccess("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }



    @RequestMapping("/cancelFollow")
    @ResponseBody
    @ApiOperation(value = "取消此条关注", httpMethod = "POST")
    public ServerResponse cancelFollow(String wechatId,String followerId){
        EntityWrapper<CommunityFollow> wrapper=new EntityWrapper<>();
        wrapper.eq("open_id",wechatId)
                .eq("follower_id",followerId);
        boolean b=communityFollowService.delete(wrapper);
        if (b){
            return ServerResponse.createBySuccess("已删除");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }


    @RequestMapping("/checkFollow")
    @ResponseBody
    @ApiOperation(value = "检查是否已关注", httpMethod = "POST")
    public ServerResponse checkFollow(String wechatId,String followerId){
        EntityWrapper<CommunityFollow> wrapper=new EntityWrapper<>();
        wrapper.eq("open_id",wechatId)
                .eq("follower_id",followerId);
        int i=communityFollowService.selectCount(wrapper);
        if (i==0){
            return ServerResponse.createBySuccess("未关注");
        }
        return ServerResponse.createByErrorMessage("已关注");
    }


    @RequestMapping("/myFollow")
    @ResponseBody
    @ApiOperation(value = "我的关注列表方法", httpMethod = "POST")
    public ServerResponse myFollow(String wechatId,
                                   @RequestParam(defaultValue = "1") int pageNum,
                                   @RequestParam(defaultValue = "10") int pageSize){
        Page<CommunityFollow> page=new Page<>(pageNum,pageSize);
        EntityWrapper<CommunityFollow> wrapper=new EntityWrapper<>();
        wrapper.eq("open_id",wechatId);
        System.out.println(communityFollowService.selectPage(page, wrapper));
        List<CommunityFollow> list=communityFollowService.selectPage(page,wrapper).getRecords();
        return ServerResponse.createBySuccess(list);
    }




}
