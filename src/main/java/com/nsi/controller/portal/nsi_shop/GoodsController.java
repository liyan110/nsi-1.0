package com.nsi.controller.portal.nsi_shop;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Configure;
import com.nsi.pojo.ShopGoods;
import com.nsi.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2018-12-28
 **/
@RequestMapping("/goods")
@Controller
public class GoodsController {

    @Autowired
    private IGoodsService iGoodsService;

    /**
     * 前台搜索返回list
     *
     * @param type * @param state
     * @return
     */
    @RequestMapping(value = "/goods_list.do")
    @ResponseBody
    public ServerResponse productList(@RequestParam(value = "type", defaultValue = "新学说书籍") String type,
                                      @RequestParam(value = "state", defaultValue = "", required = false) String state,
                                      @RequestParam(value = "searchKey", defaultValue = "", required = false) String searchKey,
                                      @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        try {
            return iGoodsService.productList(type, state, searchKey, pageNum, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 详情方法
     *
     * @param Id
     * @param Id
     * @return4
     */
    @RequestMapping(value = "/goods_detail.do")
    @ResponseBody
    public ServerResponse productDetail(@RequestParam(value = "Id") int Id) {
        try {
            return iGoodsService.productDetail(Id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 添加方法
     *
     * @param shopGoods
     * @return4
     */
    @RequestMapping(value = "/goods_add.do")
    @ResponseBody
    public ServerResponse productSave(ShopGoods shopGoods) {
        try {
            return iGoodsService.productSave(shopGoods);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "添加失败");
    }

    /**
     * 删除方法
     *
     * @param id
     * @return4
     */
    @RequestMapping(value = "/goods_delete.do")
    @ResponseBody
    public ServerResponse productRemove(int id) {
        try {
            return iGoodsService.productRemove(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "删除失败");
    }

    /**
     * 商品修改方法
     *
     * @param good
     * @return
     */
    @RequestMapping(value = "/goods_modify.do")
    @ResponseBody
    public ServerResponse productUpdate(ShopGoods good) {
        iGoodsService.productUpdate(good);
        return ServerResponse.createByErrorCodeMessage(500, "修改失败");
    }

//商城首页配置

    /**
     * 获取现时特惠列表配置
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/Get_Home_Config.do")
    @ResponseBody
    public ServerResponse setIndexConfig(String type) {
        return iGoodsService.getHomeConfig(type);
    }
//

    /**
     * 设置首页配置
     *
     * @param configure
     * @return
     */
    @RequestMapping(value = "/Set_Home_Config.do")
    @ResponseBody
    public ServerResponse setHomeConfig(Configure configure) {

        return iGoodsService.setHomeConfig(configure);
    }


}
