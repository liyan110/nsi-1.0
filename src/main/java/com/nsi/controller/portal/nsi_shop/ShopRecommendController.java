package com.nsi.controller.portal.nsi_shop;

import com.nsi.common.ServerResponse;
import com.nsi.dao.CourseListMapper;
import com.nsi.dao.ShopGoodsMapper;
import com.nsi.pojo.CourseList;
import com.nsi.pojo.ShopGoods;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-04-28
 **/
@RequestMapping("/ShopRecommend")
@Controller
@Api(value = "/ShopRecommend", description = "商城内容推介系统")
public class ShopRecommendController {
    //   商品推介模块
    @Autowired
    private ShopGoodsMapper goodsMapper;

    @Autowired
    private CourseListMapper courseListMapper;

    /**
     * 获取推介商品list
     *
     * @param goodsId
     * @return4
     */
    @RequestMapping(value = "/getRecommend.do")
    @ResponseBody
    @ApiOperation(value = "获取推介商品list", httpMethod = "POST")
    public ServerResponse getRecommend(String goodsId,String type) {
        int id =Integer.parseInt(goodsId);
        switch (type){
            case "书籍" :
                List<ShopGoods> list =goodsMapper.goodsRecommend(id);
                return ServerResponse.createBySuccess(list);
            case "课程" :
                List<CourseList> listCourse = courseListMapper.CourseRecommend(id);
                return ServerResponse.createBySuccess(listCourse);
            case "活动" :
                break;
            default:
                return ServerResponse.createByErrorCodeMessage(500,"异常:type参数");
        }
        return ServerResponse.createByErrorCodeMessage(500,"异常:type参数");
    }
}
