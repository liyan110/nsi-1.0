package com.nsi.controller.portal.nsi_shop;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.ShopAddress;
import com.nsi.service.IShopAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 商城-商品管理
 *
 * @author Li Yan
 * @create 2018-12-28
 **/
@RequestMapping("/ShopAddress")
@Controller
public class ShopAddressController {

    @Autowired
    IShopAddressService IShopAddressService;

    /**
     * 新增地址
     *
     * @param shopAddress
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    public ServerResponse add(ShopAddress shopAddress) {
        return IShopAddressService.add(shopAddress);
    }

    /**
     * 修改地址
     *
     * @param shopAddress
     * @return4
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    public ServerResponse update(ShopAddress shopAddress) {
        return IShopAddressService.update(shopAddress);
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    public ServerResponse detail(Integer id) {
        ShopAddress shop = IShopAddressService.selectById(id);
        return ServerResponse.createBySuccess(shop);
    }

    /**
     * 查询地址
     *
     * @param wechatId
     * @return4
     */
    @RequestMapping(value = "/getList.do")
    @ResponseBody
    public ServerResponse getList(@RequestParam(value = "wechatId") String wechatId,
                                  @RequestParam(value = "unionId", required = false) String unionId) {
        return IShopAddressService.getAddress(wechatId, unionId);
    }
}
