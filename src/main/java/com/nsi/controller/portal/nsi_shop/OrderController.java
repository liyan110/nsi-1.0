package com.nsi.controller.portal.nsi_shop;

import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseList;
import com.nsi.pojo.Order;
import com.nsi.pojo.User;
import com.nsi.service.ICourseListService;
import com.nsi.service.IOrderService;
import com.nsi.service.IUserService;
import com.nsi.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 订单控制层
 *
 * @author Luo Zhen
 * @create 2019-01-08 14:50
 */
@Slf4j
@Controller
@RequestMapping("/order/")
@Api(value = "/order/", description = "商城—订单业务接口")
public class OrderController {

    @Autowired
    private IOrderService iOrderService;
    @Autowired
    private IUserService iUserService;
    @Autowired
    private ICourseListService iCourseListService;

    /**
     * 创建订单
     *
     * @return
     */
    @RequestMapping("create.do")
    @ResponseBody
    @ApiOperation(value = "创建订单", httpMethod = "GET")
    public ServerResponse create(Order order) {
        if (StringUtils.isBlank(order.getWechatId())) {
            return ServerResponse.createByErrorMessage("请登陆后购买!");
        }
        return iOrderService.createOrder(order);
    }

    /**
     * 创建课程订单
     *
     * @return
     */
    @RequestMapping("create_course.do")
    @ResponseBody
    @ApiOperation(value = "创建课程订单", httpMethod = "GET")
    public ServerResponse createOrderByCourse(Order order) {
        if (StringUtils.isBlank(order.getWechatId())) {
            return ServerResponse.createByErrorMessage("请登陆后购买!");
        }
        CourseList course = iCourseListService.findCourseListById(order.getGoodsId());
        return iOrderService.createOrderByCourse(order, course);
    }

    /**
     * 创建活动订单
     *
     * @param order
     * @return
     */
    @RequestMapping("create_activity.do")
    @ResponseBody
    @ApiOperation(value = "创建活动订单", httpMethod = "POST")
    public ServerResponse createOrderByActivity(Order order) {
        return iOrderService.createOrderByActivity(order);
    }

    /**
     * 创建订单根据购物车
     *
     * @param order
     * @return
     */
    @RequestMapping("create_cart.do")
    @ResponseBody
    @ApiOperation(value = "创建订单根据购物车", httpMethod = "POST")
    public ServerResponse createOrderByShoppingCart(Order order) {
        try {
            if (StringUtils.isBlank(order.getWechatId())) {
                return ServerResponse.createByErrorMessage("请登陆后购买!");
            }
            //      待完善验证 unioniD
            return iOrderService.createOrderByShoppingCart(order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "创建购物车订单失败");
    }

    /**
     * 返回用户的订单 取最新的一条
     *
     * @param wechatId
     * @return
     */
    @RequestMapping("get_order_item.do")
    @ResponseBody
    @ApiOperation(value = "根据wechatId 返回订单详情", httpMethod = "GET")
    public ServerResponse getOrderByWechatId(String wechatId, String unionId) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("请登陆后购买!");
        }
        if (StringUtils.isBlank(unionId)) {
            return ServerResponse.createByErrorMessage("参数缺少unionId");
        }
        return iOrderService.getOrderByWechatIdAndUnionId(wechatId, unionId);
    }

    /**
     * 取消订单(创建最新的一笔)
     *
     * @param wechatId
     * @return
     */
    @RequestMapping("cancel.do")
    @ResponseBody
    @ApiOperation(value = "取消订单", httpMethod = "GET")
    public ServerResponse cancel(String wechatId, String unionId) {
        if (StringUtils.isBlank(wechatId) || StringUtils.isBlank(unionId)) {
            return ServerResponse.createByErrorMessage("请求参数不完整");
        }
        return iOrderService.cancel(wechatId, unionId);
    }

    /**
     * 我的详情页中取消订单
     *
     * @param wechatId
     * @param orderNo
     * @return
     */
    @RequestMapping("cancel_me.do")
    @ResponseBody
    @ApiOperation(value = "我的详情页中取消订单", httpMethod = "GET")
    public ServerResponse cancelMe(@RequestParam("wechatId") String wechatId,
                                   @RequestParam("orderNo") String orderNo) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("请登陆后购买!");
        }
        return iOrderService.cancelMe(wechatId, orderNo);
    }


    /**
     * 个人页面 返回订单list
     *
     * @param wechatId
     */
    @RequestMapping("get_order_list.do")
    @ResponseBody
    @ApiOperation(value = "个人页面 返回书店/购物车订单详情", httpMethod = "GET")
    public ServerResponse getOrderList(String wechatId, String unionId) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("未登录,请重新登陆");
        }
        if (StringUtils.isBlank(unionId)) {
            return ServerResponse.createByErrorMessage("参数缺少unionId");
        }
        try {
            return iOrderService.getOrderList(wechatId, unionId);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 个人页面 返回课程订单详情
     *
     * @param wechatId
     * @return
     */
    @RequestMapping("get_order_course_list.do")
    @ResponseBody
    @ApiOperation(value = "个人页面 返回课程订单详情", httpMethod = "GET")
    public ServerResponse getOrderCourseList(String wechatId, String unionId) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("未登录,请重新登陆");
        }
        try {
            List<Order> orderCourseList = iOrderService.findOrderList(wechatId, unionId, Const.ProductTypeEnum.MEETING.getValue());
            if (CollectionUtils.isEmpty(orderCourseList)) {
                return ServerResponse.createByErrorMessage("您还没有创建订单");
            }
            return iOrderService.getOrderCourseList(orderCourseList);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 根据wechatId和订单号查询详情
     *
     * @param orderNo
     * @return
     */
    @RequestMapping("find_order_item.do")
    @ResponseBody
    @ApiOperation(value = "根据wechatId和订单号查询详情", httpMethod = "GET")
    public ServerResponse findOrderItemByWechatIdAndOrderNo(@RequestParam("orderNo") String orderNo) {

        return iOrderService.findOrderItemByOrderNo(orderNo);
    }

    /**
     * 根据wechatId返回订单状态条数
     *
     * @param wechatId
     * @return
     */
    @RequestMapping("find_order_count.do")
    @ResponseBody
    @ApiOperation(value = "根据wechatId返回订单状态条数", httpMethod = "GET")
    public ServerResponse findOrderStatusCount(@RequestParam("wechatId") String wechatId, @RequestParam("unionId") String unionId) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("未登录,请重新登陆");
        }
        try {
            return iOrderService.findOrderStatusCount(wechatId, unionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 根据wechatId查询用户详情
     *
     * @param wechatId
     * @return
     */
    @RequestMapping("valid_userInfo.do")
    @ResponseBody
    @ApiOperation(value = "根据wechatId查询用户详情", httpMethod = "GET")
    public ServerResponse validUserInfo(@RequestParam("wechatId") String wechatId,
                                        @RequestParam("unionId") String unionId) {
        try {
            User user = iUserService.findByWechatIdAndUnionId(wechatId, unionId);
            if (user != null) {
                UserVo userVo = new UserVo();
                BeanUtils.copyProperties(user, userVo);
                return ServerResponse.createBySuccess(userVo);
            }
            return ServerResponse.createByError();
        } catch (Exception e) {
            log.info("UserController:" + e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 商城注册
     *
     * @param user
     * @return
     */
    @RequestMapping("mall_register.do")
    @ResponseBody
    @ApiOperation(value = "商城注册", httpMethod = "POST")
    public ServerResponse validUserInfo(User user) {
        try {
            User currentUser = iUserService.selectByUserMail(user.getUsername());
            // 注册
            if (currentUser == null) {
                iUserService.saveMallRegister(user);
                return ServerResponse.createBySuccess();
            }

            // 根据邮箱绑定weichatId
            currentUser.setUsername(user.getUsername());
            currentUser.setUserTurename(user.getUserTurename());
            currentUser.setUserOrganization(user.getUserOrganization());
            currentUser.setWechatid(user.getWechatid());
            currentUser.setUnionId(user.getUnionId());

            ServerResponse<String> response = iUserService.updateInfomation(currentUser);
            if (response.isSuccess()) {
                return ServerResponse.createBySuccess();
            } else {
                return ServerResponse.createByErrorMessage("修改失败");
            }
        } catch (Exception e) {
            log.info("UserController:" + e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "添加失败");
    }


}
