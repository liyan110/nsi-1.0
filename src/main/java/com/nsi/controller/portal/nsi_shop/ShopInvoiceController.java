package com.nsi.controller.portal.nsi_shop;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ShopInvoice;
import com.nsi.service.IShopInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * 发票系统
 *
 * @author Li Yan
 * @create 2019-03-14
 **/
@RequestMapping("/Invoice")
@Controller
@Api(value = "/Invoice", description = "发票管理系统")
public class ShopInvoiceController {

    @Autowired
    private IShopInvoiceService iShopInvoiceService;

    /**
     * 创建商城发票
     *
     * @param shopInvoice
     * @return
     */
    @RequestMapping(value = "/ShopInvoiceCreate.do")
    @ResponseBody
    public ServerResponse ShopInvoiceCreate(ShopInvoice shopInvoice) {
        if (StringUtils.isBlank(shopInvoice.getUserOrderNum())) {
            return ServerResponse.createByErrorMessage("订单号不能为空");
        }
        int resultCount = iShopInvoiceService.selectCount(new EntityWrapper<ShopInvoice>().eq("user_order_num", shopInvoice.getUserOrderNum()));
        if (resultCount > 0) {
            return ServerResponse.createByErrorMessage("发票信息已存在,请勿重复提交");
        }
        shopInvoice.setManageName("研究院商城");
        shopInvoice.setManagePaymentMethod("微信支付平台");
        shopInvoice.setManageState("确认通过");
        shopInvoice.setFinanceState("未开票");
        shopInvoice.setCreateTime(new Date());
        shopInvoice.setUpdateTime(shopInvoice.getCreateTime());
        return iShopInvoiceService.ShopInvoiceCreate(shopInvoice);
    }


    /**
     * 创建活动发票
     *
     * @param shopInvoice
     * @return4
     */
    @RequestMapping(value = "/EventInvoiceCreate.do")
    @ResponseBody
    public ServerResponse EventInvoiceCreate(ShopInvoice shopInvoice) {
        if (StringUtils.isBlank(shopInvoice.getUserOrderNum())) {
            return ServerResponse.createByErrorMessage("订单号不能为空");
        }
        int resultCount = iShopInvoiceService.selectCount(new EntityWrapper<ShopInvoice>().eq("user_order_num", shopInvoice.getUserOrderNum()));
        if (resultCount > 0) {
            return ServerResponse.createByErrorMessage("发票信息已存在,请勿重复提交");
        }
        shopInvoice.setManageName("活动缴费");
        shopInvoice.setManageState("确认通过");
        shopInvoice.setFinanceState("未开票");
        shopInvoice.setCreateTime(new Date());
        shopInvoice.setUpdateTime(shopInvoice.getCreateTime());
        return iShopInvoiceService.ShopInvoiceCreate(shopInvoice);
    }

    /**
     * 获取发票历史
     *
     * @param wechatId
     * @return
     */
    @RequestMapping(value = "/get_invoice_history.do")
    @ResponseBody
    public ServerResponse getEventInvoiceList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                              @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                              @RequestParam("wechatId") String wechatId) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        PageHelper.startPage(pageNum, pageSize);
        List<ShopInvoice> invoices = iShopInvoiceService.selectList(new EntityWrapper<ShopInvoice>().eq("wechat_id", wechatId).orderBy("create_time", false));
        PageInfo pageInfo = new PageInfo(invoices);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 根据Id返回详情
     *
     * @param invoiceId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    public ServerResponse detail(Integer invoiceId) {
        if (invoiceId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        return ServerResponse.createBySuccess(iShopInvoiceService.selectById(invoiceId));
    }

    /**
     * 后台手动创建发票
     *
     * @param shopInvoice
     * @return4
     */
    @RequestMapping(value = "/HandInvoiceCreate.do")
    @ResponseBody
    public ServerResponse HandInvoiceCreate(ShopInvoice shopInvoice) {

        shopInvoice.setManageName("后台手动创建发票");
        shopInvoice.setManageState("确认通过");
        shopInvoice.setFinanceState("未开票");

        shopInvoice.setCreateTime(new Date());
        shopInvoice.setUpdateTime(shopInvoice.getCreateTime());
        return iShopInvoiceService.ShopInvoiceCreate(shopInvoice);
    }

    /**
     * 财务-发票列表
     *
     * @param SearchKey
     * @return4
     */
    @RequestMapping(value = "/InvoiceList.do")
    @ResponseBody
    public ServerResponse InvoiceList(@RequestParam(value = "SearchKey", defaultValue = "", required = false) String SearchKey,
                                      @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                      String manageState,
                                      String financeState) {
        try {
            return iShopInvoiceService.InvoiceList(SearchKey, manageState, financeState, pageNum, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "");
    }

    /**
     * 财务-发票订单 联查列表
     *
     * @param SearchKey
     * @return4
     */
    @RequestMapping(value = "/InvoiceOrderList.do")
    @ResponseBody
    public ServerResponse InvoiceOrderList(@RequestParam(value = "SearchKey", defaultValue = "", required = false) String SearchKey,
                                           @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                           String manageState,
                                           String financeState) {
        try {
            return iShopInvoiceService.InvoiceOrderList(SearchKey, manageState, financeState, pageNum, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "");
    }

    /**
     * 财务-通过发票
     *
     * @param Id
     * @return4
     */
    @RequestMapping(value = "/PassShopInvoice.do")
    @ResponseBody
    public ServerResponse PassShopInvoice(Integer Id) {
        return iShopInvoiceService.PassShopInvoice(Id);
    }

    /**
     * 根据订单号验证是否存在发票信息
     *
     * @param orderNo
     * @return
     */
    @RequestMapping("/check_invoice.do")
    @ResponseBody
    public ServerResponse checkInvoice(@RequestParam("orderNo") String orderNo) {
        return iShopInvoiceService.checkInvoice(orderNo);
    }

    /**
     * 删除发票信息
     *
     * @param id
     * @return
     */
    @RequestMapping("/del.do")
    @ResponseBody
    public ServerResponse del(@RequestParam("id") Integer id) {
        return iShopInvoiceService.del(id);
    }


}
