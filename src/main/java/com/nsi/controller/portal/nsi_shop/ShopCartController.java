package com.nsi.controller.portal.nsi_shop;

import com.nsi.common.ServerResponse;
import com.nsi.service.IShopCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2019-02-20
 **/
//购物车功能
@RequestMapping("/ShopCart")
@Controller
public class ShopCartController {

    @Autowired
    private IShopCartService iShopCartService;

    /**
     * 创建购物车
     *
     * @param openId
     * @return4
     */
    @RequestMapping(value = "/create.do")
    @ResponseBody
    public ServerResponse create(@RequestParam(value = "openId") String openId) {
//        返回购物车 ID
        return iShopCartService.create(openId);
    }


    /**
     * 更新购物车
     *
     * @param goodsJson
     * @return4
     */
    @RequestMapping(value = "/updateCart.do")
    @ResponseBody
    public ServerResponse updateCart(@RequestParam(value = "openId") String openId,
                                     @RequestParam(value = "goodsJson") String goodsJson) {
//        过滤json
        return iShopCartService.updateCart(openId, goodsJson);
    }

    /**
     * 锁定购物车
     *
     * @param cartId
     * @return4
     */
    @RequestMapping(value = "/lockCart.do")
    @ResponseBody
    public ServerResponse lockCart(@RequestParam(value = "cartId", required = false) String cartId) {
        return iShopCartService.lockCart(cartId);
    }

    /**
     * 购物车商品详情-开票
     *
     * @param cartId
     * @return4
     */
    @RequestMapping(value = "/cartDetail.do")
    @ResponseBody
    public ServerResponse cartDetail(@RequestParam(value = "cartId", required = false) String cartId) {
//      返回商品信息
        return iShopCartService.cartDetail(cartId);
    }


}
