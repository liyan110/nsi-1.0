package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.EntryAudit;
import com.nsi.service.EntryAuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-09-17 14:12
 */
@Controller
@RequestMapping("/entryAudit/")
public class EntryAuditController {

    @Autowired
    EntryAuditService entryAuditService;

    @RequestMapping(value = "add.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse saveEntryAudit(EntryAudit entryAudit) {
        return entryAuditService.add(entryAudit);
    }

}
