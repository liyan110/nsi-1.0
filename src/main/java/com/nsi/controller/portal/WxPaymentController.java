package com.nsi.controller.portal;


import com.nsi.common.ServerResponse;
import com.nsi.service.IWxPayService;
import com.wechat.pay.PayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Luo Zhen
 * @create 2018-08-07
 */
@Controller
@RequestMapping("/wxPay")
@Api(value = "/wxPay", description = "第三方——微信支付接口")
public class WxPaymentController {


    @Autowired
    private IWxPayService iWxPayService;

    /**
     * 微信小程序 获取openId用户信息
     *
     * @param type
     * @param code
     * @param encryptedData
     * @param iv
     * @return
     */
    @RequestMapping(value = "/decodeUserInfo.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "微信小程序获取用户信息", httpMethod = "POST")
    public ServerResponse deCodeUserInfo(String type,
                                         @RequestParam(value = "code") String code,
                                         @RequestParam(value = "encryptedData") String encryptedData,
                                         @RequestParam(value = "iv") String iv) {
        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(encryptedData) || StringUtils.isEmpty(iv)) {
            return ServerResponse.createByErrorMessage("param not empty");
        }
        return iWxPayService.getUserByCodeAndIv(type, code, encryptedData, iv);

    }

    /**
     * 授权获取微信用户信息——公众号
     *
     * @param code
     * @return
     */
    @RequestMapping("/get_wx_info.do")
    @ResponseBody
    @ApiOperation(value = "授权获取微信用户信息", httpMethod = "GET")
    public ServerResponse getWXUserInfo(String code) {
        return iWxPayService.getUserInfo(code);
    }

    /**
     * 微信——公众号支付
     *
     * @param openid    用户标识
     * @param body      商品内容
     * @param total_fee 商品价格
     * @param attach    附加内容(姓名加邮箱)
     * @return
     */
    @RequestMapping("/pay.do")
    @ResponseBody
    @ApiOperation(value = "微信公众号支付", httpMethod = "GET")
    public ServerResponse wxPayment(String openid, String body, String total_fee, String attach) {
        return iWxPayService.wxPayment(openid, body, total_fee, attach);
    }

    /**
     * 微信——h5网页方法
     *
     * @param body      商品内容
     * @param total_fee 商品价格
     * @param attach    商品附加
     * @param sceneInfo 场景信息
     * @return
     */
    @RequestMapping("/h5_pay.do")
    @ResponseBody
    @ApiOperation(value = "微信h5网页方法", httpMethod = "GET")
    public ServerResponse wxPaymentH5(HttpServletRequest request, String body, String total_fee, String attach, String sceneInfo) {
        String spbillIp = request.getRemoteAddr();
        return iWxPayService.wxPaymentH5(body, total_fee, attach, spbillIp, sceneInfo);
    }

    /**
     * 微信——原生扫码支付
     *
     * @param body
     * @param total_fee
     * @param attach
     * @return
     */
    @RequestMapping("/pay_qrCode.do")
    @ResponseBody
    @ApiOperation(value = "微信原生扫码支付", httpMethod = "GET")
    public ServerResponse wxPaymentQR(String body, String total_fee, String attach) {
//        兼容写法
        String out_trade_no = PayUtil.getCurrTime() + PayUtil.getRandomString(5);
        return iWxPayService.wxPaymentQrCode(body, total_fee, attach,out_trade_no);
    }

    /**
     * 微信——轮询接口__根据attach
     *
     * @param attach
     * @return
     */
    @ResponseBody
    @RequestMapping("query_wxpay_status.do")
    @ApiOperation(value = "微信轮询功能", httpMethod = "GET")
    public ServerResponse queryOrderPayStatus(String attach) {
        // TODO: 2019/6/25 废弃，迁移至paymentController
        return iWxPayService.queryOrderPayStatus(attach);
    }

    /**
     * 微信——分享功能
     *
     * @param url
     * @return
     */
    @RequestMapping("/wx_chat_share.do")
    @ResponseBody
    @ApiOperation(value = "微信分享功能", httpMethod = "GET")
    public ServerResponse wxChatShare(String url) {
        return iWxPayService.wxChatShare(url);
    }

    /**
     * 微信——支付回调接口
     *
     * @return
     */
    @RequestMapping("/pay_callback.do")
    @ResponseBody
    @ApiOperation(value = "微信回调方法", httpMethod = "GET")
    public void wxCallBack(HttpServletRequest request, HttpServletResponse response) {
        iWxPayService.WxPay_callback(request, response);
    }

    /**
     * 微信——轮询接口
     *
     * @param wechatId
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping("query_order_pay_status.do")
    @ApiOperation(value = "微信轮询,根据订单号", httpMethod = "GET")
    public ServerResponse queryOrderPayStatus(String wechatId, String orderNo) {
        if (StringUtils.isBlank(wechatId)) {
            return ServerResponse.createByErrorMessage("未登录,请重新登陆");
        }
        ServerResponse response = iWxPayService.queryOrderPayStatus(wechatId, orderNo);
        if (response.isSuccess()) {
            return ServerResponse.createBySuccess(true);
        }
        return ServerResponse.createBySuccess(false);
    }


}
