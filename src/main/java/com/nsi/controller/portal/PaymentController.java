package com.nsi.controller.portal;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.aliyun.alipay.demo.AlipayConfig;
import com.google.common.collect.Maps;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.service.IAliPayService;
import com.nsi.service.IWxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Li Yan
 * @create 2019-01-09
 **/
@Slf4j
@Controller
@RequestMapping("/Pay")
@Api(value = "/Pay", description = "支付平台-支付接口")
public class PaymentController {

    @Autowired
    private IWxPayService iWxPayService;
    @Autowired
    private IAliPayService iAliPayService;

    /*------------------------------------------------- 微信支付 ------------------------------------*/

    /**
     * 微信—公众号支付
     *
     * @param openid    用户标识
     * @param body      商品内容
     * @param total_fee 商品价格
     * @return
     */
    @RequestMapping("/WxPay_public.do")
    @ResponseBody
    @ApiOperation(value = "微信公众号支付", httpMethod = "GET")
    public ServerResponse wxPayment(String openid, String body, String total_fee, String out_trade_no) {
        String attach = "公众号支付-新";
        return iWxPayService.WxPay_public(openid, body, total_fee, out_trade_no, attach);
    }

    /**
     * 微信—小程序支付
     *
     * @param body 商品内容
     * @return
     */
    @RequestMapping("/WxPay_miniProgram.do")
    @ResponseBody
    @ApiOperation(value = "微信小程序支付", httpMethod = "GET")
    public ServerResponse WxPay_miniProgram(String openid, String body, String total_fee, String out_trade_no, String attach) {
        return iWxPayService.WxPay_miniProgram(openid, body, total_fee, out_trade_no, attach);
    }

    /**
     * 微信——原生扫码支付
     *
     * @param body
     * @param total_fee
     * @param attach
     * @return
     */
    @RequestMapping("/pay_qrCode.do")
    @ResponseBody
    @ApiOperation(value = "微信原生扫码支付", httpMethod = "GET")
    public ServerResponse wxPaymentQR(String body, String total_fee, String attach, String out_trade_no) {
        return iWxPayService.wxPaymentQrCode(body, total_fee, attach, out_trade_no);
    }

    /**
     * 微信扫码支付——轮询接口-根据订单号
     *
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping("query_wxpay_status.do")
    @ApiOperation(value = "微信轮询功能", httpMethod = "GET")
    public ServerResponse queryOrderPayStatus(String orderNo) {
        return iWxPayService.queryOrderPayStatus(orderNo);
    }

    /**
     * 微信—支付回调接口
     *
     * @return
     */
    @RequestMapping("/WxPay_callback.do")
    @ResponseBody
    @ApiOperation(value = "微信回调方法", httpMethod = "GET")
    public void WxPay_callback(HttpServletRequest request, HttpServletResponse response) {
        iWxPayService.WxPay_callback(request, response);
    }


    /*------------------------------------------------- 支付宝支付 ------------------------------------*/

    /**
     * 支付宝支付
     *
     * @param subject   商品标题
     * @param body      商品明细
     * @param total_fee 商品价格
     * @return
     */
    @RequestMapping("/ali_app_pay.do")
    @ResponseBody
    @ApiOperation(value = "支付宝手机支付", httpMethod = "GET")
    public void aliAppPayment(String subject, String body, String total_fee, HttpServletResponse response, String returnUrl) {
        try {
            iAliPayService.appPay(subject, body, total_fee, response, returnUrl);
        } catch (IOException e) {
            log.info("手机支付异常!", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 支付宝PC网页支付
     *
     * @param subject
     * @param body
     * @param total_fee
     * @param response
     */
    @RequestMapping("/ali_pc_pay.do")
    @ResponseBody
    @ApiOperation(value = "支付宝PC网页支付", httpMethod = "GET")
    public void aliPaymentHtml(String subject, String body, String total_fee, HttpServletResponse response, String returnUrl) {
        try {
            iAliPayService.appPayHtml(subject, body, total_fee, response, returnUrl);
        } catch (IOException e) {
            log.info("PC网页支付异常!", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 支付宝回调地址
     *
     * @param request
     * @return
     */
    @RequestMapping("/alipay_callback.do")
    @ResponseBody
    @ApiOperation(value = "支付宝回调结果", httpMethod = "POST")
    public Object alipayCallback(HttpServletRequest request) {

        Map<String, String> params = Maps.newHashMap();

        Map requestParams = request.getParameterMap();
        //发起请求之后，支付宝会回调，我们查看要遍历map的值看看有什么
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);

            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        //打印个日志
        log.info("支付宝回调,sign:{},trade_status:{},参数:{}", params.get("sign"), params.get("trade_status"), params.toString());

        //约定去除sign_type 进行验签
        params.remove("sign_type");
        try {
            boolean alipayRSACheckedV2 = AlipaySignature.rsaCheckV2(params, AlipayConfig.ALIPAY_PUBLIC_KEY, "utf-8", AlipayConfig.SIGNTYPE);
            if (!alipayRSACheckedV2) {
                return ServerResponse.createByErrorMessage("非法请求，验证不通过，在恶意请求我就报警了!");
            }
        } catch (AlipayApiException e) {
            log.info("支付宝回调异常", e);
            e.printStackTrace();
        }

        ServerResponse response = iAliPayService.aliCallback(params);
        if (response.isSuccess()) {
            return Const.AlipayCallback.RESPONSE_SUCCESS;
        }
        return Const.AlipayCallback.RESPONSE_FAILED;
    }

    /**
     * 轮询接口
     *
     * @param orderNo 订单号
     * @return
     */
    @ResponseBody
    @RequestMapping("query_order_pay_status.do")
    @ApiOperation(value = "支付宝轮寻功能", httpMethod = "GET")
    public ServerResponse queryOrderAliPayStatus(String orderNo) {
        return iAliPayService.queryOrderPayStatus(orderNo);
    }
}
