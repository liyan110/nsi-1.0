package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ClassComment;
import com.nsi.service.ICommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2018-08-22
 **/
@RequestMapping("/Comment")
@Controller
@Api(value = "/Comment", description = "新学说课堂——评论管理(前台)接口")
public class CommentController {

    @Autowired
    private ICommentService iCommentService;

    //课程页面 展示列表
    //个人中心页面展示列表
    //修改评论
    //审核评论

    /**
     * 插入评论
     *
     * @param classComment
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "插入评论", httpMethod = "GET")
    public ServerResponse add(ClassComment classComment) {
        return iCommentService.add(classComment);
    }

    /**
     * 讲师插入评论
     *
     * @param classComment
     * @return
     */
    @RequestMapping(value = "/TeacherAdd.do")
    @ResponseBody
    @ApiOperation(value = "讲师身份插入评论", httpMethod = "GET")
    public ServerResponse TeacherAdd(ClassComment classComment) {
        return iCommentService.TeacherAdd(classComment);
    }

    /**
     * 课程页面 展示列表 审核通过后
     *
     * @param CourseId
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "课程页面 展示列表", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "CourseId", required = false) String CourseId) {
        return iCommentService.list(CourseId);
    }

    /**
     * 评论-后台管理 获取列表，layui格式
     *
     * @param objectId
     * @return
     */
    @RequestMapping(value = "/Admin_list.do")
    @ResponseBody
    @ApiOperation(value = "评论-后台管理 获取列表，layui格式", httpMethod = "GET")
    public ServerResponse getList(@RequestParam(value = "objectId", required = false) String objectId,
                                  @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCommentService.Admin_list(objectId, pageNum, pageSize);
    }

    /**
     * 个人中心页面展示列表
     *
     * @param UserMail
     * @return
     */
    @RequestMapping(value = "/myComment.do")
    @ResponseBody
    @ApiOperation(value = "个人中心页面展示列表", httpMethod = "GET")
    public ServerResponse myComment(String UserMail) {
        return iCommentService.myComment(UserMail);
    }

    /**
     * 修改评论
     *
     * @param classComment
     * @return
     */
    @RequestMapping(value = "/alter.do")
    @ResponseBody
    @ApiOperation(value = "修改评论", httpMethod = "GET")
    public ServerResponse alter(ClassComment classComment) {
        return iCommentService.alter(classComment);
    }

    /**
     * 审核评论
     *
     * @param CommentId
     * @return
     */
    @RequestMapping(value = "/CommentVerify.do")
    @ResponseBody
    @ApiOperation(value = "审核评论", httpMethod = "GET")
    public ServerResponse CommentVerify(String CommentId) {
        return iCommentService.CommentVerify(CommentId);
    }


}
