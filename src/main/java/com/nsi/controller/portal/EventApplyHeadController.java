package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventApplyHead;
import com.nsi.pojo.EventCourse;
import com.nsi.service.IEventApplyHeadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 活动表报名表头
 *
 * @author Li Yan
 * @create 2019-08-16
 **/
@RequestMapping("/EventApplyHead")
@Controller
public class EventApplyHeadController {

    @Autowired
    private IEventApplyHeadService iEventApplyHeadService;


    @RequestMapping("/insert.do")
    @ResponseBody
    public ServerResponse insert(EventApplyHead eventApplyHead) {
        int i = iEventApplyHeadService.insert(eventApplyHead);
        if (i == 1) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }


    @RequestMapping("/update.do")
    @ResponseBody
    public ServerResponse update(EventApplyHead eventApplyHead) {
        int i = iEventApplyHeadService.update(eventApplyHead);
        if (1 == i) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorCodeMessage(500, "操作失败");
    }

    /**
     * 通过eventId查询
     *
     * @param eventId
     * @return
     */
    @RequestMapping("/detailByEventId.do")
    @ResponseBody
    public ServerResponse detailByEventId(Integer eventId) {
        EventApplyHead eventApplyHead = iEventApplyHeadService.detailByEventId(eventId);
        return ServerResponse.createBySuccess("操作成功", eventApplyHead);
    }

}
