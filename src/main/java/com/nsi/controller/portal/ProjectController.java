package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Projects;
import com.nsi.service.IProjectService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/11/26 10:49
 * @description:
 */
@RequestMapping("/project")
@Controller
@Api(value = "/project", description = "官网——项目(前台)管理接口")
public class ProjectController {

    @Autowired
    private IProjectService iProjectService;

    /**
     * 添加项目库方法
     *
     * @param projects
     * @return
     */
    @RequestMapping(value = "/add_project.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加项目库方法", httpMethod = "POST")
    public ServerResponse addSchool(Projects projects) {
        if (StringUtils.isBlank(projects.getSubjectname())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        iProjectService.addProject(projects);
        return ServerResponse.createBySuccess();
    }

    /**
     * 关键字返回详情分页
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "关键字返回详情分页", httpMethod = "GET")
    public ServerResponse getProjectsList(@RequestParam(value = "searchKey", required = false) String searchKey,
                                          @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iProjectService.getProjectList(searchKey, pageNum, pageSize);
    }

    /**
     * 根据Id返回详情
     *
     * @param projectId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id返回详情", httpMethod = "GET")
    public ServerResponse getProjectsById(Integer projectId) {
        Projects projects = iProjectService.selectById(projectId);
        return ServerResponse.createBySuccess(projects);
    }


}
