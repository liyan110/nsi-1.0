package com.nsi.controller.portal;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Institution;
import com.nsi.service.IInstitutionService;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author: Luo Zhen
 * @date: 2018/9/28 11:28
 * @description: 四库全书{机构库}前台管理接口
 */

@RequestMapping("/institution")
@Controller
@Api(value = "/institution", description = "四库全书——机构库(前台)管理接口")
public class InstitutionController {
    @Autowired
    private IInstitutionService iInstitutionService;

    /**
     * 机构库——添加
     *
     * @param institution
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加机构库", httpMethod = "POST")
    public ServerResponse addInstitutionCategory(Institution institution) {
        institution.setLoadTime(new Date());
        institution.setVerifySign(ResponseCode.CHECK.getCode());
        try {
            return iInstitutionService.addInstitution(institution);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 机构库——根据条件返回分页详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "根据条件返回详情带分页", httpMethod = "GET")
    public ServerResponse getInstitutionCategoryList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                     @RequestParam(value = "searchKey", required = false) String searchKey) {
        return iInstitutionService.getList(pageNum, pageSize, searchKey, ResponseCode.PASS.getCode());
    }

    /**
     * 机构库——根据id返回详情
     *
     * @param institutionId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id返回详情", httpMethod = "GET")
    public ServerResponse getInstitutionCategoryById(Integer institutionId) {
        return iInstitutionService.findInstitutionById(institutionId);
    }

    /**
     * 机构库——修改方法
     *
     * @param institution
     * @return list
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse<String> alter(Institution institution) {
        try {
            return iInstitutionService.modifyInstitution(institution);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 机构库——验证机构库名是否存在
     *
     * @param InstitutionName
     * @return
     */
    @RequestMapping(value = "/check_name.do")
    @ResponseBody
    @ApiOperation(value = "验证机构库名是否存在", httpMethod = "GET")
    public ServerResponse checkInstitutionName(String InstitutionName) {
        return iInstitutionService.checkValidetName(InstitutionName);
    }

    /**
     * 机构库——搜索智能提示
     *
     * @param keyword
     * @return
     */
    @RequestMapping(value = "/suggest_search.do")
    @ResponseBody
    @ApiOperation(value = "搜索智能提示", httpMethod = "GET")
    public ServerResponse suggest_search(@RequestParam(value = "keyword", required = false) String keyword) {
        return iInstitutionService.suggest_search(keyword);
    }


}
