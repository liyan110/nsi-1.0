package com.nsi.controller.portal;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventCollect;
import com.nsi.service.EventCollectService;
import com.nsi.vo.EventCollectOrderVo;
import com.wordnik.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 活动用户报名表
 *
 * @author luo Zhen
 * @since 2020-03-23k9
 */
@Controller
@RequestMapping("/event_collect")
@Api(value = "/event_collect", description = "四库全书———-活动报名表(前台)")
public class EventCollectController {

    @Autowired
    private EventCollectService eventCollectService;

    /**
     * 添加用户报名信息
     *
     * @param eventCollect
     * @return
     */
    @RequestMapping(value = "/add_event_collect.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加用户报名信息", httpMethod = "POST")
    public ServerResponse addEventCollect(EventCollect eventCollect) {
        if (StringUtils.isBlank(eventCollect.getName())
                || eventCollect.getActiveId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        eventCollect.setCheckIn(Const.CheckStatusEnum.NOT_SIGN_IN.getCode());
        eventCollectService.insert(eventCollect);
        return ServerResponse.createBySuccess(eventCollect);
    }


    /**
     * 个人中心-根据手机号返回购票详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "List_byPhone.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "个人中心 通过手机号返回报名列表-已支付状态", httpMethod = "GET")
    public ServerResponse List_byPhone(@RequestParam(value = "phone") String phone,
                                       @RequestParam(value = "activeId") Integer activeId,
                                       @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<EventCollectOrderVo> list = eventCollectService.List_byPhone(phone, activeId);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
