package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseCategory;
import com.nsi.pojo.CourseList;
import com.nsi.service.ICourseCategoryService;
import com.nsi.service.ICourseListService;
import com.nsi.vo.CourseCategoryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 课程详情表
 *
 * @author Luo Zhen
 * @create 2019-02-13 11:45
 */
@Slf4j
@RequestMapping("/category")
@Controller
@Api(value = "/category", description = "官网——录播课程详情(前台)管理接口")
public class CourseCategoryController {

    @Autowired
    private ICourseCategoryService iCourseCategoryService;
    @Autowired
    private ICourseListService iCourseListService;

    /**
     * 返回详情
     *
     * @param listId
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse getCourseListItem(@RequestParam(value = "listId") Integer listId) {
        List<CourseCategory> courseCategories = null;
        try {
            courseCategories = iCourseCategoryService.findCourseCategoryByListId(listId);
            CourseList course = iCourseListService.findCourseListById(listId);
            CourseCategoryVo categoryVo = new CourseCategoryVo();
            categoryVo.setDescription(course.getListDescription());
            categoryVo.setCourseLists(courseCategories);
            return ServerResponse.createBySuccess(categoryVo);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }


}
