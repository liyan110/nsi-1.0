package com.nsi.controller.portal;

/**
 * @author Li Yan
 * @create 2019-08-14
 **/

import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventCourse;
import com.nsi.service.IEventCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 活动中的课程内容
 *
 * @author Luo Zhen
 * @create 2019-02-12 16:32
 */
@RequestMapping("/EventCourse")
@Controller
@Api(value = "/EventCourse", description = "活动—活动中的课程接口")
public class EventCourseController {

    @Autowired
    private IEventCourseService iEventCourseService;

    /**
     * 插入活动课程内容
     *
     * @param eventCourse
     * @return
     */
    @RequestMapping("/insert.do")
    @ResponseBody
    @ApiOperation(value = "插入活动课程内容", httpMethod = "GET")
    public ServerResponse insert(EventCourse eventCourse) {
        int i = iEventCourseService.insert(eventCourse);
        if (1 == i) {
            return ServerResponse.createBySuccess("插入成功");
        }
        return ServerResponse.createByErrorCodeMessage(500, "插入失败");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/del.do")
    @ResponseBody
    @ApiOperation(value = "通过id删除", httpMethod = "GET")
    public ServerResponse del(int id) {
        int i = iEventCourseService.del(id);
        if (1 == i) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorCodeMessage(500, "操作失败");
    }

    /**
     * 修改操作
     *
     * @param eventCourse
     * @return
     */
    @RequestMapping("/update.do")
    @ResponseBody
    @ApiOperation(value = "修改操作", httpMethod = "GET")
    public ServerResponse update(EventCourse eventCourse) {
        int i = iEventCourseService.update(eventCourse);
        if (1 == i) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorCodeMessage(500, "操作失败");
    }

    /**
     * 通过eventId查询
     *
     * @param EventId
     * @return
     */
    @RequestMapping("/listByEventId.do")
    @ResponseBody
    @ApiOperation(value = "通过eventId查询", httpMethod = "GET")
    public ServerResponse listByEventId(Integer EventId) {
        List<EventCourse> list = iEventCourseService.listByEventId(EventId);
        return ServerResponse.createBySuccess("操作成功", list);
    }


}
