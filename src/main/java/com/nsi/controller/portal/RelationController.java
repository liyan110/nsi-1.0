package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.service.IRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2019-05-21
 **/
@RequestMapping("/relation")
@Controller
@Api(value = "/relation", description = "四库全书-数据关联")
public class RelationController {

    @Autowired
    private IRelationService iRelationService;

    /**
     * 查询关系
     *
     * @param searchId
     * @return
     */
    @RequestMapping(value = "/search.do")
    @ResponseBody
    @ApiOperation(value = "查询关系", httpMethod = "GET")
    public ServerResponse search(@RequestParam(value = "type", required = false) String type,
                                 @RequestParam(value = "searchId", required = false) String searchId) {
        return iRelationService.search(type, searchId);
    }

}
