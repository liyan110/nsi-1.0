package com.nsi.controller.portal;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseList;
import com.nsi.service.ICourseListService;
import com.nsi.vo.CourseListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 课程合集
 *
 * @author Luo Zhen
 * @create 2019-02-12 16:32
 */
@RequestMapping("/courseList")
@Controller
@Api(value = "/courseList", description = "官网——课程合集(前台)管理接口")
public class CourseListController {

    @Autowired
    private ICourseListService iCourseListService;

    /**
     * 返回详情分页
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/get_course_list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情分页", httpMethod = "GET")
    public ServerResponse getCourseListItem(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<CourseList> courseLists = null;
        try {
            courseLists = iCourseListService.findAll(String.valueOf(ResponseCode.SUCCESS.getCode()));
            PageInfo pageInfo = new PageInfo(courseLists);
            return ServerResponse.createBySuccess(pageInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 根据Id返回详情
     *
     * @param listId
     * @return
     */
    @RequestMapping("/get_course_item.do")
    @ResponseBody
    @ApiOperation(value = "根据Id返回详情", httpMethod = "GET")
    public ServerResponse getCourseByListId(@RequestParam("listId") Integer listId) {
        try {
            CourseList course = iCourseListService.findCourseListById(listId);
            CourseListVo courseListVo = new CourseListVo();

            BeanUtils.copyProperties(course,courseListVo);
            return ServerResponse.createBySuccess(courseListVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }


}
