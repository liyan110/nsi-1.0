package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.MyCollect;
import com.nsi.service.MyCollectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.server.standard.ServerEndpointRegistration;

import java.awt.print.Pageable;

/**
 * <p>
 * 四库全书-我的收藏表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-09-02
 */
@Controller
@RequestMapping("/myCollect")
public class MyCollectController {

    @Autowired
    MyCollectService collectService;

    /**
     * 验证是否收藏
     *
     * @param openId
     * @param cId
     * @param collectType
     * @return
     */
    @RequestMapping(value = "/check.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse addCollect(@RequestParam("openId") String openId,
                                     @RequestParam("cId") Integer cId,
                                     @RequestParam("collectType") Integer collectType) {
        if (StringUtils.isBlank(openId)) {
            return ServerResponse.createByErrorMessage("用户标识不能为空");
        }
        if (cId == null || collectType == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        int result = collectService.selectCount(
                new EntityWrapper<MyCollect>()
                        .eq("open_id", openId)
                        .eq("c_id", cId)
                        .eq("collect_type", collectType));
        if (result > 0) {
            return ServerResponse.createByErrorMessage("已收藏！");
        }
        return ServerResponse.createBySuccessMessage("未收藏");
    }

    /**
     * 收藏
     *
     * @param collect
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse addCollect(MyCollect collect) {
        if (StringUtils.isBlank(collect.getOpenId())) {
            return ServerResponse.createByErrorMessage("用户标识不能为空");
        }
        if (collect.getCId() == null || collect.getCollectType() == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        collectService.insert(collect);
        return ServerResponse.createBySuccessMessage("收藏成功！");
    }

    /**
     * 取消收藏
     *
     * @param cId
     * @return
     */
    @RequestMapping(value = "/remove.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse addCollect(@RequestParam("cId") Integer cId,
                                     @RequestParam("openId") String openId,
                                     @RequestParam("collectType") String collectType) {
        if (cId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        if (StringUtils.isBlank(openId) || StringUtils.isBlank(collectType)) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        collectService.delete(new EntityWrapper<MyCollect>()
                .eq("c_id", cId)
                .eq("open_id", openId)
                .eq("collect_type", collectType));
        return ServerResponse.createBySuccessMessage("删除成功！");
    }

    /**
     * 我的页面-取消收藏
     *
     * @param collectId
     * @return
     */
    @RequestMapping(value = "/my_collect_remove.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse addCollect(@RequestParam("collectId") Integer collectId) {
        if (collectId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        collectService.deleteById(collectId);
        return ServerResponse.createBySuccessMessage("删除成功！");
    }


    /**
     * 收藏列表
     *
     * @param pageNum
     * @param pageSize
     * @param openId
     * @param collectType
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse myCollectList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                        @RequestParam(value = "openId") String openId,
                                        @RequestParam(value = "collectType") Integer collectType) {
        return collectService.findCollectList(pageNum, pageSize, openId, collectType);
    }

}
