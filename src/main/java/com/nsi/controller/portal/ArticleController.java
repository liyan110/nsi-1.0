package com.nsi.controller.portal;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Advertisement;
import com.nsi.service.IArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 */
@RequestMapping("/article")
@Controller
@Api(value = "/article", description = "官网——文章(前台)管理接口")
@Slf4j
public class ArticleController {


    @Autowired
    IArticleService iArticleService;


    /**
     * 根据Id返回详细信息
     *
     * @param articleId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详细信息", httpMethod = "GET")
    public ServerResponse detail(Integer articleId) {
        try {
            return iArticleService.detailOn(articleId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 前台搜索返回list
     *
     * @param pageNum
     * @param pageSize
     * @return4
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "获取list方法", httpMethod = "GET")
    public ServerResponse<PageInfo> list(@RequestParam(value = "siftType", defaultValue = "", required = false) String siftType,
                                         @RequestParam(value = "articleCat", defaultValue = "", required = false) String articleCat,
                                         @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        try {
            return iArticleService.list(siftType, articleCat, pageNum, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 关键字搜索文章列表
     *
     * @return
     */
    @RequestMapping(value = "/find_article_list.do")
    @ResponseBody
    @ApiOperation(value = "关键字搜索文章列表", httpMethod = "GET")
    public ServerResponse findArticleList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                          @RequestParam(value = "searchKey", required = false) String searchKey) {
        try {
            return iArticleService.findArticleList(pageNum, pageSize, searchKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 文章右侧广告接口-获取广告信息
     *
     * @param typeName
     * @return
     */
    @RequestMapping(value = "/getArticleAd.do")
    @ResponseBody
    @ApiOperation(value = "获取广告信息", httpMethod = "GET")
    public ServerResponse getArticleAd(@RequestParam(value = "typeName", defaultValue = "") String typeName) {
        try {
            return iArticleService.getArticleAd(typeName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 文章右侧广告接口-发送广告信息
     *
     * @param advertisement
     * @return
     */
    @RequestMapping(value = "/setArticleAd.do")
    @ResponseBody
    @ApiOperation(value = "设置广告信息", httpMethod = "GET")
    public ServerResponse setArticleAd(Advertisement advertisement) {
        return iArticleService.setArticleAd(advertisement);
    }

    /**
     * 返回广告标签
     *
     * @return
     */
    @RequestMapping(value = "/query_article_list.do")
    @ResponseBody
    @ApiOperation(value = "返回广告标签", httpMethod = "GET")
    public ServerResponse getArticleList() {
        return iArticleService.queryByArticle();
    }

    /**
     * 根据关键字查询文章标题
     *
     * @param keyword
     * @return
     */
    @RequestMapping(value = "/find_article_name.do")
    @ResponseBody
    @ApiOperation(value = "根据关键字查询文章标题", httpMethod = "GET")
    public ServerResponse findArticles(@RequestParam("keyword") String keyword) {
        return iArticleService.findArticleName(keyword);
    }

}
