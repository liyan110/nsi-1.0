package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ActivityTickets;
import com.nsi.service.ActivityTicketsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author luo Zhen
 * @since 2020-03-26
 */
@Controller
@RequestMapping("/activity_tickets")
@Api(value = "/activity_tickets", description = "官网——活动门票(前台)管理接口")
public class ActivityTicketsController {

    @Autowired
    private ActivityTicketsService activityTicketsService;

    /**
     * 根据活动Id 返回票列表
     *
     * @param activeId
     * @return
     */
    @RequestMapping(value = "/find_item.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "/根据活动Id返回票详情", httpMethod = "GET")
    public ServerResponse findOne(Integer activeId) {
        if (activeId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        List<ActivityTickets> tickets = activityTicketsService.selectList(new EntityWrapper<ActivityTickets>()
                .eq("active_id", activeId));
        return ServerResponse.createBySuccess(tickets);
    }

}
