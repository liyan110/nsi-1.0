package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.service.SchoolStaService;
import com.nsi.vo.SchoolTeacherStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学校库统计
 *
 * @author Luo Zhen
 */
@RequestMapping("/school/stat")
@Controller
public class SchoolStatisticsController {

    @Autowired
    SchoolStaService schoolStaService;

    /**
     * 统计总数(正常/倒闭/非正常/学校总数/教师总数量/外籍教师总数量/在校生总数/学位总数)
     *
     * @return
     */
    @RequestMapping("count.do")
    @ResponseBody
    public ServerResponse count() {
        // 返回学校各状态统计
        List<Map<String, Object>> resultMap =
                schoolStaService.getOperatorStateCount();
        // 返回学校各教师数量统计
        List<SchoolTeacherStatistics> schoolTeacherSta = schoolStaService.getSchoolTeacherSta();

        Map<String, Object> response = new HashMap<>();
        response.put("schoolSta", resultMap);
        response.put("teacherSta", schoolTeacherSta);
        return ServerResponse.createBySuccess("返回成功", response);
    }


    //---------------------------------学校统计分布-------------------------------------

    /**
     * 返回所有省份列表
     *
     * @return
     */
    @RequestMapping("all_province.do")
    @ResponseBody
    public ServerResponse allProvince() {
        List<String> provinces = schoolStaService.getAllProvince();
        return ServerResponse.createBySuccess(provinces);
    }

    /**
     * 学校属性占比
     */
    @RequestMapping("school_ratio.do")
    @ResponseBody
    public ServerResponse schoolRatio() {
        List<Map<String, Integer>> resultMap = schoolStaService.getSchoolRatio();
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 返回所有学校认证数
     *
     * @return
     */
    @RequestMapping("all_school_ratio.do")
    @ResponseBody
    public ServerResponse allSchoolRatio() {
        Map<String, Object> resultMap = schoolStaService.getAllSchoolRatio();
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 不同学段的统计数目
     *
     * @return
     */
    @RequestMapping("no_student_section.do")
    @ResponseBody
    public ServerResponse noStudentSection() {
        Map<String, Object> resultMap = schoolStaService.getStudentSection();
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 十年内各阶段学校增长数目
     */
    @RequestMapping("get_ten_year_num.do")
    @ResponseBody
    public ServerResponse getTenYearsSchoolNumber(@RequestParam(value = "time", defaultValue = "2012") Integer time) {
        Map<String, Object> resultMap = schoolStaService.getTenYearSchoolNum(time);
        return ServerResponse.createBySuccess(resultMap);
    }

    //---------------------------------课程统计分布-------------------------------------

    /**
     * 课程市场占有率
     *
     * @return
     */
    @RequestMapping("course_ratio.do")
    @ResponseBody
    public ServerResponse courseRatio() {
        List<Map<String, Object>> resultMap = schoolStaService.getCourseRatio();
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 学校分类课程百分比
     *
     * @return
     */
    @RequestMapping("course_percent.do")
    @ResponseBody
    public ServerResponse coursePercent(@RequestParam(defaultValue = "", required = false) String province) {
        Map<String, Object> resultMap = schoolStaService.getCoursePercent(province);
        return ServerResponse.createBySuccess(resultMap);
    }

//---------------------------------学费统计分布-------------------------------------

    /**
     * 各省幼/小/初/高平均学费统计
     *
     * @return
     */
    @RequestMapping("average_tuition.do")
    @ResponseBody
    public ServerResponse averageTuition() {
        Map<String, Object> resultMap = schoolStaService.getAverageTuition();
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 不同城市各阶段平均学费
     *
     * @param province
     * @return
     */
    @RequestMapping("average_provice.do")
    @ResponseBody
    public ServerResponse averageTuitionByProvice(@RequestParam(value = "", required = false) String province) {
        Map<String, Object> resutMap = schoolStaService.getAverageTuitionByProvince(province);
        return ServerResponse.createBySuccess(resutMap);
    }

    /**
     * 不同城市个认证机构学校数量
     *
     * @param province
     * @return
     */
    @RequestMapping("school_certification.do")
    @ResponseBody
    public ServerResponse schoolCertification(@RequestParam(value = "", required = false)
                                                      String province) {
        Map<String, Object> resutMap = schoolStaService.getSchoolCertification(province);
        return ServerResponse.createBySuccess(resutMap);
    }


}
