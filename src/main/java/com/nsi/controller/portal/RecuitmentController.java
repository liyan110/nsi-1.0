package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Recuitment;
import com.nsi.service.IRecuitmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/11/23 10:41
 * @description: 人才招聘
 */
@RequestMapping("/recuitment")
@Controller
@Api(value = "/recuitment", description = "四库全书————人才招聘(前台)管理接口")
public class RecuitmentController {

    @Autowired
    private IRecuitmentService iRecuitmentService;

    /**
     * 添加人才招聘信息
     *
     * @param recuitment
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addRecuitment(Recuitment recuitment) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRecuitmentService.addRecuitment(recuitment);
    }
}
