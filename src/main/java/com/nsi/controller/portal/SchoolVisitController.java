package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.SchoolVisit;
import com.nsi.service.IVisitSchoolService;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-07-25 11:40
 */
@RequestMapping("/visit/school/")
@Controller
@Api(value = "/visit/school/", description = "四库全书————申请访校库(前台)最新管理接口")
public class SchoolVisitController {

    @Autowired
    private IVisitSchoolService iVisitSchoolService;

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findALl(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                  @RequestParam(value = "schoolId", required = false, defaultValue = "") Integer schoolId,
                                  @RequestParam(value = "openId", required = false) String openId) {
        return iVisitSchoolService.findAll(pageNum, pageSize, schoolId, openId);
    }

    @RequestMapping(value = "save.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse save(SchoolVisit schoolVisit) {
        if (StringUtils.isBlank(schoolVisit.getName())) {
            return ServerResponse.createByErrorMessage("名字不能为空");
        }
        if (null == schoolVisit.getSchoolId()) {
            return ServerResponse.createByErrorMessage("学校id不能为空");
        }
        iVisitSchoolService.insert(schoolVisit);
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(SchoolVisit schoolVisit) {
        if (StringUtils.isBlank(schoolVisit.getName()) || StringUtils.isBlank(schoolVisit.getTelphone())) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        iVisitSchoolService.updateById(schoolVisit);
        return ServerResponse.createBySuccessMessage("修改成功");
    }


}
