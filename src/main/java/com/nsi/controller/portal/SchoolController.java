package com.nsi.controller.portal;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.School;
import com.nsi.pojo.SchoolCertification;
import com.nsi.service.ISchoolService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Luo Zhen
 * @create 2018-08-30
 */
@RequestMapping("/school")
@Controller
@Api(value = "/school", description = "四库全书————学校库(前台)管理接口")
public class SchoolController {


    @Autowired
    private ISchoolService iSchoolService;

    @RequestMapping(value = "/item/{schoolId}")
    @ApiOperation(value = "根据id返回页面", httpMethod = "GET")
    public String getSchoolInfo(@PathVariable Integer schoolId, HttpServletRequest request) {
        ServerResponse response = iSchoolService.getDetail(schoolId);
        School school = (School) response.getData();
        request.setAttribute("school", school);
        return "school";
    }


    /**
     * 添加学校信息操作
     *
     * @param school
     * @return
     */
    @RequestMapping(value = "/addSchool.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详情", httpMethod = "POST")
    public ServerResponse addSchool(School school, SchoolCertification certification) {
        school.setVerifysign(String.valueOf(ResponseCode.CHECK.getCode()));
        return iSchoolService.addSchoolInfo(school, certification);
    }

    /**
     * 根据Id获取详情
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详情", httpMethod = "GET")
    public ServerResponse detail(Integer schoolId) {
        return iSchoolService.getDetail(schoolId);
    }

    /**
     * 搜索功能
     *
     * @param keyword
     * @return
     */
    @RequestMapping("/suggest_search.do")
    @ResponseBody
    @ApiOperation(value = "智能搜索功能", httpMethod = "GET")
    public ServerResponse suggestSearch(@RequestParam(value = "keyword", required = false) String keyword) {
        return iSchoolService.getSchoolName(keyword);
    }

    /**
     * 学校库 高级搜索(第一页)
     *
     * @param properties
     * @param area
     * @param system
     * @param course
     * @param fondTime
     * @param pageSize
     * @return
     */
    @RequestMapping("/power_search.do")
    @ResponseBody
    @ApiOperation(value = "高级搜索第一页", httpMethod = "GET")
    public ServerResponse<PageInfo> getSchoolPowerSearch(@RequestParam(value = "properties", required = false) String[] properties,
                                                         @RequestParam(value = "area", required = false) String[] area,
                                                         @RequestParam(value = "system", required = false) String[] system,
                                                         @RequestParam(value = "course", required = false) String[] course,
                                                         @RequestParam(value = "fondTime", required = false) String fondTime,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iSchoolService.searchPower(properties, area, system, course, fondTime, pageNum, pageSize);
    }

    /**
     * 学校库 高级搜索(第二页)
     *
     * @param area
     * @param system
     * @param properties
     * @param authority
     * @param evaluation
     * @param organization
     * @param course
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/power_search_list.do")
    @ResponseBody
    @ApiOperation(value = "高级搜索第二页", httpMethod = "GET")
    public ServerResponse<PageInfo> getSchoolPowerSearch(@RequestParam(value = "area", required = false) String[] area,
                                                         @RequestParam(value = "system", required = false) String[] system,
                                                         @RequestParam(value = "properties", required = false) String[] properties,
                                                         @RequestParam(value = "authority", required = false) String[] authority,
                                                         @RequestParam(value = "evaluation", required = false) String[] evaluation,
                                                         @RequestParam(value = "organization", required = false) String[] organization,
                                                         @RequestParam(value = "course", required = false) String[] course,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iSchoolService.searchPowerList(area, system, properties, authority, evaluation, organization, course, pageNum, pageSize);
    }


    /**
     * 返回list
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */

    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回学校list", httpMethod = "GET")
    public ServerResponse getSchoolInfo(@RequestParam(value = "searchKey", required = false) String searchKey,
                                        @RequestParam(value = "orderBy", required = false, defaultValue = "") String orderBy,
                                        @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iSchoolService.searchList(searchKey, orderBy, pageNum, pageSize, String.valueOf(ResponseCode.PASS.getCode()));
    }

    /**
     * 测试加密返回list
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/test_list.do")
    @ResponseBody
    @ApiOperation(value = "返回学校list", httpMethod = "GET")
    public ServerResponse test_list(@RequestParam(value = "searchKey", required = false) String searchKey,
                                    @RequestParam(value = "orderBy", required = false, defaultValue = "") String orderBy,
                                    @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                    @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iSchoolService.searchList(searchKey, orderBy, pageNum, pageSize, String.valueOf(ResponseCode.PASS.getCode()));
    }

    /**
     * 展示学校库广告位
     *
     * @param keyword
     * @return
     */
    @RequestMapping("/get_school_boards.do")
    @ResponseBody
    @ApiOperation(value = "展示学校库广告位", httpMethod = "GET")
    public ServerResponse getSchoolBoards(@RequestParam(value = "keyword") String keyword) {
        return iSchoolService.showSchoolBoards(keyword);
    }

    /**
     * 验证学校名是否重复
     *
     * @param schoolName
     * @return
     */
    @RequestMapping("/check_valid.do")
    @ResponseBody
    @ApiOperation(value = "验证学校名是否重复", httpMethod = "GET")
    public ServerResponse checkValid(@RequestParam(value = "schoolName") String schoolName) {
        return iSchoolService.checkValidedByName(schoolName);
    }

    /**
     * 学校库可视化——省份学校数量查询
     *
     * @return
     */
    @RequestMapping("/echart_school_num.do")
    @ResponseBody
    @ApiOperation(value = "学校库可视化——省份学校数量查询", httpMethod = "GET")
    public ServerResponse getEchartSchoolNum(@RequestParam(value = "option", defaultValue = "", required = false) String option) {
        return iSchoolService.getSchoolNum(option);
    }


}
