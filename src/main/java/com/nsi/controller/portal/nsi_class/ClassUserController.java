package com.nsi.controller.portal.nsi_class;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Course_user;
import com.nsi.pojo.User;
import com.nsi.service.IClassUserService;
import com.nsi.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2018-10-26
 **/
@Controller
@RequestMapping("/ClassUser")
@Api(value = "/ClassUser", description = "在线课堂—课程用户 权限管理接口")
public class ClassUserController {

    @Autowired
    private IClassUserService iClassUserService;
    @Autowired
    private IUserService iUserService;

    /**
     * 插入课程观看权限
     *
     * @param ClassId
     * @param UserMail
     * @return
     */
    @RequestMapping(value = "/Course_User_Insert.do")
    @ResponseBody
    @ApiOperation(value = "插入课程观看权限", httpMethod = "GET")
    public ServerResponse Course_User_Insert(@RequestParam(value = "ClassId") String ClassId,
                                             @RequestParam(value = "UserMail") String UserMail) {
        System.out.println("插入课程观看权限:"+ClassId+":"+UserMail);
        return iClassUserService.saveCourseUser(ClassId, UserMail);
    }


    /**
     * 教育研究院插入课程观看权限
     */
    @RequestMapping(value = "/shopCourse_User_Insert.do")
    @ResponseBody
    @ApiOperation(value = "插入课程观看权限", httpMethod = "GET")
    public ServerResponse shopCourse_User_Insert(@RequestParam(value = "ClassId") String ClassId,
                                             @RequestParam(value = "wechatId") String wechatId) {
        System.out.println("shopCourse_User_Insert插入课程观看权限:"+ClassId+":"+wechatId);
        return iClassUserService.shopSaveCourseUser(ClassId, wechatId);
    }

    /**
     * 验证课程观看权限
     *
     * @param classId
     * @param usermail
     * @return
     */
    @RequestMapping(value = "/vierify_open.do")
    @ResponseBody
    @ApiOperation(value = "验证课程观看权限", httpMethod = "GET")
    public ServerResponse verifyCourseOpen(@RequestParam(value = "classId") String classId,
                                           @RequestParam(value = "usermail") String usermail) {
        if (StringUtils.isEmpty(classId) || StringUtils.isEmpty(usermail)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            Course_user user = iClassUserService.findCourseUserByClassidAndUsermail(classId, usermail);
            if (user != null) {
                return ServerResponse.createBySuccess();
            }
            return ServerResponse.createByError();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 验证课程观看权限根据weichatId
     *
     * @param classId
     * @param wechatId
     * @return
     */
    @RequestMapping(value = "/verify_open_by_wechatid.do")
    @ResponseBody
    @ApiOperation(value = "验证课程观看权限根据weichatId", httpMethod = "GET")
    public ServerResponse verifyCourseOpenByWechatId(@RequestParam(value = "classId") String classId,
                                                     @RequestParam(value = "wechatId") String wechatId) {
        if (StringUtils.isEmpty(classId) || StringUtils.isEmpty(wechatId)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            User user = iUserService.findUserByWechatId(wechatId);
            if (user == null) {
                return ServerResponse.createByError();
            }
            Course_user courseInfo = iClassUserService.findCourseUserByClassidAndUsermail(classId, user.getUsername());
            if (courseInfo != null) {
                return ServerResponse.createBySuccess();
            }
            return ServerResponse.createByErrorMessage("该用户无观看权限");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

}
