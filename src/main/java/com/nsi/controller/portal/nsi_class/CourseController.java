package com.nsi.controller.portal.nsi_class;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Course;
import com.nsi.service.ICourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Li Yan
 * @create 2018-10-18
 **/


@Controller
@RequestMapping("/Course")
@Api(value = "/Course", description = "在线课堂—课程管理(前台)接口")
public class CourseController {

    @Autowired
    private ICourseService iCourseService;


//    废弃的接口：
//    Course_InsertHtml
//    Course_GetHtml


    //    返回课程列表
    @RequestMapping(value = "/Course_list.do")
    @ResponseBody
    @ApiOperation(value = "返回课程列表", httpMethod = "GET")
    public ServerResponse Course_list(@RequestParam(value = "Id", defaultValue = "", required = false) String Id,
                                      @RequestParam(value = "CourseSubject", defaultValue = "", required = false) String CourseSubject) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.Course_list(Id, CourseSubject);
    }


    //课程主题？问题
    // 返回我的课程列表
    @RequestMapping(value = "/MyCourse.do")
    @ResponseBody
    @ApiOperation(value = "返回我的课程列表", httpMethod = "GET")
    public ServerResponse MyCourse(@RequestParam(value = "UserMail") String UserMail) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.MyCourse(UserMail);
    }

    //    后台
    // 插入课程
    @RequestMapping(value = "/Insert_Course.do")
    @ResponseBody
    @ApiOperation(value = "插入课程", httpMethod = "GET")
    public ServerResponse Insert_Course(Course Course) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.Insert_Course(Course);
    }

    // 修改课程
    @RequestMapping(value = "/alter_Course.do")
    @ResponseBody
    @ApiOperation(value = "修改课程", httpMethod = "GET")
    public ServerResponse alter_Course(Course course) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.alter_Course(course);
    }

    // 删除课程
    @RequestMapping(value = "/Delete_Course.do")
    @ResponseBody
    @ApiOperation(value = "删除课程", httpMethod = "GET")
    public ServerResponse Delete_Course(@RequestParam(value = "Class_Id") int Class_Id) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.Delete_Course(Class_Id);
    }

    // 后台 课程列表
    @RequestMapping(value = "/Admin_Course_list.do")
    @ResponseBody
    @ApiOperation(value = "后台 课程列表", httpMethod = "GET")
    public ServerResponse Admin_Course_list() {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.Admin_Course_list();
    }


//  直播 配置
//    LiveCourseSet
//    ShowLiveCourse

    // 直播课程配置
    @RequestMapping(value = "/LiveCourseSet.do")
    @ResponseBody
    @ApiOperation(value = "直播课程配置", httpMethod = "GET")
    public ServerResponse LiveCourseSet(@RequestParam(value = "TeacherId") String TeacherId,
                                        @RequestParam(value = "CourseId") String CourseId) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.LiveCourseSet(TeacherId, CourseId);
    }

    // 查看课程配置
    @RequestMapping(value = "/ShowLiveCourse.do")
    @ResponseBody
    @ApiOperation(value = "查看课程配置", httpMethod = "GET")
    public Map<String, Object> ShowLiveCourse() {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.ShowLiveCourse();
    }


//    查看用户权限
//    AdminSearch_Course_User

    // 查看课程配置
    @RequestMapping(value = "/AdminSearch_Course_User.do")
    @ResponseBody
    @ApiOperation(value = "查看用户权限", httpMethod = "GET")
    public ServerResponse AdminSearch_Course_User(@RequestParam(value = "UserMail") String UserMail,
                                                  @RequestParam(value = "ClassId") String ClassId,
                                                  @RequestParam(value = "pageNum") String pageNum,
                                                  @RequestParam(value = "OnePageNum") String OnePageNum
    ) {
        //DynamicDataSourceHolder.setDataSource("dataSource2");
        return iCourseService.AdminSearch_Course_User(UserMail, ClassId, pageNum, OnePageNum);
    }


//    MyCourse
//    Insert_Course
//    alter_Course
//    Delete_Course
//    Admin_Search_Course

//         直播配置-相关
//    原stickyInformation 设置当前直播功能
//    原showInformation  显示当前直播配置

//    查看课程 用户观看权限
//    原AdminSearch_Course_User


}
