package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventCourse;
import com.nsi.pojo.ExperienceUser;
import com.nsi.service.ExperienceUserService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 申请用户体验功能
 * </p>
 *
 * @author luo Zhen
 * @since 2021-03-17
 */
@Controller
@RequestMapping("/experience")
public class ExperienceUserController {

    @Autowired
    ExperienceUserService experienceUserService;

    /**
     * 添加功能
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/save.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse insert(ExperienceUser user) {
        if (StringUtils.isBlank(user.getUsername())
                || StringUtils.isBlank(user.getTelphone())) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        int result = experienceUserService.selectCount(
                new EntityWrapper<ExperienceUser>()
                        .eq("telphone", user.getTelphone()));
        if (result > 0) {
            return ServerResponse.createByErrorMessage("已提交申请，请勿重复提交");
        }
        user.setIsRelation(Const.NUM_DEFAULT_VALUE);
        experienceUserService.insert(user);
        return ServerResponse.createBySuccessMessage("申请成功！稍后会与您取得联系");
    }



}
