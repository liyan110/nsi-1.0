package com.nsi.controller.portal;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.service.ISchoolEighteenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-04-09 10:53
 */
@RequestMapping("/schoolEighteen")
@Controller
@Api(value = "/schoolEighteen", description = "四库全书————学校库(前台2018年)管理接口")
public class SchoolEighteenController {


    @Autowired
    private ISchoolEighteenService iSchoolEighteenService;

    /**
     * 返回list
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回学校list", httpMethod = "GET")
    public ServerResponse getSchoolInfo(@RequestParam(value = "searchKey", required = false) String searchKey,
                                        @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        try {
            return iSchoolEighteenService.searchList(searchKey, pageNum, pageSize, String.valueOf(ResponseCode.PASS.getCode()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }


    /**
     * 根据Id获取详情
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详情", httpMethod = "GET")
    public ServerResponse detail(Integer schoolId) {

        try {
            return iSchoolEighteenService.getDetail(schoolId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

}
