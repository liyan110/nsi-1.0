package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.enums.CheckStateEnum;
import com.nsi.pojo.NewTalent;
import com.nsi.service.NewTalentService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-11-26
 */
@Controller
@RequestMapping("/newTalent")
@Api(value = "/newTalent", description = "四库全书————新人才库(前台)最新管理接口")
public class TalentNewController {

    @Autowired
    private NewTalentService newTalentService;

    /**
     * 添加方法
     *
     * @param newTalent
     * @return
     */
    @RequestMapping(value = "add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "新增方法", httpMethod = "POST")
    public ServerResponse create(NewTalent newTalent) {
        newTalent.setIsCheck(0);
        newTalent.setCreateTime(new Date());
        newTalent.setUpdateTime(newTalent.getCreateTime());
        newTalentService.insert(newTalent);
        return ServerResponse.createBySuccess(newTalent.getId());
    }

    /**
     * 更新接口
     *
     * @param newTalent
     * @return
     */
    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "更新方法", httpMethod = "POST")
    public ServerResponse update(NewTalent newTalent) {
        newTalent.setUpdateTime(new Date());
        newTalentService.updateById(newTalent);
        return ServerResponse.createBySuccess();
    }

    /**
     * 根据邮箱验证是否上传简历
     *
     * @param userMail
     * @return
     */
    @RequestMapping(value = "find_talent_info.do")
    @ResponseBody
    @ApiOperation(value = "根据邮箱验证是否上传简历", httpMethod = "GET")
    public ServerResponse findTalentInfoByUserMail(String userMail) {
        if (StringUtils.isBlank(userMail)) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        NewTalent talent = newTalentService.selectOne(
                new EntityWrapper<NewTalent>()
                        .eq("user_mail", userMail)
        );
        if (talent != null) {
            return ServerResponse.createBySuccess(talent);
        }
        return ServerResponse.createByErrorMessage("该用户不存在!");

    }

    /**
     * 人才列表
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "详情列表", httpMethod = "GET")
    public ServerResponse list(@RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize,
                               @RequestParam(required = false) String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<NewTalent> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(searchKey)) {
            wrapper.like("username", searchKey)
                    .or()
                    .like("profession", searchKey)
                    .or()
                    .like("position", searchKey)
                    .or()
                    .like("work_experience", searchKey);
        }
        wrapper.eq("is_public", CheckStateEnum.IN_APPROVE.getCode());
        wrapper.eq("is_check", CheckStateEnum.IN_APPROVE.getCode());
        wrapper.orderBy("id", false);

        List<NewTalent> talentList = newTalentService.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(talentList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 根据提交人邮箱，返回添加简历列表
     *
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "search.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据提交人返回详情", httpMethod = "GET")
    public ServerResponse query(String searchKey) {
        return newTalentService.queryList(searchKey);
    }

    /**
     * 根据id返回人才详情
     *
     * @param talentId
     * @return
     */
    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id返回人才详情", httpMethod = "GET")
    public ServerResponse findById(Integer talentId) {
        NewTalent talnet = newTalentService.selectById(talentId);
        return ServerResponse.createBySuccess(talnet);
    }

}
