package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.service.IPreviousArticlesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2018-07-30
 * 官网旧文章接口
 **/
@RequestMapping("/previousArticles")
@Controller
@Api(value = "/previousArticles", description = "官网——旧文章(前台)管理接口")
public class PreviousArticlesController {

    @Autowired
    private IPreviousArticlesService iPreviousArticlesService;

    /**
     * 根据分页，返回详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "根据分页，返回详情", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iPreviousArticlesService.getArticleList(pageNum, pageSize);
    }

    /**
     * 根据id,返回详情
     *
     * @param oldArticleId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id,返回详情", httpMethod = "GET")
    public ServerResponse detail(Integer oldArticleId) {
        return iPreviousArticlesService.getAllById(oldArticleId);
    }


}
