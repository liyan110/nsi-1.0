package com.nsi.controller.portal;

import com.nsi.common.Const;
import com.nsi.util.RedisUtils;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Activity;
import com.nsi.pojo.Vis2019;
import com.nsi.service.IActivityService;
import com.nsi.util.MailUtil;
import com.nsi.util.PropertiesUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author Luo Zhen
 */
@Slf4j
@Controller
@RequestMapping("/activity")
@Api(value = "/activity", description = "活动-(前台)接口")
public class ActivityController {

    @Autowired
    private IActivityService iActivityService;

    /**
     * 图片活动
     *
     * @return
     */
    @RequestMapping("/get_image_url.do")
    @ResponseBody
    @ApiOperation(value = "返回图片活动地址", httpMethod = "GET")
    public ServerResponse getImageUrl(@RequestParam(value = "nickName") String nickName, Integer mark) {
        return iActivityService.resultImageUrl(nickName, mark);
    }

    /**
     * 返回 vis图片合成地址
     *
     * @param qrImgUrl
     * @param username
     * @return
     */
    @RequestMapping("/get_vis_image_url.do")
    @ResponseBody
    @ApiOperation(value = "返回vis门票图片地址", httpMethod = "GET")
    public ServerResponse getVISImageUrl(@RequestParam(value = "qrImgUrl") String qrImgUrl,
                                         @RequestParam(value = "username") String username,
                                         @RequestParam(value = "type", required = false, defaultValue = "zx") String type) {
        return iActivityService.getVISImgUrl(qrImgUrl, username, type);
    }

    /**
     * 返回图片合成地址
     *
     * @return
     */
    @RequestMapping("/get_image_synthesis.do")
    @ResponseBody
    @ApiOperation(value = "返回图片合成地址", httpMethod = "GET")
    public ServerResponse getImageSynthesis(@RequestParam(value = "qrImgUrl") String qrImgUrl,
                                            @RequestParam(value = "username") String username,
                                            @RequestParam(value = "type") String type) {
        return iActivityService.getImageSynthesis(qrImgUrl, username, type);
    }


    /**
     * 获得门票图片地址
     *
     * @param imageUrl
     * @return
     */
    @RequestMapping("/get_ticket_image.do")
    @ResponseBody
    @ApiOperation(value = "获得门票图片", httpMethod = "GET")
    public ServerResponse getTicketImageUrl(@RequestParam(value = "imageUrl") String imageUrl) {
        return iActivityService.getTicketImageUrl(imageUrl);
    }

    /**
     * 添加活动
     *
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    @ApiOperation(value = "添加活动", httpMethod = "POST")
    public ServerResponse getImageUrl(Activity activity) {
        return iActivityService.saveon(activity);
    }

    /**
     * 根据id 返回详情
     *
     * @param activityId
     * @return
     */
    @RequestMapping("/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse getImageUrl(Integer activityId) {
        Activity activity = iActivityService.findOne(activityId);
        return ServerResponse.createBySuccess(activity);
    }

    /**
     * 修改活动表状态信息
     *
     * @param token
     * @param usermail
     * @param classId
     * @return
     */
    @RequestMapping("/modify_pay_info.do")
    @ResponseBody
    @ApiOperation(value = "修改活动表状态信息", httpMethod = "POST")
    public ServerResponse modifyPayUserInfo(String randNum, String token, String usermail, String classId) {
        //发送管理员邮件
        String adminEmail = PropertiesUtil.getProperty("admin.email");
        try {
            MailUtil.SendNotifyMail(adminEmail, "redis取出token");
        } catch (Exception e) {
            log.error("【活动表修改异常】msg:{}", e.getMessage());
        }
        String searchKey = RedisUtils.searchRedis(Const.SECRET_KEY + randNum);
        if (searchKey != null && token.equals(searchKey)) {
            return iActivityService.modifyUserInfo(usermail, classId);
        } else {
            return ServerResponse.createByErrorMessage("验证失败!非法请求");
        }
    }

//    18年年会拔河活动-（configure表）
//    TugOfWar

    /**
     * 拔河活动-新增接口
     *
     * @param nickname
     * @param portrait
     * @param number
     * @return
     */
    @RequestMapping("/TugOfWar_Insert.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-新增接口", httpMethod = "POST")
    public ServerResponse TugOfWar_Insert(@RequestParam(value = "nickname") String nickname,
                                          @RequestParam(value = "portrait") String portrait,
                                          @RequestParam(value = "openid") String openid,
                                          @RequestParam(value = "number") String number,
                                          @RequestParam(value = "camp") String camp) {
        return iActivityService.TugOfWar_Insert(nickname, portrait, openid, number, camp);
    }

    /**
     * 拔河活动-更新接口
     *
     * @param number
     * @return
     */
    @RequestMapping("/TugOfWar_update.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-更新数字接口", httpMethod = "POST")
    public ServerResponse TugOfWar_update(@RequestParam(value = "openid") String openid,
                                          @RequestParam(value = "number") String number) {
        return iActivityService.TugOfWar_update(openid, number);
    }

    /**
     * 拔河活动-我的统计接口
     *
     * @param openid
     * @return
     */
    @RequestMapping("/TugOfWar_MyNumber.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-我的统计接口", httpMethod = "POST")
    public ServerResponse TugOfWar_MyNumber(@RequestParam(value = "openid") String openid) {
        return iActivityService.TugOfWar_MyNumber(openid);
    }

    /**
     * 拔河活动-阵容统计接口
     *
     * @param camp
     * @return
     */
    @RequestMapping("/TugOfWar_Camp.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-阵营统计接口", httpMethod = "POST")
    public ServerResponse TugOfWar_Camp(@RequestParam(value = "camp") String camp) {
        return iActivityService.TugOfWar_Camp(camp);
    }

    /**
     * 拔河活动-总统计接口
     *
     * @return
     */
    @RequestMapping("/TugOfWar_Count.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-总统计接口", httpMethod = "POST")
    public ServerResponse TugOfWar_Count() {
        return iActivityService.TugOfWar_Count();
    }

    /**
     * 拔河活动-阵容人员列表接口
     *
     * @param camp
     * @return
     */
    @RequestMapping("/TugOfWar_PeopleList.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-人员列表接口", httpMethod = "POST")
    public ServerResponse TugOfWar_PeopleList(@RequestParam(value = "camp") String camp) {
        return iActivityService.TugOfWar_PeopleList(camp);
    }

    /**
     * 拔河活动-全部人员列表接口
     *
     * @return
     */
    @RequestMapping("/TugOfWar_AllPeopleList.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-全部人员列表接口", httpMethod = "POST")
    public ServerResponse TugOfWar_AllPeopleList() {
        return iActivityService.TugOfWar_AllPeopleList();
    }


    /**
     * 拔河活动-删除接口
     *
     * @return
     */
    @RequestMapping("/TugOfWar_Delete.do")
    @ResponseBody
    @ApiOperation(value = "拔河活动-删除接口", httpMethod = "POST")
    public ServerResponse TugOfWar_Delete(int id) {
        return iActivityService.TugOfWar_Delete(id);
    }


//    vis 2019 会议报名模块


    /**
     * vis报名-新增
     *
     * @param vis2019
     * @return
     */
    @RequestMapping("/vis_insert.do")
    @ResponseBody
    @ApiOperation(value = "vis报名-新增", httpMethod = "POST")
    public ServerResponse vis_insert(Vis2019 vis2019) {
        vis2019.setCreattime(new Date());
        if (Const.ACTIVITY_TYPE_VIS2019.equalsIgnoreCase(vis2019.getType())) {
            //异步验证是否支付订单
            //asyncTask.sendNoPayMessage(vis2019.getPhone());
        }
        return iActivityService.vis_insert(vis2019);
    }

    /**
     * 活动表-list
     *
     * @param pageNum
     * @param pageSize
     * @param type
     * @return
     */
    @RequestMapping("/vis_list.do")
    @ResponseBody
    @ApiOperation(value = "vis报名-list", httpMethod = "POST")
    public ServerResponse vis_list(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
                                   @RequestParam(required = false, defaultValue = "") String type) {
        return iActivityService.vis_list(type, pageNum, pageSize);
    }

    /**
     * vis报名-detail
     *
     * @param id
     * @return
     */
    @RequestMapping("/vis_detail.do")
    @ResponseBody
    @ApiOperation(value = "vis报名详细信息-detail", httpMethod = "POST")
    public ServerResponse vis_detail(int id) {
        return iActivityService.vis_detail(id);
    }

    /**
     * vis报名-通过手机号查询已支付的订单报名表
     *
     * @param phone
     * @return
     */
    @RequestMapping("/vis_list_byPhone.do")
    @ResponseBody
    @ApiOperation(value = "通过手机号查询已支付的报名表", httpMethod = "GET")
    public ServerResponse vis_list_byPhone(String phone) {
        return iActivityService.vis_list_byPhone(phone);
    }

    /**
     * vis报名-查询已支付的订单报名表
     *
     * @param pageNum
     * @param pageSize
     * @param type
     * @return
     */
    @RequestMapping("/vis_orderList.do")
    @ResponseBody
    @ApiOperation(value = "查询已支付的报名表", httpMethod = "POST")
    public ServerResponse vis_orderList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
                                        @RequestParam(required = false, defaultValue = "") String type) {
        return iActivityService.vis_orderList(type, pageNum, pageSize);
    }


    /** ===============================================签到小程序===============================================*/

    /**
     * 小程序——电子票查询
     *
     * @param collectId
     * @return
     */
    @RequestMapping("/check_info.do")
    @ResponseBody
    @ApiOperation(value = "签到-电子票信息查询", httpMethod = "GET")
    public ServerResponse checkInfo(Integer collectId) {
        return iActivityService.activityCheckInfo(collectId);
    }

    /**
     * 小程序——签到功能
     *
     * @param collectId
     * @return
     */
    @RequestMapping("/check_in.do")
    @ResponseBody
    @ApiOperation(value = "签到-签到按钮功能", httpMethod = "GET")
    public ServerResponse checkIn(Integer collectId) {
        return iActivityService.activityCheckIn(collectId);
    }

    /**
     * 返回签到关联订单列表信息
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/checkVis_list.do")
    @ResponseBody
    public ServerResponse checkListJoinOrderAndEventCollect(@RequestParam(value = "searchKey", defaultValue = "", required = false) String searchKey,
                                                            @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                            @RequestParam(value = "activeId", required = false) Integer activeId) {
        return iActivityService.activityCheckList(searchKey, activeId, pageNum, pageSize);
    }

    /**
     * 小程序——签到统计付费和线下付费
     *
     * @param activeId
     * @return
     */
    @RequestMapping("/pay_statistics.do")
    @ResponseBody
    public ServerResponse checkCount(Integer activeId) {
        if (activeId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        return iActivityService.getCheckStatistics(activeId);
    }

    /**
     * 小程序———签到统计回馈和嘉宾
     *
     * @param activeId
     * @return
     */
    @RequestMapping("/feedback_guest_statistics.do")
    @ResponseBody
    public ServerResponse checkFeedbackGuestStatistics(Integer[] activeId) {
        if (activeId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        return iActivityService.getCheckFeedBackGuestStatistics(activeId);
    }

}
