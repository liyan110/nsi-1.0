package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Splitter;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.NsiEvent;
import com.nsi.service.NsiEventService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 活动定义表 前端控制器
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@Controller
@RequestMapping("/nsiEvent")
public class NsiEventController {

    @Autowired
    private NsiEventService nsiEventService;

    private Splitter splitter = Splitter.on(";");

    /**
     * 活动新增
     *
     * @param nsiEvent
     * @return
     */
    @RequestMapping("/insert")
    @ResponseBody
    public ServerResponse insert(NsiEvent nsiEvent) {
        if (StringUtils.isBlank(nsiEvent.getEventName())
                || StringUtils.isBlank(nsiEvent.getEventSummary())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }

        nsiEvent.setCreateTime(new Date());
        nsiEvent.setUpdateTime(nsiEvent.getCreateTime());

        nsiEventService.insert(nsiEvent);
        return ServerResponse.createBySuccess(nsiEvent);
    }

    /**
     * 活动列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse list(@RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper wrapper = new EntityWrapper();
        wrapper.orderBy("sort", false);
        wrapper.orderBy("create_time", false);
        List<NsiEvent> list = nsiEventService.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        int count = (int) pageInfo.getTotal();
        return ServerResponse.createBySuccess("success", count, pageInfo.getList());
    }

    /**
     * 活动详情
     *
     * @param id
     * @return
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ServerResponse detail(@RequestParam Integer id) {
        NsiEvent nsiEvent = nsiEventService.selectById(id);
        if (nsiEvent != null && StringUtils.isNotBlank(nsiEvent.getNotes())) {
            List<String> noteList = splitter.splitToList(nsiEvent.getNotes());
            nsiEvent.setNotesList(noteList);
        }
        return ServerResponse.createBySuccess(nsiEvent);
    }

    /**
     * 修改功能
     *
     * @param nsiEvent
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public ServerResponse update(NsiEvent nsiEvent) {
        nsiEvent.setUpdateTime(new Date());
        nsiEventService.updateById(nsiEvent);
        return ServerResponse.createBySuccessMessage("success");
    }

    /**
     * 删除功能
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ServerResponse delete(@RequestParam Integer id) {
        nsiEventService.deleteById(id);
        return ServerResponse.createBySuccessMessage("success");
    }

    /**
     * 置顶功能
     *
     * @param eventId
     * @return
     */
    @RequestMapping("/top")
    @ResponseBody
    public ServerResponse isTop(@RequestParam Integer eventId) {
        if (eventId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }

        NsiEvent nsiEvent = new NsiEvent();
        nsiEvent.setSort(Const.NUM_DEFAULT_VALUE);
        // 所有活动清空排序
        nsiEventService.update(nsiEvent, new EntityWrapper<NsiEvent>());

        nsiEvent.setId(eventId);
        nsiEvent.setSort(Const.CANCEL_TOP);
        nsiEvent.setUpdateTime(new Date());
        // 根据活动id置顶活动
        nsiEventService.updateById(nsiEvent);

        return ServerResponse.createBySuccess();
    }
}
