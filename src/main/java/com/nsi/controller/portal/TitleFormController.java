package com.nsi.controller.portal;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.TitleForm;
import com.nsi.service.TitleFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author luo Zhen
 * @since 2020-03-20
 */
@Controller
@RequestMapping("/title_form")
public class TitleFormController {

    @Autowired
    private TitleFormService titleFormService;

    /**
     * 获取活动标题信息
     *
     * @param activeId
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findTitleForm(@RequestParam Integer activeId) {
        TitleForm titleForm = titleFormService.selectOne(new EntityWrapper<TitleForm>()
                .eq("active_id", activeId));
        return ServerResponse.createBySuccess(titleForm);
    }
}
