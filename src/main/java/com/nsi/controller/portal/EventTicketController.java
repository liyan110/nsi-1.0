package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventApplyHead;
import com.nsi.pojo.EventTicket;
import com.nsi.service.IEventTicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-09-03
 **/
@RequestMapping("/EventTicket")
@Controller
@Api(value = "/EventTicket", description = "活动—活动表-票务信息")
public class EventTicketController {

    @Autowired
    private IEventTicketService iEventTicketService;



    /*插入操作*/
    @RequestMapping("/insert.do")
    @ResponseBody
    @ApiOperation(value = "插入操作", httpMethod = "GET")
    public ServerResponse insert(EventTicket eventTicket){
        int i=iEventTicketService.insert(eventTicket);
        if (i==1) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }

    /**
     * 修改操作
     *
     * @param eventTicket
     * @return
     */
    @RequestMapping("/update.do")
    @ResponseBody
    @ApiOperation(value = "修改操作", httpMethod = "GET")
    public ServerResponse update(EventTicket eventTicket) {
        int i= iEventTicketService.update(eventTicket);
        if (1==i) {
            return ServerResponse.createBySuccess("操作成功");
        }
        return ServerResponse.createByErrorCodeMessage(500, "操作失败");
    }

    /**
     *通过eventId查询
     *
     * @param eventId
     * @return
     */
    @RequestMapping("/detailByEventId.do")
    @ResponseBody
    @ApiOperation(value = "通过eventId查询", httpMethod = "GET")
    public ServerResponse detailByEventId(Integer eventId) {
        List<EventTicket> list= iEventTicketService.detailByEventId(eventId);
        return ServerResponse.createBySuccess("操作成功",list);
    }

}
