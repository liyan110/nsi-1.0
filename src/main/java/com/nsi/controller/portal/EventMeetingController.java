package com.nsi.controller.portal;

import com.nsi.common.ServerResponse;
import com.nsi.service.EventMeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-08-16 9:33
 */
@RequestMapping("/meeting/")
@Controller
@Api(value = "/meeting/", description = "官网——活动会议(后台)管理接口")
public class EventMeetingController {

    @Autowired
    private EventMeetingService meetingService;

    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回个人参会活动列表", httpMethod = "GET")
    public ServerResponse findEventMeetingList(@RequestParam("phone") String phone) {
        return meetingService.findEventMeetingList(phone);
    }

    @RequestMapping("/detail.do")
    @ResponseBody
    @ApiOperation(value = "返回活动详情", httpMethod = "GET")
    public ServerResponse findOne(@RequestParam("eventId") Integer enentId) {
        return meetingService.findOne(enentId);
    }
}
