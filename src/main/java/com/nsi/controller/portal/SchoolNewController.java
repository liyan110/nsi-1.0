package com.nsi.controller.portal;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.enums.CheckStateEnum;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.MyCollect;
import com.nsi.pojo.SchoolNew;
import com.nsi.pojo.SchoolSubmit;
import com.nsi.service.*;
import com.nsi.util.PropertiesUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Luo Zhen
 * @create 2019-05-13 17:40
 */
@RequestMapping("/new/school")
@Controller
public class SchoolNewController {

    @Autowired
    IFileService iFileService;
    @Autowired
    INewSchoolService iNewSchoolService;
    @Autowired
    SchoolSubmitService schoolSubmitService;
    @Autowired
    MyCollectService myCollectService;
    @Autowired
    CommunityCommentService communityCommentService;

    /**
     * 新增学校信息
     *
     * @param schoolNew
     * @return
     */
    @RequestMapping(value = "/insert.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse insert(SchoolNew schoolNew) {

        schoolNew.setVerifySign(CheckStateEnum.IN_REVIEW.getCode());
        schoolNew.setIsUseNuohui(Const.NUM_DEFAULT_VALUE);
        schoolNew.setCreateTime(new Date());
        // 添加学校库
        SchoolNew schools = iNewSchoolService.insertSchool(schoolNew);

        // 添加提交信息表
        SchoolSubmit schoolSubmit = new SchoolSubmit();
        schoolSubmit.setSchoolId(schools.getId());
        schoolSubmit.setName(schoolNew.getSubmitName());
        schoolSubmit.setCompany(schoolNew.getCompany());
        schoolSubmit.setTelphone(schoolNew.getTelphone());
        schoolSubmitService.insert(schoolSubmit);

        return ServerResponse.createBySuccess();
    }

    /**
     * 修改学校信息
     *
     * @param schoolNew
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(SchoolNew schoolNew) {
        if (schoolNew == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        iNewSchoolService.updateById(schoolNew);
        return ServerResponse.createBySuccess();
    }

    /**
     * 学校列表
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findAll(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                  @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey,
                                  @RequestParam(value = "verifySign", required = false, defaultValue = "1") Integer verifySign) {
        return ServerResponse.createBySuccess(iNewSchoolService.findAll(pageNum, pageSize, searchKey, verifySign));
    }

    /**
     * 返回学校名字和地址列表
     *
     * @return
     */
    @RequestMapping(value = "/item.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findNewSchoolItem() {
        return ServerResponse.createBySuccess(iNewSchoolService.findAll());
    }

    /**
     * 高级搜索
     *
     * @param area       省
     * @param system     学制
     * @param properties 学校性质
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/power_search_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getSchoolPowerSearch(@RequestParam(value = "area", required = false) String[] area,
                                                         @RequestParam(value = "system", required = false) String[] system,
                                                         @RequestParam(value = "properties", required = false) String[] properties,
                                                         @RequestParam(value = "course", required = false) String[] course,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iNewSchoolService.searchPowerList(area, system, properties, course, pageNum, pageSize);
    }


    /**
     * 新学校库的智能搜索
     *
     * @param keyword
     * @return
     */
    @RequestMapping("/suggest_search.do")
    @ResponseBody
    public ServerResponse suggestSearch(@RequestParam(value = "keyword", required = false) String keyword) {
        List<SchoolNew> schoolNews = iNewSchoolService.selectList(
                new EntityWrapper<SchoolNew>()
                        .like("school_name", keyword)
        );
        List<String> schoolNameList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(schoolNews)) {
            schoolNameList = schoolNews.stream()
                    .map(SchoolNew::getSchoolName)
                    .limit(10)
                    .collect(Collectors.toList());
        }
        return ServerResponse.createBySuccess(schoolNameList);
    }

    /**
     * 同城推荐学校
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/find_city_wide.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findCityWideList(Integer id) {
        return iNewSchoolService.findCityWideList(id);
    }

    /**
     * 学校库详情
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findById(@RequestParam("schoolId") Integer schoolId) {
        return ServerResponse.createBySuccess(iNewSchoolService.findById(schoolId));
    }


    /**
     * 验证学校名是否重复
     *
     * @param schoolName
     * @return
     */
    @RequestMapping("/check_valid.do")
    @ResponseBody
    public ServerResponse checkValid(@RequestParam(value = "schoolName") String schoolName) {
        return iNewSchoolService.checkValidedByName(schoolName);
    }


    /**
     * 请求学校数据更新按钮
     *
     * @param schoolId
     * @return
     */
    @RequestMapping("/school_UpdataButton.do")
    @ResponseBody
    public ServerResponse school_UpdataButton(@RequestParam(value = "schoolId") String schoolId,
                                              @RequestParam(value = "schoolName") String schoolName,
                                              @RequestParam(value = "userMail") String userMail) {
        return iNewSchoolService.school_UpdataButton(schoolId, schoolName, userMail);
    }

    /**
     * 列表-请求学校数据更新按钮
     *
     * @return
     */
    @RequestMapping("/list_schoolUpdataButton.do")
    @ResponseBody
    public ServerResponse list_schoolUpdataButton() {
        return iNewSchoolService.list_schoolUpdataButton();
    }

    /**
     * 删除-请求学校数据更新按钮
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete_schoolUpdataButton.do")
    @ResponseBody
    public ServerResponse delete_schoolUpdataButton(int id) {
        return iNewSchoolService.delete_schoolUpdataButton(id);
    }


    /**
     * 学校logo图片上传
     *
     * @param file     图片
     * @param type     文件路径
     * @param schoolId 学校id
     * @return
     */
    @RequestMapping(value = "/upload_logo.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse uploadOSSLogo(@RequestParam(value = "file", required = false) MultipartFile file,
                                        @RequestParam(value = "type", required = false, defaultValue = "nsi-pc/SchoolLogo/") String type,
                                        @RequestParam(value = "schoolId") Integer schoolId) {
        String targetFileName = iFileService.uploadSchoolLogo(file, type, schoolId);
        if ("".equalsIgnoreCase(targetFileName)) {
            return ServerResponse.createByErrorMessage("非法请求");
        }
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + targetFileName;
        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", targetFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 学校大图上传
     *
     * @param file     图片
     * @param type     文件路径
     * @param schoolId 学校id
     * @return
     */
    @RequestMapping(value = "/upload_img.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse uploadBigImage(@RequestParam(value = "file", required = false) MultipartFile file,
                                         @RequestParam(value = "type", required = false, defaultValue = "nsi-pc/SchoolShow/") String type,
                                         @RequestParam(value = "schoolId") Integer schoolId) {
        String targetFileName = iFileService.uploadSchoolBigImage(file, type, schoolId);
        if ("".equalsIgnoreCase(targetFileName)) {
            return ServerResponse.createByErrorMessage("非法请求");
        }
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + targetFileName;
        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", targetFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 返回评论数和收藏数
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/get_comment_count.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse getCommentCount(@RequestParam("schoolId") Integer schoolId) {

        // 返回评论数
        int commentNum = communityCommentService.selectCount(new EntityWrapper<CommunityComment>()
                .eq("object_id", schoolId)
                .eq("comment_type", 2)
                .eq("verify_sign", 1));

        // 返回收藏数
        int collectNum = myCollectService.selectCount(new EntityWrapper<MyCollect>()
                .eq("c_id", schoolId)
                .eq("collect_type", 1));

        Map<String, Integer> result = new HashMap<>();
        result.put("commentNum", commentNum);
        result.put("collectNum", collectNum);
        return ServerResponse.createBySuccess(result);
    }
}
