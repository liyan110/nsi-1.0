package com.nsi.controller.portal;


import com.nsi.common.ServerResponse;
import com.nsi.service.DiscountsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 优惠用户信息表 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-21
 */
@RequestMapping("/discount/")
@Controller
@Api(value = "/discount/", description = "官网——优惠信息(前台)管理接口")
public class DiscountsController {

    @Autowired
    private DiscountsService discountsService;

    @RequestMapping(value = "check_valid.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "验证是否可以优惠", httpMethod = "GET")
    public ServerResponse checkValid(@RequestParam("telphone") String telphone) {
        return discountsService.checkValid(telphone);
    }

}
