package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Members;
import com.nsi.service.IMembersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/10 15:19
 * @description: 官网前台会员功能
 */
@Controller
@RequestMapping("/members")
@Api(value = "/members", description = "官网——会员(前台)管理接口")
public class MembersController {

    @Autowired
    private IMembersService iMembersService;

    /**
     * 添加会员
     *
     * @param members
     * @return
     */
    @RequestMapping(value = "/add_members.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加会员", httpMethod = "POST")
    public ServerResponse addMembers(Members members) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iMembersService.addMembersInfo(members);
    }

    /**
     * 后台————返回详情
     *
     * @return
     */
    @RequestMapping(value = "/get_members_list.do")
    @ResponseBody
    @ApiOperation(value = "后台——返回详情", httpMethod = "GET")
    public ServerResponse listMembers(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                      @RequestParam(value = "searchKey", defaultValue = "0") Integer searchKey) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iMembersService.getMembersList(pageNum, pageSize, searchKey);
    }

    /**
     * 后台——修改状态
     *
     * @param members
     * @return
     */
    @RequestMapping(value = "/set_members_states.do")
    @ResponseBody
    @ApiOperation(value = "后台——修改状态", httpMethod = "POST")
    public ServerResponse setMembersStates(Members members) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iMembersService.updateMembersStates(members);
    }


}
