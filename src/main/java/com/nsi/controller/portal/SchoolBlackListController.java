package com.nsi.controller.portal;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.ForeignTeacher;
import com.nsi.service.ISchoolBlackListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/28 15:15
 * @description:
 */
@RequestMapping("/foreign")
@Controller
@Api(value = "/foreign", description = "四库全书——黑外教(前台)管理接口")
public class SchoolBlackListController {

    @Autowired
    private ISchoolBlackListService iSchoolBlackListService;

    /**
     * 添加方法
     *
     * @param foreignTeacherCondition
     * @return
     */
    @RequestMapping(value = "/add_foreign_black_list.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addForeignTeacher(ForeignTeacher foreignTeacherCondition) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        try {
            return iSchoolBlackListService.addSchoolBlackList(foreignTeacherCondition);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 根据条件返回分页详情
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/get_black_list.do")
    @ResponseBody
    @ApiOperation(value = "根据条件返回分页详情", httpMethod = "GET")
    public ServerResponse getForeignTeacherList(@RequestParam(value = "searchKey", required = false) String searchKey,
                                                @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.getSchoolBlackList(searchKey, pageNum, pageSize);
    }

    /**
     * 根据id返回详情
     *
     * @param foreignId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id返回详情", httpMethod = "GET")
    public ServerResponse getForeignTeacherInfo(Integer foreignId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.getForeignInfo(foreignId);
    }
}
