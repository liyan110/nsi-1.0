package com.nsi.controller.portal;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Talent;
import com.nsi.service.IFileService;
import com.nsi.service.ITalentService;
import com.nsi.util.PropertiesUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Luo Zhen
 * @create 2018-08-20
 * 人才库控制层
 */
@RequestMapping("/manager/talent")
@Controller
@Api(value = "/manager/talent", description = "四库全书——人才库(前台)管理接口")
public class TalentController {

    @Autowired
    private ITalentService iTalentService;

    @Autowired
    private IFileService iFileService;

    /**
     * 添加操作
     *
     * @param talent
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse<String> add(Talent talent) {
        talent.setVerifySign(String.valueOf(ResponseCode.CHECK.getCode()));
        return iTalentService.add(talent);
    }

    /**
     * 人才简历详情(前台)搜索操作
     *
     * @param talent_searchKey
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "前台获取list人才", httpMethod = "GET")
    public ServerResponse search(
            @RequestParam(value = "talent_searchKey", required = false) String talent_searchKey,
            @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iTalentService.search(talent_searchKey, pageNum, pageSize);
    }


    /**
     * 我的简历 搜索操作t
     *
     * @param userMail
     * @return list
     */
    @RequestMapping(value = "/myTalent.do")
    @ResponseBody
    @ApiOperation(value = "根据用户名查询用户多个信息", httpMethod = "GET")
    public ServerResponse<String> myTalent(@RequestParam(value = "userMail") String userMail) {
        return iTalentService.searchByMail(userMail);
    }

    /**
     * 修改简历 修改操作
     *
     * @param talent
     * @return list
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse<String> alter(Talent talent) {
        return iTalentService.update(talent);
    }

    /**
     * 详情简历 指定查询操作
     *
     * @param talentId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详情", httpMethod = "GET")
    public ServerResponse<String> detail(Integer talentId) {
        return iTalentService.detail(talentId);
    }

    /**
     * 验证是否上传简历
     *
     * @param userMail
     * @return
     */
    @RequestMapping(value = "/check_upfile.do")
    @ResponseBody
    @ApiOperation(value = "验证是否上传简历", httpMethod = "GET")
    public ServerResponse checkByUpFile(String userMail) {
        return iTalentService.checkByUpFile(userMail);
    }

    /**
     * 根据id 删除数据
     *
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "根据id 删除数据", httpMethod = "GET")
    public ServerResponse delete(Integer talentId) {
        return iTalentService.deleteOn(talentId);
    }

    /**
     * 删除简历
     *
     * @return
     */
    @RequestMapping(value = "/delete_file.do")
    @ResponseBody
    @ApiOperation(value = "根据用户名和简历地址删除简历", httpMethod = "GET")
    public ServerResponse deleteFile(String userMail, String fileUrl) {
        return iTalentService.deleteFile(userMail, fileUrl);
    }

    /**
     * 上传简历 文件上传
     *
     * @param file
     * @return list
     */
    @RequestMapping(value = "/upfile.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传简历附件", httpMethod = "POST")
    public ServerResponse upfile(@RequestParam(value = "file", required = false) MultipartFile file,
                                 @RequestParam(value = "userMail", required = false) String userMail,
                                 @RequestParam(value = "userTrueName", required = false) String userTrueName,
                                 @RequestParam(value = "type", defaultValue = "1") String type) {
        String path = null;
        String targetFileName = null;
        String url = null;

        // 1:人才简历 2:企业介绍
        if ("1".equals(type)) {
            path = "C:" + File.separator + "upFile" + File.separator + "talent" + File.separator;
            targetFileName = iFileService.upFile(file, userMail, userTrueName, path);
            //服务器需要的改法
            url = PropertiesUtil.getProperty("nsi.file.url") + "/upFile/talent/" + targetFileName;
        } else {
            path = "C:" + File.separator + "upFile" + File.separator + "recruitment" + File.separator;
            targetFileName = iFileService.upFile(file, userMail, userTrueName, path);
            url = PropertiesUtil.getProperty("nsi.file.url") + "/upFile/recruitment/" + targetFileName;
        }
        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", targetFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 图片上传
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/upload.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传图片/文件至 阿里云-通用接口", httpMethod = "POST")
    public ServerResponse upload(@RequestParam(value = "file", required = false) MultipartFile file, String type) {
        if (StringUtils.isBlank(type)) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        String targetFileName = iFileService.upload(file, type);
        if ("".equalsIgnoreCase(targetFileName)) {
            return ServerResponse.createByErrorMessage("非法请求");
        }
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + targetFileName;
        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", targetFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 富文本图片上传
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/editor_img_upload.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "富文本图片上传", httpMethod = "POST")
    public Map wangEditorImgUpload(@RequestParam(value = "file", required = false) MultipartFile file, String type) {
        List<String> urls = Lists.newArrayList();
        Map resultMap = Maps.newHashMap();
        if (StringUtils.isBlank(type)) {
            resultMap.put("error", 1);
            resultMap.put("msg", "参数不合法");
            return resultMap;
        }
        String targetFileName = iFileService.upload(file, type);
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + targetFileName;
        urls.add(url);
        resultMap.put("error", 0);
        resultMap.put("data", urls);
        return resultMap;
    }

    /**
     * Base64解码上传
     *
     * @param strImage
     * @param type
     * @return
     */
    @RequestMapping(value = "/get_base64_image.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Base64解码上传", httpMethod = "POST")
    public ServerResponse base64getImage(@RequestParam("strImage") String strImage,
                                         @RequestParam("type") String type) {
        Map map = Maps.newHashMap();
        if (StringUtils.isBlank(strImage) || StringUtils.isBlank(type)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        String targetFileName = iFileService.base64Upload(strImage, type);
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + targetFileName;
        map.put("uri", targetFileName);
        map.put("url", url);
        return ServerResponse.createBySuccess(map);
    }

    /**
     * 上传简历 文件上传
     *
     * @param file
     * @return list
     */
    @RequestMapping(value = "/upResume.do")
    @ResponseBody
    @ApiOperation(value = "上传简历附件", httpMethod = "POST")
    public ServerResponse uploadResume(@RequestParam(value = "file") MultipartFile file,
                                       @RequestParam(value = "userMail") String userMail,
                                       @RequestParam(value = "talentId") String talentId,
                                       @RequestParam(value = "type") String type) {
        if (StringUtils.isEmpty(userMail) ||
                StringUtils.isEmpty(talentId) ||
                StringUtils.isEmpty(type)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        String fileName = file.getOriginalFilename();
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
        String newFileName = talentId + "-" + userMail + "." + fileExtensionName;
        newFileName = iFileService.uploadResume(file, type, newFileName);
        String url = PropertiesUtil.getProperty("aliyun.image.url") + type + newFileName;

        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", newFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

}
