package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.RecuitmentInfo;
import com.nsi.service.IRecuitmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/11/23 11:06
 * @description: 人才招聘职位信息
 */
@RequestMapping("/recuitmentInfo")
@Controller
@Api(value = "/recuitmentInfo", description = "四库全书————人才招聘职位信息(前台)管理接口")
public class RecuitmentInfoController {

    @Autowired
    private IRecuitmentService iRecuitmentService;

    /**
     * 人才招聘职位信息添加
     *
     * @param recuitmentInfo
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "人才招聘职位信息添加", httpMethod = "POST")
    public ServerResponse addRecuitmentInfo(RecuitmentInfo recuitmentInfo) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRecuitmentService.addRecuitmentInfo(recuitmentInfo);
    }

    /**
     * 根据关键字返回分页信息
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "关键字返回分页详情", httpMethod = "GET")
    public Map<String, Object> getRecuitmentInfo(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                 @RequestParam(value = "searchKey", required = false) String searchKey) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRecuitmentService.getRecuitmentInfoList(pageNum, pageSize, searchKey);
    }

    /**
     * 根据id 返回详情
     *
     * @param recuitmentId
     * @return
     */
    @RequestMapping(value = "/get_info.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse getRecuitmentInfoById(Integer recuitmentId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRecuitmentService.selectAllById(recuitmentId);
    }

}
