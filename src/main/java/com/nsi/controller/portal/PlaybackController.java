package com.nsi.controller.portal;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.service.IPlaybackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/playback")
@Controller
@Api(value = "/playback", description = "新学说课堂——回访(前台)接口")
public class PlaybackController {

    @Autowired
    private IPlaybackService iPlaybackService;

    /**
     * 回放 获得回放链接
     *
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/getLiveUrl.do")
    @ResponseBody
    @ApiOperation(value = "获得回放链接", httpMethod = "GET")
    public ServerResponse<String> search(Integer courseId) {

        return iPlaybackService.search(courseId);
    }


}
