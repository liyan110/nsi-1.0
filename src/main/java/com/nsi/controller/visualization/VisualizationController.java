package com.nsi.controller.visualization;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Coordinate;
import com.nsi.pojo.SchoolNew;
import com.nsi.service.INewSchoolService;
import com.nsi.service.ISchoolService;
import com.nsi.util.PropertiesUtil;
import com.nsi.vo.SchoolInfoIntegrity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Luo Zhen
 * @create 2018-08-17
 * 可视化控制层
 **/
@RequestMapping("/visualization")
@Controller
@Api(value = "/visualization", description = "可视化接口参数")
@Slf4j
public class VisualizationController {

    @Autowired
    private ISchoolService iSchoolService;
    @Autowired
    private INewSchoolService iNewSchoolService;

    /**
     * 返回可视化 学校模型
     *
     * @return
     */
    @RequestMapping("/get_school_list.do")
    @ResponseBody
    @ApiOperation(value = "返回学校list", httpMethod = "GET")
    public ServerResponse getSchoolInfo() {
        return iSchoolService.getVisualizationSchoolList();
    }

    /**
     * 添加学校经纬度
     *
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    @ApiOperation(value = "学校库添加经纬度", httpMethod = "POST")
    public ServerResponse addSchoolInfo(Coordinate coordinate) {
        return iSchoolService.saveOn(coordinate);
    }

    /**
     * 国际学校不同学段平均学费统计
     *
     * @return
     */
    @RequestMapping("/get_school_item.do")
    @ResponseBody
    @ApiOperation(value = "国际学校不同学段平均学费统计", httpMethod = "GET")
    public ServerResponse getSchoolItem() {
        return iSchoolService.getSchoolItemList();
    }

    /**
     * 返回学校信息完整度
     *
     * @param orderBy
     * @return
     */
    @RequestMapping("/get_school_integrity.do")
    @ResponseBody
    @ApiOperation(value = "返回学校信息完整度")
    public ServerResponse getSchoolIntegrity(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                             @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                             @RequestParam(value = "orderBy", defaultValue = "DESC") String orderBy) {
        PageHelper.startPage(pageNum, pageSize);
        List<SchoolNew> schoolList = iNewSchoolService.findAllList(null, null);
        List<SchoolInfoIntegrity> lists = new ArrayList<>();
        List<SchoolInfoIntegrity> currentList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(schoolList)) {
            for (SchoolNew schoolNew : schoolList) {
                int integrity = this.allfieldIsNull(schoolNew);
                SchoolInfoIntegrity schoolInfo = this.assemble(schoolNew, integrity);
                lists.add(schoolInfo);
            }
        }
        if ("DESC".equalsIgnoreCase(orderBy)) {
            currentList = lists.stream().sorted(Comparator.comparing(SchoolInfoIntegrity::getPercent).reversed()).limit(100).collect(Collectors.toList());
        } else {
            currentList = lists.stream().sorted(Comparator.comparing(SchoolInfoIntegrity::getPercent)).limit(100).collect(Collectors.toList());
        }
        PageInfo pageInfo = new PageInfo(currentList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 返回学校信息度高的数据
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/find_school_integrity.do")
    @ResponseBody
    @ApiOperation(value = "返回学校信息度高的数据")
    public ServerResponse findSchoolIntegrity(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                              @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<String> schoolIds = PropertiesUtil.getPropertys("school.integrity.ids");
        List<SchoolNew> schoolNews = iNewSchoolService.selectBatchIds(schoolIds);
        PageInfo pageInfo = new PageInfo(schoolNews);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 返回统计数目
     *
     * @param obj
     * @return
     */
    private int allfieldIsNull(Object obj) {
        int integrity = 0;
        for (Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            try {
                if (f.get(obj) != null
                        && !f.get(obj).equals("0")) {
                    // 判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
                    integrity++;
                }
            } catch (Exception e) {
                log.error("【统计学校库数据完整度异常】,{}", e.getMessage());
                e.printStackTrace();
            }
        }
        return integrity;
    }

    private SchoolInfoIntegrity assemble(SchoolNew schoolNew, int integrity) {
        SchoolInfoIntegrity info = new SchoolInfoIntegrity();
        info.setSchoolId(schoolNew.getId());
        info.setSchoolName(schoolNew.getSchoolName());
        info.setPercent(integrity);
        info.setPercentDesc("数据完整度：" + integrity);
        return info;
    }

}
