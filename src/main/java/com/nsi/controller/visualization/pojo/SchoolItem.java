package com.nsi.controller.visualization.pojo;

/**
 * 简化 学校库可视化模型
 *
 * @author Luo Zhen
 * @create 2019-01-08 13:42
 */
public class SchoolItem extends SchoolVisualization {

    private Integer foundedTime;

    public Integer getFoundedTime() {
        return foundedTime;
    }

    public void setFoundedTime(Integer foundedTime) {
        this.foundedTime = foundedTime;
    }
}
