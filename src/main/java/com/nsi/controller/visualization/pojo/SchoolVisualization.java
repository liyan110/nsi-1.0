package com.nsi.controller.visualization.pojo;

/**
 * @author Luo Zhen
 * @create 2018-08-17
 * 学校库 试图可视化模型
 **/
public class SchoolVisualization {

    private Integer id;

    private String schoolName;

    private String schoolProperties;

    private String schoolSystem;

    private String schoolLogo;

    private String course;

    private String areas;

    private String areas02;

    private String areas03;

    private String tuition01;

    private String tuition02;

    private String tuition03;

    private String tuition04;

    private String longitude;

    private String latitude;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolProperties() {
        return schoolProperties;
    }

    public void setSchoolProperties(String schoolProperties) {
        this.schoolProperties = schoolProperties;
    }

    public String getSchoolSystem() {
        return schoolSystem;
    }

    public void setSchoolSystem(String schoolSystem) {
        this.schoolSystem = schoolSystem;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas;
    }

    public String getAreas02() {
        return areas02;
    }

    public void setAreas02(String areas02) {
        this.areas02 = areas02;
    }

    public String getAreas03() {
        return areas03;
    }

    public void setAreas03(String areas03) {
        this.areas03 = areas03;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTuition01() {
        return tuition01;
    }

    public void setTuition01(String tuition01) {
        this.tuition01 = tuition01;
    }

    public String getTuition02() {
        return tuition02;
    }

    public void setTuition02(String tuition02) {
        this.tuition02 = tuition02;
    }

    public String getTuition03() {
        return tuition03;
    }

    public void setTuition03(String tuition03) {
        this.tuition03 = tuition03;
    }

    public String getTuition04() {
        return tuition04;
    }

    public void setTuition04(String tuition04) {
        this.tuition04 = tuition04;
    }
}
