package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.service.IVisitSchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-08-22 14:16
 */
@RequestMapping("/visit/manager/school/")
@Controller
@Api(value = "/visit/manager/school/", description = "四库全书————申请访校库(后台)最新管理接口")
public class SchoolVisitManagerController {

    @Autowired
    private IVisitSchoolService iVisitSchoolService;

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse findALl(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return iVisitSchoolService.findAll(pageNum, pageSize, null, null);
    }

}
