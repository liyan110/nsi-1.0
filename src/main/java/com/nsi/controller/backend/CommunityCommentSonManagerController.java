package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.service.CommunityCommentSonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2019-12-23
 **/
@Controller
@RequestMapping("/manager/communityCommentSon")
@Api(value = "/manager/communityCommentSon", description = "社区小程序——2级 子评论 后台管理接口")
public class CommunityCommentSonManagerController {

    @Autowired
    private CommunityCommentSonService communityCommentSonService;

    @RequestMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "POST")
    public ServerResponse delete(int id) {
        return communityCommentSonService.delete(id);
    }

    @RequestMapping("/verify_list")
    @ResponseBody
    @ApiOperation(value = "审核列表方法", httpMethod = "POST")
    public ServerResponse verify_list(String type,
                                      @RequestParam(defaultValue = "", required = false) String searchKey,
                                      @RequestParam(defaultValue = "1") int pageNum,
                                      @RequestParam(defaultValue = "10") int pageSize) {
        return communityCommentSonService.verify_list(type, searchKey, pageNum, pageSize);
    }


    @RequestMapping("/verify_pass")
    @ResponseBody
    @ApiOperation(value = "审核通过方法", httpMethod = "POST")
    public ServerResponse verify_pass(int id) {
        int i = communityCommentSonService.verify_pass(id);
        if (i == 1) {
            return ServerResponse.createBySuccess("success");
        } else {
            return ServerResponse.createByErrorMessage("审核失败");
        }
    }


    @RequestMapping("/verify_reject")
    @ResponseBody
    @ApiOperation(value = "审核拒绝方法", httpMethod = "POST")
    public ServerResponse verify_reject(int id) {

        return communityCommentSonService.verify_reject(id);
    }

}
