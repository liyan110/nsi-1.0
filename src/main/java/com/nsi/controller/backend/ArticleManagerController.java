package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Article;
import com.nsi.service.IArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 后台文章管理接口
 *
 * @author Luo Zhen
 */
@RequestMapping("/manager/article")
@Controller
@Api(value = "/manager/article", description = "官网——文章(后台)管理接口")
public class ArticleManagerController {

    @Autowired
    private IArticleService iArticleService;

    /**
     * 添加方法
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "文章添加方法", httpMethod = "POST")
    public ServerResponse add(Article article) {
        return iArticleService.saveOn(article);
    }

    /**
     * 删除方法
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "文章删除方法", httpMethod = "GET")
    public ServerResponse delete(Integer articleId) {
        return iArticleService.deleteOn(articleId);
    }

    /**
     * 文章修改方法
     *
     * @param article
     * @return
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "文章修改方法", httpMethod = "POST")
    public ServerResponse update(Article article) {
        return iArticleService.updateOn(article);
    }

    /**
     * 根据Id返回详细信息
     *
     * @param articleId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详细信息", httpMethod = "GET")
    public ServerResponse detail(Integer articleId) {
        return iArticleService.detailOn(articleId);
    }

    /**
     * 根据所有ID，替换更新最新文章
     *
     * @return
     */
    @RequestMapping(value = "/reset_all_template.do")
    @ResponseBody
    @ApiOperation(value = "(慎用)根据所有ID，替换更新最新文章", httpMethod = "GET")
    public ServerResponse updateArticleTemplate() {
        return iArticleService.updateTemplate();
    }

    /**
     * 根据boardName 返回文章Url
     *
     * @return
     */
    @RequestMapping(value = "/get_redirect_url.do")
    @ResponseBody
    @ApiOperation(value = "获取跳转文章地址", httpMethod = "GET")
    public ServerResponse getRedirectUrl() {
        return iArticleService.findUrlByBoardName();
    }

    /**
     * 返回list
     *
     * @param pageNum
     * @param pageSize
     * @return4
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "获取list方法", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "keyword", defaultValue = "", required = false) String keyword,
                               @RequestParam(value = "siftType", defaultValue = "", required = false) String siftType,
                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iArticleService.managerList(siftType, keyword, pageNum, pageSize);
    }

    /**
     * 根据boardName 修改文章跳转地址
     *
     * @param sid
     * @return
     */
    @RequestMapping(value = "/update_redirect_url.do")
    @ResponseBody
    @ApiOperation(value = "修改文章地址", httpMethod = "GET")
    public ServerResponse updateRedirectUrl(String sid) {
        return iArticleService.updateUrlByBoardName(sid);
    }


    /**
     * 文章访问量统计
     * 访问量+1
     * 访问量查询
     * 后台查询访问量时，统计信息录入mysql
     */
    @RequestMapping(value = "/getStatistics.do")
    @ResponseBody
    @ApiOperation(value = "文章访问量+1", httpMethod = "GET")
    public ServerResponse getStatistics(@RequestParam(value = "articleId") int articleId) {
        return iArticleService.pageStatistics(articleId);
    }


    @RequestMapping(value = "/RefreshStatistics.do")
    @ResponseBody
    @ApiOperation(value = "获取文章访问量 统计数据", httpMethod = "GET")
    public ServerResponse RefreshStatistics(@RequestParam(value = "articleId") int articleId) {
        return iArticleService.refreshStatistics(articleId);
    }


    /**
     * 活动二维码 分销统计Fcode
     * 页面加载完毕 30秒，发起一次请求，高频接口
     *
     * @param Fcode
     * @return
     */
    @RequestMapping(value = "/FcodeStatistics.do")
    @ResponseBody
    @ApiOperation(value = "分销统计Fcode +1", httpMethod = "GET")
    public ServerResponse FcodeStatistics(@RequestParam(value = "F") String Fcode) {
        return iArticleService.FcodeStatistics(Fcode);
    }

    //    活动二维码 分销统计Fcode-返回统计数据（单条）
    @RequestMapping(value = "/ShowFcodeStatistics.do")
    @ResponseBody
    @ApiOperation(value = "分销统计Fcode-返回统计数据", httpMethod = "GET")
    public ServerResponse ShowFcodeStatistics(@RequestParam(value = "F") String Fcode) {
        return iArticleService.ShowFcodeStatistics(Fcode);
    }

    /**
     * 验证文章标题是否存在
     *
     * @param title
     * @return
     */
    @RequestMapping("/check_title.do")
    @ResponseBody
    @ApiOperation(value = "验证文章标题", httpMethod = "GET")
    public ServerResponse checkTitle(String title) {
        return iArticleService.checkTitle(title);
    }

}
