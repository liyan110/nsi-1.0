package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Preview;
import com.nsi.service.IPreviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 12984
 */
@Controller
@RequestMapping("/manager/preview")
@Api(value = "/manager/preview", description = "官网——文章预览(后台)管理接口")
public class PreviewManagerController {

    @Autowired
    private IPreviewService iPreviewService;

    /**
     * 添加方法
     *
     * @param preview
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse save(Preview preview) {
        return iPreviewService.saveon(preview);
    }

    /**
     * 返回详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse save(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iPreviewService.list(pageNum, pageSize);
    }


}
