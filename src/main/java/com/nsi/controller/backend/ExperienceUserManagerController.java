package com.nsi.controller.backend;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ExperienceUser;
import com.nsi.service.ExperienceUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/manager/experience")
public class ExperienceUserManagerController {

    @Autowired
    ExperienceUserService experienceUserService;

    /**
     * 列表功能
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse list(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                               @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        List<ExperienceUser> userList = experienceUserService.selectList(new EntityWrapper<ExperienceUser>()
                .like(StringUtils.isNotBlank(searchKey), "telphone", searchKey)
                .or()
                .like(StringUtils.isNotBlank(searchKey), "username", searchKey)
                .orderBy("create_time", false));
        PageInfo pageInfo = new PageInfo(userList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 详情
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse detail(Integer userId) {
        if (userId == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }

        ExperienceUser user = experienceUserService.selectById(userId);
        return ServerResponse.createBySuccess(user);
    }

    /**
     * 修改功能
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(ExperienceUser user) {
        if (user.getId() == null
                || StringUtils.isBlank(user.getUsername())) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }
        experienceUserService.updateById(user);
        return ServerResponse.createBySuccess();
    }

    /**
     * 修改状态
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "update_status.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse updateStatus(Integer userId, Integer status) {
        if (userId == null || status == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }

        ExperienceUser currentUser = new ExperienceUser();
        currentUser.setId(userId);
        currentUser.setIsRelation(status);

        experienceUserService.updateById(currentUser);
        return ServerResponse.createBySuccess();
    }

    /**
     * 删除功能
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "delete.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse delete(Integer userId) {
        if (userId == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }

        experienceUserService.deleteById(userId);
        return ServerResponse.createBySuccess();
    }


}
