package com.nsi.controller.backend;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ElectronicTicket;
import com.nsi.pojo.EventCollect;
import com.nsi.pojo.NsiEvent;
import com.nsi.service.ElectronicTicketService;
import com.nsi.service.EventCollectService;
import com.nsi.service.NsiEventService;
import com.nsi.vo.EventCollectOrderVo;
import com.nsi.vo.EventOrderVo;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


/**
 * 活动报名后台
 *
 * @author Luo Zhen
 */
@Controller
@RequestMapping("/manager/event_collect")
@Api(value = "/manager/event_collect", description = "通用活动系统———-活动报名表(后台)")
public class EventCollectManagerController {

    @Autowired
    private EventCollectService eventCollectService;
    @Autowired
    private ElectronicTicketService electronicTicketService;
    @Autowired
    private NsiEventService eventService;

    /**
     * 返回活动订单列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "activity_order_list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findActivityOrderList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                @RequestParam(value = "verify", required = false, defaultValue = "") Integer verify,
                                                @RequestParam(value = "status", required = false, defaultValue = "") Integer status,
                                                @RequestParam(value = "activeId", required = false, defaultValue = "") Integer artiveId,
                                                @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        List<EventCollectOrderVo> lists = eventCollectService.findEventList(verify, status, artiveId, searchKey);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 返回小程序
     *
     * @param pageNum
     * @param pageSize
     * @param activeId
     * @return
     */
    @RequestMapping(value = "activity_pay_list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findActivityPayList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                              @RequestParam(value = "activeId") Integer activeId,
                                              @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        List<EventOrderVo> eventList = eventCollectService.findByActivePayList(activeId, searchKey);
        PageInfo pageInfo = new PageInfo(eventList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 返回活动列表
     *
     * @param pageNum
     * @param pageSize
     * @param activeId
     * @return
     */
    @RequestMapping(value = "activity_list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findAcitvityList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                           @RequestParam(value = "verify", required = false, defaultValue = "") Integer verify,
                                           @RequestParam(value = "activeId", required = false, defaultValue = "") Integer[] activeId,
                                           @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<EventCollect> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(searchKey)) {
            if (StringUtils.isNumeric(searchKey)) {
                wrapper.like("telphone", searchKey);
            } else {
                wrapper.like("name", searchKey);
            }
        }
        wrapper.in((activeId != null), "active_id", Arrays.asList(activeId));
        wrapper.eq((verify != null), "verify", verify);
        wrapper.orderBy("create_time", false);
        List<EventCollect> lists = eventCollectService.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 返回该活动的电子门票信息
     *
     * @param activeId
     * @return
     */
    @RequestMapping(value = "ele_ticket.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回该活动的电子门票信息", httpMethod = "GET")
    public ServerResponse findAcitvityList(@RequestParam("activeId") Integer activeId) {
        List<ElectronicTicket> tickets = electronicTicketService.selectList(new EntityWrapper<ElectronicTicket>()
                .eq("active_id", activeId)
                .orderBy("create_time", false));
        return ServerResponse.createBySuccess(tickets);
    }


    /**
     * 返回用户已支付的订单列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "orderList_byPhone.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse list_byPhone(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<EventCollect> eventList = eventCollectService.selectList(new EntityWrapper<EventCollect>()
                .orderBy("create_time", false));
        PageInfo pageInfo = new PageInfo(eventList);
        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     * 返回该活动的报名用户列表——添加搜索条件
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "List_byEventId.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse List_byEventId(@RequestParam(value = "EventId") Integer EventId,
                                         @RequestParam(value = "orderStatus", required = false) Integer orderStatus,
                                         @RequestParam(value = "searchKey", required = false, defaultValue = "") String searchKey,
                                         @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<EventCollectOrderVo> list = eventCollectService.List_byEventId(EventId, orderStatus, searchKey);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     * 删除
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = "del_event_collect.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse delEventCollect(Integer eventId) {
        if (eventId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        eventCollectService.deleteById(eventId);
        return ServerResponse.createBySuccessMessage("删除用户报名信息成功");
    }

    /**
     * 审核通过
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = "/check_success.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse checkStatusSuccess(Integer eventId) {
        EventCollect eventCollect = new EventCollect();
        eventCollect.setId(eventId);
        eventCollect.setVerify(ResponseCode.PASS.getCode());

        eventCollectService.updateById(eventCollect);
        return ServerResponse.createBySuccess();
    }

    /**
     * 审核拒绝
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = "check_failed.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse checkStatusFailed(Integer eventId) {
        EventCollect eventCollect = new EventCollect();
        eventCollect.setId(eventId);
        eventCollect.setVerify(ResponseCode.REFUSE.getCode());

        eventCollectService.updateById(eventCollect);
        return ServerResponse.createBySuccess();
    }

    /**
     * 修改—增票类型
     *
     * @param collectId
     * @param ticketType
     * @return
     */
    @RequestMapping(value = "update_ticket_type", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse updateTicketType(
            @RequestParam("collectId") Integer collectId,
            @RequestParam("ticketType") String ticketType) {
        EventCollect eventCollect = new EventCollect();
        eventCollect.setId(collectId);
        eventCollect.setOption5(ticketType);

        eventCollectService.updateById(eventCollect);
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    /**
     * 活动详情—开票
     *
     * @param collectId
     * @return
     */
    @RequestMapping(value = "event_detail.do")
    @ResponseBody
    public ServerResponse eventDetail(@RequestParam Integer collectId) {
        // 查询用户报名信息
        EventCollect collectInfo = eventCollectService.selectById(collectId);
        if (collectInfo != null) {
            // 用户报名信息确定活动Id
            NsiEvent eventInfo = eventService.selectById(collectInfo.getActiveId());
            return ServerResponse.createBySuccess(eventInfo);
        }
        return ServerResponse.createByError();


    }


}
