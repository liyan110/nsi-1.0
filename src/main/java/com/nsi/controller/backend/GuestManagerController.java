package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Guest;
import com.nsi.service.IGuestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/11/27 10:20
 * @description: 演讲嘉宾详情表
 */
@RequestMapping("/manager/guest")
@Controller
@Api(value = "/manager/guest", description = "四库全书——嘉宾详情(后台)管理接口")
public class GuestManagerController {

    @Autowired
    private IGuestService iGuestService;

    /**
     * 添加方法
     *
     * @param guest
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse saveGuest(Guest guest) {
        return iGuestService.saveGuest(guest);
    }

    /**
     * 关键字返回详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping("/get_list.do")
    @ResponseBody
    @ApiOperation(value = "关键字返回详情", httpMethod = "GET")
    public ServerResponse getGuestList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                       @RequestParam(value = "searchKey", required = false) String searchKey) {
        return iGuestService.getGuestList(pageNum, pageSize, searchKey);
    }

    /**
     * 修改方法
     *
     * @param guest
     * @return
     */
    @RequestMapping("/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyGuest(Guest guest) {
        return iGuestService.modifyGuest(guest);
    }

    /**
     * 删除方法
     *
     * @param guestId
     * @return
     */
    @RequestMapping("/remove.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse removeGuest(Integer guestId) {
        return iGuestService.removeGuest(guestId);
    }

    /**
     * 根据id 返回详情
     *
     * @param guestId
     * @return
     */
    @RequestMapping("/get_guest_info.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse getGuestInfo(Integer guestId) {
        return iGuestService.getGuestInfo(guestId);
    }

    /**
     * 验证名字的数量
     *
     * @param name
     * @return
     */
    @RequestMapping("/check_name.do")
    @ResponseBody
    @ApiOperation(value = "验证姓名", httpMethod = "GET")
    public ServerResponse checkByName(@RequestParam(value = "name", required = false) String name) {
        return iGuestService.checkValidByName(name);

    }

}
