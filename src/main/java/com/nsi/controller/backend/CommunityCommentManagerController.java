package com.nsi.controller.backend;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.CommunityMessage;
import com.nsi.service.CommunityCommentService;
import com.nsi.service.PostCountCommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 社区-标准评论表 前端控制器
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
@Controller
@RequestMapping("/manager/communityComment")
@Api(value = "/manager/communityComment", description = "社区小程序——1级评论 后台 管理接口")
public class CommunityCommentManagerController {

    @Autowired
    private CommunityCommentService communityCommentService;

    @Autowired
    private PostCountCommonService postCountCommonService;



    @RequestMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "POST")
    public ServerResponse delete(int id){

        return communityCommentService.delete(id);
    }


    @RequestMapping("/verify_list")
    @ResponseBody
    @ApiOperation(value = "审核列表方法", httpMethod = "POST")
    public ServerResponse verify_list(String type,
                                      @RequestParam(defaultValue = "",required = false )String searchKey,
                                      @RequestParam(defaultValue = "1") int pageNum,
                                      @RequestParam(defaultValue = "10") int pageSize){
        return communityCommentService.verify_list(type,searchKey,pageNum,pageSize);
    }


    @RequestMapping("/verify_pass")
    @ResponseBody
    @ApiOperation(value = "审核通过方法", httpMethod = "POST")
    public ServerResponse verify_pass(int id){
        int i =communityCommentService.verify_pass(id);
        if(i==1){
            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("审核失败");
    }


    @RequestMapping("/verify_reject")
    @ResponseBody
    @ApiOperation(value = "审核拒绝方法", httpMethod = "POST")
    public ServerResponse verify_reject(int id){

        return communityCommentService.verify_reject(id);
    }

}
