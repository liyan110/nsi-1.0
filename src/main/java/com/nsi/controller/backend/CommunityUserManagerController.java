package com.nsi.controller.backend;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.CommunityCommentSon;
import com.nsi.pojo.CommunityUser;
import com.nsi.pojo.PostCategoryItem;
import com.nsi.service.CommunityCommentService;
import com.nsi.service.CommunityCommentSonService;
import com.nsi.service.CommunityUserService;
import com.nsi.service.PostCategoryItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Li Yan
 * @create 2019-12-18
 **/
@Controller
@RequestMapping("/manager/communityUser")
@Api(value = "/manager/communityUser", description = "社区小程序——用户(后台)管理接口")
public class CommunityUserManagerController {

    @Autowired
    private CommunityUserService communityUserService;

    @Autowired
    private PostCategoryItemService postCategoryItemService;

    @Autowired
    private CommunityCommentService communityCommentService;

    @Autowired
    private CommunityCommentSonService communityCommentSonService;


    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation(value = "查询用户列表方法", httpMethod = "POST")
    public ServerResponse list(@RequestParam(value = "orderBy", defaultValue = "create_time") String orderBy,
                               @RequestParam(value = "searchKey", required = false) String searchKey,
                               @RequestParam(value = "gradeSign", required = false, defaultValue = "") String gradeSign,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {
        return communityUserService.list(orderBy, searchKey, gradeSign, pageNum, pageSize);
    }


    @RequestMapping("/vip_VerifyPass")
    @ResponseBody
    @ApiOperation(value = "Vip用户 审核通过", httpMethod = "POST")
    public ServerResponse vip_VerifyPass(Integer id) {
        CommunityUser communityUser= communityUserService.selectById(id);
        communityUser.setGradeSign(5);
        return communityUserService.update(communityUser);
    }


    @RequestMapping("/panel_list")
    @ResponseBody
    @ApiOperation(value = "数据面板 用户列表方法", httpMethod = "POST")
    public ServerResponse panel_list() {

        return communityUserService.panel_list();
    }


    @RequestMapping("/panel_number")
    @ResponseBody
    @ApiOperation(value = "数据面板 数据统计", httpMethod = "POST")
    public ServerResponse panel_number() {
//        三个审核
        EntityWrapper<PostCategoryItem> postWrapper = new EntityWrapper<>();
        postWrapper.eq("is_check", 0);
        int post_count = postCategoryItemService.selectCount(postWrapper);

        EntityWrapper<CommunityComment> commentWrapper = new EntityWrapper<>();
        commentWrapper.eq("verify_sign", 0);
        int comment_count = communityCommentService.selectCount(commentWrapper);

        EntityWrapper<CommunityCommentSon> commentSonWrapper = new EntityWrapper<>();
        commentSonWrapper.eq("verify_sign", 0);
        int commentSon_count = communityCommentSonService.selectCount(commentSonWrapper);

        //总数
        EntityWrapper<CommunityUser> userWrapper = new EntityWrapper<>();
        int user_all_count = communityUserService.selectCount(userWrapper);

        EntityWrapper<PostCategoryItem> post_all_Wrapper = new EntityWrapper<>();
        post_all_Wrapper.eq("is_check", 1);
        int post_all_count = postCategoryItemService.selectCount(post_all_Wrapper);

        EntityWrapper<CommunityComment> comment_all_Wrapper = new EntityWrapper<>();
        comment_all_Wrapper.eq("verify_sign", 1);
        int comment_all_count = communityCommentService.selectCount(comment_all_Wrapper);

        EntityWrapper<CommunityCommentSon> commentSon_all_Wrapper = new EntityWrapper<>();
        commentSon_all_Wrapper.eq("verify_sign", 1);
        int commentSon_all_count = communityCommentSonService.selectCount(commentSon_all_Wrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("post_count", post_count);
        map.put("comment_count", comment_count);
        map.put("commentSon_count", commentSon_count);
        map.put("user_all_count", user_all_count);
        map.put("post_all_count", post_all_count);
        map.put("comment_all_count", comment_all_count + commentSon_all_count);

        return ServerResponse.createBySuccess(map);
    }


}
