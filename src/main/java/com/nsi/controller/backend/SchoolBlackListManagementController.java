package com.nsi.controller.backend;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ForeignTeacher;
import com.nsi.service.ISchoolBlackListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/30 11:31
 * @description:
 */
@RequestMapping("/manager/foreign")
@Controller
@Api(value = "/manager/foreign", description = "四库全书——黑外教(后台)管理接口")
public class SchoolBlackListManagementController {

    @Autowired
    private ISchoolBlackListService iSchoolBlackListService;

    /**
     * 返回未审核列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/get_review_list.do")
    @ResponseBody
    @ApiOperation(value = "审核列表", httpMethod = "GET")
    public ServerResponse getReviewList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.getReviewList(pageNum, pageSize);
    }

    /**
     * 返回审核通过列表
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/get_list.do")
    @ResponseBody
    @ApiOperation(value = "返回审核通过列表", httpMethod = "GET")
    public ServerResponse getForeignTeacherList(@RequestParam(value = "searchKey", required = false) String searchKey,
                                                @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.getForeignList(searchKey, pageNum, pageSize);
    }

    /**
     * 返回审核通过列表
     *
     * @param foreignTeacher
     * @return
     */
    @RequestMapping("/update_foreign.do")
    @ResponseBody
    @ApiOperation(value = "修改审核状态列表", httpMethod = "GET")
    public ServerResponse modifyForeign(ForeignTeacher foreignTeacher) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.modifyForeignInfo(foreignTeacher);
    }

    /**
     * 根据id 删除
     *
     * @param foreignId
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "根据id删除", httpMethod = "GET")
    public ServerResponse removeForeignTeacherInfo(Integer foreignId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.removeForeignInfo(foreignId);
    }


    /**
     * 根据id 返回详情
     *
     * @param foreignId
     * @return
     */
    @RequestMapping(value = "/get_foreign_info.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse getForeignTeacherInfo(Integer foreignId,
                                                @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iSchoolBlackListService.getForeignTeacherInfo(foreignId, pageNum, pageSize);
    }


}
