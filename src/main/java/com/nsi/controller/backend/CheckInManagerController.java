package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.CheckIn;
import com.nsi.service.ICheckInService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@RequestMapping("/manager/checkIn")
@Controller
@Api(value = "/manager/checkIn", description = "小程序——签到(后台)接口")
public class CheckInManagerController {

    @Autowired
    private ICheckInService iCheckInService;

    /**
     * 添加操作
     *
     * @param checkIn
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse<String> add(CheckIn checkIn) {
        //切换数据库
        return iCheckInService.saveOn(checkIn);
    }

    /**
     * 返回list
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "获取list方法", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "type", defaultValue = "0") int type,
                               @RequestParam(value = "keyword", required = false) String keyword,
                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //切换数据库
        return iCheckInService.list(type, keyword, pageNum, pageSize);
    }

    /**
     * 修改方法
     *
     * @param checkIn
     * @return
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse update(CheckIn checkIn) {
        //切换数据库
        return iCheckInService.updateOn(checkIn);
    }

    /**
     * 删除方法
     *
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "批量删除方法", httpMethod = "GET")
    public ServerResponse delete(String checkIds) {
        //切换数据库
        return iCheckInService.delete(checkIds);
    }

    /**
     * 根据token返回用户详情
     *
     * @param token
     * @return
     */
    @RequestMapping(value = "/get_info.do")
    @ResponseBody
    @ApiOperation(value = "根据token返回用户详情", httpMethod = "GET")
    public ServerResponse getInfo(String token) {
        return iCheckInService.getCheckInfo(token);
    }

    /**
     * 根据token修改用户签到时间
     *
     * @param token
     * @return
     */
    @RequestMapping(value = "/check_in.do")
    @ResponseBody
    @ApiOperation(value = "根据token修改用户签到时间", httpMethod = "GET")
    public ServerResponse updateCheckIn(String token) {
        return iCheckInService.updateCheckInTime(token);
    }

}
