package com.nsi.controller.backend;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Comments;
import com.nsi.service.ICommentsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/11/28 10:09
 * @description: 评论功能
 */
@RequestMapping("/manager/comments")
@Controller
@Api(value = "/manager/comments", description = "四库全书——评论详情(后台)管理接口")
public class CommentsManagerController {


    @Autowired
    private ICommentsService iCommentsService;

    /**
     * 添加方法
     *
     * @param comments
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse saveGuest(Comments comments) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iCommentsService.saveComments(comments);
    }

    /**
     * 返回详情
     *
     * @param guestId
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "GET")
    public ServerResponse getCommentsList(Integer guestId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iCommentsService.selectByGuestId(guestId);
    }

    /**
     * 修改方法
     *
     * @param comments
     * @return
     */
    @RequestMapping("/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyComments(Comments comments) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iCommentsService.modifyConmments(comments);
    }

    /**
     * 根据评论类型,返回详情
     *
     * @param guestId
     * @param type
     * @return
     */
    @RequestMapping("/get_comments_list.do")
    @ResponseBody
    @ApiOperation(value = "根据评论类型,返回详情", httpMethod = "GET")
    public ServerResponse getCommentsByType(@RequestParam("guestId") Integer guestId,
                                            @RequestParam("type") String type) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iCommentsService.getCommentsByType(guestId, type);
    }
}
