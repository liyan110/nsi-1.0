package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.service.ISysExceptionLogService;
import com.nsi.service.IVisitSchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Li Yan
 * @create 2019-10-21
 **/
@RequestMapping("/SysExceptionLog")
@Controller
@Api(value = "/SysExceptionLog", description = "错误上报管理接口")
public class SysExceptionLogController {

    @Autowired
    private ISysExceptionLogService iSysExceptionLogService;

    @RequestMapping(value = "list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse list() {
        return iSysExceptionLogService.list();
    }


    @RequestMapping(value = "delete.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse delete(Integer id) {
        return iSysExceptionLogService.delete(id);
    }

}
