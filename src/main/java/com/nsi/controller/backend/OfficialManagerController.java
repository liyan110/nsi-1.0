package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Configure;
import com.nsi.service.IConfigureService;
import com.wordnik.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/manager/official")
@Controller
@Api(value = "/manager/official", description = "后台管理——配置")
public class OfficialManagerController {

    @Autowired
    private IConfigureService iConfigureService;

    /**
     * 修改方法
     *
     * @param configure
     * @return
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse update(Configure configure) {
        return iConfigureService.update(configure);
    }

    /**
     * 返回list
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "获取list方法", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "type", defaultValue = "0") String type) {
        return iConfigureService.list(type);
    }
}
