package com.nsi.controller.backend;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.EntryAudit;
import com.nsi.service.EntryAuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-16
 */
@Controller
@RequestMapping("/manager/entryAudit/")
@Api(value = "/manager/entryAudit/", description = "官网——增票审核(后台)管理接口")
public class EntryAuditManagerController {

    @Autowired
    EntryAuditService entryAuditService;

    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse updateEntryAudit(EntryAudit entryAudit) {
        return entryAuditService.update(entryAudit);
    }

    @RequestMapping(value = "delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteById(Integer entryId) {
        entryAuditService.deleteById(entryId);
        return ServerResponse.createBySuccess("删除成功");
    }

    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id返回详情", httpMethod = "GET")
    public ServerResponse findById(Integer entryId) {
        return entryAuditService.selectById(entryId);
    }

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回列表", httpMethod = "GET")
    public ServerResponse findList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                   @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                   EntryAudit entryAudit) {
        return entryAuditService.selectList(pageNum, pageSize, entryAudit);
    }

    @RequestMapping(value = "success_audit.do")
    @ResponseBody
    @ApiOperation(value = "审核通过", httpMethod = "GET")
    public ServerResponse successAudit(EntryAudit entryAudit) {
        return entryAuditService.updateSuccessAudit(entryAudit);
    }

}
