package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Configure;
import com.nsi.service.IConfigureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2018-08-07
 **/
@RequestMapping("/manager/configure")
@Controller
@Api(value = "/manager/configure", description = "官网——文章模板(后台)管理接口")
public class ConfigureManagerController {

    @Autowired
    private IConfigureService iConfigureService;

    /**
     * 文章修改方法
     *
     * @param templateHtml
     * @return
     */
    @RequestMapping(value = "/update_template.do")
    @ResponseBody
    @ApiOperation(value = "文章模板修改方法", httpMethod = "POST")
    public ServerResponse update(String templateHtml) {
        return iConfigureService.updateTemplate(templateHtml);
    }

    /**
     * 新增——配置项
     *
     * @param configure
     * @return
     */
    @RequestMapping(value = "add_configure_info.do")
    @ResponseBody
    @ApiOperation(value = "配置项——新增", httpMethod = "POST")
    public ServerResponse addConfigure(Configure configure) {
        return iConfigureService.insertConfigure(configure);
    }

    /**
     * 根据关键字 返回详情
     *
     * @param typeName
     * @return
     */
    @RequestMapping(value = "get_configure_list.do")
    @ResponseBody
    @ApiOperation(value = "配置项——根据关键类型返回详情", httpMethod = "GET")
    public ServerResponse getConfigureList(String typeName) {
        return iConfigureService.getConfigureList(typeName);
    }

    /**
     * 根据关键字 修改
     *
     * @param configure
     * @return
     */
    @RequestMapping(value = "modify_configure.do")
    @ResponseBody
    @ApiOperation(value = "配置项——修改", httpMethod = "POST")
    public ServerResponse modifyConfigure(Configure configure) {
        return iConfigureService.modifyConfigure(configure);
    }


}
