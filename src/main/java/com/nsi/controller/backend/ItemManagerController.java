package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Item;
import com.nsi.service.IItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 项目详情表
 *
 * @author Luo Zhen
 * @create 2018-12-27 15:14
 */
@Controller
@RequestMapping("/manager/items")
@Api(value = "/manager/items", description = "四库全书——项目财务库(后台)信息表")
public class ItemManagerController {

    @Autowired
    private IItemService iItemService;

    /**
     * 添加方法
     *
     * @param item
     * @return
     */
    @RequestMapping(value = "/add_item.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addItemCat(Item item) {
        try {
            return iItemService.saveItem(item);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 根据id 返回详情
     *
     * @return
     */
    @RequestMapping("/get_item.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse findItemCatList(Integer itemId) {
        return iItemService.findItemById(itemId);
    }

    /**
     * 修改方法
     *
     * @param item
     * @return
     */
    @RequestMapping("/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyItemCat(Item item) {
        try {
            return iItemService.modifyItem(item);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 返回详情分页
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping("/get_item_list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情,分页", httpMethod = "GET")
    public ServerResponse findItemList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                       @RequestParam(value = "searchKey", required = false) String searchKey) {
        return iItemService.findItemList(pageNum, pageSize, searchKey);
    }

    /**
     * 删除方法
     *
     * @param itemId
     * @return
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse removeItem(Integer itemId) {
        try {
            return iItemService.removeItem(itemId);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

}
