package com.nsi.controller.backend;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ActivityTickets;
import com.nsi.service.ActivityTicketsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author luo Zhen
 * @since 2020-03-26
 */
@Controller
@RequestMapping("/manager/activity_tickets")
@Api(value = "/manager/activity_tickets", description = "官网——活动门票(后台)管理接口")
public class ActivityTicketsManagerController {


    @Autowired
    private ActivityTicketsService activityTicketsService;

    /**
     * 根据活动Id 返回票详情
     *
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findOne(Integer id) {
        if (id == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        ActivityTickets ticket = activityTicketsService.selectById(id);
        return ServerResponse.createBySuccess(ticket);
    }

    /**
     * 添加门票信息
     *
     * @param activityTickets
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse addActivityTicket(ActivityTickets activityTickets) {
        if (activityTickets.getActiveId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        activityTickets.setStatus(Const.SHOW_STATUS);
        activityTicketsService.insert(activityTickets);
        return ServerResponse.createBySuccessMessage("添加成功！");
    }

    /**
     * 修改门票信息
     *
     * @param activityTickets
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse updateActivityTicket(ActivityTickets activityTickets) {
        if (activityTickets.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        activityTicketsService.updateById(activityTickets);
        return ServerResponse.createBySuccessMessage("修改成功！");
    }

    /**
     * 根据活动Id 删除门票信息
     *
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse deleteTickets(Integer id) {
        if (id == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        activityTicketsService.deleteById(id);
        return ServerResponse.createBySuccessMessage("删除门票信息！");
    }

    /**
     * 返回门票信息列表
     *
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse deleteTickets(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                        Integer activeId) {
        PageHelper.startPage(pageNum, pageSize);
        List<ActivityTickets> tickets = activityTicketsService.selectList(new EntityWrapper<ActivityTickets>()
                .eq(activeId != null, "active_id", activeId)
                .orderBy("create_time", false));
        PageInfo pageInfo = new PageInfo<>(tickets);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
