package com.nsi.controller.backend;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseList;
import com.nsi.service.ICourseCategoryService;
import com.nsi.service.ICourseListService;
import com.nsi.vo.CourseItemVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程合集后台
 *
 * @author Luo Zhen
 * @create 2019-02-12 12:34
 */
@RequestMapping("/manager/courseList")
@Controller
public class CourseListManagerController {

    @Autowired
    ICourseListService iCourseListService;
    @Autowired
    ICourseCategoryService iCourseCategoryService;

    /**
     * 添加方法
     *
     * @param courseList
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    public ServerResponse saveGuest(CourseList courseList) {
        if (StringUtils.isBlank(courseList.getListTitle()) || StringUtils.isBlank(courseList.getSyllabus())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            iCourseListService.saveCourseList(courseList);
            return ServerResponse.createBySuccess();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(201, "添加失败");
    }

    /**
     * 课程列表
     *
     * @param pageNum
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    public ServerResponse getCourseListItem(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<CourseItemVo> courseItemVos = new ArrayList<>();
        List<CourseList> courseLists = iCourseListService.selectList(null);
        for (CourseList course : courseLists) {
            CourseItemVo courseListVo = this.assertCourseItemVo(course);
            courseItemVos.add(courseListVo);
        }
        PageInfo pageInfo = new PageInfo(courseItemVos);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 组装VO
     *
     * @param course
     * @return
     */
    private CourseItemVo assertCourseItemVo(CourseList course) {
        CourseItemVo courseItemVo = new CourseItemVo();
        BeanUtils.copyProperties(course, courseItemVo);
        courseItemVo.setParentId(Const.PARENT_ID);
        List<CourseItemVo> childrens
                = iCourseCategoryService.selectByListId(course.getListId(), null);
        childrens.forEach(children -> {
            children.setParentId(Const.CHILDREN_ID);
        });
        courseItemVo.setChildren(childrens);
        return courseItemVo;
    }

    /**
     * 修改方法
     *
     * @param courseList 返回详情
     * @return
     */
    @RequestMapping("/modify.do")
    @ResponseBody
    public ServerResponse modifyComments(CourseList courseList) {
        iCourseListService.updateCourseList(courseList);
        return ServerResponse.createBySuccess("修改成功");
    }

    /**
     * 删除方法
     *
     * @param courseListId
     * @return
     */
    @RequestMapping("/del.do")
    @ResponseBody
    public ServerResponse deleteCourseList(@RequestParam("courseListId") Integer courseListId) {
        try {
            // 删除课程合集
            iCourseListService.deleteCourseList(courseListId);
            // 删除课程详情表listId 的子数据
            iCourseCategoryService.deleteCourseCategoryByListId(courseListId);
            return ServerResponse.createBySuccess();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "删除失败");
    }

    /**
     * 根据id 返回详情
     *
     * @param courseListId
     * @return
     */
    @RequestMapping("/detail.do")
    @ResponseBody
    public ServerResponse queryCourseList(Integer courseListId) {
        CourseList courseItem = iCourseListService.findCourseListById(courseListId);
        return ServerResponse.createBySuccess(courseItem);
    }

}
