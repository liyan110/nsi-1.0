package com.nsi.controller.backend;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ActivityTickets;
import com.nsi.pojo.ElectronicTicket;
import com.nsi.service.ElectronicTicketService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author luo Zhen
 * @since 2020-04-01
 */
@Controller
@RequestMapping("/manager/ele_ticket")
@Api(value = "/manager/ele_ticket", description = "官网——电子门票(后台)管理接口")
public class ElectronicTicketManagerController {

    @Autowired
    private ElectronicTicketService electronicTicketService;

    /**
     * 根据活动Id 返回票详情
     *
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "/根据活动Id返回电子票详情", httpMethod = "GET")
    public ServerResponse findOne(Integer id) {
        if (id == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        ElectronicTicket ticket = electronicTicketService.selectById(id);
        return ServerResponse.createBySuccess(ticket);
    }

    /**
     * 添加门票信息
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "/添加电子门票信息", httpMethod = "POST")
    public ServerResponse addEleTicket(ElectronicTicket ticket) {
        if (ticket.getActiveId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        electronicTicketService.insert(ticket);
        return ServerResponse.createBySuccessMessage("添加成功！");
    }


    /**
     * 修改门票信息
     *
     * @param ticket
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "/修改门票信息", httpMethod = "POST")
    public ServerResponse updateEleTicket(ElectronicTicket ticket) {
        if (ticket.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        electronicTicketService.updateById(ticket);
        return ServerResponse.createBySuccessMessage("修改成功！");
    }

    /**
     * 根据活动Id 删除门票信息
     *
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "/根据Id删除门票信息", httpMethod = "GET")
    public ServerResponse deleteTickets(Integer id) {
        if (id == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        electronicTicketService.deleteById(id);
        return ServerResponse.createBySuccessMessage("删除门票信息！");
    }

    /**
     * 返回门票信息列表
     *
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "/返回门票信息列表", httpMethod = "GET")
    public ServerResponse getEleTickets(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                        Integer activeId) {
        PageHelper.startPage(pageNum, pageSize);
        List<ElectronicTicket> tickets = electronicTicketService.selectList(new EntityWrapper<ElectronicTicket>()
                .eq(activeId != null, "active_id", activeId)
                .orderBy("create_time", false));
        PageInfo pageInfo = new PageInfo<>(tickets);
        return ServerResponse.createBySuccess(pageInfo);
    }

}
