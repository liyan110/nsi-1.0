package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.service.IVerifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/11/20 10:30
 * @description: 后台审核功能模块
 */
@RequestMapping("/manager/verify")
@Controller
@Api(value = "/manager/verify", description = "四库全书——审核(后台)管理接口")
public class VerifyManagementController {

    @Autowired
    private IVerifyService iVerifyService;


    /**
     * 返回学校库和机构库的未审核信息数目
     *
     * @return
     */
    @RequestMapping(value = "/verify_notification.do")
    @ResponseBody
    @ApiOperation(value = "返回学校库和机构库的未审核信息数目", httpMethod = "GET")
    public Map<String, Object> getSchoolAndInstitutionInfo() {
        return iVerifyService.getVerifyNotificationInfo();
    }


    /**
     * 机构库————未审核信息详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/verify_In_search.do")
    @ResponseBody
    @ApiOperation(value = "返回机构库未审核信息详情", httpMethod = "GET")
    public ServerResponse getInstitutionInfo(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iVerifyService.getVerifyInstitutionInfo(pageNum, pageSize, ResponseCode.CHECK.getCode());
    }

    /**
     * 机构库————修改信息通过
     *
     * @param institutionId
     * @return
     */
    @RequestMapping(value = "/verify_pass.do")
    @ResponseBody
    @ApiOperation(value = "修改机构库信息通过", httpMethod = "GET")
    public ServerResponse modifyVerifyPassStatus(Integer institutionId) {
        return iVerifyService.modifyInstitutionVerifyStatus(institutionId, ResponseCode.PASS.getCode());
    }

    /**
     * 机构库————修改审核信息不通过
     *
     * @param institutionId
     * @return
     */
    @RequestMapping(value = "/verify_failed.do")
    @ResponseBody
    @ApiOperation(value = "修改机构库信息不通过", httpMethod = "GET")
    public ServerResponse modifyVerifyFailedStatus(Integer institutionId) {
        return iVerifyService.modifyInstitutionVerifyStatus(institutionId, ResponseCode.FAILED.getCode());
    }

    /**
     * 学校库————未审核信息详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/verify_school_search.do")
    @ResponseBody
    @ApiOperation(value = "返回学校库未审核信息详情", httpMethod = "GET")
    public ServerResponse getSchoolInfo(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iVerifyService.getNoVerifySchoolInfo(pageNum, pageSize, String.valueOf(ResponseCode.CHECK.getCode()));
    }

    /**
     * 学校库————修改信息通过
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/verify_school_pass.do")
    @ResponseBody
    @ApiOperation(value = "修改学校库信息通过", httpMethod = "GET")
    public ServerResponse modifyVerifyPassStatusBySchool(Integer schoolId) {
        return iVerifyService.modifySchoolVerifyStatus(schoolId, String.valueOf(ResponseCode.PASS.getCode()));
    }

    /**
     * 学校库————修改审核信息不通过
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/verify_school_failed.do")
    @ResponseBody
    @ApiOperation(value = "修改学校库信息不通过", httpMethod = "GET")
    public ServerResponse modifyVerifyFailedStatusBySchool(Integer schoolId) {
        return iVerifyService.modifySchoolVerifyStatus(schoolId, String.valueOf(ResponseCode.FAILED.getCode()));
    }


}
