package com.nsi.controller.backend;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.service.SchoolSubmitService;
import com.nsi.vo.SchoolSubmitVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/manager/verify/school")
@Controller
@Api(value = "/manager/verify/school", description = "四库全书————新学校库(后台)审核管理接口")
public class SchoolVerifyManagerController {

    @Autowired
    SchoolSubmitService schoolSubmitService;

    @RequestMapping("/list.do")
    @ResponseBody
    public ServerResponse list(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                               @RequestParam(value = "verify", required = false, defaultValue = "0") String verify) {
        PageHelper.startPage(pageNum, pageSize);
        List<SchoolSubmitVo> lists = schoolSubmitService.findListByVerify(verify);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
