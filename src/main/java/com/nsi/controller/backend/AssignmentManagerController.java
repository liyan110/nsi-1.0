package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.service.IAssignmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/5 11:45
 * @description: 课堂作业(后台)
 */
@RequestMapping("/manager/assignment")
@Controller
@Api(value = "/manager/assignment", description = "新学说课堂——作业(后台)接口")
public class AssignmentManagerController {

    @Autowired
    private IAssignmentService iAssignmentService;

    /**
     * 后台功能(返回学生作业列表-未审核)D
     *
     * @return
     */
    @RequestMapping("/query_student_assignment.do")
    @ResponseBody
    @ApiOperation(value = "返回学生作业列表-未审核", httpMethod = "GET")
    public ServerResponse queryStudent(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                       @RequestParam(value = "courseId", required = false) String courseId,
                                       @RequestParam(value = "validCode", defaultValue = "") Integer validCode) {
        return iAssignmentService.queryStudentAssignment(pageNum, pageSize, courseId, validCode);
    }

    /**
     * 后台功能(修改审核状态)
     *
     * @param assignmentId
     * @return
     */
    @RequestMapping("/set_assignment_status.do")
    @ResponseBody
    @ApiOperation(value = "修改审核状态", httpMethod = "GET")
    public ServerResponse setAssignmentStatus(Integer assignmentId) {
        return iAssignmentService.setStudentAssignmentStutus(assignmentId);
    }

    /**
     * 根据id 删除作业
     *
     * @param assignmentId
     * @return
     */
    @RequestMapping("/remove_assignment.do")
    @ResponseBody
    @ApiOperation(value = "根据id 删除作业", httpMethod = "GET")
    public ServerResponse removeAssignmentById(Integer assignmentId) {
        return iAssignmentService.removeAssignment(assignmentId);
    }


}
