package com.nsi.controller.backend;

import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.SchoolNew;
import com.nsi.service.INewSchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author Luo Zhen
 * @create 2019-07-22 11:08
 */
@RequestMapping("/manager/new/school")
@Controller
public class SchoolNewManagerController {

    @Autowired
    private INewSchoolService iNewSchoolService;

    /**
     * 后台修改
     *
     * @param schoolNew
     * @return
     */
    @RequestMapping(value = "/insert.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse insertSchoolNew(SchoolNew schoolNew) {
        schoolNew.setVerifySign(1);
        schoolNew.setIsUseNuohui(Const.NUM_DEFAULT_VALUE);
        schoolNew.setCreateTime(new Date());
        iNewSchoolService.insertSchool(schoolNew);
        return ServerResponse.createBySuccess("添加学校成功", schoolNew);
    }

    /**
     * 后台修改
     *
     * @param schoolNew
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse updateSchool(SchoolNew schoolNew) {
        return iNewSchoolService.modifyNewSchool(schoolNew);
    }

    /**
     * 后台删除
     *
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse updateSchool(Integer schoolId) {
        return iNewSchoolService.deleteById(schoolId);
    }


    /**
     * 根据学校模板Id生成学校详情文件
     *
     * @return
     */
    @RequestMapping(value = "/rest_all_school_template.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse updateSchoolTemplate() {
        return iNewSchoolService.updateTemplate();
    }

    /**
     * 修改审核状态
     *
     * @param schoolId
     * @param status
     * @return
     */
    @RequestMapping(value = "/verify_status.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse verifyStatus(Integer schoolId, Integer status) {
        return iNewSchoolService.verifyStatus(schoolId, status);
    }

}



