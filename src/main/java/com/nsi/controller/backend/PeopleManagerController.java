package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.People;
import com.nsi.service.IPeopleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/21 13:36
 * @description: 人员名信片
 */
@RequestMapping("/manager/people")
@Controller
@Api(value = "/manager/people", description = "四库全书——人员名信片(后台)管理接口")
public class PeopleManagerController {

    @Autowired
    private IPeopleService iPeopleService;

    /**
     * 验证参数
     *
     * @param str
     * @param type
     * @return
     */
    @RequestMapping(value = "/check_valid.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "验证参数", httpMethod = "POST")
    public ServerResponse checkValid(String str, String type) {
        return iPeopleService.checkValid(str, type);
    }

    /**
     * 后台查询根据关键字带分页
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "后台查询根据关键字带分页", httpMethod = "GET")
    public ServerResponse getPeopleList(@RequestParam(value = "searchKey", required = false) String searchKey,
                                        @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iPeopleService.getPeopleList(searchKey, pageNum, pageSize);
    }

    /**
     * 后台高级查询根据类型
     *
     * @param searchKey
     * @param type
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/advanced_query.do")
    @ResponseBody
    @ApiOperation(value = "后台查询根据类型", httpMethod = "GET")
    public ServerResponse getAdvancedPeopleListByType(@RequestParam(value = "searchKey", required = false) String searchKey,
                                                      @RequestParam(value = "type", defaultValue = "1") int type,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iPeopleService.getPeopleListByType(searchKey, type, pageNum, pageSize);
    }

    /**
     * 后台添加操作
     *
     * @param people
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加操作", httpMethod = "POST")
    public ServerResponse addPeople(People people) {
        try {
            return iPeopleService.addPeople(people);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 后台根据id返回详情
     *
     * @param peopleId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id返回详情", httpMethod = "GET")
    public ServerResponse getPeopleList(Integer peopleId) {
        return iPeopleService.findAll(peopleId);
    }

    /**
     * 后台修改操作
     *
     * @param people
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改操作", httpMethod = "POST")
    public ServerResponse modifyPeople(People people) {
        try {
            return iPeopleService.modifyPeople(people);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 删除功能
     *
     * @param peopleId
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "后台添加操作", httpMethod = "GET")
    public ServerResponse removePeople(Integer peopleId) {
        return iPeopleService.removePeople(peopleId);
    }


}
