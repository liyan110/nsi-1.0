package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.ItemCat;
import com.nsi.service.IItemCatService;
import com.nsi.service.IItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 项目财务信息表
 *
 * @author Luo Zhen
 * @create 2018-12-27 15:14
 */
@Controller
@RequestMapping("/manager/item")
@Api(value = "/manager/item", description = "四库全书——项目财务库(后台)信息表")
public class ItemCatManagerController {

    @Autowired
    private IItemCatService iItemCatService;
    @Autowired
    private IItemService iItemService;

    /**
     * 添加方法
     *
     * @param itemCat
     * @return
     */
    @RequestMapping(value = "/add_item_cat.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addItemCat(ItemCat itemCat) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        try {
            return iItemCatService.saveItemCat(itemCat);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 根绝父类id 返回详情
     *
     * @param pageNum
     * @param pageSize
     * @param parentId
     * @return
     */
    @RequestMapping(value = "/get_item_cat_list.do")
    @ResponseBody
    @ApiOperation(value = "根据父类id 返回详情", httpMethod = "GET")
    public ServerResponse findItemCatList(@RequestParam(defaultValue = "1") int pageNum,
                                          @RequestParam(defaultValue = "10") int pageSize,
                                          @RequestParam(defaultValue = "0") Integer parentId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iItemCatService.findItemCatById(pageNum, pageSize, parentId);
    }

    /**
     * 根据父类id 修改余额
     *
     * @return
     */
    @RequestMapping(value = "/modify_item_cat.do")
    @ResponseBody
    @ApiOperation(value = "根据父类id 修改余额", httpMethod = "GET")
    public ServerResponse modifyItemCatById(Integer itemId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        try {
            return iItemCatService.modifyItemCatById(itemId);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 修改方法
     *
     * @param itemCat
     * @return
     */
    @RequestMapping(value = "/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyItemCat(ItemCat itemCat) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        try {
            return iItemCatService.modifyItemCat(itemCat);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 删除方法
     *
     * @param itemCatId
     * @return
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse removeItemCat(Integer itemCatId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        try {
            return iItemCatService.removeItemCat(itemCatId);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }

    }

    /**
     * 返回item 表详情
     *
     * @return
     */
    @RequestMapping("/get_item_list.do")
    @ResponseBody
    @ApiOperation(value = "返回item列表", httpMethod = "GET")
    public ServerResponse getItemList() {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iItemService.findItemList();
    }

}
