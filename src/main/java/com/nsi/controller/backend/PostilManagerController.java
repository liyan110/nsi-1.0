package com.nsi.controller.backend;

import com.aliyuncs.utils.StringUtils;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Postil;
import com.nsi.service.IPostilService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-06-04 14:45
 */
@RequestMapping("/manager/postil/")
@Controller
@Api(value = "/manager/postil", description = "四库全书——批注(后台)管理接口")
public class PostilManagerController {

    @Autowired
    private IPostilService iPostilService;

    @RequestMapping(value = "/save.do")
    @ResponseBody
    @ApiOperation(value = "添加批注信息", httpMethod = "POST")
    public ServerResponse save(Postil postil) {
        ServerResponse res = this.checkValue(postil);
        if (!res.isSuccess()) {
            return res;
        }
        iPostilService.save(postil);
        return ServerResponse.createBySuccess("添加成功");
    }

    private ServerResponse checkValue(Postil postil) {
        if (StringUtils.isEmpty(postil.getType())) {
            return ServerResponse.createByErrorMessage("类型不能为空");
        }
        if (postil.getSchoolId() == null) {
            return ServerResponse.createByErrorMessage("学校id不能为空");
        }
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改批注信息", httpMethod = "POST")
    public ServerResponse modify(Postil postil) {
        iPostilService.modify(postil);
        return ServerResponse.createBySuccess("修改成功");
    }

    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "删除批注信息", httpMethod = "GET")
    public ServerResponse modify(@RequestParam("postilId") Integer postilId) {
        iPostilService.delete(postilId);
        return ServerResponse.createBySuccess("删除成功");
    }

    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "修改批注信息", httpMethod = "GET")
    public ServerResponse list(@RequestParam("schoolId") Integer schoolId) {
        List<Postil> postils = iPostilService.list(schoolId);
        return ServerResponse.createBySuccess(postils);
    }


}
