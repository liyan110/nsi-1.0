package com.nsi.controller.backend;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.TitleForm;
import com.nsi.service.TitleFormService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2020-03-23
 */
@Controller
@RequestMapping("/manager/title_form")
@Api(value = "/manager/title_form", description = "四库全书———-标题活动表(后台)")
@Slf4j
public class TitleFormManagerController {

    @Autowired
    private TitleFormService titleFormService;

    /**
     * 添加活动标题
     *
     * @param titleForm
     * @return
     */
    @RequestMapping(value = "/add_title_form.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加活动标题信息", httpMethod = "POST")
    public ServerResponse addTitleForm(TitleForm titleForm) {
        if (titleForm.getActiveId() == null) {
            return ServerResponse.createByErrorMessage("活动ID不能为空!");
        }
        if (titleForm.getExpriyTime() == null) {
            return ServerResponse.createByErrorMessage("活动截至时间不能为空！");
        }
        int result = titleFormService.selectCount(new EntityWrapper<TitleForm>()
                .eq("active_id", titleForm.getActiveId()));

        if (result > 0) {
            return ServerResponse.createByErrorMessage("该活动已存在!");
        }

        titleForm.setCreateTime(new Date());
        titleFormService.insert(titleForm);
        return ServerResponse.createBySuccessMessage("添加活动标题成功!");
    }

    /**
     * 修改活动标题
     *
     * @param titleForm
     * @return
     */
    @RequestMapping(value = "/update_title_form.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改活动标题", httpMethod = "POST")
    public ServerResponse updateTItleForm(TitleForm titleForm) {
        if (titleForm.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }

        titleFormService.updateById(titleForm);
        return ServerResponse.createBySuccessMessage("修改活动标题成功！");
    }

    /**
     * 根据Id返回活动标题详情
     *
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据Id返回活动标题详情", httpMethod = "GET")
    public ServerResponse detail(Integer activeId) {
        if (activeId == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        TitleForm titleForm = titleFormService.selectOne(new EntityWrapper<TitleForm>()
                .eq("active_id", activeId));
        return ServerResponse.createBySuccess(titleForm);
    }


}
