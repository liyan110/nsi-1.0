package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.service.IIndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-03-04 16:22
 */
@RequestMapping("/manager/index")
@Controller
@Api(value = "/manager/index", description = "官网——主页(后台)管理接口")
public class IndexManagerController {

    @Autowired
    private IIndexService iIndexService;

    /**
     * 返回当日订单数据统计
     *
     * @return
     */
    @RequestMapping("/get_today_list.do")
    @ResponseBody
    @ApiOperation(value = "返回当日订单数据统计", httpMethod = "GET")
    public ServerResponse getIndexList() {
        try {
            return iIndexService.getDaysStatistics();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 返回本周订单数据统计
     *
     * @return
     */
    @RequestMapping("/get_week_list.do")
    @ResponseBody
    @ApiOperation(value = "返回本周订单数据统计", httpMethod = "GET")
    public ServerResponse getWeekList() {
        try {
            return iIndexService.getWeekStatistics();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 返回本月订单数据统计
     *
     * @return
     */
    @RequestMapping("/get_month_list.do")
    @ResponseBody
    @ApiOperation(value = "返回本月订单数据统计", httpMethod = "GET")
    public ServerResponse getMonthList() {
        try {
            return iIndexService.getMonthStatistics();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 返回7天内 每天统计数据
     *
     * @return
     */
    @RequestMapping("/get_seven_days_list.do")
    @ResponseBody
    @ApiOperation(value = "返回7天内 每天统计数据", httpMethod = "GET")
    public ServerResponse getSevenDaysListCount() {
        try {
            return iIndexService.getSevenDaysListStatistics();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 返回今年总计收入
     *
     * @return
     */
    @RequestMapping("/get_year_list.do")
    @ResponseBody
    @ApiOperation(value = "返回今年总计收入和课程和书店的成交单数", httpMethod = "GET")
    public ServerResponse getYearListCount(@RequestParam(value = "year", required = false, defaultValue = "2019") String year) {
        try {
            return iIndexService.getYearListStatistics(year);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }


}
