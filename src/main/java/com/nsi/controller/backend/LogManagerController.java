package com.nsi.controller.backend;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Log;
import com.nsi.service.ILogService;
import com.nsi.vo.UserPayInfoVo;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-14
 */
@RequestMapping("/manager/Log")
@Controller
@Api(value = "/manager/Log", description = "日志管理接口")
public class LogManagerController {

    @Autowired
    private ILogService iLogService;

    /**
     * 删除方法
     *
     * @param logId
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse deleteById(Integer logId) {
        iLogService.deleteById(logId);
        return ServerResponse.createBySuccessMessage("删除成功!");
    }

    /**
     * 根据推荐人邮箱返回详情
     *
     * @param keyword
     * @return
     */
    @RequestMapping(value = "/get_referrer_detail.do")
    @ResponseBody
    public ServerResponse getReferrerDetail(@RequestParam(value = "keyword", required = false) String keyword) {
        return iLogService.getList(keyword);
    }

}
