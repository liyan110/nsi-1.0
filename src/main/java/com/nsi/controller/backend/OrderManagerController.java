package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Order;
import com.nsi.service.IOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 订单后台
 *
 * @author Luo Zhen
 * @create 2019-01-22 11:17
 */
@RequestMapping("/manager/order/")
@Controller
@Api(value = "/manager/order/", description = "商城—订单业务(后台)接口")
public class OrderManagerController {

    @Autowired
    private IOrderService iOrderService;

    /**
     * 后台返回订单详情
     *
     * @param pageNum
     * @param pageSize
     * @param status
     * @param productType
     * @return
     */
    @RequestMapping("get_order_list.do")
    @ResponseBody
    @ApiOperation(value = "后台返回订单详情", httpMethod = "GET")
    public ServerResponse getOrderList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                       @RequestParam(value = "status", defaultValue = "", required = false) String status,
                                       @RequestParam(value = "productType", defaultValue = "", required = false) String productType,
                                       @RequestParam(value = "searchKey", defaultValue = "", required = false) String searchKey) {
        return iOrderService.findOrderByPage(pageNum, pageSize, status, productType, searchKey);
    }

    /**
     * 后台根据订单号 删除订单
     *
     * @return
     */
    @RequestMapping("del.do")
    @ResponseBody
    @ApiOperation(value = "后台删除订单", httpMethod = "GET")
    public ServerResponse removeOrderList(@RequestParam("orderNo") Long orderNo) {
        return iOrderService.removeOrder(orderNo);
    }

    /**
     * 后台根据订单号 修改订单
     *
     * @param order
     * @return
     */
    @RequestMapping("modify.do")
    @ResponseBody
    @ApiOperation(value = "后台修改订单", httpMethod = "POST")
    public ServerResponse modifyOrder(Order order) {
        return iOrderService.modifyOrder(order);
    }




}
