package com.nsi.controller.backend;

import com.aliyuncs.utils.StringUtils;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Coupon;
import com.nsi.service.ICouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-05-16 17:28
 */
@Controller
@RequestMapping("/manager/coupon")
@Api(value = "/manager/coupon", description = "官网——优惠券(后台)管理接口")
public class CouponManagerController {

    @Autowired
    private ICouponService iCouponService;

    @RequestMapping(value = "/save.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "创建方法", httpMethod = "POST")
    public ServerResponse saveCoupon(Coupon coupon) {
        ServerResponse response = this.checkValue(coupon);
        if (!response.isSuccess()) {
            return response;
        }
        iCouponService.save(coupon);
        return ServerResponse.createBySuccess();
    }

    private ServerResponse checkValue(Coupon coupon) {
        if (StringUtils.isEmpty(coupon.getName()))
            return ServerResponse.createByErrorMessage("优惠券名字不能为空");
        if (coupon.getMonthActivityType() == null)
            return ServerResponse.createByErrorMessage("活动类型不能为空");
        if (coupon.getCouponType() == null)
            return ServerResponse.createByErrorMessage("优惠券类型不能为空");
        if (coupon.getWithAmount() == null)
            return ServerResponse.createByErrorMessage("满减金额不能为空");
        if (coupon.getUsedAmount() == null)
            return ServerResponse.createByErrorMessage("优惠金额不能为空");
        if (coupon.getEndTime() == null) {
            return ServerResponse.createByErrorMessage("过期时间不能为空");
        }
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "/modify.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modify(Coupon coupon) {
        ServerResponse response = this.checkValue(coupon);
        if (!response.isSuccess()) {
            return response;
        }
        if (coupon.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        iCouponService.modify(coupon);
        return ServerResponse.createBySuccess("修改成功");
    }

    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse delete(@RequestParam("couponId") Integer couponId) {
        iCouponService.delete(couponId);
        return ServerResponse.createBySuccess("删除成功");
    }


    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回优惠卷列表", httpMethod = "GET")
    public ServerResponse findAll(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                  @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
        PageInfo page = iCouponService.findALl(pageNum, pageSize);
        return ServerResponse.createBySuccess(page);
    }


}
