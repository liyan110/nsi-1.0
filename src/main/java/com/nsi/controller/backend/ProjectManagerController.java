package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Projects;
import com.nsi.service.IProjectService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/9/26 10:26
 * @description: 前台项目库(官网)
 */
@RequestMapping("/manager/project")
@Controller
@Api(value = "/manager/project", description = "官网——项目(后台)管理接口")
public class ProjectManagerController {

    @Autowired
    private IProjectService iProjectService;

    /**
     * 添加项目库方法
     *
     * @param projects
     * @return
     */
    @RequestMapping(value = "/add_project.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加项目库方法", httpMethod = "POST")
    public ServerResponse addSchool(Projects projects) {
        iProjectService.addProject(projects);
        return ServerResponse.createBySuccess();
    }

    /**
     * 关键字返回详情分页
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "关键字返回详情分页", httpMethod = "GET")
    public ServerResponse getProjectsList(@RequestParam(value = "searchKey", required = false) String searchKey,
                                          @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return iProjectService.getProjectList(searchKey, pageNum, pageSize);
    }

    /**
     * 修改项目库方法
     *
     * @param projects
     * @return
     */
    @RequestMapping(value = "/modify_project.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改项目库方法", httpMethod = "POST")
    public ServerResponse modifyProjects(Projects projects) {
        if (projects.getId() == null || StringUtils.isBlank(projects.getSubjectname())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        iProjectService.modifyProject(projects);
        return ServerResponse.createBySuccess();
    }

    /**
     * 根据Id返回详情
     *
     * @param projectId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据Id返回详情", httpMethod = "GET")
    public ServerResponse getProjectsById(Integer projectId) {
        Projects projects = iProjectService.selectById(projectId);
        return ServerResponse.createBySuccess(projects);
    }

    /**
     * 根据提交人返回详情
     *
     * @param userMail
     * @return
     */
    @RequestMapping(value = "/get_projects_list.do")
    @ResponseBody
    @ApiOperation(value = "根据提交人返回详情", httpMethod = "GET")
    public ServerResponse getProjectsByUserMail(String userMail) {
        return iProjectService.getProjectByUserMail(userMail);
    }


}
