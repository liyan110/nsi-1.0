package com.nsi.controller.backend;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.Exhibitor;
import com.nsi.service.ExhibitorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 展位表 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-02
 */
@Controller
@RequestMapping("/manager/exhibitor/")
@Api(value = "/manager/exhibitor/", description = "官网——展商(后台)管理接口")
public class ExhibitorManagerController {

    @Autowired
    private ExhibitorService exhibitorService;

    @RequestMapping("insert.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addExhibitor(Exhibitor exhibitor) {
        exhibitorService.insert(exhibitor);
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @RequestMapping("update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse updateExhibitor(Exhibitor exhibitor) {
        exhibitorService.updateById(exhibitor);
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    @RequestMapping("detail.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse detail(Integer exhibitorId) {
        Exhibitor exhibitor = exhibitorService.selectById(exhibitorId);
        return ServerResponse.createBySuccess(exhibitor);
    }

    @RequestMapping("list.do")
    @ResponseBody
    @ApiOperation(value = "返回列表", httpMethod = "GET")
    public ServerResponse findAll(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                  Exhibitor exhibitor) {
        return exhibitorService.findList(pageNum, pageSize, exhibitor);
    }

}
