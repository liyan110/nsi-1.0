package com.nsi.controller.backend;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseCategory;
import com.nsi.pojo.CourseList;
import com.nsi.service.ICourseCategoryService;
import com.nsi.service.ICourseListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 课程详情后台
 *
 * @author Luo Zhen
 * @create 5009-02-13 10:08
 */
@Slf4j
@RequestMapping("/manager/category")
@Controller
@Api(value = "/manager/category", description = "官网—录播课程详情(后台)管理接口")
public class CourseCategoryManagerController {

    @Autowired
    private ICourseCategoryService iCourseCategoryService;
    @Autowired
    private ICourseListService iCourseListService;

    /**
     * 添加方法
     *
     * @param category
     * @return
     */
    @RequestMapping("/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse saveGuest(CourseCategory category) {
        if (StringUtils.isBlank(category.getCourseName())
                || category.getListId() == null
                || category.getCourseId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            iCourseCategoryService.saveCourseCategory(category);
            return ServerResponse.createBySuccess();
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "添加失败");

    }

    /**
     * 返回详情
     *
     * @param pageNum
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse getCourseListItem(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<CourseCategory> courseLists = null;
        try {
            courseLists = iCourseCategoryService.findAll(null);
            PageInfo pageInfo = new PageInfo(courseLists);
            return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询失败");
    }

    /**
     * 修改方法
     *
     * @param category
     * @return
     */
    @RequestMapping("/modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyComments(CourseCategory category) {
        iCourseCategoryService.updateCourseCategory(category);
        return ServerResponse.createBySuccess("修改成功");
    }

    /**
     * 删除方法
     *
     * @param courseId
     * @return
     */
    @RequestMapping("/del.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteCourseList(Integer courseId) {
        try {
            iCourseCategoryService.deleteCourseCategory(courseId);
            return ServerResponse.createBySuccess();
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "删除失败");
    }

    /**
     * 根据id 返回详情
     *
     * @param courseId
     * @return
     */
    @RequestMapping("/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id 返回详情", httpMethod = "GET")
    public ServerResponse queryCourseList(Integer courseId) {
        try {
            CourseCategory category =
                    iCourseCategoryService.findCourseCategoryById(courseId);
            return ServerResponse.createBySuccess(category);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询详情失败");
    }

    /**
     * 返回课程合集下拉列表
     *
     * @return
     */
    @RequestMapping("/get_course_item.do")
    @ResponseBody
    @ApiOperation(value = "返回合集主题", httpMethod = "GET")
    public ServerResponse queryCourseListItem() {
        try {
            List<CourseList> courseList =
                    iCourseListService.findCourseListItem();
            return ServerResponse.createBySuccess(courseList);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ServerResponse.createByErrorCodeMessage(500, "查询详情失败");
    }

}
