package com.nsi.controller.backend;

import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.User;
import com.nsi.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: Luo Zhen
 * @date: 2018/11/21 17:24
 * @description:
 */
@Controller
@RequestMapping("/manager/user")
public class UserManagerController {

    private static final String SYS_USER_SUFFIX = "xinxueshuo";

    @Autowired
    private IUserService iUserService;

    /**
     * 后台登陆功能
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/login.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse login(@RequestParam("username") String username,
                                @RequestParam("password") String password) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        if (!username.contains(SYS_USER_SUFFIX)) {
            return ServerResponse.createByErrorMessage("请联系管理员开通权限！");
        }
        User currentUser = iUserService.sysLogin(username, password);
        if (currentUser == null || !password.equals(currentUser.getPassword())) {
            return ServerResponse.createByErrorMessage("用户名或密码不正确！");
        }

        currentUser.setPassword(null);
        return ServerResponse.createBySuccess(currentUser);
    }

    /**
     * 后台管理员注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/register.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse register(User user) {
        if (StringUtils.isBlank(user.getUsername())) {
            return ServerResponse.createByErrorMessage("用户名不能为空!");
        }
        if (!user.getUsername().contains("xinxueshuo")) {
            return ServerResponse.createByErrorMessage("格式不正确,请使用企业级邮箱注册!");
        }
        user.setUserRegistercode("" + Const.NUM_DEFAULT_VALUE);
        user.setUserScore(Const.NUM_DEFAULT_VALUE);
        user.setWechatid("" + Const.NUM_DEFAULT_VALUE);
        user.setUnionId("" + Const.NUM_DEFAULT_VALUE);
        user.setUserPortrait("" + Const.NUM_DEFAULT_VALUE);
        user.setLoadTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        user.setRegisterType(Const.REGISTER_MANAGER);
        user.setUpdateTime(new Date());

        iUserService.saveUser(user);
        return ServerResponse.createBySuccessMessage("注册成功!");
    }

    /**
     * 后台用户列表
     *
     * @param searchKey
     * @return
     */
    @RequestMapping("/get_user_list.do")
    @ResponseBody
    public ServerResponse<String> Admin_UserList(@RequestParam(value = "search_Key") String searchKey,
                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                 @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        return iUserService.getUserList(searchKey, pageNum, pageSize);
    }

    /**
     * 后台用户列表删除功能
     *
     * @param userId
     * @return
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    public ServerResponse delete(@RequestParam Integer userId) {
        if (userId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        iUserService.deleteByUserId(userId);
        return ServerResponse.createBySuccessMessage("删除成功");
    }
}
