package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.School;
import com.nsi.pojo.SchoolCertification;
import com.nsi.service.IFileService;
import com.nsi.service.ISchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/10/18 15:15
 * @description:
 */
@RequestMapping("/manager/school")
@Controller
@Api(value = "/manager/school", description = "四库全书——学校库(后台)管理接口")
public class SchoolManagerController {

    @Autowired
    private ISchoolService iSchoolService;
    @Autowired
    private IFileService iFileService;

    /**
     * 后台添加学校信息 默认是审核通过  VerifySign = 1
     *
     * @param school
     * @return
     */
    @RequestMapping(value = "/addSchool.do")
    @ResponseBody
    @ApiOperation(value = "根据Id获取详情", httpMethod = "POST")
    public ServerResponse addSchool(School school, SchoolCertification certification) {
        school.setVerifysign(String.valueOf(ResponseCode.PASS.getCode()));
        try {
            return iSchoolService.addSchoolInfo(school, certification);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.toString());
        }
    }

    /**
     * 返回list
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list.do")
    @ResponseBody
    @ApiOperation(value = "返回学校list", httpMethod = "GET")
    public ServerResponse getSchoolInfo(@RequestParam(value = "searchKey", required = false) String searchKey,
                                        @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                        @RequestParam(value = "verifyCode", defaultValue = "0") String verifyCode) {
        return iSchoolService.getSchoolList(searchKey, pageNum, pageSize, verifyCode);
    }

    /**
     * 后台——根据 id删除信息
     *
     * @param schoolId
     * @return
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    @ApiOperation(value = "根据 id删除信息", httpMethod = "GET")
    public ServerResponse removeSchoolInfo(Integer schoolId) {
        try {
            return iSchoolService.removeSchoolList(schoolId);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.toString());
        }
    }

    /**
     * 后台—— 修改学校信息和认证信息
     *
     * @param school
     * @param schoolCertification
     * @return
     */
    @RequestMapping(value = "modify_school.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据id 修改信息", httpMethod = "POST")
    public ServerResponse modifySchoolInfo(School school, SchoolCertification schoolCertification) {
        try {
            return iSchoolService.modifySchoolCategory(school,schoolCertification);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.toString());
        }
    }

    @RequestMapping(value = "get_time_list.do")
    @ResponseBody
    @ApiOperation(value = "根据时间段返回详情", httpMethod = "GET")
    public ServerResponse getTimeList(@RequestParam(value = "beginTime", required = false) String beginTime,
                                      @RequestParam(value = "endTime", required = false) String endTime) {
        return iSchoolService.getTimeList(beginTime, endTime);
    }

    /**
     * Base64 学校logo上传
     *
     * @param strImage
     * @param schoolId
     * @return
     */
    @RequestMapping(value = "/uplogo.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Base64解码上传", httpMethod = "POST")
    public ServerResponse base64getImage(@RequestParam("strImage") String strImage,
                                         @RequestParam("schoolId") Integer schoolId) {
        if (StringUtils.isBlank(strImage) || schoolId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Map modelMap = iFileService.base64Upload(strImage, schoolId);
        return ServerResponse.createBySuccess(modelMap);
    }

}
