package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Relation;
import com.nsi.service.IRelationService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-11-20 11:10
 */
@RequestMapping("/manager/relation")
@Controller
@Api(value = "/manager/relation", description = "四库全书-(后台)数据关联")
public class RelationManagerController {

    @Autowired
    private IRelationService iRelationService;

    /**
     * 新增
     *
     * @param relation
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "新增方法", httpMethod = "POST")
    public ServerResponse create(Relation relation) {
        return iRelationService.create(relation);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/del.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse del(Integer id) {
        iRelationService.delate(id);
        return ServerResponse.createBySuccess();
    }

    /**
     * 修改
     *
     * @param relation
     * @return
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modify(Relation relation) {
        iRelationService.modify(relation);
        return ServerResponse.createBySuccess("修改成功");
    }
}
