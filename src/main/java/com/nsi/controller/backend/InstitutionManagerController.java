package com.nsi.controller.backend;

import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Institution;
import com.nsi.service.IInstitutionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 人才库
 *
 * @author Luo Zhen
 * @create 2018-08-20
 **/
@RequestMapping("/manager/institution")
@Controller
@Api(value = "/manager/institution", description = "四库全书——机构库(后台)管理接口")
public class InstitutionManagerController {

    @Autowired
    private IInstitutionService iInstitutionService;

    /**
     * 添加操作
     *
     * @param institution
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse<String> add(Institution institution) {
        institution.setVerifySign(ResponseCode.PASS.getCode());
        try {
            return iInstitutionService.addInstitution(institution);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 修改简历 修改操作
     *
     * @param institution
     * @return list
     */
    @RequestMapping(value = "/update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "GET")
    public ServerResponse<String> alter(Institution institution) {
        try {
            return iInstitutionService.modifyInstitution(institution);
        } catch (NsiOperationException e) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), e.getMessage());
        }
    }

    /**
     * 根据id 删除数据
     *
     * @return
     */
    @RequestMapping(value = "/delete.do")
    @ResponseBody
    @ApiOperation(value = "根据id 删除数据", httpMethod = "GET")
    public ServerResponse delete(Integer institutionId) {
        return iInstitutionService.deleteOn(institutionId);
    }

    /**
     * 返回详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    @ApiOperation(value = "根据关键字，返回详情", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                               @RequestParam(value = "searchKey", required = false) String searchKey,
                               @RequestParam(value = "verifySign", required = false) Integer verifySign) {
        return iInstitutionService.getList(pageNum, pageSize, searchKey, verifySign);
    }

    /**
     * 根据id 返回详情
     *
     * @param institutionId
     * @return
     */
    @RequestMapping(value = "/detail.do")
    @ResponseBody
    @ApiOperation(value = "根据id，返回详情", httpMethod = "GET")
    public ServerResponse detail(Integer institutionId) {
        return iInstitutionService.findInstitutionById(institutionId);
    }


}
