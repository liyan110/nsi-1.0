package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Log;
import com.nsi.service.IActivityService;
import com.nsi.service.ILogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 后台文章管理接口
 *
 * @author Luo Zhen
 */
@RequestMapping("/manager/activity")
@Controller
@Api(value = "/manager/activity", description = "小程序——活动(后台)接口")
public class ActivityManagerController {

    @Autowired
    private IActivityService iActivityService;
    @Autowired
    private ILogService iLogService;

    /**
     * 查询活动列表
     *
     * @return
     */
    @RequestMapping("/search_activity.do")
    @ResponseBody
    @ApiOperation(value = "查询活动列表", httpMethod = "GET")
    public ServerResponse searchActivity() {
        return iActivityService.searchList();
    }

    /**
     * 查询信息列表功能
     *
     * @param title1
     * @return
     */
    @RequestMapping("/search_info.do")
    @ResponseBody
    @ApiOperation(value = "查询信息列表功能", httpMethod = "GET")
    public ServerResponse getInfo(String title1) {
        return iActivityService.searchInfo(title1);
    }


    /**
     * log插入
     *
     * @param log
     * @return
     */
    @RequestMapping("/add_h5_log.do")
    @ResponseBody
    @ApiOperation(value = "添加h5log活动", httpMethod = "POST")
    public ServerResponse addH5Log(Log log) {
        return iLogService.saveLog(log);
    }

    /**
     * 获取h5log活动
     *
     * @return
     */
    @RequestMapping("/get_h5_log.do")
    @ResponseBody
    @ApiOperation(value = "获取h5log活动", httpMethod = "POST")
    public ServerResponse getH5LogList(@RequestParam(value = "index03") String index03,
                                       @RequestParam(value = "index04") String index04,
                                       @RequestParam(value = "index05") String index05,
                                       @RequestParam(value = "index06") String index06) {
        return iLogService.getH5LogList(index03, index04, index05, index06);
    }

}
