package com.nsi.controller.backend;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventMeeting;
import com.nsi.service.EventMeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-14
 */
@RequestMapping("/manager/meeting/")
@Controller
@Api(value = "/manager/meeting/", description = "官网——活动会议(后台)管理接口")
public class EventMeetingManagerController {

    @Autowired
    private EventMeetingService eventMeetingService;

    /**
     * 新增方法
     *
     * @param eventMeeting
     * @return
     */
    @RequestMapping(value = "/save.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "新增方法", httpMethod = "POST")
    public ServerResponse saveMeeting(EventMeeting eventMeeting) {
        return eventMeetingService.saveMeeting(eventMeeting);
    }

    /**
     * 新增方法
     *
     * @param eventMeeting
     * @return
     */
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse updateMeeting(EventMeeting eventMeeting) {
        return eventMeetingService.updateMeeting(eventMeeting);
    }

    /**
     * 返回会议活动详情
     *
     * @param pageNum
     * @param pageSize
     * @param type
     * @param startTime
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回会议活动详情", httpMethod = "GET")
    public ServerResponse getMeetingList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                         @RequestParam(value = "type", defaultValue = "", required = false) String type,
                                         @RequestParam(value = "startTime", defaultValue = "DESC") String startTime) {
        return eventMeetingService.getMeetingList(pageNum, pageSize, type, startTime);
    }

    /**
     * 删除方法
     *
     * @param meetId
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteMeeting(Integer meetId) {
        return eventMeetingService.deleteMeeting(meetId);
    }


}
