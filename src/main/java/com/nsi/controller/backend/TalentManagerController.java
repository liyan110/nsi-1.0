package com.nsi.controller.backend;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.NewTalent;
import com.nsi.pojo.Talent;
import com.nsi.service.ITalentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: Luo Zhen
 * @date: 2018/12/6 12:09
 * @description: 人才库后台
 */
@RequestMapping("/manager/talents")
@Controller
@Api(value = "/manager/talents", description = "四库全书——人才库(后台)管理接口")
public class TalentManagerController {

    @Autowired
    private ITalentService iTalentService;

    /**
     * 后台添加操作
     *
     * @param talent
     * @return
     */
    @RequestMapping(value = "/add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse<String> add(Talent talent) {
        talent.setVerifySign(String.valueOf(ResponseCode.PASS.getCode()));
        return iTalentService.add(talent);
    }

    /**
     * 后台返回详情
     *
     * @param talent_searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/get_list.do")
    @ResponseBody
    @ApiOperation(value = "后台获取list详情", httpMethod = "GET")
    public ServerResponse getList(@RequestParam(value = "talent_searchKey", required = false) String talent_searchKey,
                                  @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                  @RequestParam(value = "verifySign", defaultValue = "0") String verifySign) {
        return iTalentService.getList(talent_searchKey, pageNum, pageSize, verifySign);
    }

}
