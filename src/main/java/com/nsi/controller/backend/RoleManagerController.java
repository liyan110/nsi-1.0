package com.nsi.controller.backend;

import com.nsi.config.DynamicDataSourceHolder;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Role;
import com.nsi.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luo Zhen
 * @create 2019-01-16 17:19
 */
@RequestMapping("/manager/role")
@Controller
@Api(value = "/manager/role", description = "官网——权限(后台)管理接口")
public class RoleManagerController {

    @Autowired
    private IRoleService iRoleService;

    @RequestMapping("add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse saveRole(Role role) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRoleService.saveOn(role);
    }

    @RequestMapping("modify.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyRole(Role role) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRoleService.modifyRole(role);
    }

    @RequestMapping("del.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteRole(Integer roleId) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRoleService.deleteRoleById(roleId);
    }

    @RequestMapping("list.do")
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse getRoleList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRoleService.getRoleList(pageNum, pageSize);
    }

    @RequestMapping("get_role_item.do")
    @ResponseBody
    @ApiOperation(value = "根据用户邮箱返回详情", httpMethod = "GET")
    public ServerResponse getRoleItem(@RequestParam("userMail") String userMail) {
        DynamicDataSourceHolder.setDataSource("dataSource1");
        return iRoleService.getRoleItem(userMail);

    }
}
