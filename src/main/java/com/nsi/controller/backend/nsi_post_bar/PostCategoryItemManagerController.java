package com.nsi.controller.backend.nsi_post_bar;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.enums.CheckStateEnum;
import com.nsi.pojo.PostCategoryItem;
import com.nsi.service.PostCategoryItemService;
import com.nsi.vo.nsi_shop_bar.PostInfoVO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Luo Zhen
 * @create 2019-12-18 9:53
 */
@Controller
@RequestMapping("/manager/postItem/")
@Api(value = "/manager/postItem/", description = "新学说贴吧——(后台)帖子详情")
public class PostCategoryItemManagerController {

    private static final Integer LIMIT_NUM = 10;

    @Autowired
    private PostCategoryItemService postCategoryItemService;

    /**
     * 根据帖子id 返回详情
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findByCategoryId(Integer itemId) {
        PostCategoryItem postCategory = postCategoryItemService.selectById(itemId);
        return ServerResponse.createBySuccess(postCategory);
    }

    /**
     * 返回帖子列表
     *
     * @param pageNum
     * @param pageSize
     * @param categoryItem
     * @param orderColumn
     * @return
     */
    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findPostCategoryList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                                               @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                               PostCategoryItem categoryItem,
                                               @RequestParam(value = "orderColumn", required = false, defaultValue = "create_time") String orderColumn) {
        return postCategoryItemService.selectCategoryItemList(pageNum, pageSize, categoryItem, orderColumn);
    }


    /**
     * 返回最热|最新文章列表(10条)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "index_list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse findIndexHotAndLastList() {
        PostInfoVO postInfoVO = new PostInfoVO();

        List<PostCategoryItem> itemList = postCategoryItemService.selectList(
                new EntityWrapper<PostCategoryItem>()
                        .eq("is_check", CheckStateEnum.IN_APPROVE.getCode())
                        .orderBy("item_id", false)
        );
        // 返回最新十条
        List<PostCategoryItem> lastList = itemList.stream()
                .limit(LIMIT_NUM)
                .collect(Collectors.toList());
        postInfoVO.setLastItemList(lastList);

        // 返回最热十条
        List<PostCategoryItem> hotList = itemList.stream()
                .sorted(Comparator.comparing(PostCategoryItem::getWatchNum).reversed())
                .limit(LIMIT_NUM)
                .collect(Collectors.toList());
        postInfoVO.setHotItemList(hotList);
        return ServerResponse.createBySuccess(postInfoVO);

    }

    /**
     * 帖子审核通过
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "verify_success.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse verifySuccess(Integer[] itemId) {
        if (itemId == null || itemId.length < 0) {
            return ServerResponse.createBySuccessMessage("参数不合法");
        }
        List<PostCategoryItem> updateItem = new ArrayList<>();
        for (int i = 0; i < itemId.length; i++) {
            PostCategoryItem categoryItem = new PostCategoryItem();
            categoryItem.setItemId(itemId[i]);
            categoryItem.setIsCheck(CheckStateEnum.IN_APPROVE.getCode());
            updateItem.add(categoryItem);
        }
        postCategoryItemService.updateBatchById(updateItem);
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    /**
     * 帖子审核拒绝
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "verify_failed.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse verifyFailed(Integer[] itemId) {
        if (itemId == null || itemId.length < 0) {
            return ServerResponse.createBySuccessMessage("参数不合法");
        }
        List<PostCategoryItem> updateItem = new ArrayList<>();
        for (int i = 0; i < itemId.length; i++) {
            PostCategoryItem categoryItem = new PostCategoryItem();
            categoryItem.setItemId(itemId[i]);
            categoryItem.setIsCheck(CheckStateEnum.IN_UPAPPROVE.getCode());
            updateItem.add(categoryItem);
        }
        postCategoryItemService.updateBatchById(updateItem);
        return ServerResponse.createBySuccessMessage("修改成功");
    }


    /**
     * 删除帖子
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "delete.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse deleteByCategoryItem(Integer itemId) {
        postCategoryItemService.deleteById(itemId);
        return ServerResponse.createBySuccess("删除成功");
    }

    /**
     * 置顶功能
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "top.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse itemTopStick(Integer itemId) {
        PostCategoryItem item = new PostCategoryItem();
        item.setItemId(itemId);
        item.setIsTop(Const.TOP_VALUE);

        postCategoryItemService.updateById(item);
        return ServerResponse.createBySuccessMessage("置顶成功");
    }

    /**
     * 取消置顶
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "cancel_top.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse itemCancelTop(Integer itemId) {
        PostCategoryItem item = new PostCategoryItem();
        item.setItemId(itemId);
        item.setIsTop(Const.CANCEL_TOP);

        postCategoryItemService.updateById(item);
        return ServerResponse.createBySuccessMessage("取消置顶");
    }


}
