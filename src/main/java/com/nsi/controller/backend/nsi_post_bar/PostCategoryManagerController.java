package com.nsi.controller.backend.nsi_post_bar;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.PostCategory;
import com.nsi.service.PostCategoryService;
import com.nsi.vo.nsi_shop_bar.PostVO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 帖子类目表 前端控制器
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@Controller
@RequestMapping("/manager/post/")
@Api(value = "/manager/post/", description = "新学说贴吧——(后台)贴吧类目")
public class PostCategoryManagerController {

    @Autowired
    private PostCategoryService postCategoryService;

    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse updatePostCategory(PostCategory postCategory) {
        if (postCategory.getId() == null) {
            return ServerResponse.createBySuccessMessage("参数不合法");
        }
        postCategoryService.updateById(postCategory);
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteByCategoryId(Integer categoryId) {
        postCategoryService.deleteById(categoryId);
        return ServerResponse.createBySuccess("删除成功");
    }

    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id返回详情", httpMethod = "GET")
    public ServerResponse findByCategoryId(Integer categoryId) {
        PostCategory postCategory = postCategoryService.selectById(categoryId);
        return ServerResponse.createBySuccess(postCategory);
    }

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回列表", httpMethod = "GET")
    public ServerResponse findPostCategoryList() {
        List<PostVO> lists = postCategoryService.findList();
        return ServerResponse.createBySuccess(lists);
    }
}
