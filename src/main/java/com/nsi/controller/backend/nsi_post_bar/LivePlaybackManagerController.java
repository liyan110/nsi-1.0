package com.nsi.controller.backend.nsi_post_bar;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.LivePlayback;
import com.nsi.service.LivePlaybackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 火龙果小程序—直播回放
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
@Controller
@RequestMapping("/manager/playback")
@Api(value = "/manager/playback", description = "火龙果小程序(后台)—回放地址功能")
public class LivePlaybackManagerController {

    @Autowired
    private LivePlaybackService livePlaybackService;

    /**
     * 回放课程列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "回放课程列表", httpMethod = "GET")
    public ServerResponse findCourseList(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<LivePlayback> playbackList = livePlaybackService.selectList(
                new EntityWrapper<LivePlayback>()
                        .orderBy("course_id", false)
        );

        PageInfo pageInfo = new PageInfo(playbackList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 添加方法
     *
     * @param livePlayback
     * @return
     */
    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse addLivePlayback(LivePlayback livePlayback) {
        if (livePlayback.getCourseId() == null) {
            return ServerResponse.createByErrorMessage("课程Id不能为空!");
        }
        if (StringUtils.isBlank(livePlayback.getCourseType())
                || StringUtils.isBlank(livePlayback.getVidioTitile())) {
            return ServerResponse.createByErrorMessage("参数不合法!");
        }

        livePlayback.setWatchNum(Const.NUM_DEFAULT_VALUE);
        livePlaybackService.insert(livePlayback);
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    /**
     * 修改方法
     *
     * @param livePlayback
     * @return
     */
    @RequestMapping(value = "/modify.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse modifyLivePlayback(LivePlayback livePlayback) {
        if (livePlayback.getCourseId() == null) {
            return ServerResponse.createBySuccessMessage("参数不合法!");
        }

        livePlaybackService.updateById(livePlayback);
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse deleteLivePlayback(Integer id) {
        livePlaybackService.deleteById(id);
        return ServerResponse.createBySuccessMessage("删除成功!");
    }

    /**
     * 根据课程id返回详情
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据课程id返回详情", httpMethod = "GET")
    public ServerResponse findByCourseId(Integer id) {
        LivePlayback livePlayback = livePlaybackService.selectById(id);
        return ServerResponse.createBySuccess(livePlayback);
    }

}
