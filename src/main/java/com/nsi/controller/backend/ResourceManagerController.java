package com.nsi.controller.backend;

import com.google.common.collect.Maps;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Resource;
import com.nsi.service.IResourceService;
import com.nsi.util.PropertiesUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Map;

/**
 * 后台资源下载接口
 *
 * @author Luo Zhen
 */
@RequestMapping("/manager/resource")
@Controller
@Api(value = "/manager/resource", description = "官网——资源(后台)管理接口")
public class ResourceManagerController {

    private static Logger logger = LoggerFactory.getLogger(ResourceManagerController.class);

    @Autowired
    private IResourceService iResourceService;

    /**
     * 上传简历 文件上传
     *
     * @param file
     * @return list
     */
    @RequestMapping(value = "/upfile.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传资源", httpMethod = "POST")
    public ServerResponse upfile(@RequestParam(value = "file", required = false) MultipartFile file) {
        String path = PropertiesUtil.getProperty("article.html.url");
        String targetFileName = iResourceService.upFile(file, path);

        //服务器需要的改法
        String url = PropertiesUtil.getProperty("nsi.html.url") + targetFileName;
        System.out.println("url:" + url);
        Map resultMap = Maps.newHashMap();
        resultMap.put("uri", targetFileName);
        resultMap.put("url", url);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 添加方法
     *
     * @param resource
     * @return
     */
    @RequestMapping(value = "add.do")
    @ResponseBody
    @ApiOperation(value = "添加方法", httpMethod = "POST")
    public ServerResponse add(Resource resource) {
        return iResourceService.saveOn(resource);
    }

    /**
     * 删除方法
     *
     * @param resourceId
     * @return
     */
    @RequestMapping(value = "del.do")
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse del(Integer resourceId) {
        return iResourceService.delete(resourceId);
    }

    /**
     * 刪除方法
     *
     * @param resource
     * @return
     */
    @RequestMapping(value = "update.do")
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse update(Resource resource) {
        return iResourceService.update(resource);
    }

    /**
     * 返回前台资源详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "list.do")
    @ResponseBody
    @ApiOperation(value = "返回前/后资源详情", httpMethod = "GET")
    public ServerResponse list(@RequestParam(value = "type", defaultValue = "", required = false) String type,
                               @RequestParam(value = "year", defaultValue = "", required = false) String year,
                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "8") int pageSize) {
        return iResourceService.getList(type, year, pageNum, pageSize);
    }

    /**
     * 返回资源详情
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "detail.do")
    @ResponseBody
    @ApiOperation(value = "返回资源详情", httpMethod = "GET")
    public ServerResponse detail(@RequestParam(value = "id",required = false) int id) {
        return iResourceService.detail(id);
    }


}
