package com.nsi.controller.backend;

import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.NewTalent;
import com.nsi.service.NewTalentService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author Luo Zhen
 * @create 2019-11-26 13:48
 */
@Controller
@RequestMapping("/manager/newTalent")
@Api(value = "/manager/newTalent", description = "四库全书————新人才库(后台)最新管理接口")
public class TalentNewManagerController {

    @Autowired
    private NewTalentService newTalentService;

    @RequestMapping(value = "add.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "新增方法", httpMethod = "POST")
    public ServerResponse create(NewTalent newTalent) {
        newTalent.setIsCheck(1);
        newTalent.setCreateTime(new Date());
        newTalent.setUpdateTime(newTalent.getCreateTime());
        newTalentService.insert(newTalent);
        return ServerResponse.createBySuccess(newTalent.getId());
    }

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "详情列表", httpMethod = "GET")
    public ServerResponse list(@RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize,
                               @RequestParam(defaultValue = "", required = false) String searchKey,
                               @RequestParam(defaultValue = "1", required = false) Integer isCheck) {
        return newTalentService.findList(pageNum, pageSize, searchKey, isCheck);
    }

    @RequestMapping(value = "udpate.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "修改方法", httpMethod = "POST")
    public ServerResponse udpate(NewTalent newTalent) {
        if (newTalent.getId() == null) {
            return ServerResponse.createBySuccessMessage("参数不合法");
        }
        newTalent.setUpdateTime(new Date());
        newTalentService.updateById(newTalent);
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "delete.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除方法", httpMethod = "GET")
    public ServerResponse delete(Integer talentId) {
        if (talentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        newTalentService.deleteById(talentId);
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "detail.do", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "返回详情", httpMethod = "GET")
    public ServerResponse detail(Integer talentId) {
        if (talentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        NewTalent newTalent = newTalentService.selectById(talentId);
        return ServerResponse.createBySuccess(newTalent);
    }

    /**
     * 根据Id修改内部描述信息
     *
     * @param talentId
     * @param internaleDesc
     * @return
     */
    @RequestMapping(value = "udpate_by_internal.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据Id修改内部描述信息", httpMethod = "POST")
    public ServerResponse udpate(Integer talentId, String internaleDesc) {
        if (talentId == null) {
            throw new NsiOperationException("参数不合法!");
        }
        NewTalent newTalent = new NewTalent();
        newTalent.setId(talentId);
        newTalent.setInternalDesc(internaleDesc);
        newTalent.setUpdateTime(new Date());
        newTalentService.updateById(newTalent);
        return ServerResponse.createBySuccess();
    }


}
