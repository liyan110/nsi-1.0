package com.nsi.service;

import com.nsi.common.ServerResponse;

/**
 * <p>
 * 优惠用户信息表 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-21
 */
public interface DiscountsService {

    /**
     * 手机号验证是否有无优惠
     *
     * @param telphone
     * @return
     */
    ServerResponse checkValid(String telphone);
}
