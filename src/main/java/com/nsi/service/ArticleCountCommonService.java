package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.Article;

public interface ArticleCountCommonService extends IService<Article> {

    /**
     * 添加观看数
     *
     * @param article 文章
     */
    void addWatchNum(Article article);
}
