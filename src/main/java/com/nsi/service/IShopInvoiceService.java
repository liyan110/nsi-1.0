package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ShopInvoice;

/**
 * @author Li Yan
 * @create 2019-03-14
 **/
public interface IShopInvoiceService extends IService<ShopInvoice> {

    ServerResponse<String> ShopInvoiceCreate(ShopInvoice shopInvoice);

    ServerResponse<String> PassShopInvoice(Integer Id);

    ServerResponse<String> InvoiceList(String SearchKey, String manageState, String financeState, int pageNum, int pageSize);

    ServerResponse<String> InvoiceOrderList(String SearchKey, String manageState, String financeState, int pageNum, int pageSize);

    ServerResponse checkInvoice(String orderNo);

    ServerResponse del(int id);
}
