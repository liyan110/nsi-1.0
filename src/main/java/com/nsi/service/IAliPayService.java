package com.nsi.service;

import com.nsi.common.ServerResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author 12984
 */
public interface IAliPayService {


    /**
     * 手机H5支付
     *
     * @param subject
     * @param body
     * @param total_fee
     * @param response
     * @param returnUrl
     * @throws IOException
     */
    void appPay(String subject, String body, String total_fee, HttpServletResponse response, String returnUrl) throws IOException;

    /**
     * PC网页支付
     *
     * @param subject
     * @param body
     * @param total_fee
     * @param response
     * @param returnUrl
     * @throws IOException
     */
    void appPayHtml(String subject, String body, String total_fee, HttpServletResponse response, String returnUrl) throws IOException;

    /**
     * 回调方法
     *
     * @param params
     * @return
     */
    ServerResponse aliCallback(Map<String, String> params);


    /**
     * 轮询接口
     *
     * @param body
     * @return
     */
    ServerResponse queryOrderPayStatus(String body);
}
