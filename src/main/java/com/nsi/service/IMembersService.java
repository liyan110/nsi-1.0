package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Members;

/**
 * @author: Luo Zhen
 * @date: 2018/9/10 15:24
 * @description:
 */
public interface IMembersService {

    /**
     * 添加会员方法
     *
     * @param membersm
     * @return
     */
    ServerResponse addMembersInfo(Members membersm);

    /**
     * 后台——返回会员详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    ServerResponse getMembersList(int pageNum, int pageSize, Integer searchKey);

    /**
     * 后台——修改状态
     *
     * @param members
     * @return
     */
    ServerResponse updateMembersStates(Members members);
}
