package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Preview;

/**
 * @author Luo Zhen
 */
public interface IPreviewService {

    /**
     * 添加方法
     */
    ServerResponse saveon(Preview preview);

    /**
     * 返回详情
     */
    ServerResponse list(int pageNum, int pageSize);
}
