package com.nsi.service;

import com.nsi.pojo.NsiEvent;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 活动定义表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
public interface NsiEventService extends IService<NsiEvent> {

}
