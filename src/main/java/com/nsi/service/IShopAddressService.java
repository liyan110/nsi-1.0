package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.ShopAddress;

/**
 * @author Li Yan
 * @create 2019-01-02
 **/
public interface IShopAddressService extends IService<ShopAddress> {

    ServerResponse<String> add(ShopAddress shopAddress);

    ServerResponse<String> update(ShopAddress shopAddress);

    ServerResponse<ShopAddress> getAddress(String wechatId, String unionId);
}
