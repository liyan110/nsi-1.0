package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Role;

public interface IRoleService {
    ServerResponse saveOn(Role role);

    ServerResponse modifyRole(Role role);

    ServerResponse deleteRoleById(Integer roleId);

    ServerResponse getRoleList(int pageNum, int pageSize);

    ServerResponse getRoleItem(String userMail);

}
