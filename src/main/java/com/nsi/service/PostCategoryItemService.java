package com.nsi.service;


import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.PostCategoryItem;

/**
 * <p>
 * 帖子详情表 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
public interface PostCategoryItemService extends IService<PostCategoryItem> {

    void savePostCategoryItem(PostCategoryItem postCategoryItem);

    ServerResponse selectCategoryItemList(int pageNum,
                                          int pageSize,
                                          PostCategoryItem categoryItem,
                                          String orderColumn);

}
