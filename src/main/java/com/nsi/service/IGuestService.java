package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Guest;

/**
 * @author: Luo Zhen
 * @date: 2018/11/27 10:23
 * @description:
 */
public interface IGuestService extends IService<Guest> {

    ServerResponse saveGuest(Guest geust);

    ServerResponse getGuestList(int pageNum, int pageSize, String searchKey);

    ServerResponse modifyGuest(Guest guest);

    ServerResponse removeGuest(Integer guestId);

    ServerResponse getGuestInfo(Integer guestId);

    ServerResponse checkValidByName(String name);
}
