package com.nsi.service;

import com.nsi.common.ServerResponse;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author Luo Zhen
 * @create 2018-08-07
 */
public interface IWxPayService {

    /**
     * 微信公众号授权获取openid
     *
     * @param code
     * @return
     */
    ServerResponse getOpenId(String code);

    /**
     * 根据授权请求获取微信用户信息
     *
     * @param code
     * @return
     */
    ServerResponse getUserInfo(String code);

    /**
     * 微信公众号支付
     *
     * @param openid
     * @param body
     * @param total_fee
     * @param attach
     * @return
     */
    ServerResponse wxPayment(String openid, String body, String total_fee, String attach);

    /**
     * 微信h5支付
     *
     * @param body
     * @param total_fee
     * @param attach
     * @param spbillIp
     * @param sceneInfo
     * @return
     */
    ServerResponse wxPaymentH5(String body, String total_fee, String attach, String spbillIp, String sceneInfo);


    /**
     * 微信分享功能
     *
     * @param url
     * @return
     */
    ServerResponse wxChatShare(String url);

    /**
     * 微信原生扫码支付
     *
     * @param body
     * @param total_fee
     * @param attach
     * @return
     */
    ServerResponse wxPaymentQrCode(String body, String total_fee, String attach, String out_trade_no);

    /**
     * 微信轮询接口
     *
     * @param orderNo
     * @return
     */
    ServerResponse queryOrderPayStatus(String orderNo);

    /**
     * 微信获取用户信息——小程序
     *
     * @param code
     * @return
     */
    ServerResponse getUserByCodeAndIv(String type, String code, String encryptedData, String iv);


    /**
     * 公众号支付
     *
     * @param openid
     * @param body
     * @param total_fee
     * @param out_trade_no
     * @param attach
     * @return
     */
    ServerResponse WxPay_public(String openid, String body, String total_fee, String out_trade_no, String attach);

    /**
     * 小程序支付
     *
     * @param openid
     * @param body
     * @param total_fee
     * @param out_trade_no
     * @param attach
     * @return
     */
    ServerResponse WxPay_miniProgram(String openid, String body, String total_fee, String out_trade_no, String attach);

    /**
     * 微信所有支付回调
     *
     * @param request
     * @param response
     */
    void WxPay_callback(HttpServletRequest request, HttpServletResponse response);

    /**
     * 根据weichatId和订单号查询订单状态
     *
     * @param wechatId
     * @param orderNo
     * @return
     */
    ServerResponse queryOrderPayStatus(String wechatId, String orderNo);
}
