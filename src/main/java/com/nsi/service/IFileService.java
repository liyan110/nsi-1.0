package com.nsi.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Luo Zhen
 */
public interface IFileService {

    /**
     * 上传简历
     *
     * @param file
     * @param userMail
     * @param userTrueName
     * @param path
     * @return
     */
    String upFile(MultipartFile file, String userMail, String userTrueName, String path);

    /**
     * 上传简历
     *
     * @param file
     * @param file
     * @param type
     * @return
     */
    String uploadResume(MultipartFile file, String type, String newFileName);

    /**
     * 上传图片
     *
     * @param file
     * @param type
     * @return
     */
    String upload(MultipartFile file, String type);

    /**
     * 上传学校库 log
     *
     * @param file
     * @param type
     * @return
     */
    String uploadSchoolLogo(MultipartFile file, String type, Integer schoolId);

    String uploadSchoolBigImage(MultipartFile file, String type, Integer schoolId);

    /**
     * base64 图片上传
     *
     * @param strImage
     * @param type
     * @return
     */
    String base64Upload(String strImage, String type);


    /**
     * 学校库 logo base64 上传
     *
     * @param strImage
     * @param schoolId
     * @return
     */
    Map<String, String> base64Upload(String strImage, Integer schoolId);

}
