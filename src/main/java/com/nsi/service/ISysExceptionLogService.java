package com.nsi.service;

import com.nsi.common.ServerResponse;

/**
 * @author Li Yan
 * @create 2019-10-21
 **/
public interface ISysExceptionLogService {

    ServerResponse<String> list();

    ServerResponse<String> delete(int id);
}
