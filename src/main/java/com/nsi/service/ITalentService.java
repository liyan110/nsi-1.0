package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Talent;


/**
 * @author Luo Zhen
 * @create 2018-08-20
 */
public interface ITalentService {


    /**
     * 添加方法
     *
     * @param talent
     * @return
     */
    ServerResponse add(Talent talent);

    /**
     * 前台查询人才方法 is_public=1 展示
     *
     * @param talent_searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse search(String talent_searchKey, int pageNum, int pageSize);

    /**
     * 后台 返回人才库详情
     *
     * @param talent_searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getList(String talent_searchKey, int pageNum, int pageSize, String verifySign);

    /**
     * 通过邮箱来查找
     *
     * @param UserMail
     * @return
     */
    ServerResponse searchByMail(String UserMail);

    /**
     * 修改方法
     *
     * @param record
     * @return
     */
    ServerResponse update(Talent record);

    /**
     * 根据Id获取详情
     *
     * @param talentId
     * @return
     */
    ServerResponse detail(Integer talentId);

    /**
     * 验证是否上传邮箱
     *
     * @param userMail
     * @return
     */
    ServerResponse checkByUpFile(String userMail);

    /**
     * 删除简历
     *
     * @param userMail
     * @param imageUrl
     * @return
     */
    ServerResponse deleteFile(String userMail, String imageUrl);

    /**
     * 根据id 删除数据
     *
     * @param talentId
     * @return
     */
    ServerResponse deleteOn(Integer talentId);

}
