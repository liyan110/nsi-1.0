package com.nsi.service;


import com.baomidou.mybatisplus.service.IService;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.SchoolNew;

import java.util.List;

public interface INewSchoolService extends IService<SchoolNew> {

    /**
     * 新增学校信息
     *
     * @param schoolNew
     * @return
     */
    SchoolNew insertSchool(SchoolNew schoolNew);

    PageInfo findAll(Integer pageNum, Integer pageSize, String searchKey, Integer verifySign);

    ServerResponse findAll();

    SchoolNew findById(Integer schoolId);

    ServerResponse modifyNewSchool(SchoolNew schoolNew);

    ServerResponse deleteById(Integer schoolId);

    ServerResponse<PageInfo> searchPowerList(String[] area, String[] system, String[] properties, String[] course,int pageNum, int pageSize);

    ServerResponse findCityWideList(Integer id);

    ServerResponse checkValidedByName(String schoolName);

    ServerResponse updateTemplate();

    //    请求学校数据更新按钮- 相关功能
    ServerResponse school_UpdataButton(String schoolId, String schoolName, String userMail);

    ServerResponse list_schoolUpdataButton();

    ServerResponse delete_schoolUpdataButton(int id);

    ServerResponse verifyStatus(Integer schoolId, Integer status);

    List<SchoolNew> findAllList(String searchKey, Integer verifySign);
}
