package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.SchoolSubmit;
import com.nsi.vo.SchoolSubmitVo;

import java.util.List;

/**
 * <p>
 * 学校提交人信息数据库表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-10-15
 */
public interface SchoolSubmitService extends IService<SchoolSubmit> {

    List<SchoolSubmitVo> findListByVerify(String verify);
}
