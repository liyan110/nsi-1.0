package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.ItemCat;

public interface IItemCatService {

    ServerResponse saveItemCat(ItemCat itemCat);

    ServerResponse modifyItemCat(ItemCat itemCat);

    ServerResponse findItemCatById(int pageNum, int pageSize, Integer parentId);

    ServerResponse modifyItemCatById(Integer itemId);

    ServerResponse removeItemCat(Integer itemId);
}
