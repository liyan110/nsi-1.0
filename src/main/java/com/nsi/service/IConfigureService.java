package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Configure;


/**
 * @author Luo Zhen
 * @create 2018-08-07
 */

public interface IConfigureService extends IService<Configure> {

    /**
     * 返回详情
     *
     * @param type
     * @return
     */
    ServerResponse list(String type);

    /**
     * 修改方法
     *
     * @param configure
     * @return
     */
    ServerResponse<String> update(Configure configure);

    /**
     * 修改模板
     *
     * @param templateHtml
     * @return
     */
    ServerResponse updateTemplate(String templateHtml);


    /**
     * 后台——新增
     *
     * @param configure
     * @return
     */
    ServerResponse insertConfigure(Configure configure);

    /**
     * 后台——根据配置类型返回名字
     *
     * @param typeName
     * @return
     */
    ServerResponse getConfigureList(String typeName);

    /**
     * 后台——修改
     *
     * @param configure
     * @return
     */
    ServerResponse modifyConfigure(Configure configure);
}
