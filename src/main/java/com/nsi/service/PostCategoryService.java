package com.nsi.service;


import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.PostCategory;
import com.nsi.vo.nsi_shop_bar.PostVO;

import java.util.List;

/**
 * <p>
 * 帖子类目表 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
public interface PostCategoryService extends IService<PostCategory> {

    /**
     * 返回类目信息
     *
     * @return
     */
    List<PostVO> findList();

    List<PostCategory> findCategoryList();
}
