package com.nsi.service;

import com.nsi.common.ServerResponse;

public interface MsgCheckService {

    String getToken(String appId, String appSecret, String prefix) throws Exception;

    ServerResponse msgSecCheck(String accessToken, String content);
}
