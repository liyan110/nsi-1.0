package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.PostCollect;
import com.nsi.vo.nsi_shop_bar.PostCollectVO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
public interface PostCollectService extends IService<PostCollect> {

    /**
     * 返回我的收藏列表
     *
     * @param openId
     * @return
     */
    List<PostCollectVO> findCollectList(String openId);

    /**
     * 返回我的收藏列表数
     *
     * @param openId
     * @return
     */
    int findCollectCount(String openId);

    /**
     * 判断是否收藏
     *
     * @param openId
     * @param itemId
     * @return
     */
    int findIsCollect(String openId, Integer itemId);


}
