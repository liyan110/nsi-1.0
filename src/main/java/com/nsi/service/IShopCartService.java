package com.nsi.service;

import com.nsi.common.ServerResponse;

/**
 * @author Li Yan
 * @create 2019-03-05
 **/
public interface IShopCartService {

    ServerResponse<String> create(String openId);

    ServerResponse<String> addGoods(String cartId, String goodsId, String goodsName, String goodsPrice, String goodsNum);

    ServerResponse<String> updateCart(String openId, String goodsJson);

    ServerResponse<String> lockCart(String cartId);

    ServerResponse<String> cartDetail(String cartId);

}
