package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.ForeignTeacher;

/**
 * @author: Luo Zhen
 * @date: 2018/9/28 15:16
 * @description:
 */
public interface ISchoolBlackListService {

    /**
     * 添加方法
     *
     * @param foreignTeacherCondition
     * @return
     * @throws NsiOperationException
     */
    ServerResponse addSchoolBlackList(ForeignTeacher foreignTeacherCondition) throws NsiOperationException;

    /**
     * 根据关键字返回分页详情
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getSchoolBlackList(String searchKey, int pageNum, int pageSize);

    /**
     * 根据id返回详情
     *
     * @param id
     * @param pageNum
     * @return
     */
    ServerResponse getForeignTeacherInfo(Integer id, int pageNum, int pageSize);

    /**
     * 后台——返回未审核列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getReviewList(int pageNum, int pageSize);

    /**
     * 后台——返回列表
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getForeignList(String searchKey, int pageNum, int pageSize);

    /**
     * 后台——修改信息(包括审核状态)
     *
     * @param foreignTeacher
     * @return
     * @throws NsiOperationException
     */
    ServerResponse modifyForeignInfo(ForeignTeacher foreignTeacher) throws NsiOperationException;

    /**
     * 根据id 删除信息
     *
     * @param foreignId
     * @return
     */
    ServerResponse removeForeignInfo(Integer foreignId);

    /**
     * 根据id 查询信息
     *
     * @param foreignId
     * @return
     */
    ServerResponse getForeignInfo(Integer foreignId);
}
