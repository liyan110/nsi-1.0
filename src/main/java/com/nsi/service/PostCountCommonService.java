package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.PostCategoryItem;

/**
 * 帖子计数公共方法
 *
 * @author Luo Zhen
 */
public interface PostCountCommonService extends IService<PostCategoryItem> {

    /**
     * 添加观看数
     *
     * @param itemId
     */
    void addWatchNum(Integer itemId);

    /**
     * 添加评论数
     *
     * @param itemId
     */
    void addCommentNum(Integer itemId);

    /**
     * 添加收藏数
     *
     * @param itemId
     */
    void addCollectNum(Integer itemId);

    /**
     * 添加分享数
     *
     * @param itemId
     */
    void addShareNum(Integer itemId);


}
