package com.nsi.service;

import com.nsi.common.ServerResponse;

public interface IIndexService {
    /**返回当天数据统计*/
    ServerResponse getDaysStatistics();
    /**返回本周数据统计*/
    ServerResponse getWeekStatistics();
    /**返回本月数据统计*/
    ServerResponse getMonthStatistics();
    /**返回前7天 每天数据统计*/
    ServerResponse getSevenDaysListStatistics();
    /**返回今年的总金额统计*/
    ServerResponse getYearListStatistics(String year);


}
