package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.TitleForm;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
public interface TitleFormService extends IService<TitleForm> {

}
