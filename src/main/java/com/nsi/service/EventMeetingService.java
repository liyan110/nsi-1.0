package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventMeeting;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-14
 */
public interface EventMeetingService {

    /**
     * 后台-新增方法
     *
     * @param eventMeeting
     * @return
     */
    ServerResponse saveMeeting(EventMeeting eventMeeting);

    /**
     * 后台-修改方法
     *
     * @param eventMeeting
     * @return
     */
    ServerResponse updateMeeting(EventMeeting eventMeeting);

    /**
     * 后台-返回详情分页(默认最新活动倒序)
     *
     * @param pageNum
     * @param pageSize
     * @param type
     * @param startTime
     * @return
     */
    ServerResponse getMeetingList(int pageNum, int pageSize, String type, String startTime);

    /**
     * 后台-根据id删除会议活动
     *
     * @param meetId
     * @return
     */
    ServerResponse deleteMeeting(Integer meetId);

    /**
     * 前台-返回个人参会详情(已付款)
     *
     * @param phone
     * @return
     */
    ServerResponse findEventMeetingList(String phone);

    /**
     * 返回活动详情
     *
     * @param enentId
     * @return
     */
    ServerResponse findOne(Integer enentId);
}
