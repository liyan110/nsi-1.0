package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Talent;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Li Yan
 * @create 2018-09-14
 **/
//通用接口
public interface ICommonApiService {

//    接口异常警告
    ServerResponse ErrorNotify(String env,String data,String url,String api, String errMsg);
//    微信小程序登录（研究院商城）
    ServerResponse MiniProgramLogin_shop(String code);

    String[] checkVerityCode(String code);
}
