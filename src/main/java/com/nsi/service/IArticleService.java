package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Advertisement;
import com.nsi.pojo.Article;

/**
 * @author Luo Zhen
 * @create 2018-08-06
 */
public interface IArticleService {

    /**
     * 添加文章
     *
     * @param article
     * @return
     */
    ServerResponse saveOn(Article article);

    /**
     * 删除文章
     *
     * @param articleId
     * @return
     */
    ServerResponse deleteOn(Integer articleId);

    /**
     * 修改文章
     *
     * @param article
     * @return
     */
    ServerResponse updateOn(Article article);

    /**
     * 后台搜索 关键字
     *
     * @param siftType
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse managerList(String siftType,String keyword, int pageNum, int pageSize);

    /**
     * 更换文章模板
     *
     * @return
     */
    ServerResponse updateTemplate();

    /**
     * 前台搜索 类型和分类
     *
     * @param siftType
     * @param articleCat
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse list(String siftType, String articleCat, int pageNum, int pageSize);


    /**
     * 百度收录
     * @return
     */
    String baiduUrlList(String siftType, String articleCat, int pageNum, int pageSize);
    /**
     * 根据Id 返回详情
     *
     * @param articleId
     * @return
     */
    ServerResponse detailOn(Integer articleId);

    /**
     * 根据boardName返回文章Url
     *
     * @return
     */
    ServerResponse findUrlByBoardName();

    /**
     * 根据boardName修改文章Url
     *
     * @param sid
     * @return
     */
    ServerResponse updateUrlByBoardName(String sid);

    /**
     * 文章阅读统计
     *
     * @param articleId
     * @return
     */
    ServerResponse pageStatistics(Integer articleId);

    /**
     * 展示阅读统计值
     *
     * @param articleId
     * @return
     */
    ServerResponse refreshStatistics(Integer articleId);

    /**
     * 链接分销Fcode统计
     *
     * @param Fcode
     * @return
     */
    ServerResponse FcodeStatistics(String Fcode);

    /**
     * 展示 Fcode统计
     *
     * @param Fcode
     * @return
     */
    ServerResponse ShowFcodeStatistics(String Fcode);

    /**
     * 广告组件获取广告
     *
     * @param typeName
     * @return
     */
    ServerResponse getArticleAd(String typeName);

    /**
     * 广告组件发送广告
     *
     * @param advertisement
     * @return
     */
    ServerResponse setArticleAd(Advertisement advertisement);

    /**
     * 返回广告详情
     *
     * @return
     */
    ServerResponse queryByArticle();

    /**
     * 验证文章标题存在
     *
     * @param title
     * @return
     */
    ServerResponse checkTitle(String title);

    ServerResponse findArticleList(int pageNum,int pageSize,String searchKey);

    ServerResponse findArticleName(String keyword);
}
