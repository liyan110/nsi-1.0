package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.EntryAudit;
import com.nsi.vo.ActiveOrderVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-16
 */
public interface EntryAuditService extends IService<EntryAudit> {

    ServerResponse add(EntryAudit entryAudit);

    ServerResponse update(EntryAudit entryAudit);

    ServerResponse selectList(int pageNum, int pageSize, EntryAudit entryAudit);

    ServerResponse updateSuccessAudit(EntryAudit entryAudit);

    void deleteById(Integer entryId);

    List<ActiveOrderVo> checkTicketApproved(String telphone);


    ServerResponse selectById(Integer entryId);

    List<ActiveOrderVo> getEntryListLikekey(String searchKey);

}
