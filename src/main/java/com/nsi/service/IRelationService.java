package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Relation;

/**
 * @author Li Yan
 * @create 2019-05-22
 **/
public interface IRelationService {

    ServerResponse create(Relation relation);

    ServerResponse search(String type, String searchId);

    void delate(int id);

    void modify(Relation relation);
}
