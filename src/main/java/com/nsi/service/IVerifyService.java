package com.nsi.service;

import com.nsi.common.ServerResponse;

import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/11/20 10:32
 * @description:
 */
public interface IVerifyService {

    Map<String, Object> getVerifyNotificationInfo();

    ServerResponse getVerifyInstitutionInfo(int pageNum, int pageSize, Integer verifySign);

    ServerResponse modifyInstitutionVerifyStatus(Integer institutionId, Integer verifySign);

    ServerResponse getNoVerifySchoolInfo(int pageNum, int pageSize, String verifySign);

    ServerResponse modifySchoolVerifyStatus(Integer schoolId, String verifySign);
}
