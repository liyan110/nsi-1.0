package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.NewTalent;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-11-26
 */
public interface NewTalentService extends IService<NewTalent> {

    /**
     * 返回详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    ServerResponse findList(int pageNum, int pageSize, String searchKey, Integer isCheck);

    /**
     * 根据提交人邮箱，返回人才简历列表
     *
     * @param searchKey
     * @return
     */
    ServerResponse queryList(String searchKey);
}
