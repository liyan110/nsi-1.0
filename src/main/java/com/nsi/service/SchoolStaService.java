package com.nsi.service;

import com.nsi.vo.SchoolTeacherStatistics;

import java.util.List;
import java.util.Map;

public interface SchoolStaService {

    /**
     * 学校属性占比
     *
     * @return
     */
    List<Map<String, Integer>> getSchoolRatio();


    /**
     * 所有认证学校数量
     *
     * @return
     */
    Map<String, Object> getAllSchoolRatio();

    /**
     * 课程占有率
     *
     * @return
     */
    List<Map<String, Object>> getCourseRatio();


    /**
     * 不同类型课程分布占比率
     *
     * @return
     */
    Map<String, Object> getCoursePercent(String province);

    /**
     * 统计学校各状态总数
     *
     * @return
     */
    List<Map<String, Object>> getOperatorStateCount();

    /**
     * 统计教师总数/外籍教师总数/在校生总数/学位总数
     *
     * @return
     */
    List<SchoolTeacherStatistics> getSchoolTeacherSta();

    /**
     * 不同学段的(公办/民办/外籍)数目统计
     *
     * @return
     */
    Map<String, Object> getStudentSection();

    /**
     * 返回十年内学校数目增长数量
     *
     * @return
     */
    Map<String, Object> getTenYearSchoolNum(Integer time);

    /**
     * 返回所有省份
     *
     * @return
     */
    List<String> getAllProvince();

    /**
     * 各个省份幼/小/初/高平均学费
     *
     * @return
     */
    Map<String, Object> getAverageTuition();

    /**
     * 各省不同学段平均学费
     *
     * @param province
     * @return
     */
    Map<String, Object> getAverageTuitionByProvince(String province);

    /**
     * 不同城市个认证机构学校数量
     *
     * @param province
     * @return
     */
    Map<String, Object> getSchoolCertification(String province);
}
