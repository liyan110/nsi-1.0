package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.ClassComment;

/**
 * @author Li Yan
 * @create 2018-08-22
 **/
public interface ICommentService {

    ServerResponse<String> add(ClassComment classComment);

    ServerResponse<String> TeacherAdd(ClassComment classComment);

    ServerResponse<String> list(String CourseId);

    ServerResponse<String> myComment(String UserMail);

    ServerResponse<String> alter(ClassComment classComment);

    ServerResponse<String> CommentVerify(String CommentId);

    /**
     * 返回课程详情带分页
     *
     * @param objectId
     * @return
     */
    ServerResponse Admin_list(String objectId, int pageNum, int pageSize);
}
