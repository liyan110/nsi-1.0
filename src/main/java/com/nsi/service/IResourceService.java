package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Luo Zhen
 * @create 2018-08-03
 */
public interface IResourceService {

    /**
     * 资源文件上传
     *
     * @param file
     * @param path
     * @return
     */
    String upFile(MultipartFile file, String path);

    /**
     * 添加方法
     *
     * @param resource
     * @return
     */
    ServerResponse saveOn(Resource resource);

    /**
     * 修改方法
     *
     * @param resource
     * @return
     */
    ServerResponse update(Resource resource);

    /**
     * 刪除方法
     *
     * @param resourceId
     * @return
     */
    ServerResponse delete(Integer resourceId);

    /**
     * 获取列表详细信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getList(String type, String year, int pageNum, int pageSize);

    ServerResponse detail(int id);
}
