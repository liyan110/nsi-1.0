package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.nsi.pojo.CommunityCommentSon;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 社区-子评论表 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-17
 */
public interface CommunityCommentSonService extends IService<CommunityCommentSon> {

    ServerResponse insertCommentSon(CommunityCommentSon communityCommentSon);

    ServerResponse delete(int id);

    ServerResponse list(String id,int pageNum,int pageSize);

    CommunityCommentSon detail(int id);

    List<CommunityCommentSon> listPage3(int id);

    ServerResponse verify_list(String type,String searchKey,int pageNum,int pageSize);

    int verify_pass(int id);

    ServerResponse verify_reject(int id);

//    自动审核相关

    List<CommunityCommentSon> auto_verify_list(String type);

//    int auto_verify_pass(int id);

}
