package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Institution;

/**
 * @author Luo Zhen
 * @create 2018-08-20
 **/
public interface IInstitutionService extends IService<Institution> {


    /**
     * 添加方法
     *
     * @param institution
     * @return
     */
    ServerResponse<String> addInstitution(Institution institution) throws NsiOperationException;

    /**
     * 修改方法
     *
     * @param institution
     * @return
     */
    ServerResponse<String> modifyInstitution(Institution institution) throws NsiOperationException;


    /**
     * 根据id 删除数据
     *
     * @param institutionId
     * @return
     */
    ServerResponse<String> deleteOn(Integer institutionId);


    /**
     * 根据关键字查询
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    ServerResponse getList(int pageNum, int pageSize, String searchKey, Integer verifyCode);

    /**
     * 根据id 返回详情
     *
     * @param institutionId
     * @return
     */
    ServerResponse findInstitutionById(Integer institutionId);


    ServerResponse checkValidetName(String institutionName);

    ServerResponse suggest_search(String keyword);

}
