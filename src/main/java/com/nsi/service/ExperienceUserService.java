package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.ExperienceUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2021-03-17
 */
public interface ExperienceUserService extends IService<ExperienceUser> {

}
