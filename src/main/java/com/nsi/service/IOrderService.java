package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.CourseList;
import com.nsi.pojo.Order;

import java.util.List;

public interface IOrderService {

    /**
     * 创建订单
     */
    ServerResponse createOrder(Order order);

    /**
     * 创建课程订单
     */
    ServerResponse createOrderByCourse(Order order, CourseList courseList);

    /**
     * 根据weichatId获取订单详情
     */
    ServerResponse getOrderByWechatIdAndUnionId(String wechatId, String unionId);

    /**
     * 取消订单
     */
    ServerResponse cancel(String wechatId, String unionId);

    /**
     * 创建订单通用
     */
    int saveOrder(Order order);

    /**
     * 我的页面  返回书店订单list
     */
    ServerResponse getOrderList(String wechatId, String unionId);

    /**
     * 我的页面  返回课程订单list
     */
    ServerResponse getOrderCourseList(List<Order> orderList);

    List<Order> findOrderList(String wechatId, String unionId, String productType);

    /**
     * 我的页面  根据订单号返回订单详情
     */
    ServerResponse findOrderItemByOrderNo(String orderNo);

    /**
     * 我的页面  根据wechatId和orderNo 取消订单
     */
    ServerResponse cancelMe(String wechatId, String orderNo);


    /**
     * 后台 返回订单详情
     */
    ServerResponse findOrderByPage(int pageNum, int pageSize, String status, String productType, String searchKey);

    /**
     * 后台 根据订单号 删除订单
     */
    ServerResponse removeOrder(Long orderNo);

    ServerResponse modifyOrder(Order order);

    ServerResponse findOrderStatusCount(String wechatId, String unionId);

    ServerResponse createOrderByShoppingCart(Order order);

    Order findOrderstatusBywechatId(String wechatId);

    ServerResponse createOrderByActivity(Order order);

    /**
     * 根据订单号修改订单状态
     *
     * @param orderNo
     */
    void updateByOrderNo(String orderNo);

    int findOrderNoAndStatus(String orderNo);
}
