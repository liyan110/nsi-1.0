package com.nsi.service;

import com.nsi.pojo.EventApplyHead;
import com.nsi.pojo.EventTicket;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-09-03
 **/
public interface IEventTicketService {

    int insert(EventTicket eventTicket);

    int update(EventTicket eventTicket);

    List<EventTicket> detailByEventId(Integer eventId);

}
