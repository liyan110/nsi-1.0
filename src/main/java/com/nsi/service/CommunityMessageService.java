package com.nsi.service;

import com.nsi.pojo.CommunityMessage;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 社区消息 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-26
 */
public interface CommunityMessageService extends IService<CommunityMessage> {

    int insertMessage(String messageType,String messageWechatId,String messageContent,String objectId,String jumpId);

    List<CommunityMessage> list (String wechatId,int checkState,int pageNum,int pageSize);

    int checked(int messageId);

}
