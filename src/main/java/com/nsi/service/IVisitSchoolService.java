package com.nsi.service;


import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.SchoolVisit;

public interface IVisitSchoolService extends IService<SchoolVisit> {

    ServerResponse findAll(int pageNum, int pageSize, Integer schoolId, String openId);

    ServerResponse save(SchoolVisit schoolVisit);
}
