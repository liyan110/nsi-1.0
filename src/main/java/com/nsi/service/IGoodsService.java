package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Configure;
import com.nsi.pojo.ShopGoods;

/**
 * @author Li Yan
 * @create 2018-12-28
 **/
public interface IGoodsService {

    /**
     * 商品列表
     *
     * @param type
     * @param state
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<ShopGoods> productList(String type, String state, String searchKey, int pageNum, int pageSize);

    /**
     * 商品详情
     *
     * @param Id
     * @return
     */
    ServerResponse<ShopGoods> productDetail(int Id);

    /**
     * 添加商品
     *
     * @param goods
     * @return
     */
    ServerResponse<ShopGoods> productSave(ShopGoods goods);

    /**
     * 删除商品
     *
     * @param id
     * @return
     */
    ServerResponse<ShopGoods> productRemove(int id);

    /**
     * 商品修改
     *
     * @param good
     * @return
     */
    ServerResponse productUpdate(ShopGoods good);

    /**
     * 获取首页配置
     *
     * @param type
     * @return
     */
    ServerResponse getHomeConfig(String type);

    /**
     * 设置首页配置
     *
     * @param configure
     * @return
     */
    ServerResponse setHomeConfig(Configure configure);
}
