package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.EventCollect;
import com.nsi.vo.EventCollectOrderVo;
import com.nsi.vo.EventOrderVo;

import java.util.List;

/**
 * <p>
 * 活动用户报名表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-23
 */
public interface EventCollectService extends IService<EventCollect> {

    List List_byEventId(Integer EventId, Integer orderStatus,String searchKey);

    List List_byPhone(String phone,Integer activeId);

    List<EventCollectOrderVo> findEventList(Integer verify, Integer status, Integer artiveId, String searchKey);

    List<EventOrderVo> findByActiveIdOrEventCollectAndOrder(Integer acticeId, String searchKey);

    List<EventOrderVo> findByActivePayList(Integer activeId,String searchKey);

    int findByActivePayListCount(Integer activityId);

    int findByActiveCheckInCount(Integer activityId);

}
