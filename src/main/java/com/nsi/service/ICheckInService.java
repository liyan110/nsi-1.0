package com.nsi.service;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CheckIn;

public interface ICheckInService {

    ServerResponse<String> saveOn(CheckIn checkIn);

    ServerResponse<PageInfo> list(int type, String keyword, int pageNum, int pageSize);

    ServerResponse<String> updateOn(CheckIn checkIn);

    ServerResponse delete(String checkIds);

    ServerResponse getCheckInfo(String token);

    ServerResponse updateCheckInTime(String token);

}
