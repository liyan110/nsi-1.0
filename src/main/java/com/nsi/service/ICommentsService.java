package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Comments;

/**
 * @author: Luo Zhen
 * @date: 2018/11/28 10:14
 * @description:
 */
public interface ICommentsService {

    ServerResponse saveComments(Comments comments);

    ServerResponse selectByGuestId(Integer guestId);

    ServerResponse modifyConmments(Comments comments);

    ServerResponse getCommentsByType(Integer guestId, String type);
}
