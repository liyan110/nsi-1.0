package com.nsi.service;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.OldArticles;

/**
 * @author Luo Zhen
 * @create 2018-07-30
 **/
public interface IPreviousArticlesService {

    /**
     * 根据分页返回详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<PageInfo> getArticleList(int pageNum, int pageSize);

    /**
     * 根据Id 返回详情
     *
     * @param oldArticleId
     * @return
     */
    ServerResponse<OldArticles> getAllById(Integer oldArticleId);
}
