package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Log;
import com.nsi.vo.UserPayInfoVo;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-14
 */
public interface ILogService extends IService<Log> {

    /**
     * 根据推荐人返回详情
     *
     * @param keyword
     * @return
     */
    ServerResponse getList(String keyword);

    /**
     * 根据邮箱查询活动表详情(微信/支付宝)
     *
     * @return
     */
  //  List<UserPayInfoVo> getPayUserInfoList(List<Log> listLog);

    ServerResponse saveLog(Log log);

    ServerResponse getH5LogList(String index03, String index04, String index05, String index06);
}
