package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Recuitment;
import com.nsi.pojo.RecuitmentInfo;

import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/11/23 10:43
 * @description:
 */
public interface IRecuitmentService {

    ServerResponse addRecuitment(Recuitment recuitment);

    ServerResponse addRecuitmentInfo(RecuitmentInfo recuitmentInfo);

    Map<String, Object> getRecuitmentInfoList(int pageNum, int pageSize, String searchKey);

    ServerResponse selectAllById(Integer recuitmentId);
}
