package com.nsi.service;

import com.nsi.pojo.Postil;

import java.util.List;

public interface IPostilService {

    /**
     * 新增
     *
     * @param postil
     */
    void save(Postil postil);

    /**
     * 修改
     *
     * @param postil
     */
    void modify(Postil postil);

    /**
     * 删除
     *
     * @param postilId
     */
    void delete(Integer postilId);

    /**
     * 返回详情
     *
     * @param schoolId
     * @return
     */
    List<Postil> list(Integer schoolId);
}
