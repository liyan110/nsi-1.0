package com.nsi.service;

import com.nsi.pojo.Configure;
import com.nsi.pojo.SchoolNew;

public interface TemplateService {

    /**
     * 上传学校库模板
     *
     * @param schoolNew
     */
    void uploadSchoolTemplateOOS(SchoolNew schoolNew, Configure configure);
}
