package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.Exhibitor;

/**
 * <p>
 * 展位表 服务类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-02
 */
public interface ExhibitorService extends IService<Exhibitor> {

    boolean insert(Exhibitor exhibitor);

    ServerResponse findList(int pageNum, int pageSize, Exhibitor exhibitor);

    void thumbup(Integer exhibitorId);

    ServerResponse thumbupForMaster(Integer masterId);

    boolean getThumbUpConfigureType(String token);

}
