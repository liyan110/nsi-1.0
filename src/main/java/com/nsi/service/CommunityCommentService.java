package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 社区-标准评论表 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
public interface CommunityCommentService extends IService<CommunityComment> {


    ServerResponse insertComment(CommunityComment communityComment) throws Exception;

    ServerResponse delete(int id);

    ServerResponse list(String id, int pageNum, int pageSize);

    CommunityComment detail(int id);

    List<CommunityComment> CommentAndSonlist(String id, int pageNum, int pageSize);

    ServerResponse myComment(String wechatId, int pageNum, int pageSize);

    int verify_pass(int id);

    ServerResponse verify_reject(int id);

    ServerResponse verify_list(String type, String searchKey, int pageNum, int pageSize);

    //更新评论表 的 子评论数

    int Add_son_comments_num(int id);

    //    自动审核相关
    List<CommunityComment> auto_verify_list(String type);


}
