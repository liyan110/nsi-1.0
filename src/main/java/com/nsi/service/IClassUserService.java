package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Course_user;

/**
 * @author Li Yan
 * @create 2018-10-26
 **/
public interface IClassUserService {

    ServerResponse saveCourseUser (String ClassId,String UserMail);

    ServerResponse shopSaveCourseUser (String ClassId,String wechatId);



    Course_user findCourseUserByClassidAndUsermail(String classId, String usermail);
}
