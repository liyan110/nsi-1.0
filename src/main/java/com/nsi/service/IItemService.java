package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Item;

public interface IItemService {

    ServerResponse saveItem(Item item);

    ServerResponse findItemById(Integer itemId);

    ServerResponse modifyItem(Item item) throws NsiOperationException;

    ServerResponse findItemList(int pageNum, int pageSize, String searchKey);

    ServerResponse removeItem(Integer itemId);

    ServerResponse findItemList();

}
