package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.ActivityTickets;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-26
 */
public interface ActivityTicketsService extends IService<ActivityTickets> {

}
