package com.nsi.service;

import com.nsi.pojo.CourseCategory;
import com.nsi.vo.CourseItemVo;
import com.nsi.vo.CourseListVo;

import java.util.List;

public interface ICourseCategoryService {

    void saveCourseCategory(CourseCategory category);

    CourseCategory findCourseCategoryById(Integer courseListId);

    void deleteCourseCategory(Integer courseListId);

    void updateCourseCategory(CourseCategory category);

    List<CourseCategory> findAll(String status);

    List<CourseCategory> findCourseCategoryByListId(Integer listId);

    void deleteCourseCategoryByListId(Integer courseListId);

    CourseCategory findCourseCategoryByCourseId(Integer courseId);

    List<CourseItemVo> selectByListId(Integer listId, Integer status);
}
