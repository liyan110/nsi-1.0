package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityUser;

/**
 * <p>
 * 社区用户 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-13
 */
public interface CommunityUserService extends IService<CommunityUser> {

    //    废弃
    ServerResponse login(String wechatId);
    //    废弃
    ServerResponse register(CommunityUser communityUser);

    ServerResponse userLogin(CommunityUser communityUser);

    ServerResponse update(CommunityUser communityUser);

    ServerResponse list(String orderByColumn,String searchKey,String gradeSign,int pageNum,int pageSize);

    ServerResponse panel_list();

    ServerResponse test(String imgUrl);

    CommunityUser detail(String wechatId);

    int SendSubscribeMessage() throws Exception;

    CommunityUser getUserInfo(String wechatId);
//    增加用户积分
    int AddScore(int Num,String wechatId);

}
