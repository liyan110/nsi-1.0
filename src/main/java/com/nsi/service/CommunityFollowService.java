package com.nsi.service;

import com.nsi.pojo.CommunityFollow;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 社区关注 服务类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
public interface CommunityFollowService extends IService<CommunityFollow> {

}
