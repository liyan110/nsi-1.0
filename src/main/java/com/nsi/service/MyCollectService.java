package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.MyCollect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 四库全书-我的收藏表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-09-02
 */
public interface MyCollectService extends IService<MyCollect> {

    ServerResponse findCollectList(int pageNum, int pageSize, String openId, Integer collectType);
}
