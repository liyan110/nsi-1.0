package com.nsi.service;

import com.nsi.pojo.EventApplyHead;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-08-16
 **/
public interface IEventApplyHeadService {

    int insert(EventApplyHead eventApplyHead);

    int update(EventApplyHead eventApplyHead);

    EventApplyHead detailByEventId(Integer EventId);

}
