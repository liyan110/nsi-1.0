package com.nsi.service;

import com.nsi.pojo.CommunityAnswer;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 提问互动-回复表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
public interface CommunityAnswerService extends IService<CommunityAnswer> {

    List<CommunityAnswer> listByAsk_id(int id,int pageNum,int pageSize);

}
