package com.nsi.service;

import com.nsi.pojo.CourseList;
import com.nsi.pojo.EventCourse;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-08-14
 **/
public interface IEventCourseService {

    int insert (EventCourse eventCourse);

    int del (int id);

    int update (EventCourse eventCourse);

    List<EventCourse> listByEventId(Integer EventId);
}
