package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.People;

/**
 * @author: Luo Zhen
 * @date: 2018/9/21 13:42
 * @description:
 */
public interface IPeopleService {

    /**
     * 验证参数
     *
     * @param str
     * @param type
     * @return
     */
    ServerResponse checkValid(String str, String type);

    /**
     * 后台查询根据关键字带分页
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getPeopleList(String searchKey, int pageNum, int pageSize);

    /**
     * 后台查询根据关键字带分页
     *
     * @param searchKey
     * @param type
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getPeopleListByType(String searchKey, int type, int pageNum, int pageSize);

    /**
     * 后台添加操作
     *
     * @param people
     * @return
     */
    ServerResponse addPeople(People people) throws NsiOperationException;

    /**
     * 根据Id 返回详情
     *
     * @param peopleId
     * @return
     */
    ServerResponse findAll(Integer peopleId);

    /**
     * 后台修改操作
     *
     * @param people
     * @return
     */
    ServerResponse modifyPeople(People people) throws NsiOperationException;

    ServerResponse removePeople(Integer peopleId);
}
