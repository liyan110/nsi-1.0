package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.CourseList;

import java.util.List;

public interface ICourseListService extends IService<CourseList> {

    void saveCourseList(CourseList courseList);

    List<CourseList> findAll(String status);

    void updateCourseList(CourseList courseList);

    void deleteCourseList(Integer courseListId);

    CourseList findCourseListById(Integer courseListId);

    List<CourseList> findCourseListItem();

}
