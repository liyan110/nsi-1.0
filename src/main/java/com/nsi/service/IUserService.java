package com.nsi.service;


import com.nsi.common.ServerResponse;
import com.nsi.pojo.User;

import javax.servlet.http.HttpServletRequest;

public interface IUserService {

    void deleteByUserId(Integer userId);

    ServerResponse<String> register(User user);

    ServerResponse<String> phoneRegister(User user);

    ServerResponse<String> UsermailVerify(String Usermail, String VerifyCode);

    ServerResponse<String> login(String username, String password);

    ServerResponse<String> verify(String userName, String member_sign, String UserVerifyCode);

    void saveUser(User user);

    /**
     * 根据wechatId查询用户详情
     */
    User findUserByWechatId(String wechatId);

    User findByWechatIdAndUnionId(String wechatId, String unionId);

    ServerResponse<String> UserMailCheck(String UserMail);

    ServerResponse<User> getUserInfo(String userName);

    ServerResponse<String> updateInfomation(User user);

    ServerResponse<String> updateUserInformation(User user);

    ServerResponse getUserInformation(String userName);


    /**
     * 忘记密码01-发送验证码
     */
    ServerResponse forgetPW(String UserMail);

    /**
     * 忘记密码02-验证
     */
    ServerResponse forgetPWverify(String UserMail, String Code);

    /**
     * 修改密码-忘记密码03（通用）
     */
    ServerResponse PWAlter(String UserMail, String password);

    //    微信扫码相关
//    获取微信 OpenId
    ServerResponse<String> WechatGetOpenId(String code);

    //    微信扫码登录
    ServerResponse WechatLogin(String OpenId);

    //    微信用户绑定
    ServerResponse WechatBinding(String UserName, String Password, String OpenId);

    //用户反馈
    ServerResponse feedback(String UserName, String Content, String Contact);

    //增加用户积分
    ServerResponse Score(String UserMail, String ScoreNum);

    ServerResponse getUserList(String search_Key, int pageNum, int pageSize);

    //删除测试账号
    ServerResponse deleteTestUser(String UserMail);

    ServerResponse completionWechatUser(User user);

    void saveMallRegister(User user);

    User selectByUserMail(String username);

    ServerResponse modifyUserByWechatId(String wechatId, String unionId);

    /**
     * 微信扫码关注公众号登录
     *
     * @return
     */
    ServerResponse getQrcodeStr();

    /**
     * 微信扫码登录回调 用户信息入库
     *
     * @param request
     */
    void getCallback(HttpServletRequest request);

    /**
     * 微信轮询验证是否登录
     *
     * @param sceneStr
     * @return
     */
    ServerResponse checkLoginBySceneStr(String sceneStr);

    /**
     * 发送短信
     *
     * @param mobile
     */
    void sendSms(String mobile);


    //公众号扫码登录 发送公众号通知
    Boolean sendWechatPublic_msg(String openId);

    /**
     * 根据用户名和密码绑定unionId
     *
     * @param username
     * @param unionId
     * @return
     */
    ServerResponse wechatBindUnionId(String username, String unionId);

    ServerResponse registerBySiKu(User user);

    ServerResponse findUserByUnIonId(String unionId);

    /**
     * 后台登陆
     *
     * @param username
     * @param password
     * @return
     */
    User sysLogin(String username, String password);
}
