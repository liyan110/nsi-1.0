package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Log;

/**
 * @author Li Yan
 * @create 2018-10-13
 **/
public interface IVisWechatUserService {

    ServerResponse<String> VisWechatUserLaunch_insert(Log log);

    ServerResponse<String> VisWechatUserLaunch_check(String OpenId);

    ServerResponse<String> VisWechatUserShare_check(String OpenId,String LaunchOpenId);

    ServerResponse<String> VisWechatUserLaunch_list(String OpenId);

    ServerResponse<String> VisWechatUserShare_insert(Log log,String LaunchOpenId);



}
