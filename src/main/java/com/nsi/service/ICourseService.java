package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Course;

import java.util.Map;

/**
 * @author Li Yan
 * @create 2018-10-23
 **/
public interface ICourseService {

    ServerResponse<Course> Course_list(String Id,String CourseSubject);

    ServerResponse<Course> MyCourse(String UserMail);

    ServerResponse<String> Insert_Course(Course course);

    ServerResponse<String> alter_Course(Course course);

    ServerResponse<String> Delete_Course(int Class_Id);

    ServerResponse<Course> Admin_Course_list();

    ServerResponse<String>  LiveCourseSet(String TeacherId, String CourseId);

    Map ShowLiveCourse();

    ServerResponse<String>  AdminSearch_Course_User(String UserMail, String ClassId,String pageNum, String OnePageNum);




}
