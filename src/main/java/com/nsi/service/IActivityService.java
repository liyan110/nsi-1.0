package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Activity;
import com.nsi.pojo.Vis2019;

/**
 * @author Luo Zhen
 */
public interface IActivityService {

    /**
     * 图片活动 返回图片地址
     *
     * @param nickName
     * @param mark
     * @return
     */
    ServerResponse resultImageUrl(String nickName, Integer mark);

    /**
     * 添加活动
     *
     * @param activity
     * @return
     */
    ServerResponse saveon(Activity activity);

    /**
     * 查询活动列表功能
     *
     * @return
     */
    ServerResponse searchList();


    Activity findOne(Integer activityId);

    /**
     * 查询信息列表功能
     *
     * @param title1
     * @return
     */
    ServerResponse searchInfo(String title1);

    /**
     * 返回合成图片地址
     *
     * @param imageUrl
     * @return
     */
    ServerResponse getTicketImageUrl(String imageUrl);

    /**
     * 返回VIS门票地址
     *
     * @param referrer
     * @param username
     * @return
     */
    ServerResponse getVISImgUrl(String referrer, String username, String type);

    /**
     * 支付成功，根据用户邮箱和课程id 更改购买状态
     *
     * @param usermail
     * @param classId
     * @return
     */
    ServerResponse modifyUserInfo(String usermail, String classId);

    //年会拔河
    ServerResponse TugOfWar_Insert(String nickname, String portrait, String openid, String number, String camp);

    ServerResponse TugOfWar_update(String openid, String number);

    ServerResponse TugOfWar_MyNumber(String openid);

    ServerResponse TugOfWar_Camp(String camp);

    ServerResponse TugOfWar_Count();

    ServerResponse TugOfWar_PeopleList(String camp);

    ServerResponse TugOfWar_AllPeopleList();

    ServerResponse TugOfWar_Delete(int id);

    //2019vis
    ServerResponse vis_insert(Vis2019 vis2019);

    ServerResponse vis_list(String type, int pageNum, int pageSize);

    ServerResponse vis_detail(int id);

    ServerResponse vis_list_byPhone(String phone);

    ServerResponse vis_orderList(String type, int pageNum, int pageSize);

    /**
     * 活动查询用户信息功能
     *
     * @param collectId
     * @return
     */
    ServerResponse activityCheckInfo(int collectId);

    /**
     * 活动签到功能
     *
     * @param collectId 活动Id
     * @return
     */
    ServerResponse activityCheckIn(int collectId);

    /**
     * 活动查询用户关联订单列表功能
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse activityCheckList(String searchKey, Integer acticeId, int pageNum, int pageSize);

    /**
     * 图片合成功能
     *
     * @param qrImgUrl
     * @param username
     * @param type
     * @return
     */
    ServerResponse getImageSynthesis(String qrImgUrl, String username, String type);

    /**
     * 小程序——付费和线下付费统计
     *
     * @param activeId
     * @return
     */
    ServerResponse getCheckStatistics(Integer activeId);

    /**
     * 小程序——回馈和嘉宾统计
     *
     * @param activeId
     * @return
     */
    ServerResponse getCheckFeedBackGuestStatistics(Integer[] activeId);
}
