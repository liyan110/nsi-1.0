package com.nsi.service;

import com.nsi.common.ServerResponse;
import com.nsi.pojo.Assignment;

/**
 * @author Luo Zhen
 * @create 2018-08-15
 **/
public interface IAssignmentService {

    /**
     * 作业添加
     *
     * @param assignment
     * @return
     */
    ServerResponse<String> saveOn(Assignment assignment);

    /**
     * 根据用户邮箱，返回详情
     *
     * @param usermail
     * @return
     */
    ServerResponse<Assignment> detail(String usermail);

    /**
     * 根据课程id 返回详情
     *
     * @param courseId
     * @return
     */
    ServerResponse getInfoByCourseId(Integer courseId);

    /**
     * 返回教师提交的作业列表
     *
     * @param courseId
     * @return
     */
    ServerResponse getTeacherAssignment(Integer courseId);

    /**
     * 修改方法
     *
     * @param assignment
     * @return
     */
    ServerResponse setAssignment(Assignment assignment);

    /**
     * 返回学生提交作业详情
     *
     * @param courseId
     * @return
     */
    ServerResponse findAssignmentByCourseId(Integer courseId);


    /**
     * 后台功能——返回学生作业列表未审核
     *
     * @param pageNum
     * @param pageSize
     * @param courseId
     * @param validCode
     * @return
     */
    ServerResponse queryStudentAssignment(int pageNum, int pageSize, String courseId, Integer validCode);

    /**
     * 后台功能——修改作业状态
     *
     * @param assignmentId
     * @return
     */
    ServerResponse setStudentAssignmentStutus(Integer assignmentId);

    /**
     * 根据id 删除作业列表
     *
     * @param assignmentId
     * @return
     */
    ServerResponse removeAssignment(Integer assignmentId);
}
