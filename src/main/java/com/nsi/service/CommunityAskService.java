package com.nsi.service;

import com.nsi.pojo.CommunityAsk;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提问互动-提问表 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
public interface CommunityAskService extends IService<CommunityAsk> {

}
