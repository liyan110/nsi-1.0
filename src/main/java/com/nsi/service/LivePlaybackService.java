package com.nsi.service;

import com.nsi.pojo.LivePlayback;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
public interface LivePlaybackService extends IService<LivePlayback> {

    /**
     * 添加观看数
     *
     * @param id
     */
    void addWatchNum(Integer id);

}
