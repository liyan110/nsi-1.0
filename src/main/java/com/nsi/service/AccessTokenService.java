package com.nsi.service;

public interface AccessTokenService {

    String getAccessToken(String appId, String secret) throws Exception;
}
