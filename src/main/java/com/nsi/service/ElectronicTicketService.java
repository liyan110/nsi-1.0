package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.pojo.ElectronicTicket;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-04-01
 */
public interface ElectronicTicketService extends IService<ElectronicTicket> {

}
