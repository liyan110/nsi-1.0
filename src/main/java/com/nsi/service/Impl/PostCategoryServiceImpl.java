package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.PostCategoryMapper;
import com.nsi.pojo.PostCategory;
import com.nsi.service.PostCategoryService;
import com.nsi.vo.nsi_shop_bar.PostVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 帖子类目表 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@Service
public class PostCategoryServiceImpl extends ServiceImpl<PostCategoryMapper, PostCategory> implements PostCategoryService {

    private static final Integer PARENT_ID = 0;

    @Autowired
    private PostCategoryMapper postCategoryMapper;

    @Override
    public List<PostVO> findList() {
        // 查询父类
        EntityWrapper wrapper = new EntityWrapper();
        wrapper.eq("parent_id", PARENT_ID);
        List<PostCategory> postList = this.selectList(wrapper);

        // 组装PostVO
        PostVO postVO = null;
        List<PostVO> postVOS = new ArrayList<>();
        for (PostCategory category : postList) {
            postVO = this.assemblePostVO(category);
            postVOS.add(postVO);
        }
        return postVOS;
    }

    private PostVO assemblePostVO(PostCategory category) {
        PostVO postVo = new PostVO();
        BeanUtils.copyProperties(category, postVo);
        List<PostCategory> postCategoryList = new ArrayList<>();
        if (!postVo.getId().equals(PARENT_ID)) {
            postCategoryList = postCategoryMapper.selectList(
                    new EntityWrapper<PostCategory>().eq("parent_id", category.getId()));

        }
        postVo.setPostCategory(postCategoryList);
        return postVo;
    }


    @Override
    public List<PostCategory> findCategoryList() {
        return this.selectList(
                new EntityWrapper<PostCategory>()
                        .setSqlSelect("id", "category_name as categoryName")
                        .ne("parent_id", PARENT_ID)
        );
    }

}
