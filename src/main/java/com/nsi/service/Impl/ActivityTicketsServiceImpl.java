package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.ActivityTicketsMapper;
import com.nsi.pojo.ActivityTickets;
import com.nsi.service.ActivityTicketsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-26
 */
@Service
public class ActivityTicketsServiceImpl extends ServiceImpl<ActivityTicketsMapper, ActivityTickets> implements ActivityTicketsService {

}
