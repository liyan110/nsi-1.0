package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.dao.EventApplyHeadMapper;
import com.nsi.pojo.EventApplyHead;
import com.nsi.service.IEventApplyHeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Li Yan
 * @create 2019-08-16
 **/
@Service
public class EventApplyHeadServiceImpl implements IEventApplyHeadService {

    @Autowired
    private EventApplyHeadMapper eventApplyHeadMapper;

    @Override
    public int insert(EventApplyHead eventApplyHead) {
        int i = eventApplyHeadMapper.insert(eventApplyHead);
        return i;
    }

    @Override
    public int update(EventApplyHead eventApplyHead) {
        EntityWrapper<EventApplyHead> wrapper = new EntityWrapper<>();
        wrapper.eq("id", eventApplyHead.getId());
        int i = eventApplyHeadMapper.update(eventApplyHead, wrapper);
        return i;
    }

    @Override
    public EventApplyHead detailByEventId(Integer EventId) {
        EntityWrapper<EventApplyHead> wrapper = new EntityWrapper<>();
        wrapper.eq("event_id", EventId);
        EventApplyHead eventApplyHead = eventApplyHeadMapper.selectList(wrapper).get(0);
        return eventApplyHead;
    }
}
