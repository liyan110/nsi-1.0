package com.nsi.service.Impl;

import com.nsi.common.ServerResponse;
import com.nsi.dao.RelationMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Relation;
import com.nsi.service.IRelationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2019-05-22
 **/

@Service
public class RelationServiceImpl implements IRelationService {

    @Autowired
    private RelationMapper relationMapper;

    @Override
    @Transactional
    public ServerResponse create(Relation relation) {
        if (StringUtils.isEmpty(relation.getType())) {
            throw new NsiOperationException("参数不合法");
        }
        //type 转换
        relation.setCreatetime(new Date());
        relationMapper.insert(relation);
        return ServerResponse.createBySuccess("返回成功", relation.getId());
    }

    @Override
    public ServerResponse search(String type, String searchId) {
        List<Relation> list = relationMapper.selectByType(type, searchId);
        return ServerResponse.createBySuccess("返回成功", list);
    }

    @Override
    @Transactional
    public void delate(int id) {
        relationMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void modify(Relation relation) {
        relationMapper.updateById(relation);
    }


}
