package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ShopAddressMapper;
import com.nsi.pojo.ShopAddress;
import com.nsi.service.IShopAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-01-02
 **/
@Service
public class ShopAddressServiceImpl extends ServiceImpl<ShopAddressMapper, ShopAddress> implements IShopAddressService {

    @Autowired
    ShopAddressMapper ShopAddressMapper;

    @Override
    public ServerResponse add(ShopAddress shops) {
        this.insert(shops);
        return ServerResponse.createBySuccess("返回成功");
    }

    @Override
    public ServerResponse update(ShopAddress shops) {
        this.updateById(shops);
        return ServerResponse.createBySuccess("返回成功");
    }


    @Override
    public ServerResponse getAddress(String wechatId, String unionId) {
        List<ShopAddress> addressList = ShopAddressMapper.selectByWechatIdOrUnionId(wechatId, unionId);
        if (CollectionUtils.isEmpty(addressList)) {
            return ServerResponse.createByErrorMessage("返回失败");
        } else {
            return ServerResponse.createBySuccess("返回成功", addressList);
        }
    }
}
