package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ForeignTeacherMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.ForeignTeacher;
import com.nsi.service.ISchoolBlackListService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: Luo Zhen
 * @date: 2018/9/28 15:16
 * @description:
 */
@Service
public class SchoolBlackListServiceImpl implements ISchoolBlackListService {

    @Autowired
    private ForeignTeacherMapper foreignTeacherMapper;

    @Override
    public ServerResponse addSchoolBlackList(ForeignTeacher foreignTeacherCondition) throws NsiOperationException {
        if (StringUtils.isBlank(foreignTeacherCondition.getTeacherName()) || StringUtils.isBlank(foreignTeacherCondition.getInformant())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }

        foreignTeacherCondition.setCreateTime(new Date());
        foreignTeacherCondition.setEnableStatus(ResponseCode.CHECK.getCode());
        try {
            int rowCount = foreignTeacherMapper.insert(foreignTeacherCondition);
            if (rowCount <= 0) {
                return ServerResponse.createByErrorMessage("添加失败");
            } else {
                return ServerResponse.createBySuccessMessage("添加成功");
            }
        } catch (Exception e) {
            throw new NsiOperationException("addSchoolBlackList error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse getSchoolBlackList(String searchKey, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ForeignTeacher> blackLists = foreignTeacherMapper.querySchoolBlackList(searchKey, ResponseCode.PASS.getCode());
        PageInfo pageInfo = new PageInfo(blackLists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse getForeignTeacherInfo(Integer foreignId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ForeignTeacher> list = new ArrayList<>();
        if (foreignId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        ForeignTeacher foreignInfo = foreignTeacherMapper.selectByPrimaryKey(foreignId);
        list.add(foreignInfo);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse getReviewList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ForeignTeacher> foreignList = foreignTeacherMapper.querySchoolBlackList(null, ResponseCode.CHECK.getCode());
        PageInfo pageInfo = new PageInfo(foreignList);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse getForeignList(String searchKey, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ForeignTeacher> blackLists = foreignTeacherMapper.querySchoolBlackList(searchKey, ResponseCode.PASS.getCode());
        PageInfo pageInfo = new PageInfo(blackLists);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse modifyForeignInfo(ForeignTeacher foreignTeacher) throws NsiOperationException {
        try {
            int rowCount = foreignTeacherMapper.updateByPrimaryKeySelective(foreignTeacher);
            if (rowCount <= 0) {
                return ServerResponse.createByErrorMessage("更新信息失败");
            } else {
                return ServerResponse.createBySuccessMessage("更新信息成功");
            }
        } catch (Exception e) {
            throw new NsiOperationException("modify ForeignInfo error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse removeForeignInfo(Integer foreignId) {
        if (foreignId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = foreignTeacherMapper.deleteByPrimaryKey(foreignId);
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("删除信息失败");
        }
        return ServerResponse.createBySuccessMessage("删除信息成功");
    }

    @Override
    public ServerResponse getForeignInfo(Integer foreignId) {
        ForeignTeacher foreignTeacher = foreignTeacherMapper.selectByPrimaryKey(foreignId);
        return ServerResponse.createBySuccess(foreignTeacher);
    }


}
