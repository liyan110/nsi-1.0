package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.AssignmentMapper;
import com.nsi.dao.TeacherMapper;
import com.nsi.dao.UserMapper;
import com.nsi.pojo.Assignment;
import com.nsi.pojo.Teacher;
import com.nsi.pojo.User;
import com.nsi.service.IAssignmentService;
import com.nsi.vo.AssignmentVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-15
 **/
@Service
public class AssignmentServiceImpl implements IAssignmentService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TeacherMapper teacherMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AssignmentMapper assignmentMapper;

    /**
     * 添加方法
     *
     * @param assignment
     * @return
     */
    @Override
    public ServerResponse<String> saveOn(Assignment assignment) {
        if (assignment.getCourseId() == null || StringUtils.isBlank(assignment.getUsermail())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        // 默认是：0   通过：1
        assignment.setVerify(0);
        assignment.setCreateTime(System.currentTimeMillis());

        int rowCount = assignmentMapper.insert(assignment);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }

    /**
     * 根据用户邮箱，返回详情
     *
     * @param usermail
     * @return
     */
    @Override
    public ServerResponse<Assignment> detail(String usermail) {
        if (StringUtils.isBlank(usermail)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Assignment assignment = assignmentMapper.findAssignmentByUserMail(usermail);
        if (assignment != null) {
            return ServerResponse.createBySuccess(assignment);
        }
        return ServerResponse.createBySuccess();
    }

    /**
     * 根据课程id 返回详情
     *
     * @param courseId
     * @return
     */
    @Override
    public ServerResponse getInfoByCourseId(Integer courseId) {
        if (courseId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Assignment assignmentByCourseId = assignmentMapper.findAssignmentByCourseId(courseId);
        if (assignmentByCourseId != null) {
            return ServerResponse.createBySuccess(assignmentByCourseId);
        }
        return ServerResponse.createBySuccess();
    }

    /**
     * 返回教师提交的作业列表
     *
     * @param courseId
     * @return
     */
    @Override
    public ServerResponse getTeacherAssignment(Integer courseId) {
        if (courseId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Assignment assignment = assignmentMapper.findInfoByTeacherId(courseId);
        if (assignment != null) {
            AssignmentVo assignmentVo = assemble(assignment);
            return ServerResponse.createBySuccess(assignmentVo);
        }
        return ServerResponse.createBySuccess();

    }

    /**
     * 修改方法
     *
     * @param assignment
     * @return
     */
    @Override
    public ServerResponse setAssignment(Assignment assignment) {
        if (assignment.getCourseId() == null || StringUtils.isBlank(assignment.getUsermail())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = assignmentMapper.updateByPrimaryKeySelective(assignment);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }

    /**
     * `
     * 返回学生课程详情
     *
     * @param courseId
     * @return
     */
    @Override
    public ServerResponse findAssignmentByCourseId(Integer courseId) {
        if (courseId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        List<Assignment> list = assignmentMapper.findAssignmentListByCourseId(courseId);
        List<AssignmentVo> assignmentVoList = new ArrayList<>();
        for (Assignment assignment : list) {
            AssignmentVo assignmentVo = assemble(assignment);
            assignmentVoList.add(assignmentVo);
        }
        return ServerResponse.createBySuccess(assignmentVoList);
    }

    /**
     * 后台功能——返回学生作业列表未审核
     *
     * @param pageNum
     * @param pageSize
     * @param courseId
     * @param validCode
     * @return
     */
    @Override
    public ServerResponse queryStudentAssignment(int pageNum, int pageSize, String courseId, Integer validCode) {
        PageHelper.startPage(pageNum, pageSize);
        List<Assignment> list;
        if (validCode != null) {
            list = assignmentMapper.findAll(courseId, validCode);
        } else {
            list = assignmentMapper.findOneByVerify(courseId);
        }
        PageInfo pageInfo = new PageInfo(list);
        if (CollectionUtils.isEmpty(list)) {
            logger.info("该课程没有学生提交作业");
        }
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    /**
     * 后台功能——修改作业状态
     *
     * @param assignmentId
     * @return
     */
    @Override
    public ServerResponse setStudentAssignmentStutus(Integer assignmentId) {
        if (assignmentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = assignmentMapper.setAssignmentStatus(assignmentId);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改状态成功");
        }
        return ServerResponse.createByErrorMessage("修改状态失败");
    }

    @Override
    public ServerResponse removeAssignment(Integer assignmentId) {
        if (assignmentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = assignmentMapper.deleteByPrimaryKey(assignmentId);
        if (rowCount < 0) {
            return ServerResponse.createByErrorMessage("删除失败");
        }
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    /**
     * 组合高级模型
     *
     * @param assignment
     * @return
     */
    private AssignmentVo assemble(Assignment assignment) {
        AssignmentVo assignmentVo = new AssignmentVo();
        assignmentVo.setId(assignment.getId());
        assignmentVo.setCourseId(assignment.getCourseId());
        assignmentVo.setUserMail(assignment.getUsermail());
        if (StringUtils.isBlank(assignment.getTeacherId())) {
            //切换数据源
            //DynamicDataSourceHolder.setDataSource("dataSource1");
            User user = userMapper.selectByUserName(assignment.getUsermail());
            if (user != null) {
                assignmentVo.setUserName(user.getUserTurename());
                assignmentVo.setUserPortrait(user.getUserPortrait());
            }
        } else {
            Teacher teacher = teacherMapper.selectByPrimaryKey(Integer.parseInt(assignment.getTeacherId()));
            if (teacher != null) {
                assignmentVo.setUserName(teacher.getTeachername());
                assignmentVo.setUserPortrait(teacher.getTeacherimage());
            }
        }
        assignmentVo.setAssignmentContent(assignment.getAssignmentContent());
        assignmentVo.setAttachmentNameOne(assignment.getAttachmentNameOne());
        assignmentVo.setAttachmentUrlOne(assignment.getAttachmentUrlOne());
        assignmentVo.setAttachmentNameTwo(assignment.getAttachmentNameTwo());
        assignmentVo.setAttachmentUrlTwo(assignment.getAttachmentUrlTwo());
        assignmentVo.setAttachmentNameThree(assignment.getAttachmentNameThree());
        assignmentVo.setAttachmentUrlThree(assignment.getAttachmentUrlThree());

        assignmentVo.setTeacherId(assignment.getTeacherId());
        assignmentVo.setCreateTime(assignment.getCreateTime());
        return assignmentVo;


    }


}
