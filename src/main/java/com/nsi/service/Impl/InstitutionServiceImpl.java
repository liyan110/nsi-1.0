package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.InstitutionMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Institution;
import com.nsi.service.IInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Luo Zhen
 * @create 2018-08-20
 **/
@Service
public class InstitutionServiceImpl extends ServiceImpl<InstitutionMapper, Institution> implements IInstitutionService {

    @Autowired
    private InstitutionMapper institutionMapper;

    /**
     * 添加方法
     *
     * @param institution
     * @return
     */
    @Override
    public ServerResponse<String> addInstitution(Institution institution) {
        if (StringUtils.isBlank(institution.getName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            institution.setLoadTime(new Date());
            int rowCount = institutionMapper.insert(institution);
            if (rowCount < 0) {
                throw new NsiOperationException("添加机构库失败");
            } else {
                return ServerResponse.createBySuccessMessage("添加机构库成功");
            }
        } catch (Exception e) {
            throw new NsiOperationException("addInstitution error:" + e.getMessage());
        }
    }

    /**
     * 修改方法
     *
     * @param institution
     * @return
     */
    @Override
    public ServerResponse<String> modifyInstitution(Institution institution) {
        if (StringUtils.isBlank(institution.getName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            int rowCount = institutionMapper.updateById(institution);
            if (rowCount < 0) {
                return ServerResponse.createByErrorMessage("修改失败");
            } else {
                return ServerResponse.createBySuccessMessage("修改成功");
            }
        } catch (Exception e) {
            throw new NsiOperationException("modifyInstitution error:" + e.getMessage());
        }
    }

    /**
     * 删除失败
     *
     * @param institutionId
     * @return
     */
    @Override
    public ServerResponse<String> deleteOn(Integer institutionId) {
        if (institutionId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = institutionMapper.deleteById(institutionId);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }

    /**
     * 根据关键字查询
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @Override
    public ServerResponse getList(int pageNum, int pageSize, String searchKey, Integer verifyCode) {
        PageHelper.startPage(pageNum, pageSize);
        List<Institution> list = institutionMapper.selectList(this.convertWrapper(searchKey, verifyCode));
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("查询成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }


    private EntityWrapper<Institution> convertWrapper(String searchKey, Integer verifyCode) {
        EntityWrapper wrapper = new EntityWrapper();
        if (StringUtils.isNotBlank(searchKey)) {
            wrapper.like("name", searchKey).or()
                    .like("found_time", searchKey).or()
                    .like("type", searchKey).or()
                    .like("label", searchKey);
        }
        wrapper.eq((verifyCode != null), "verify_sign", verifyCode);
        wrapper.orderBy("id", false);
        return wrapper;
    }

    /**
     * 根据id 返回详情
     *
     * @param institutionId
     * @return
     */
    @Override
    public ServerResponse findInstitutionById(Integer institutionId) {
        if (institutionId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Institution institution = institutionMapper.selectById(institutionId);
        return ServerResponse.createBySuccess(institution);
    }

    @Override
    public ServerResponse checkValidetName(String institutionName) {
        if (StringUtils.isBlank(institutionName)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = institutionMapper.selectCount(
                new EntityWrapper<Institution>()
                        .eq("name", institutionName)
        );
        if (rowCount > 0) {
            return ServerResponse.createByErrorMessage("该名称已存在");
        }
        return ServerResponse.createBySuccessMessage("该名称不存在可以使用");
    }

    @Override
    public ServerResponse suggest_search(String keyword) {
        List<Institution> InstitutionNameList = institutionMapper.selectList(
                new EntityWrapper<Institution>()
                        .setSqlSelect("name")
                        .like("name", keyword)
                        .orderBy("load_time", false)
        );
        List<String> institutionName = InstitutionNameList.stream()
                .map(i -> i.getName())
                .collect(Collectors.toList());
        return ServerResponse.createBySuccess("返回成功", institutionName);
    }


}
