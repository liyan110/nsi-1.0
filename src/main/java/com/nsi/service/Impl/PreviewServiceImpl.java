package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.PreviewMapper;
import com.nsi.pojo.Preview;
import com.nsi.service.IPreviewService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 12984
 */
@Service
public class PreviewServiceImpl implements IPreviewService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PreviewMapper previewMapper;

    /**
     * 添加方法
     *
     * @param preview
     * @return
     */
    @Override
    public ServerResponse saveon(Preview preview) {
        if (StringUtils.isBlank(preview.getUserId())) {
            return ServerResponse.createByErrorMessage("用户id不能为空");
        }
        Preview currentPreview = new Preview();
        currentPreview.setUserId(preview.getUserId());
        currentPreview.setFileUrl(preview.getFileUrl());
        currentPreview.setFileName(preview.getFileName());
        currentPreview.setFileCategory(preview.getFileCategory());

        int rowCount = previewMapper.insert(currentPreview);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }

    /**
     * 返回详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ServerResponse list(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Preview> list = previewMapper.getList();
        logger.info("list:{}", list);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
