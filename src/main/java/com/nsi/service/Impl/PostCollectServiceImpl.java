package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.PostCollectMapper;
import com.nsi.pojo.PostCollect;
import com.nsi.service.PostCollectService;
import com.nsi.vo.nsi_shop_bar.PostCollectVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@Service
public class PostCollectServiceImpl extends ServiceImpl<PostCollectMapper, PostCollect> implements PostCollectService {

    @Autowired
    private PostCollectMapper postCollectMapper;

    @Override
    public List<PostCollectVO> findCollectList(String openId) {
        return postCollectMapper.findCollectList(openId);
    }

    public int findCollectCount(String openId) {
        return postCollectMapper.findCollectCount(openId);
    }

    @Override
    public int findIsCollect(String openId, Integer itemId) {
        return postCollectMapper.selectCount(
                new EntityWrapper<PostCollect>()
                        .eq("open_id", openId)
                        .eq("item_id", itemId)
        );

    }
}
