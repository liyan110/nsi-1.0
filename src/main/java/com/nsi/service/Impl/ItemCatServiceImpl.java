package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ItemCatMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.ItemCat;
import com.nsi.service.IItemCatService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 业务层
 *
 * @author Luo Zhen
 * @create 2018-12-27 15:20
 */
@Service
public class ItemCatServiceImpl implements IItemCatService {

    @Autowired
    private ItemCatMapper itemCatMapper;

    @Override
    public ServerResponse saveItemCat(ItemCat itemCat) throws NsiOperationException {
        if (StringUtils.isBlank(itemCat.getName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        itemCat.setCreateTime(new Date());
        itemCat.setUpdateTime(itemCat.getCreateTime());

        try {
            int resultCount = itemCatMapper.insert(itemCat);
            if (resultCount <= 0) {
                throw new NsiOperationException("添加失败");
            }
            return ServerResponse.createBySuccessMessage("添加成功");
        } catch (Exception e) {
            throw new NsiOperationException("create ItemCat error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse modifyItemCat(ItemCat itemCat) throws NsiOperationException {
        if (itemCat.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        itemCat.setUpdateTime(new Date());
        try {
            int resultCount = itemCatMapper.updateByPrimaryKeySelective(itemCat);
            if (resultCount <= 0) {
                throw new NsiOperationException("修改失败");
            }
            return ServerResponse.createBySuccessMessage("修改成功");
        } catch (Exception e) {
            throw new NsiOperationException("modify itemCat error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse findItemCatById(int pageNum, int pageSize, Integer parentId) {
        PageHelper.startPage(pageNum, pageSize);
        List<ItemCat> itemCatList = itemCatMapper.selectByParentId(parentId);
        PageInfo pageInfo = new PageInfo(itemCatList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse modifyItemCatById(Integer itemId) throws NsiOperationException {
        if (itemId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Integer total = 0;
        Integer balance = 0;
        List<ItemCat> itemCatList = itemCatMapper.selectByParentId(itemId);
        for (ItemCat itemCatTemp : itemCatList) {
            if (itemCatTemp.getAmountCased() == null) {
                itemCatTemp.setAmountCased(0);
            }
            total += itemCatTemp.getAmountCased();
        }
        ItemCat itemCat = itemCatMapper.selectByPrimaryKey(itemId);
        if (itemCat != null) {
            balance = itemCat.getItemAmount() - total;
        }
        ItemCat item = new ItemCat();
        item.setId(itemId);
        item.setReceivingTotal(total);
        item.setBalance(balance);
        try {
            ServerResponse response = this.modifyItemCat(item);
            if (response.getCode() < 0) {
                throw new NsiOperationException("修改失败");
            }
            return ServerResponse.createBySuccessMessage("修改成功");
        } catch (Exception e) {
            throw new NsiOperationException("modify itemCat error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse removeItemCat(Integer itemId) {
        if (itemId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            itemCatMapper.deleteByPrimaryKey(itemId);
            itemCatMapper.deleteByParentId(itemId);
            return ServerResponse.createBySuccessMessage("删除成功");
        } catch (Exception e) {
            throw new NsiOperationException("remove itemCat error:" + e.getMessage());
        }
    }

}
