package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.GuestMapper;
import com.nsi.pojo.Guest;
import com.nsi.service.IGuestService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author: Luo Zhen
 * @date: 2018/11/27 10:24
 * @description:
 */
@Service
public class GuestServiceImpl extends ServiceImpl<GuestMapper, Guest> implements IGuestService {

    @Autowired
    private GuestMapper guestMapper;

    @Override
    public ServerResponse saveGuest(Guest guest) {
        if (StringUtils.isBlank(guest.getGuestName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        if (StringUtils.isNotBlank(guest.getGuestBirthday())) {
            String[] split = guest.getGuestBirthday().split("-");
            guest.setBirthdayMonth(split[1]);
        }
        guest.setCreateTime(new Date());

        int resultCount = guestMapper.insert(guest);
        if (resultCount <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        }
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @Override
    public ServerResponse getGuestList(int pageNum, int pageSize, String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<Guest> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(searchKey)) {
            wrapper.like("guest_name", searchKey)
                    .or()
                    .like("guest_phone", searchKey)
                    .or()
                    .like("guest_post", searchKey)
                    .or()
                    .like("guest_company", searchKey)
                    .or()
                    .like("guest_email", searchKey);
        }
        wrapper.orderBy("guest_id", false);
        List<Guest> guestList = guestMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(guestList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse modifyGuest(Guest guest) {
        if (StringUtils.isNotBlank(guest.getGuestBirthday())) {
            String[] split = guest.getGuestBirthday().split("-");
            guest.setBirthdayMonth(split[1]);
        }

        int resultCount = guestMapper.updateById(guest);
        if (resultCount <= 0) {
            return ServerResponse.createByErrorMessage("修改失败");
        }
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    @Override
    public ServerResponse removeGuest(Integer guestId) {
        if (guestId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }

        int resultCount = guestMapper.deleteById(guestId);
        if (resultCount <= 0) {
            return ServerResponse.createByErrorMessage("删除失败");
        }
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    @Override
    public ServerResponse getGuestInfo(Integer guestId) {
        if (guestId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Guest guest = guestMapper.selectById(guestId);
        return ServerResponse.createBySuccess(guest);
    }



    @Override
    public ServerResponse checkValidByName(String name) {
        if (StringUtils.isBlank(name)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        List<Guest> guestList = guestMapper.findByName(name);
        if (CollectionUtils.isNotEmpty(guestList)) {
            return ServerResponse.createByError(guestList);
        }
        return ServerResponse.createBySuccess();
    }
}
