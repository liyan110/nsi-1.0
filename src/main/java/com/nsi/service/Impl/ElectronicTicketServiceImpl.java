package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.ElectronicTicketMapper;
import com.nsi.pojo.ElectronicTicket;
import com.nsi.service.ElectronicTicketService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-04-01
 */
@Service
public class ElectronicTicketServiceImpl extends ServiceImpl<ElectronicTicketMapper, ElectronicTicket> implements ElectronicTicketService {

}
