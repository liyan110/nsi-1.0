package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.dao.DiscountsMapper;
import com.nsi.pojo.Discounts;
import com.nsi.service.DiscountsService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 优惠用户信息表 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-21
 */
@Service
@Transactional
public class DiscountsServiceImpl implements DiscountsService {

    @Autowired
    private DiscountsMapper discountsMapper;

    @Override
    public ServerResponse checkValid(String telphone) {
        EntityWrapper wrapper = new EntityWrapper();
        wrapper.eq("telphone", telphone);

        List<Discounts> list = discountsMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(list)) {
            return ServerResponse.createByError();
        }

        Discounts discounts = list.get(0);
        discounts.setStatus(1);
        // 修改优惠状态
        discountsMapper.updateById(discounts);

        return ServerResponse.createBySuccess(discounts.getType());
    }


}
