package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.pojo.*;
import com.nsi.dao.CommunityMessageMapper;
import com.nsi.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 社区消息 服务实现类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-26
 */
@Service
public class CommunityMessageServiceImpl extends ServiceImpl<CommunityMessageMapper, CommunityMessage> implements CommunityMessageService {

    @Autowired
    private CommunityMessageMapper communityMessageMapper;

    @Autowired
    private CommunityUserService communityUserService;

    @Autowired
    private PostCategoryItemService postCategoryItemService;

    @Autowired
    private CommunityCommentService communityCommentService;

    @Autowired
    private CommunityAskService communityAskService;

//    @Override
//    public int insertMessage(CommunityMessage communityMessage) {
//
//        communityMessage.setCreateTime(new Date());
//        communityMessage.setCheckState(0);
//        int i=communityMessageMapper.insert(communityMessage);
//        return i;
//    }

    @Override
    public int insertMessage(String messageType,String messageWechatId,String messageContent,String objectId,String jumpId) {
        CommunityMessage m=new CommunityMessage();
        m.setMessageType(messageType);
        m.setMessageWechatId(messageWechatId);
        m.setObjectId(objectId);
        m.setJumpId(jumpId);
        m.setMessageContent(messageContent);
        //message消息发送人
        CommunityUser communityUser=communityUserService.detail(messageWechatId);
        m.setMessageName(communityUser.getNickname());
        m.setMessagePortrait(communityUser.getWechatPortrait());

        switch (messageType) {
            case "帖子被回复":
//                objectId 为帖子id
                EntityWrapper<PostCategoryItem> wrapper=new EntityWrapper<>();
                wrapper.eq("item_id",objectId);
                PostCategoryItem postCategoryItem = postCategoryItemService.selectOne(wrapper);

                m.setObjectTitle(postCategoryItem.getTitle());
                m.setObjectUserId(postCategoryItem.getOpenId());
                m.setReceiverWechatId(postCategoryItem.getOpenId());
                break;

            case "评论被回复":
                //objectId 为父评论id
                CommunityComment communityComment=communityCommentService.detail(Integer.valueOf(objectId));

                m.setObjectTitle(communityComment.getContent());
                m.setObjectUserId(communityComment.getWechatId());
                m.setReceiverWechatId(communityComment.getWechatId());
                break;

            case "互动被提问":
//                对象ID -> 被提问用户微信id
                m.setObjectTitle(messageContent);
                m.setObjectUserId(objectId);
                m.setReceiverWechatId(objectId);
                break;

            case "互动被回复":
//                对象ID -> 互动提问表id
                CommunityAsk communityAsk= communityAskService.selectById(objectId);
                m.setObjectTitle(messageContent);
                m.setObjectUserId(communityAsk.getMessageWechatId());
                m.setReceiverWechatId(communityAsk.getMessageWechatId());
                break;

            default:
                System.err.println("messageType 未匹配");
        }
        m.setCreateTime(new Date());
        m.setCheckState(0);
        int i=communityMessageMapper.insert(m);
        return i;
    }


    @Override
    public List<CommunityMessage> list(String wechatId,int checkState,int pageNum, int pageSize) {
        EntityWrapper<CommunityMessage> entityWrapper=new EntityWrapper<>();
        PageHelper.startPage(pageNum, pageSize);

        entityWrapper.eq("receiver_wechat_id",wechatId)
                .eq("check_state",checkState)
                .orderBy("check_state",true)
                .orderBy("create_time",false);

        List<CommunityMessage> list =communityMessageMapper.selectList(entityWrapper);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo.getList();
    }

    @Override
    public int checked(int messageId){
        CommunityMessage communityMessage=new CommunityMessage();
        communityMessage.setCheckState(1);
        communityMessage.setCheckTime(new Date());

        EntityWrapper<CommunityMessage> wrapper =new EntityWrapper<>();
        wrapper.eq("id",messageId);

        int i=communityMessageMapper.update(communityMessage,wrapper);
        return i;
    }
}
