package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ClassCommentMapper;
import com.nsi.dao.UserMapper;
import com.nsi.pojo.ClassComment;
import com.nsi.pojo.User;
import com.nsi.service.ICommentService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Li Yan
 * @create 2018-08-22
 **/
@Service
public class CommentServiceImpl implements ICommentService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ClassCommentMapper classCommentMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 添加方法
     *
     * @param classComment
     * @return
     */
    @Override
    public ServerResponse<String> add(ClassComment classComment) {
        //        设置修改时间
        classComment.setReleasetime(String.valueOf(System.currentTimeMillis()));
//        查找用户头像
        String UserMail = classComment.getCommentatormail();
        //DynamicDataSourceHolder.setDataSource("dataSource1");
        User user = userMapper.selectByUserName(UserMail);
        String UserPortrait = user.getUserPortrait();
        classComment.setCommentatorportrait(UserPortrait);

        //DynamicDataSourceHolder.setDataSource("dataSource2");
        int rowCount = classCommentMapper.insert(classComment);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }


    /**
     * 添加方法
     *
     * @param classComment
     * @return
     */
    public ServerResponse<String> TeacherAdd(ClassComment classComment) {
        //        设置修改时间
        classComment.setReleasetime(String.valueOf(System.currentTimeMillis()));
        classComment.setIdentity("1");

        //DynamicDataSourceHolder.setDataSource("dataSource2");
        int rowCount = classCommentMapper.insert(classComment);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }


    /**
     * 课程页面 展示评论
     *
     * @param ObjectId
     * @return
     */
    @Override
    public ServerResponse list(String ObjectId) {
        List<ClassComment> list = classCommentMapper.selectByObjectId(ObjectId);
        if (CollectionUtils.isEmpty(list)) {
            return ServerResponse.createBySuccess(list);
        }
        return ServerResponse.createBySuccess("success", list.size(), list);
    }

    /**
     * 个人中心页面 我的评论
     *
     * @param UserMail
     * @return
     */
    @Override
    public ServerResponse myComment(String UserMail) {

        List<ClassComment> list = classCommentMapper.selectByCommentatorMail(UserMail);
        if (CollectionUtils.isEmpty(list)) {
            return ServerResponse.createBySuccess(list);
        }
        return ServerResponse.createBySuccess("success", list.size(), list);
    }

    /**
     * 修改评论
     *
     * @param classComment
     * @return
     */
    @Override
    public ServerResponse alter(ClassComment classComment) {
//        设置修改时间
        classComment.setReleasetime(String.valueOf(System.currentTimeMillis()));
        int count = classCommentMapper.updateByPrimaryKey(classComment);
        if (count == 0) {
            return ServerResponse.createBySuccess(count);
        }
        return ServerResponse.createBySuccessMessage("success");
    }

    /**
     * 审核评论
     *
     * @param CommentId
     * @return
     */
    @Override
    public ServerResponse CommentVerify(String CommentId) {
        int count = classCommentMapper.CommentVerify(CommentId);
        if (count == 0) {
            return ServerResponse.createBySuccess(count);
        }
        return ServerResponse.createBySuccessMessage("success");
    }

    /**
     * 评论-后台管理 获取列表，layui格式
     *
     * @param objectId
     * @return
     */
    @Override
    public ServerResponse Admin_list(String objectId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ClassComment> list = classCommentMapper.selectByObjectIdVerify(objectId);
        if (CollectionUtils.isEmpty(list)) {
            logger.info("集合为空！");
        }
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

}
