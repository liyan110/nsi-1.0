package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ResourceMapper;
import com.nsi.pojo.Resource;
import com.nsi.service.IResourceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-03
 */
@Service
public class ResourceServiceImpl implements IResourceService {

    private static Logger logger = LoggerFactory.getLogger(ResourceServiceImpl.class);

    @Autowired
    private ResourceMapper resourceMapper;

    /**
     * 文化上传
     *
     * @param file
     * @param path
     * @return
     */
    @Override
    public String upFile(MultipartFile file, String path) {
        String fileName = file.getOriginalFilename();
        //拼接后的新名字
        logger.info("开始上传文件,上传文件名:{},上传路径:{}", fileName, path);

        File fileDir = new File(path);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        File targetFile = new File(path, fileName);
        try {
            //开始上传
            file.transferTo(targetFile);
        } catch (IOException e) {
            logger.info("上传失败", e);
            e.printStackTrace();
        }
        return targetFile.getName();
    }

    /**
     * 添加方法
     *
     * @param resource
     * @return
     */
    @Override
    public ServerResponse saveOn(Resource resource) {
        if (StringUtils.isBlank(resource.getImageUrl())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        if (StringUtils.isBlank(resource.getFileName()) || StringUtils.isBlank(resource.getFileUrl())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = resourceMapper.insert(resource);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }

    /**
     * 后台修改方法
     *
     * @param resource
     * @return
     */
    @Override
    public ServerResponse update(Resource resource) {
        if (resource.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Resource currentResource = new Resource();
        currentResource.setId(resource.getId());
        currentResource.setType(resource.getType());
        currentResource.setImageUrl(resource.getImageUrl());
        currentResource.setFileName(resource.getFileName());
        currentResource.setFileUrl(resource.getFileUrl());
        currentResource.setYear(resource.getYear());
        currentResource.setCreateTime(new Date());

        int rowCount = resourceMapper.updateByPrimaryKeySelective(resource);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }

    /**
     * 刪除方法
     *
     * @param resourceId
     * @return
     */
    @Override
    public ServerResponse delete(Integer resourceId) {
        if (resourceId == null) {
            return ServerResponse.createByErrorMessage("參數不合法");
        }
        int rowCount = resourceMapper.deleteByPrimaryKey(resourceId);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("刪除成功");
        }
        return ServerResponse.createByErrorMessage("刪除失败");

    }

    /**
     * 获取列表
     *
     * @return
     */
    @Override
    public ServerResponse getList(String type, String year, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Resource> list = resourceMapper.getList(type, year);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 获取详情
     *
     * @return
     */
    @Override
    public ServerResponse detail(int id) {
        Resource resource = resourceMapper.selectByPrimaryKey(id);
        return ServerResponse.createBySuccess(resource);
    }

}
