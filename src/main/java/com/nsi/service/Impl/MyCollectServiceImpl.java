package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.MyCollect;
import com.nsi.dao.MyCollectMapper;
import com.nsi.service.MyCollectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.vo.InstitutionCollectVo;
import com.nsi.vo.SchoolCollectVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 四库全书-我的收藏表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-09-02
 */
@Service
public class MyCollectServiceImpl extends ServiceImpl<MyCollectMapper, MyCollect> implements MyCollectService {

    private static final Integer SchoolType = 1;


    @Autowired
    MyCollectMapper collectMapper;

    @Override
    public ServerResponse findCollectList(int pageNum, int pageSize, String openId, Integer collectType) {
        PageHelper.startPage(pageNum, pageSize);
        if (StringUtils.isBlank(openId)
                || collectType == null) {
            return ServerResponse.createByErrorMessage("参数不合法！");
        }
        // 收藏学校列表
        if (collectType == SchoolType) {
            List<SchoolCollectVo> schoolCollectVos = collectMapper.findBySchoolCollect(openId, collectType);
            PageInfo pageInfo = new PageInfo(schoolCollectVos);
            return ServerResponse.createBySuccess(pageInfo);
            // 收藏机构列表
        } else {
            List<InstitutionCollectVo> institutionCollectVos = collectMapper.findByInstitutionCollect(openId, collectType);
            PageInfo pageInfo = new PageInfo(institutionCollectVos);
            return ServerResponse.createBySuccess(pageInfo);
        }
    }
}
