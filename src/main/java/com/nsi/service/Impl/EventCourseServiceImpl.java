package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.dao.EventCourseMapper;
import com.nsi.pojo.CourseList;
import com.nsi.pojo.EventCourse;
import com.nsi.service.IEventCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2019-08-14
 **/
@Service
public class EventCourseServiceImpl implements IEventCourseService{

    @Autowired
    private EventCourseMapper eventCourseMapper;



    @Override
    public int insert (EventCourse eventCourse) {

        int i=eventCourseMapper.insert(eventCourse);
        return i;
    }

    @Override
    public int del (int id) {

        int i=eventCourseMapper.deleteById(id);
        return i;
    }


    @Override
    public int update (EventCourse eventCourse) {
            EntityWrapper<EventCourse> wrapper = new EntityWrapper<>();
            wrapper.eq("id",eventCourse.getId());
        int i=eventCourseMapper.update(eventCourse,wrapper);
        return i;
    }


    @Override
    public List<EventCourse> listByEventId(Integer EventId){
            EntityWrapper<EventCourse> wrapper = new EntityWrapper<>();
            wrapper.eq(EventId!=null,"event_id",EventId);
        List<EventCourse> list =eventCourseMapper.selectList(wrapper);
        return list;
    }

}
