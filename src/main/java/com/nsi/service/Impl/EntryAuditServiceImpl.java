package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.EntryAuditMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.EntryAudit;
import com.nsi.service.EntryAuditService;
import com.nsi.vo.ActiveOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-16
 */
@Service
public class EntryAuditServiceImpl extends ServiceImpl<EntryAuditMapper, EntryAudit> implements EntryAuditService {

    @Autowired
    EntryAuditMapper entryAuditMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse add(EntryAudit entryAudit) {
        if (entryAudit.getEntryId() == null) {
            throw new NsiOperationException("参数不合法");
        }
        entryAudit.setEnrolmentTime(new Date());
        entryAudit.setIsCheck(ResponseCode.CHECK.getCode());

        entryAuditMapper.insert(entryAudit);
        return ServerResponse.createBySuccess(entryAudit.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse update(EntryAudit entryAudit) {
        if (entryAudit.getId() == null) {
            throw new NsiOperationException("参数不合法");
        }
        entryAuditMapper.updateById(entryAudit);
        return ServerResponse.createBySuccess("修改成功");
    }

    @Override
    public ServerResponse selectList(int pageNum, int pageSize, EntryAudit entryAudit) {
        PageHelper.startPage(pageNum, pageSize);
        List<EntryAudit> list = entryAuditMapper.selectEntryAuditList(entryAudit);
        list.stream().forEach(e -> e.setCheckMsg(e.getIsCheck() == 0 ? "审核中" : "已审核"));

        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse updateSuccessAudit(EntryAudit entryAudit) {
        entryAudit.setIsCheck(ResponseCode.PASS.getCode());
        entryAudit.setAuditTime(new Date());

        entryAuditMapper.updateById(entryAudit);
        return ServerResponse.createBySuccess();
    }


    @Override
    public void deleteById(Integer id) {
        entryAuditMapper.deleteById(id);
    }

    @Override
    public List<ActiveOrderVo> checkTicketApproved(String telphone) {
        List<ActiveOrderVo> entryList = entryAuditMapper.checkTicketApproved(telphone);

        for (ActiveOrderVo entry : entryList) {
            entry.setStatusDesc(ResponseCode.stateOf(entry.getStatus()).getDesc());
            if (entry.getType().equals("嘉宾")) {
                entry.setTicketType(entry.getName() + "-" + entry.getCompany() + "-" + "贵宾票");
            } else {
                entry.setTicketType(entry.getName() + "-" + entry.getCompany() + "-" + "尊享票");
            }
            entry.setOrderNo(0L);
            entry.setTotalPrice(0);
        }
        return entryList;
    }

    @Override
    public List<ActiveOrderVo> getEntryListLikekey(String searchKey) {
        List<ActiveOrderVo> entryList = entryAuditMapper.getEntryListBySearchKey(searchKey);
        setDefaultValue(entryList);
        return entryList;
    }

    @Override
    public ServerResponse selectById(Integer entryId) {
        EntryAudit entryAudit = entryAuditMapper.selectById(entryId);
        entryAudit.setCheckMsg(ResponseCode.stateOf(entryAudit.getIsCheck()).getDesc());
        return ServerResponse.createBySuccess(entryAudit);
    }

    /**
     * 循环Vo模型，并设置默认值
     *
     * @param entryList
     */
    private void setDefaultValue(List<ActiveOrderVo> entryList) {
        entryList.stream()
                .peek(entry -> {
                    entry.setStatusDesc(ResponseCode.stateOf(entry.getStatus()).getDesc());
                    entry.setTicketType(entry.getName() + "-" + entry.getCompany() + "-" + "尊享票");
                    entry.setOrderNo(0L);
                    entry.setTotalPrice(0);
                })
                .collect(Collectors.toList());
    }

}
