package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.PostCategoryItemMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.PostCategoryItem;
import com.nsi.service.PostCountCommonService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Luo Zhen
 * @create 2019-12-26 10:49
 */
@Transactional(rollbackFor = {NsiOperationException.class, Exception.class})
@Service
public class PostCountCommentServiceImpl extends ServiceImpl<PostCategoryItemMapper, PostCategoryItem> implements PostCountCommonService {

    private static final Integer DEFAULT_NUM = 1;


    @Override
    public void addWatchNum(Integer itemId) {
        PostCategoryItem postItem = this.selectById(itemId);
        postItem.setWatchNum(postItem.getWatchNum() + DEFAULT_NUM);
        postItem.setUpdateTime(null);
        this.updateById(postItem);
    }

    @Override
    public void addCommentNum(Integer itemId) {
        PostCategoryItem postItem = this.selectById(itemId);
        postItem.setCommentNum(postItem.getCommentNum() + DEFAULT_NUM);
        postItem.setUpdateTime(null);
        this.updateById(postItem);
    }

    @Override
    public void addCollectNum(Integer itemId) {
        PostCategoryItem postItem = this.selectById(itemId);
        postItem.setCollectNum(postItem.getCollectNum() + DEFAULT_NUM);
        postItem.setUpdateTime(null);
        this.updateById(postItem);
    }

    @Override
    public void addShareNum(Integer itemId) {
        PostCategoryItem postItem = this.selectById(itemId);
        postItem.setShareNum(postItem.getShareNum() + DEFAULT_NUM);
        postItem.setUpdateTime(null);
        this.updateById(postItem);
    }
}
