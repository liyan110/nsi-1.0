package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.pojo.CommunityComment;
import com.nsi.dao.CommunityCommentMapper;
import com.nsi.pojo.NewTalent;
import com.nsi.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 社区-标准评论表 服务实现类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
@Service
public class CommunityCommentServiceImpl extends ServiceImpl<CommunityCommentMapper, CommunityComment> implements CommunityCommentService {

    @Autowired
    private CommunityCommentMapper communityCommentMapper;
    @Autowired
    private PostCountCommonService postCountCommonService;
    @Autowired
    private CommunityMessageService communityMessageService;
    @Autowired
    private CommunityUserService communityUserService;


    /**
     * 四库全书和火龙果评论
     *
     * @param communityComment
     * @return
     */
    @Override
    public ServerResponse insertComment(CommunityComment communityComment) throws Exception {
        //审核状态设为 1，已审核（前端做内容检查）
        communityComment.setVerifySign(1);
        communityComment.setLevel(1);
        communityComment.setCreateTime(new Date());
        communityComment.setSonCommentsNum(0);
        communityCommentMapper.insert(communityComment);

        // 四库全书
        if (communityComment.getCommentType() != 2) {
            // 火龙果
            //增加发帖用户 积分
            communityUserService.AddScore(1, communityComment.getWechatId());
            //帖子评论数+1
            postCountCommonService.addCommentNum(communityComment.getObjectId());
            //添加消息
            communityMessageService.insertMessage(
                    "帖子被回复",
                    communityComment.getWechatId(),
                    communityComment.getContent(),
                    communityComment.getObjectId().toString(),
                    communityComment.getObjectId().toString());
        }
        return ServerResponse.createBySuccess("success");
    }

    @Override
    public ServerResponse delete(int id) {
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("id", id);
        int i = communityCommentMapper.delete(wrapper);
        if (i == 1) {
            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("fail");
    }

    @Override
    public ServerResponse list(String id, int pageNum, int pageSize) {
        //创建时间倒序
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("object_id", id)
                .eq("verify_sign", 1)
                .orderBy("create_time", false);

        PageHelper.startPage(pageNum, pageSize);
        List<CommunityComment> list = communityCommentMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("success", pageInfo);
    }


    @Override
    public CommunityComment detail(int id) {

        CommunityComment communityComment = new CommunityComment();
        communityComment.setId(id);
        communityComment = communityCommentMapper.selectOne(communityComment);
        return communityComment;
    }

    @Override
    public List<CommunityComment> CommentAndSonlist(String id, int pageNum, int pageSize) {
        //创建时间倒序
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("object_id", id)
                .eq("verify_sign", 1)
                .orderBy("create_time", false);

        List<CommunityComment> list = communityCommentMapper.selectPage(new Page(pageNum, pageSize), wrapper);
        return list;
    }


    @Override
    public ServerResponse myComment(String wechatId, int pageNum, int pageSize) {
        //创建时间倒序
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("wechat_id", wechatId)
                .orderBy("create_time", false);
        List<CommunityComment> list = communityCommentMapper.selectPage(new Page(pageNum, pageSize), wrapper);
        return ServerResponse.createBySuccess("success", list);
    }

    @Override
    public int verify_pass(int id) {
        CommunityComment communityComment = new CommunityComment();
        communityComment.setId(id);
        communityComment.setVerifySign(1);
        communityComment.setVerifyTime(new Date());
        int i = communityCommentMapper.updateById(communityComment);

        if (i == 1) {
            //帖子评论数+1
            communityComment = this.detail(id);
            postCountCommonService.addCommentNum(communityComment.getObjectId());
            //添加消息
            communityMessageService.insertMessage(
                    "帖子被回复",
                    communityComment.getWechatId(),
                    communityComment.getContent(),
                    communityComment.getObjectId().toString(),
                    communityComment.getObjectId().toString());
            return i;
        }
        return i;
    }

    @Override
    public ServerResponse verify_reject(int id) {
        CommunityComment communityComment = new CommunityComment();
        communityComment.setId(id);
        communityComment.setVerifySign(-1);
        communityComment.setVerifyTime(new Date());
        int i = communityCommentMapper.updateById(communityComment);
        if (i == 1) {
            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("fail");
    }

    @Override
    public ServerResponse verify_list(String type, String searchKey, int pageNum, int pageSize) {

//        枚举 审核状态
        int verify_sign = RelationType.getValueByDesc(type);
        //创建时间倒序
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("verify_sign", verify_sign);
        if (StringUtils.isNotEmpty(searchKey)) {
            wrapper.andNew().like("content", searchKey)
                    .or()
                    .like("nickname", searchKey);
        }
        wrapper.orderBy("create_time", false);

        PageHelper.startPage(pageNum, pageSize);
        List<CommunityComment> list = communityCommentMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("success", pageInfo);
    }


    @Override
    public int Add_son_comments_num(int id) {
        int i = communityCommentMapper.Add_son_comments_num(id);
        return i;
    }


    @Override
    public List<CommunityComment> auto_verify_list(String type) {

//        枚举 审核状态
        int verify_sign = RelationType.getValueByDesc(type);
        //创建时间正顺序
        EntityWrapper<CommunityComment> wrapper = new EntityWrapper<>();
        wrapper.eq("verify_sign", verify_sign);
        wrapper.orderBy("create_time", true);

        List<CommunityComment> list = communityCommentMapper.selectList(wrapper);
        return list;
    }


//    ---------   工具方法  -------

    private enum RelationType {

        WAIT_VERIFY(0, "待审核"),
        REJECT_VERIFY(-1, "已拒绝"),
        PASS_VERIFY(1, "已通过");

        private int value;
        private String desc;

        RelationType(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return value;
        }

        public String getDesc() {
            return desc;
        }

        public static int getValueByDesc(String desc) {
            for (RelationType enums : RelationType.values()) {
                if (enums.desc.equals(desc)) {
                    return enums.getValue();
                }
            }
            return -2;
        }
    }


}
