package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.CommunityCommentMapper;
import com.nsi.dao.CommunityCommentSonMapper;
import com.nsi.pojo.CommunityCommentSon;
import com.nsi.service.CommunityCommentService;
import com.nsi.service.CommunityCommentSonService;
import com.nsi.service.CommunityMessageService;
import com.nsi.service.PostCountCommonService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 社区-子评论表 服务实现类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-17
 */
@Service
public class CommunityCommentSonServiceImpl extends ServiceImpl<CommunityCommentSonMapper, CommunityCommentSon> implements CommunityCommentSonService {

    @Autowired
    private CommunityCommentSonMapper communityCommentSonMapper;

    @Autowired
    private CommunityCommentMapper communityCommentMapper;

    @Autowired
    private CommunityCommentService communityCommentService;

    @Autowired
    private PostCountCommonService postCountCommonService;

    @Autowired
    private CommunityMessageService communityMessageService;

    @Override
    public ServerResponse insertCommentSon(CommunityCommentSon communityCommentSon) {
        communityCommentSon.setCreateTime(new Date());
        //审核状态设为 1，已审核（前端做内容检查）
        communityCommentSon.setVerifySign(1);
        int i = communityCommentSonMapper.insert(communityCommentSon);
        if (i == 1) {

            //更新父评论表 的 子评论数
            int comment_id = communityCommentSon.getObjectId();
            communityCommentService.Add_son_comments_num(comment_id);
            //更新 帖子评论数
            int post_id = communityCommentService.detail(comment_id).getObjectId();
            postCountCommonService.addCommentNum(post_id);
            //添加消息
            communityMessageService.insertMessage(
                    "评论被回复",
                    communityCommentSon.getWechatId(),
                    communityCommentSon.getContent(),
                    String.valueOf(comment_id),
                    String.valueOf(post_id)
            );


            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("fail");
    }

    @Override
    public ServerResponse delete(int id) {
        int result = communityCommentSonMapper.deleteById(id);
        if (result > 0) {
            CommunityCommentSon currentSon = communityCommentSonMapper.selectById(id);
            communityCommentMapper.Reduce_son_comments_num(currentSon.getObjectId());
            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("fail");
    }

    @Override
    public ServerResponse list(String id, int pageNum, int pageSize) {
        //创建时间倒序
        EntityWrapper<CommunityCommentSon> wrapper = new EntityWrapper<>();
        wrapper.eq("object_id", id)
                .and()
                .eq("verify_sign", 1)
                .orderBy("create_time", true);

        PageHelper.startPage(pageNum, pageSize);
        List<CommunityCommentSon> list = communityCommentSonMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("success", pageInfo);
    }

    @Override
    public CommunityCommentSon detail(int id) {
        CommunityCommentSon communityCommentSon = new CommunityCommentSon();
        communityCommentSon.setId(id);
        communityCommentSon = communityCommentSonMapper.selectOne(communityCommentSon);
        return communityCommentSon;
    }

    @Override
    public List<CommunityCommentSon> listPage3(int id) {
        int pageNum = 1;
        int pageSize = 3;
        //创建时间倒序
        EntityWrapper<CommunityCommentSon> wrapper = new EntityWrapper<>();
        wrapper.eq("object_id", id)
                .eq("verify_sign", 1)
                .orderBy("create_time", false);

        PageHelper.startPage(pageNum, pageSize);
        List<CommunityCommentSon> list = communityCommentSonMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo.getList();
    }


    @Override
    public ServerResponse verify_list(String type, String searchKey, int pageNum, int pageSize) {
        //        枚举 审核状态
        int verify_sign = CommunityCommentSonServiceImpl.RelationType.getValueByDesc(type);
        //创建时间倒序
        EntityWrapper<CommunityCommentSon> wrapper = new EntityWrapper<>();
        wrapper.eq("verify_sign", verify_sign);
        if (StringUtils.isNotEmpty(searchKey)) {
            wrapper.andNew().like("content", searchKey)
                    .or()
                    .like("nickname", searchKey);
        }
        wrapper.orderBy("create_time", false);

        PageHelper.startPage(pageNum, pageSize);
        List<CommunityCommentSon> list = communityCommentSonMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("success", pageInfo);
    }


    @Override
    public List<CommunityCommentSon> auto_verify_list(String type) {
        //枚举 审核状态
        int verify_sign = CommunityCommentSonServiceImpl.RelationType.getValueByDesc(type);
        //创建时间正序
        EntityWrapper<CommunityCommentSon> wrapper = new EntityWrapper<>();
        wrapper.eq("verify_sign", verify_sign);
        wrapper.orderBy("create_time", true);
        List<CommunityCommentSon> list = communityCommentSonMapper.selectList(wrapper);
        return list;
    }


    @Override
    public int verify_pass(int id) {
        CommunityCommentSon communityCommentSon = new CommunityCommentSon();
        communityCommentSon.setId(id);
        CommunityCommentSon communityCommentSon_temp = communityCommentSonMapper.selectOne(communityCommentSon);
//        已审核过
        if (communityCommentSon_temp.getVerifySign() == 1) {
            return 0;
        }
        communityCommentSon.setVerifySign(1);
        communityCommentSon.setVerifyTime(new Date());
        int i = communityCommentSonMapper.updateById(communityCommentSon);
//        更新成功
        if (i > 0) {
            //更新父评论表 的 子评论数
            int comment_id = communityCommentSon_temp.getObjectId();
            communityCommentService.Add_son_comments_num(comment_id);
            //更新 帖子评论数
            int post_id = communityCommentService.detail(comment_id).getObjectId();
            postCountCommonService.addCommentNum(post_id);
            //添加消息
            communityMessageService.insertMessage(
                    "评论被回复",
                    communityCommentSon_temp.getWechatId(),
                    communityCommentSon_temp.getContent(),
                    String.valueOf(comment_id),
                    String.valueOf(post_id));
   
        }
        return i;
    }


//    @Override
//    public int auto_verify_pass(int id) {
//        CommunityCommentSon communityCommentSon=new CommunityCommentSon();
//        communityCommentSon.setId(id);
//        CommunityCommentSon communityCommentSon_temp=communityCommentSonMapper.selectOne(communityCommentSon);
//        if (communityCommentSon_temp.getVerifySign()==1){
//            System.out.println("已审核过:"+id);
//            return 0;
//        }
//        communityCommentSon.setVerifySign(1);
//        communityCommentSon.setVerifyTime(new Date());
//        int i=communityCommentSonMapper.updateById(communityCommentSon);
//        if (i==1){
//            //修改上级方法
//            communityCommentSon = this.detail(id);
//            int comment_id = communityCommentSon.getObjectId();
//            CommunityComment communityComment = communityCommentService.detail(comment_id);
//            int post_id = communityComment.getObjectId();
//            //更新父评论表 的 子评论数
//            communityCommentService.Add_son_comments_num(comment_id);
//            //更新帖子表 评论数
//            System.out.println("post_id:"+post_id);
//            postCountCommonService.addCommentNum(post_id);
//        }
//        System.err.println("审核失败"+id);
//        return 0;
//
//    }

    @Override
    public ServerResponse verify_reject(int id) {
        CommunityCommentSon communityCommentSon = new CommunityCommentSon();
        communityCommentSon.setId(id);
        communityCommentSon.setVerifySign(-1);
        communityCommentSon.setVerifyTime(new Date());
        int i = communityCommentSonMapper.updateById(communityCommentSon);
        if (i == 1) {
            return ServerResponse.createBySuccess("success");
        }
        return ServerResponse.createByErrorMessage("fail");
    }


    //    ---------   工具方法  -------

    private enum RelationType {

        WAIT_VERIFY(0, "待审核"),
        REJECT_VERIFY(-1, "已拒绝"),
        PASS_VERIFY(1, "已通过");

        private int value;
        private String desc;

        RelationType(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return value;
        }

        public String getDesc() {
            return desc;
        }

        public static int getValueByDesc(String desc) {
            for (CommunityCommentSonServiceImpl.RelationType enums : CommunityCommentSonServiceImpl.RelationType.values()) {
                if (enums.desc.equals(desc)) {
                    return enums.getValue();
                }
            }
            return -2;
        }
    }
}
