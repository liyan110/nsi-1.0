package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.TitleFormMapper;
import com.nsi.pojo.TitleForm;
import com.nsi.service.TitleFormService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@Service
public class TitleFormServiceImpl extends ServiceImpl<TitleFormMapper, TitleForm> implements TitleFormService {

}
