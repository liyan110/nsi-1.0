package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.dao.CouponMapper;
import com.nsi.pojo.Coupon;
import com.nsi.service.ICouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-05-16 17:45
 */
@Service
@Transactional
public class CouponServiceImpl implements ICouponService {

    @Autowired
    private CouponMapper couponMapper;

    @Override
    public void save(Coupon coupon) {
        couponMapper.insert(coupon);
    }

    @Override
    public void modify(Coupon coupon) {
        couponMapper.updateByPrimaryKeySelective(coupon);
    }

    @Override
    public void delete(Integer couponId) {
        couponMapper.deleteByPrimaryKey(couponId);
        // todo 删除用户关联该优惠卷的所有信息
    }

    @Override
    public PageInfo findALl(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Coupon> couponList = couponMapper.findAll();
        PageInfo pageInfo = new PageInfo(couponList);
        return pageInfo;
    }
}
