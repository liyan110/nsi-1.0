package com.nsi.service.Impl;

import com.nsi.common.ServerResponse;
import com.nsi.dao.Course_userMapper;
import com.nsi.dao.UserMapper;
import com.nsi.pojo.Course_user;
import com.nsi.pojo.User;
import com.nsi.service.IClassUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Li Yan
 * @create 2018-10-26
 **/
@Service
public class ClassUserServiceImpl implements IClassUserService {

    @Autowired
    Course_userMapper course_userMapper;

    @Autowired
    UserMapper userMapper;

    @Override
    public ServerResponse saveCourseUser(String ClassId, String UserMail) {
        Course_user course_user = new Course_user();
        int rowCount = 0;
        if (ClassId.indexOf(",") != -1) {
            String[] classIdList = ClassId.split(",");
            for (int i = 0; i < classIdList.length; i++) {
                course_user.setClassid(classIdList[i]);
                course_user.setUsermail(UserMail);
                rowCount = course_userMapper.insert(course_user);
            }
        } else {
            course_user.setClassid(ClassId);
            course_user.setUsermail(UserMail);
            rowCount = course_userMapper.insert(course_user);
        }
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("插入成功");
        } else {
            return ServerResponse.createByErrorMessage("插入失败");
        }
    }

//    研究院商城-用户课程权限
    @Override
    public ServerResponse shopSaveCourseUser(String ClassId, String wechatId) {

        User user= userMapper.selectByWechatId(wechatId);
        String UserMail=user.getUsername();

        Course_user course_user = new Course_user();
        int rowCount = 0;
        if (ClassId.indexOf(",") != -1) {
            String[] classIdList = ClassId.split(",");
            for (int i = 0; i < classIdList.length; i++) {
                course_user.setClassid(classIdList[i]);
                course_user.setUsermail(UserMail);
                rowCount = course_userMapper.insert(course_user);
            }
        } else {
            course_user.setClassid(ClassId);
            course_user.setUsermail(UserMail);
            rowCount = course_userMapper.insert(course_user);
        }
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("插入成功");
        } else {
            return ServerResponse.createByErrorMessage("插入失败");
        }

    }

    @Override
    public Course_user findCourseUserByClassidAndUsermail(String classId, String usermail) {
        return course_userMapper.findCourseByClassIdAndUserMail(classId, usermail);
    }
}
