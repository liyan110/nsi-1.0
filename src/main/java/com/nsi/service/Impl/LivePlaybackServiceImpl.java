package com.nsi.service.Impl;

import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.LivePlayback;
import com.nsi.dao.LivePlaybackMapper;
import com.nsi.service.LivePlaybackService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
@Transactional(rollbackFor = {NsiOperationException.class, Exception.class})
@Service
public class LivePlaybackServiceImpl extends ServiceImpl<LivePlaybackMapper, LivePlayback> implements LivePlaybackService {

    private static final Integer DEFAULT_NUM = 1;

    @Override
    public void addWatchNum(Integer id) {
        LivePlayback playback = this.selectById(id);
        playback.setWatchNum(playback.getWatchNum() + DEFAULT_NUM);
        this.updateById(playback);
    }
}
