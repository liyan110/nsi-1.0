package com.nsi.service.Impl;

import com.nsi.pojo.CommunityFollow;
import com.nsi.dao.CommunityFollowMapper;
import com.nsi.service.CommunityFollowService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 社区关注 服务实现类
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@Service
public class CommunityFollowServiceImpl extends ServiceImpl<CommunityFollowMapper, CommunityFollow> implements CommunityFollowService {

}
