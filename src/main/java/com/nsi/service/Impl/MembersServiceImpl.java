package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.MembersMapper;
import com.nsi.pojo.Members;
import com.nsi.service.IMembersService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Luo Zhen
 * @date: 2018/9/10 15:25
 * @description:
 */
@Service
public class MembersServiceImpl implements IMembersService {

    @Autowired
    private MembersMapper membersMapper;

    /**
     * 添加会员方法
     *
     * @param
     * @return
     */
    @Override
    public ServerResponse addMembersInfo(Members members) {
        members.setStates(0);
        if (StringUtils.isBlank(members.getName()) || StringUtils.isBlank(members.getMemberType())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = membersMapper.insert(members);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("注册成功");
        }
        return ServerResponse.createByErrorMessage("注册失败");
    }

    /**
     * 后台——返回会员详情
     *
     * @param pageNum
     * @param pageSize
     * @param searchKey
     * @return
     */
    @Override
    public ServerResponse getMembersList(int pageNum, int pageSize, Integer searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        List<Members> listMembers = membersMapper.queryMembers(searchKey);
        PageInfo pageInfo = new PageInfo(listMembers);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 后台——修改状态
     *
     * @param members
     * @return
     */
    @Override
    public ServerResponse updateMembersStates(Members members) {
        if (members.getId() == null || members.getStates() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = membersMapper.updateByPrimaryKeySelective(members);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }


}
