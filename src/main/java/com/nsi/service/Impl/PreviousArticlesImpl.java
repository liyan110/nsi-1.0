package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.OldArticlesMapper;
import com.nsi.pojo.OldArticles;
import com.nsi.service.IPreviousArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-07-30
 **/
@Service
public class PreviousArticlesImpl implements IPreviousArticlesService {

    @Autowired
    private OldArticlesMapper oldArticlesMapper;

    /**
     * 根据分页返回详情
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ServerResponse<PageInfo> getArticleList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<OldArticles> oldArticles = oldArticlesMapper.getList();
        PageInfo pageInfo = new PageInfo(oldArticles);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 根据Id 返回详情
     *
     * @param oldArticleId
     * @return
     */
    @Override
    public ServerResponse<OldArticles> getAllById(Integer oldArticleId) {
        if (oldArticleId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        OldArticles articles = oldArticlesMapper.selectByPrimaryKey(oldArticleId);
        return ServerResponse.createBySuccess(articles);
    }


}
