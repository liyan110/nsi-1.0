package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.TalentMapper;
import com.nsi.pojo.NewTalent;
import com.nsi.pojo.Talent;
import com.nsi.service.ITalentService;
import com.nsi.util.PropertiesUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 */
@Service
public class TalentServiceImpl implements ITalentService {

    private static Logger logger = LoggerFactory.getLogger(TalentServiceImpl.class);

    @Autowired
    private TalentMapper talentMapper;


    /**
     * 新增方法
     *
     * @param talent
     * @return
     */
    @Override
    public ServerResponse add(Talent talent) {
        if (StringUtils.isBlank(talent.getName())) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        talent.setCreateTime(new Date());
        int resultCount = talentMapper.insert(talent);
        if (resultCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败！");
    }


    /**
     * 搜索方法
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ServerResponse search(String searchKey, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Talent> list = talentMapper.selectBySearchKey(searchKey, String.valueOf(ResponseCode.IS_PUBLIC.getCode()), String.valueOf(ResponseCode.PASS.getCode()));
        if (CollectionUtils.isEmpty(list)) {
            logger.info("这是一个空的集合", list);
            return ServerResponse.createBySuccess(list);
        }
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    /**
     * 后台返回人才库详情
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ServerResponse getList(String searchKey, int pageNum, int pageSize, String verifySign) {
        PageHelper.startPage(pageNum, pageSize);
        List<Talent> talentList = talentMapper.selectBySearchKey(searchKey, "", verifySign);
        if (CollectionUtils.isEmpty(talentList)) {
            logger.info("这是一个空的集合:{}", talentList);
            return ServerResponse.createBySuccess(talentList);
        }
        PageInfo pageInfo = new PageInfo(talentList);
        return ServerResponse.createBySuccess("success", (int) pageInfo.getTotal(), pageInfo.getList());
    }


    /**
     * 通过邮箱 搜索方法
     *
     * @param UserMail
     * @return
     */
    @Override
    public ServerResponse searchByMail(String UserMail) {
        if (StringUtils.isBlank(UserMail)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        List<Talent> list = talentMapper.selectByUserMail(UserMail);
        if (list.isEmpty()) {
            return ServerResponse.createByErrorMessage("没有查到数据");
        }
        return ServerResponse.createBySuccess("success", list);
    }


    /**
     * 修改方法
     *
     * @param talent
     * @return
     */
    @Override
    public ServerResponse update(Talent talent) {
        if (StringUtils.isBlank(talent.getName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = talentMapper.updateByPrimaryKeySelective(talent);
        if (rowCount == 0) {
            return ServerResponse.createByErrorMessage("修改失败！");
        }
        return ServerResponse.createBySuccessMessage("修改成功");
    }


    /**
     * 通过ID 搜索方法 detail功能
     *
     * @param talentId
     * @return
     */
    @Override
    public ServerResponse detail(Integer talentId) {
        if (talentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Talent talent = talentMapper.selectById(talentId);
        if (talent == null) {
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createBySuccess("success", talent);
    }

    /**
     * 验证是否上传简历
     *
     * @param userMail
     * @return
     */
    @Override
    public ServerResponse checkByUpFile(String userMail) {
        if (StringUtils.isBlank(userMail)) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        int rowCount = talentMapper.ckeckByUpfile(userMail);
        if (rowCount == 0) {
            return ServerResponse.createByErrorMessage("未上传简历");
        }
        Talent talent = talentMapper.checkByHavaTalent(userMail);
        return ServerResponse.createBySuccess(talent);
    }

    /**
     * 修改简历附件,并修改表
     *
     * @param userMail
     * @param fileUrl
     * @return
     */
    @Override
    public ServerResponse deleteFile(String userMail, String fileUrl) {
        if (StringUtils.isEmpty(userMail) || StringUtils.isEmpty(fileUrl)) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        boolean isSuccess = this.deleteFile(fileUrl);
        if (!isSuccess) {
            logger.info("删除文件失败:{}", fileUrl);
        }
        int rowCount = talentMapper.updateImageByUserMail(userMail);
        //删除文件方法
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }

    /**
     * 根据id 删除数据
     *
     * @param talentId
     * @return
     */
    @Override
    public ServerResponse deleteOn(Integer talentId) {
        if (talentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = talentMapper.deleteByPrimaryKey(talentId);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }


    /**
     * 删除文件方法
     *
     * @param fileUrl
     * @return
     */
    private boolean deleteFile(String fileUrl) {
        String[] splitFile = fileUrl.split(PropertiesUtil.getProperty("nsi.file.url"));
        String fileName = "C:" + splitFile[1];
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileUrl + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileUrl + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileUrl + "不存在！");
            return false;
        }
    }


}
