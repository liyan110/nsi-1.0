package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ConfigureMapper;
import com.nsi.dao.ShopGoodsMapper;
import com.nsi.pojo.Configure;
import com.nsi.pojo.ShopGoods;
import com.nsi.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Li Yan
 * @create 2018-12-28
 **/
@Service
public class GoodsServiceImpl implements IGoodsService {

    @Autowired
    private ShopGoodsMapper goodsMapper;

    @Autowired
    private ConfigureMapper configureMapper;

    @Override
    public ServerResponse productList(String type, String state, String searchKey, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ShopGoods> list = goodsMapper.selectByTypeState(type, state, searchKey);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", pageInfo);
    }

    @Override
    public ServerResponse productDetail(int Id) {
        ShopGoods goods = goodsMapper.selectByPrimaryKey(Id);
        return ServerResponse.createBySuccess("返回成功", goods);
    }


    @Override
    public ServerResponse productSave(ShopGoods goods) {
        goods.setGoodsCreattime(new Date());
        int i = goodsMapper.insert(goods);
        return ServerResponse.createBySuccess("返回成功", i);
    }

    @Override
    public ServerResponse productRemove(int id) {
        int i = goodsMapper.deleteByPrimaryKey(id);
        if (i == 1) {
            return ServerResponse.createBySuccess("删除成功", i);
        } else {
            return ServerResponse.createByErrorMessage("删除失败");
        }
    }

    @Override
    public ServerResponse productUpdate(ShopGoods good) {
        if (good.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int count = goodsMapper.updateByPrimaryKeySelective(good);
        if (count <= 0) {
            return ServerResponse.createByErrorMessage("修改失败");
        }
        return ServerResponse.createBySuccess();
    }


    @Override
    public ServerResponse getHomeConfig(String type) {
        Map<String, Object> resultMap = new HashMap<>();
        List<Configure> list = configureMapper.selectByType(type);
        List<ShopGoods> goodsList = new ArrayList<>();
        for (Configure item : list) {
            ShopGoods currentGoods = goodsMapper.selectByPrimaryKey(Integer.parseInt(item.getContent01()));
            goodsList.add(currentGoods);
        }
        resultMap.put("configureList", list);
        resultMap.put("goodList", goodsList);
        return ServerResponse.createBySuccess(resultMap);
    }


    @Override
    public ServerResponse setHomeConfig(Configure configure) {
        if (configure.getId() == null) {
            return ServerResponse.createByErrorMessage("id不能为空");
        }
        configureMapper.updateById(configure);
        return ServerResponse.createByErrorMessage("修改成功");
    }
}
