package com.nsi.service.Impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.dao.PostCategoryItemMapper;
import com.nsi.enums.CheckStateEnum;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.PostCategoryItem;
import com.nsi.service.PostCategoryItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 帖子详情表 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@Service
public class PostCategoryItemServiceImpl extends ServiceImpl<PostCategoryItemMapper, PostCategoryItem> implements PostCategoryItemService {

    private static final Integer DEFAULT_NUM = 0;

    @Override
    public void savePostCategoryItem(PostCategoryItem postCategory) {
        if (StringUtils.isEmpty(postCategory.getOpenId())) {
            throw new NsiOperationException("openId不能为空");
        }

        postCategory.setCollectNum(DEFAULT_NUM);
        postCategory.setCommentNum(DEFAULT_NUM);
        postCategory.setWatchNum(DEFAULT_NUM);
        postCategory.setShareNum(DEFAULT_NUM);
        postCategory.setIsCheck(CheckStateEnum.IN_REVIEW.getCode());
        postCategory.setIsTop(Const.CANCEL_TOP);

        this.insert(postCategory);
    }

    @Override
    public ServerResponse selectCategoryItemList(int pageNum, int pageSize, PostCategoryItem categoryItem, String orderColumn) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<PostCategoryItem> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotEmpty(categoryItem.getTitle())) {
            wrapper.like("title", categoryItem.getTitle());
        }
        if (categoryItem.getIsCheck() != null) {
            wrapper.eq("is_check", categoryItem.getIsCheck());
        }
        wrapper.orderBy("is_top", false);
        wrapper.orderBy(orderColumn, false);
        List<PostCategoryItem> itemList = this.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(itemList);
        return ServerResponse.createBySuccess(pageInfo);
    }
}
