package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ConfigureMapper;
import com.nsi.dao.ExhibitorMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Configure;
import com.nsi.pojo.Exhibitor;
import com.nsi.service.ExhibitorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 展位表 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-02
 */
@Service
public class ExhibitorServiceImpl extends ServiceImpl<ExhibitorMapper, Exhibitor> implements ExhibitorService {

    @Autowired
    private ExhibitorMapper exhibitorMapper;

    @Autowired
    private ConfigureMapper configureMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean insert(Exhibitor exhibitor) {
        if (StringUtils.isEmpty(exhibitor.getExhibitorName())) {
            throw new NsiOperationException("参数不合法");
        }
        exhibitor.setThumbValue(0);
        exhibitor.setCreateTime(new Date());
        return super.insert(exhibitor);
    }

    @Override
    public ServerResponse findList(int pageNum, int pageSize, Exhibitor exhibitor) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<Exhibitor> wrapper = new EntityWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(exhibitor.getExhibitorName()), "exhibitor_name", exhibitor.getExhibitorName())
                .or().eq(StringUtils.isNotEmpty(exhibitor.getType()), "type", exhibitor.getType());
        wrapper.orderBy("thumb_value", false);

        List<Exhibitor> lists = exhibitorMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void thumbup(Integer exhibitorId) {
        Exhibitor exhibitor = this.selectById(exhibitorId);
        exhibitor.setThumbValue(exhibitor.getThumbValue() + 1);
        this.updateAllColumnById(exhibitor);
    }

    @Override
    public ServerResponse thumbupForMaster(Integer masterId) {
        return null;
    }

    public boolean getThumbUpConfigureType(String token) {
        //查找模板
        Configure configure = configureMapper.findByType("thumbUp");
        String thumbUp = configure.getContent01();
//        0为不开启此功能
        if (thumbUp.equals("0")) {
            thumbUp = token;
        }
//        System.out.println(thumbUp);
//        System.out.println(token);
        if (token.equals(thumbUp)) {
            return true;
        } else {
            return false;
        }

    }
}
