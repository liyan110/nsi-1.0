package com.nsi.service.Impl;

import com.nsi.dao.PostilMapper;
import com.nsi.pojo.Postil;
import com.nsi.service.IPostilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-06-04 14:53
 */
@Transactional
@Service
public class PostilServiceImpl implements IPostilService {

    @Autowired
    private PostilMapper postilMapper;

    @Override
    public void save(Postil postil) {
        postil.setCreateTime(new Date());
        postilMapper.insert(postil);
    }

    @Override
    public void modify(Postil postil) {
        postilMapper.updateByPrimaryKeySelective(postil);
    }

    @Override
    public void delete(Integer postilId) {
        postilMapper.deleteByPrimaryKey(postilId);
    }

    @Override
    public List<Postil> list(Integer schoolId) {
        return postilMapper.findBySchoolId(schoolId);
    }
}
