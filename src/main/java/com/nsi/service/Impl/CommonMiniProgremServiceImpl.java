package com.nsi.service.Impl;


import com.nsi.util.RedisUtils;
import com.nsi.service.AccessTokenService;
import com.nsi.service.CommonMiniProgremService;
import com.nsi.util.Httpsrequest;
import com.nsi.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class CommonMiniProgremServiceImpl implements CommonMiniProgremService {


    @Autowired
    private AccessTokenService accessTokenService;

    private static final String TOKEN_PREFIX = "token_miniProgramPitaya";

    @Override
    public String getMiniProgremQR_Unlimited(String page, String scene) {

        // 获取access_token
        String accessToken = RedisUtils.searchRedis(TOKEN_PREFIX);
        if (StringUtils.isEmpty(accessToken)) {
            String appId = PropertiesUtil.getProperty("miniProgram.pitaya.appid");
            String appSecrey = PropertiesUtil.getProperty("miniProgram.pitaya.appSecret");
            try {
                accessToken = accessTokenService.getAccessToken(appId, appSecrey);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 存入redis
            RedisUtils.expireSet(TOKEN_PREFIX, 7200000, accessToken);
        }
        //--本地模式测试--
//        String accessToken="32_FuOYQBqgctfTZY77y1X7Wjdw784llm_J2EU4EQlftnN1uYhUD72VpwJ0Io8gWgc1xMXB-qw4I6MOJYuTySoTraD_UNgmpoYj9y7HdPOo4CsHKU_ZppGcySYUHojkblO0HcljvsEWQFWhES76CKFgAGAYGB";

        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken;
        byte[] outByte = new byte[100];
        String param = "{\"scene\":\"" + scene + "\",\"page\":\"" + page + "\" }";
//        String param = "{\"scene\":\""+scene+"\"}";
        System.out.println(param);
        try {
            outByte = Httpsrequest.httpsRequestByte(url, "POST", param);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("outStr:" + outByte);

        //生成图片文件
        InputStream inputStream = null;
        OutputStream outputStream = null;
        String filepath = "C:/upImage/miniProQR/qr.png";
        String outfilepath = "http://data.xinxueshuo.cn/upImage/miniProQR/qr.png";
        try {
            inputStream = new ByteArrayInputStream(outByte);
            File file = new File(filepath);
            if (!file.exists()) {
                file.createNewFile();
            }
            outputStream = new FileOutputStream(file);
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("生成小程序二维码失败");
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return outfilepath;
    }
}
