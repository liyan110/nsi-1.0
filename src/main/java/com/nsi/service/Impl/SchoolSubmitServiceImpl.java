package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.SchoolSubmitMapper;
import com.nsi.pojo.SchoolSubmit;
import com.nsi.service.SchoolSubmitService;
import com.nsi.vo.SchoolSubmitVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 学校提交人信息数据库表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-10-15
 */
@Service
public class SchoolSubmitServiceImpl extends ServiceImpl<SchoolSubmitMapper, SchoolSubmit> implements SchoolSubmitService {

    @Autowired
    SchoolSubmitMapper schoolSubmitMapper;

    @Override
    public List<SchoolSubmitVo> findListByVerify(String verify) {
        return schoolSubmitMapper.findListByVerify(verify);
    }
}
