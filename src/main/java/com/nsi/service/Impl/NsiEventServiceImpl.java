package com.nsi.service.Impl;

import com.nsi.pojo.NsiEvent;
import com.nsi.dao.NsiEventMapper;
import com.nsi.service.NsiEventService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动定义表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@Service
public class NsiEventServiceImpl extends ServiceImpl<NsiEventMapper, NsiEvent> implements NsiEventService {

}
