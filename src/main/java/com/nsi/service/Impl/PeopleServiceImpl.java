package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.dao.PeopleMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.People;
import com.nsi.service.IPeopleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author: Luo Zhen
 * @date: 2018/9/21 13:43
 * @description:
 */
@Service
public class PeopleServiceImpl implements IPeopleService {

    private static final String TYPE_VIP = "会员";

    @Autowired
    private PeopleMapper peopleMapper;

    @Override
    public ServerResponse checkValid(String str, String type) {
        if (org.apache.commons.lang3.StringUtils.isNotBlank(type)) {
            int rowCount = 0;
            //不为空我开始校验
            if (Const.USERNAME.equals(type)) {
                rowCount = peopleMapper.checkUsername(str);
                return ServerResponse.createBySuccessMessage("用户名已存在" + rowCount + "条");
            }
            if (Const.EMAIL.equals(type)) {
                rowCount = peopleMapper.checkEmail(str);
                return ServerResponse.createBySuccessMessage("邮箱已存在" + rowCount + "条");
            }
            if (Const.PHONE.equals(type)) {
                rowCount = peopleMapper.checkPhone(str);
                return ServerResponse.createBySuccessMessage("电话已存在" + rowCount + "条");
            }
        } else {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        return null;
    }

    @Override
    public ServerResponse getPeopleList(String searchKey, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<People> list = peopleMapper.queryPeopleList(searchKey);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse getPeopleListByType(String searchKey, int type, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<People> peopleList = null;
        if (type == 1) {
            peopleList = peopleMapper.queryPeopleListBySearchKey(searchKey);
        } else {
            peopleList = peopleMapper.queryPeopleListByPeopleNum(searchKey);
        }
        PageInfo pageInfo = new PageInfo(peopleList);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse addPeople(People people) {
        if (StringUtils.isBlank(people.getPeopleName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        people.setPeopleLoadtime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        try {
            int rowCount = peopleMapper.insert(people);
            if (rowCount > 0) {
                return ServerResponse.createBySuccessMessage("添加成功");
            }
            return ServerResponse.createByErrorMessage("添加失败");
        } catch (Exception e) {
            throw new NsiOperationException("addPeople error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse findAll(Integer peopleId) {
        if (peopleId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        People people = peopleMapper.findAll(peopleId);
        return ServerResponse.createBySuccess(people);
    }

    @Override
    public ServerResponse modifyPeople(People people) {
        if (StringUtils.isBlank(people.getPeopleName()) && people.getPeopleId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        people.setPeopleLoadtime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        try {
            int rowCount = peopleMapper.updateByPrimaryKeySelective(people);
            if (rowCount > 0) {
                return ServerResponse.createBySuccessMessage("修改成功");
            }
            return ServerResponse.createByErrorMessage("修改失败");
        } catch (Exception e) {
            throw new NsiOperationException("modifyPeople error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse removePeople(Integer peopleId) {
        if (peopleId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = peopleMapper.deleteByPrimaryKey(peopleId);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("操作成功");
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }


}
