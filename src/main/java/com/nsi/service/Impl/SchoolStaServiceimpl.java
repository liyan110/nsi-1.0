package com.nsi.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nsi.common.Const;
import com.nsi.dao.SchoolNewMapper;
import com.nsi.service.SchoolStaService;
import com.nsi.util.PropertiesUtil;
import com.nsi.util.RedisUtils;
import com.nsi.vo.SchoolStatistics;
import com.nsi.vo.SchoolTeacherStatistics;
import com.nsi.vo.SchoolTuitionSta;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Luo Zhen
 */
@Service
public class SchoolStaServiceimpl implements SchoolStaService {

    @Autowired
    SchoolNewMapper schoolNewMapper;

    @Override
    public List<Map<String, Integer>> getSchoolRatio() {
        List<Map<String, Integer>> result = schoolNewMapper.getSchoolRatio();
        return result;
    }

    @Override
    public Map<String, Object> getAllSchoolRatio() {
        List<SchoolStatistics> ratio = schoolNewMapper.getAllSchoolRatio();

        List<String> provinceList = ratio.stream()
                .map(SchoolStatistics::getProvince)
                .collect(Collectors.toList());
        List<Integer> minban = ratio.stream().map(SchoolStatistics::getMinban).collect(Collectors.toList());
        List<Integer> gongban = ratio.stream().map(SchoolStatistics::getGongban).collect(Collectors.toList());
        List<Integer> waiji = ratio.stream().map(SchoolStatistics::getWaiji).collect(Collectors.toList());
        List<Integer> other = ratio.stream().map(SchoolStatistics::getOther).collect(Collectors.toList());

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("province", provinceList);
        resultMap.put("gongban", gongban);
        resultMap.put("waiji", waiji);
        resultMap.put("other", other);
        resultMap.put("minban", minban);

        return resultMap;
    }

    @Override
    public List<Map<String, Object>> getCourseRatio() {
        List<Map<String, Object>> result = new ArrayList<>();

        String courses = PropertiesUtil.getProperty("school.course.list");
        List<String> courseList = Arrays.asList(courses.split(","));
        for (String course : courseList) {
            Map<String, Object> map = new HashMap<>(3);
            int count = schoolNewMapper.getCourseRatio(course);
            map.put("name", course);
            map.put("value", count);

            result.add(map);
        }
        return result;
    }

    @Override
    public Map<String, Object> getCoursePercent(String province) {
        // 课程列表
        String courses = PropertiesUtil.getProperty("school.course.list");
        List<String> courseList = Arrays.asList(courses.split(","));

        List<Integer> gongban = new ArrayList<>();
        List<Integer> minban = new ArrayList<>();
        List<Integer> waiji = new ArrayList<>();
        for (String course : courseList) {
            int gongCount = schoolNewMapper.getGongSchoolRatioCount(province, course);
            gongban.add(gongCount);
            int minCount = schoolNewMapper.getMinSchoolRatioCount(province, course);
            minban.add(minCount);
            int waiCount = schoolNewMapper.getWiSchoolRatioCount(province, course);
            waiji.add(waiCount);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("courseList", courseList);
        resultMap.put("gongban", gongban);
        resultMap.put("minban", minban);
        resultMap.put("waiji", waiji);

        return resultMap;
    }

    @Override
    public Map<String, Object> getStudentSection() {
        // 学段列表
        String systems = PropertiesUtil.getProperty("school.system.list");
        List<String> systemList = Arrays.asList(systems.split(","));

        List<Integer> gongban = new ArrayList<>();
        List<Integer> minban = new ArrayList<>();
        List<Integer> waiji = new ArrayList<>();
        for (String system : systemList) {
            int gongCount = schoolNewMapper.getGongSystemRatioCount(system);
            gongban.add(gongCount);
            int minCount = schoolNewMapper.getMinSystemRatioCount(system);
            minban.add(minCount);
            int waiCount = schoolNewMapper.getWiSystemRatioCount(system);
            waiji.add(waiCount);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("systemList", systemList);
        resultMap.put("gongban", gongban);
        resultMap.put("minban", minban);
        resultMap.put("waiji", waiji);

        return resultMap;
    }

    @Override
    public Map<String, Object> getTenYearSchoolNum(Integer time) {
        List<Map<String, Object>> tenYearNum = schoolNewMapper.getTenYearSchoolNum(time);

        List<Object> times = new ArrayList<>();
        List<Object> number = new ArrayList<>();

        tenYearNum.stream().forEach(
                map -> {
                    for (String key : map.keySet()) {
                        if (key.equals("names")) {
                            times.add(map.get(key));
                        } else {
                            number.add(map.get(key));
                        }
                    }
                }
        );

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("timeList", times);
        resultMap.put("number", number);

        return resultMap;
    }

    @SneakyThrows
    @Override
    public List<String> getAllProvince() {
        String provinces = RedisUtils.searchRedis(Const.ALL_PROVINCE);

        // redis 存在
        if (StringUtils.isNotBlank(provinces)) {
            List<String> provinceList = RedisUtils.constructParameter(provinces, String.class);
            return provinceList;
        }

        // redis不存在
        List<String> provinceList =
                schoolNewMapper.getAllProvince()
                        .stream().map(
                        e -> {
                            return (String) e.get("name");
                        }
                ).collect(Collectors.toList());

        ObjectMapper mapper = new ObjectMapper();
        String jsonStr = mapper.writeValueAsString(provinceList);
        RedisUtils.expireSet(Const.ALL_PROVINCE, Const.EXPIRE_TIME, jsonStr);
        return provinceList;
    }

    @Override
    public Map<String, Object> getAverageTuition() {
        List<SchoolTuitionSta> tuitions = schoolNewMapper.getAverageTuition();

        List<String> provinceList = tuitions.stream().map(SchoolTuitionSta::getProvince).collect(Collectors.toList());
        List<Integer> kinder = tuitions.stream().map(SchoolTuitionSta::getKindergartenTuition).collect(Collectors.toList());
        List<Integer> elementart = tuitions.stream().map(SchoolTuitionSta::getElementartTuition).collect(Collectors.toList());
        List<Integer> junior = tuitions.stream().map(SchoolTuitionSta::getJuniorTuition).collect(Collectors.toList());
        List<Integer> senior = tuitions.stream().map(SchoolTuitionSta::getSeniorTuition).collect(Collectors.toList());

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("province", provinceList);
        resultMap.put("kinder", kinder);
        resultMap.put("elementart", elementart);
        resultMap.put("junior", junior);
        resultMap.put("senior", senior);
        return resultMap;
    }

    @Override
    public Map<String, Object> getAverageTuitionByProvince(String province) {
        List<SchoolTuitionSta> tuitions = schoolNewMapper.getAverageTuitionByProvince(province);

        List<String> properties = tuitions.stream().map(SchoolTuitionSta::getProperties).collect(Collectors.toList());
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("properties", properties);

        String keyName = null;
        for (SchoolTuitionSta tuition : tuitions) {
            List<Integer> numberList = new ArrayList<>();
            numberList.add(tuition.getKindergartenTuition());
            numberList.add(tuition.getElementartTuition());
            numberList.add(tuition.getJuniorTuition());
            numberList.add(tuition.getSeniorTuition());

            if (tuition.getProperties().equals("公办")) {
                keyName = "gongban";
            } else if (tuition.getProperties().equals("民办")) {
                keyName = "minban";
            } else {
                keyName = "waiji";
            }
            resultMap.put(keyName, numberList);

        }
        return resultMap;
    }

    /**
     * 统计学校各状态总数
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getOperatorStateCount() {
        return schoolNewMapper.getOperatorStateCount();
    }

    /**
     * 统计教师总数/外籍教师总数/在校生总数/学位总数
     *
     * @return
     */
    @Override
    public List<SchoolTeacherStatistics> getSchoolTeacherSta() {
        return schoolNewMapper.getTeacherNumCount();
    }


    @Override
    public Map<String, Object> getSchoolCertification(String province) {
        // 认证列表
        String authStr = PropertiesUtil.getProperty("school.auth.list");
        List<String> authList = Arrays.asList(authStr.split(","));
        List<Integer> gongban = new ArrayList<>();
        List<Integer> minban = new ArrayList<>();
        List<Integer> waiji = new ArrayList<>();
        for (String auth : authList) {
            int gongCount = schoolNewMapper.getGongSchoolAuthCount(province, auth);
            gongban.add(gongCount);
            int minCount = schoolNewMapper.getMinSchoolAuthCount(province, auth);
            minban.add(minCount);
            int waiCount = schoolNewMapper.getWiSchoolAuthCount(province, auth);
            waiji.add(waiCount);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("authList", authList);
        resultMap.put("gongban", gongban);
        resultMap.put("minban", minban);
        resultMap.put("waiji", waiji);

        return resultMap;
    }


}
