package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.CourseListMapper;
import com.nsi.pojo.CourseList;
import com.nsi.service.ICourseListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 业务层
 *
 * @author Luo Zhen
 * @create 2019-02-12 13:46
 */
@Service
public class CourseListServiceImpl extends ServiceImpl<CourseListMapper, CourseList> implements ICourseListService {

    @Autowired
    CourseListMapper courseListMapper;

    @Override
    public void saveCourseList(CourseList courseList) {
        courseList.setCreateTime(new Date());
        courseList.setUpdateTime(courseList.getCreateTime());
        courseListMapper.insert(courseList);
    }

    @Override
    public List<CourseList> findAll(String status) {
        return courseListMapper.findAll(status);
    }

    @Override
    public void updateCourseList(CourseList courseList) {
        courseList.setUpdateTime(new Date());
        courseListMapper.updateById(courseList);
    }

    @Override
    public void deleteCourseList(Integer courseListId) {
        courseListMapper.deleteById(courseListId);
    }

    @Override
    public CourseList findCourseListById(Integer courseListId) {
        return this.selectById(courseListId);
    }

    @Override
    public List<CourseList> findCourseListItem() {
        return courseListMapper.findCourseListItem();
    }
}
