package com.nsi.service.Impl;

import com.nsi.pojo.CommunityAsk;
import com.nsi.dao.CommunityAskMapper;
import com.nsi.service.CommunityAskService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提问互动-提问表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@Service
public class CommunityAskServiceImpl extends ServiceImpl<CommunityAskMapper, CommunityAsk> implements CommunityAskService {

}
