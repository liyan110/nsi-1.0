package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.dao.EventTicketMapper;
import com.nsi.pojo.EventApplyHead;
import com.nsi.pojo.EventTicket;
import com.nsi.service.IEventTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2019-09-03
 **/
@Service
public class EventTicketServiceImpl implements IEventTicketService {

    @Autowired
    private EventTicketMapper eventTicketMapper;

    @Override
    public int insert(EventTicket eventTicket) {
            eventTicket.setCreatTime(new Date());
        int i=eventTicketMapper.insert(eventTicket);
        return i;
    }

    @Override
    public int update(EventTicket eventTicket) {
            EntityWrapper<EventTicket> wrapper=new EntityWrapper<>();
            wrapper.eq("id",eventTicket.getId());
        int i=eventTicketMapper.update(eventTicket,wrapper);
        return i;
    }

    @Override
    public List<EventTicket> detailByEventId(Integer eventId) {
            EntityWrapper<EventTicket> wrapper=new EntityWrapper<>();
            wrapper.eq("event_id",eventId);
        List<EventTicket> list=eventTicketMapper.selectList(wrapper);
        return list;
    }
}
