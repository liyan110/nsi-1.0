package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.pojo.CommunityAnswer;
import com.nsi.dao.CommunityAnswerMapper;
import com.nsi.service.CommunityAnswerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 提问互动-回复表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@Service
public class CommunityAnswerServiceImpl extends ServiceImpl<CommunityAnswerMapper, CommunityAnswer> implements CommunityAnswerService {

    @Autowired
    private CommunityAnswerMapper communityAnswerMapper;

    @Override
    public List<CommunityAnswer> listByAsk_id(int ask_id, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        EntityWrapper<CommunityAnswer> wrapper=new EntityWrapper<>();
            wrapper.eq("ask_id",ask_id);
        List<CommunityAnswer> list=communityAnswerMapper.selectList(wrapper);
        System.out.println("list:--:"+list);
        PageInfo pageInfo=new PageInfo(list);
        return pageInfo.getList();
    }
}
