package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.RoleMapper;
import com.nsi.pojo.Role;
import com.nsi.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-01-16 17:29
 */
@Service
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ServerResponse saveOn(Role role) {
        role.setCreateTime(new Date());
        role.setUpdateTime(role.getCreateTime());
        roleMapper.insert(role);
        return ServerResponse.createBySuccess("添加成功");
    }

    @Override
    public ServerResponse modifyRole(Role role) {
        role.setUpdateTime(new Date());
        roleMapper.updateByPrimaryKeySelective(role);
        return ServerResponse.createBySuccess("修改成功");
    }

    @Override
    public ServerResponse deleteRoleById(Integer roleId) {
        roleMapper.deleteByPrimaryKey(roleId);
        return ServerResponse.createBySuccess();
    }

    @Override
    public ServerResponse getRoleList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Role> roles = roleMapper.selectAll();
        PageInfo pageInfo = new PageInfo(roles);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse getRoleItem(String userMail) {
        Role role = roleMapper.findRoleByUserMail(userMail);
        return ServerResponse.createBySuccess(role);
    }
}
