package com.nsi.service.Impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ShopCartMapper;
import com.nsi.pojo.ShopCart;
import com.nsi.pojo.ShopCartGoodsJson;
import com.nsi.service.IShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-03-05
 **/
@Service
public class ShopCartServiceImpl implements IShopCartService {

    @Autowired
    private ShopCartMapper shopCartMapper;

    public ServerResponse create(String openId) {
        ShopCart shopCart = new ShopCart();
        shopCart.setUserid(openId);
        shopCart.setState("开放");
//        返回购物车ID
        int i = shopCartMapper.insert(shopCart);
        if (i == 0) {
            ServerResponse.createByErrorMessage("创建失败");
        }
        i = shopCart.getId();
//        System.out.println(i);
        return ServerResponse.createBySuccess("创建成功", i);
    }

    public ServerResponse addGoods(String cartId, String goodsId, String goodsName, String goodsPrice, String goodsNum) {

        // 组装json
        ShopCartGoodsJson shopCartGoodsJson = new ShopCartGoodsJson();
        shopCartGoodsJson.setGoodsId(goodsId);
        shopCartGoodsJson.setGoodsName(goodsName);
        shopCartGoodsJson.setGoodsPrice(goodsPrice);
        shopCartGoodsJson.setGoodsNum(goodsNum);
        Gson gson = new Gson();
        String jsonString = gson.toJson(shopCartGoodsJson);
        // 更新至数据库
        ShopCart shopCart = new ShopCart();
        shopCart.setId(Integer.parseInt(cartId));
        shopCart.setGoodsjson(jsonString);
        int i = shopCartMapper.updateByPrimaryKeySelective(shopCart);
        return ServerResponse.createBySuccess("更新成功", i);
    }

    public ServerResponse updateCart(String openId, String goodsJson) {

//        通过openID拿到 CartID
        ShopCart shopCart_00 = shopCartMapper.selectByUserId(openId);
        if ("锁定".equals(shopCart_00.getState())) {
//          已锁定的购物车不可修改
            return ServerResponse.createBySuccess("购物车已锁定、更新失败", 0);
        }
//        组装更新对象
        ShopCart shopCart = new ShopCart();
        shopCart.setId(shopCart_00.getId());
        shopCart.setGoodsjson(goodsJson);
        int i = shopCartMapper.updateByPrimaryKeySelective(shopCart);
        return ServerResponse.createBySuccess("更新成功", i);
    }

    public ServerResponse lockCart(String cartId) {
        ShopCart shopCart = new ShopCart();
//        查询购物车&计算金额
        shopCart = shopCartMapper.selectByPrimaryKey(Integer.parseInt(cartId));
        Gson gson = new Gson();
        List<ShopCartGoodsJson> list = gson.fromJson(shopCart.getGoodsjson(), new TypeToken<List<ShopCartGoodsJson>>() {
        }.getType());
        int totalPrice = 0;
        for (int j = 0; j < list.size(); j++) {
            int price = Integer.valueOf(list.get(j).getGoodsPrice());
            int num = Integer.valueOf(list.get(j).getGoodsNum());
            totalPrice = totalPrice + price * num;
        }
        System.out.println(totalPrice);
        shopCart.setId(Integer.parseInt(cartId));
        shopCart.setState("锁定");
        shopCart.setPrice(String.valueOf(totalPrice));
        int i = shopCartMapper.updateByPrimaryKeySelective(shopCart);

        return ServerResponse.createBySuccess("锁定成功", totalPrice);
    }

    public ServerResponse cartDetail(String cartId) {
        ShopCart shopCart = shopCartMapper.selectByPrimaryKey(Integer.parseInt(cartId));
        Gson gson = new Gson();
        List<ShopCartGoodsJson> list = gson.fromJson(shopCart.getGoodsjson(), new TypeToken<List<ShopCartGoodsJson>>() {
        }.getType());

        return ServerResponse.createBySuccess("返回成功", list);
    }
}
