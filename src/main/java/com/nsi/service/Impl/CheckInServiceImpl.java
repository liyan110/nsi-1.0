package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Splitter;
import com.nsi.common.ServerResponse;
import com.nsi.dao.CheckInMapper;
import com.nsi.pojo.CheckIn;
import com.nsi.service.ICheckInService;
import com.nsi.vo.CheckInVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CheckInServiceImpl implements ICheckInService {

    @Autowired
    private CheckInMapper checkInMapper;

    /**
     * 添加方法
     *
     * @param checkIn
     * @return
     */
    public ServerResponse<String> saveOn(CheckIn checkIn) {
        if (StringUtils.isBlank(checkIn.getName())) {
            return ServerResponse.createByErrorMessage("姓名不能为空");
        }
        int rowCount = checkInMapper.checkInByName(checkIn.getName());
        if (rowCount > 0) {
            return ServerResponse.createByErrorMessage("用户名已存在");
        }
        checkIn.setToken(RandomStringUtils.randomNumeric(8));
        rowCount = checkInMapper.insert(checkIn);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }

    /**
     * 返回信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ServerResponse list(int type, String keyword, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<CheckIn> list = null;
        List<CheckInVo> checkInVOList = null;
        if (type == 0) {
            list = checkInMapper.selectGetList(keyword);
        } else if (type == 1) {
            list = checkInMapper.selectByCheckInTime(keyword);
        } else {
            list = checkInMapper.selectByNoCheckInTime(keyword);
        }
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    /**
     * 修改方法
     *
     * @param checkIn
     * @return
     */
    public ServerResponse<String> updateOn(CheckIn checkIn) {
        if (checkIn.getId() == null) {
            return ServerResponse.createByErrorMessage("id不能为空");
        }
        int rowCount = checkInMapper.updateCheckIn(checkIn);
        if (rowCount > 0) {
            return ServerResponse.createByErrorMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }

    /**
     * 详情方法
     *
     * @param
     * @return
     */
    public ServerResponse delete(String checkIds) {
        List<String> checkIdList = Splitter.on(",").splitToList(checkIds);
        if (CollectionUtils.isEmpty(checkIdList)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        checkInMapper.deleteByCheckIds(checkIdList);
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    /**
     * 根据token获取用户信息
     *
     * @param token
     * @return
     */
    public ServerResponse getCheckInfo(String token) {
        if (StringUtils.isBlank(token)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        CheckIn checkIn = checkInMapper.selectByToken(token);
        if (checkIn != null) {
            return ServerResponse.createBySuccess(checkIn);
        }
        return ServerResponse.createByError();
    }

    /**
     * 根据token修改用户签到时间
     *
     * @param token
     * @return
     */
    public ServerResponse updateCheckInTime(String token) {
        if (StringUtils.isBlank(token)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = checkInMapper.updateCheckInTime(token);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }
}

