package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.SchoolEighteenMapper;
import com.nsi.pojo.School;
import com.nsi.service.ISchoolEighteenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-04-09 11:00
 */
@Service
public class SchoolEighteenServiceImpl implements ISchoolEighteenService {

    @Autowired
    private SchoolEighteenMapper schoolEighteenMapper;

    @Override
    public ServerResponse searchList(String searchKey, int pageNum, int pageSize, String verifyCode) {
        PageHelper.startPage(pageNum, pageSize);
        List<School> schoolList = schoolEighteenMapper.getList(searchKey, verifyCode);
        PageInfo pageInfo = new PageInfo(schoolList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse getDetail(Integer schoolId) {
        if (schoolId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        School school = schoolEighteenMapper.selectByPrimaryKey(schoolId);
        return ServerResponse.createBySuccess(school);
    }
}
