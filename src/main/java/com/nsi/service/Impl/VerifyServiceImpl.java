package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.InstitutionMapper;
import com.nsi.dao.SchoolMapper;
import com.nsi.dao.TalentMapper;
import com.nsi.pojo.Institution;
import com.nsi.pojo.School;
import com.nsi.service.IVerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Luo Zhen
 * @date: 2018/11/20 10:35
 * @description:
 */
@Service
public class VerifyServiceImpl implements IVerifyService {

    @Autowired
    private SchoolMapper schoolMapper;
    @Autowired
    private InstitutionMapper institutionMapper;
    @Autowired
    private TalentMapper talentMapper;

    @Override
    public Map<String, Object> getVerifyNotificationInfo() {
        Map<String, Object> resultMap = new HashMap<>();
        int verifySchoolCount = schoolMapper.selectByVerifySchoolInfo(String.valueOf(ResponseCode.CHECK.getCode()));

        int verifyInstitutionCount = institutionMapper.selectCount(
                new EntityWrapper<Institution>()
                        .eq("verify_sign", ResponseCode.CHECK.getCode()));

        int verifyTalentCount = talentMapper.setlectByVerifyTalentInfo(String.valueOf(ResponseCode.CHECK.getCode()));

        resultMap.put("code", ResponseCode.SUCCESS.getCode());
        resultMap.put("SchoolData", verifySchoolCount);
        resultMap.put("InstitutionData", verifyInstitutionCount);
        resultMap.put("TalentData", verifyTalentCount);
        return resultMap;
    }

    @Override
    public ServerResponse getVerifyInstitutionInfo(int pageNum, int pageSize, Integer verifySign) {
        PageHelper.startPage(pageNum, pageSize);
        List<Institution> list = institutionMapper.selectList(
                new EntityWrapper<Institution>()
                        .eq("verify_sign", verifySign)
                        .orderBy("load_time", false)
        );
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse modifyInstitutionVerifyStatus(Integer institutionId, Integer verifySign) {
        if (institutionId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Institution institution = new Institution();
        institution.setId(institutionId);
        institution.setVerifySign(verifySign);
        int rowCount = institutionMapper.updateById(institution);
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("修改机构库审核信息失败");
        }
        return ServerResponse.createBySuccessMessage("修改机构库审核信息成功");
    }

    @Override
    public ServerResponse getNoVerifySchoolInfo(int pageNum, int pageSize, String verifySign) {
        PageHelper.startPage(pageNum, pageSize);
        List<School> schoolList = schoolMapper.selectByNoVerifyList(verifySign);
        PageInfo pageInfo = new PageInfo(schoolList);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ServerResponse modifySchoolVerifyStatus(Integer schoolId, String verifySign) {
        if (schoolId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int rowCount = schoolMapper.updateByVerifySign(schoolId, verifySign);
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("修改机构库审核信息失败");
        }
        return ServerResponse.createBySuccessMessage("修改机构库审核信息成功");
    }
}
