package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ShopInvoiceMapper;
import com.nsi.pojo.ShopInvoice;
import com.nsi.service.IShopInvoiceService;
import com.nsi.vo.InvoiceOrderVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2019-03-14
 **/
@Service
public class ShopInvoiceServiceImpl extends ServiceImpl<ShopInvoiceMapper, ShopInvoice> implements IShopInvoiceService {

    @Autowired
    private ShopInvoiceMapper shopInvoiceMapper;

    @Override
    public ServerResponse<String> ShopInvoiceCreate(ShopInvoice shopInvoice) {
        this.insert(shopInvoice);
        return ServerResponse.createBySuccess();
    }

    @Override
    public ServerResponse<String> PassShopInvoice(Integer Id) {
        ShopInvoice shopInvoice = new ShopInvoice();
        shopInvoice.setId(Id);
        shopInvoice.setFinanceState("已开票");
        shopInvoice.setUpdateTime(new Date());
        shopInvoiceMapper.updateById(shopInvoice);
        return ServerResponse.createBySuccess();
    }

    @Override
    public ServerResponse InvoiceList(String searchKey, String manageState, String financeState, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<ShopInvoice> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(searchKey)) {
            wrapper.like("user_invoice_name", searchKey).or()
                    .like("user_invoice_num", searchKey).or()
                    .like("usermail", searchKey).or()
                    .like("user_order_num", searchKey);
        }
        if (StringUtils.isNotBlank(manageState)) {
            wrapper.eq("manage_state", manageState);
        }
        if (StringUtils.isNotBlank(financeState)) {
            wrapper.eq("finance_state", financeState);
        }
        wrapper.orderBy("id", false);
        List<ShopInvoice> InvoiceList = shopInvoiceMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(InvoiceList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse InvoiceOrderList(String SearchKey, String manageState, String financeState, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<InvoiceOrderVo> invoiceOrderVo = shopInvoiceMapper.InvoiceOrderList(SearchKey, manageState, financeState);
        PageInfo pageInfo = new PageInfo(invoiceOrderVo);
        return ServerResponse.createBySuccess(pageInfo);
    }


    @Override
    public ServerResponse checkInvoice(String orderNo) {
        int rowCount = shopInvoiceMapper.selectCountByOrderNo(orderNo);
        if (rowCount > 0) {
            return ServerResponse.createByErrorMessage("发票信息以存在");
        } else {
            return ServerResponse.createBySuccess();
        }
    }

    @Override
    public ServerResponse del(int id) {
        int rowCount = shopInvoiceMapper.deleteById(id);
        if (rowCount > 0) {
            return ServerResponse.createBySuccess("删除成功");
        } else {
            return ServerResponse.createByError("操作失败");
        }
    }
}
