package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.CommunityFollowMapper;
import com.nsi.dao.SchoolVisitMapper;
import com.nsi.pojo.CommunityFollow;
import com.nsi.pojo.SchoolVisit;
import com.nsi.service.IVisitSchoolService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2019-07-25 11:44
 */
@Service
@Transactional
public class VisitSchoolServiceImpl extends ServiceImpl<SchoolVisitMapper, SchoolVisit> implements IVisitSchoolService {

    @Autowired
    private SchoolVisitMapper schoolVisitMapper;

    @Override
    public ServerResponse findAll(int pageNum, int pageSize, Integer schoolId, String openId) {
        PageHelper.startPage(pageNum, pageSize);
        List<SchoolVisit> schoolVisits = schoolVisitMapper.selectVisitList(schoolId, openId);
        PageInfo pageInfo = new PageInfo(schoolVisits);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse save(SchoolVisit schoolVisit) {
        schoolVisit.setCreateTime(new Date());
        if (StringUtils.isBlank(schoolVisit.getName()) || schoolVisit.getSchoolId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        schoolVisitMapper.insert(schoolVisit);
        return ServerResponse.createBySuccessMessage("添加成功");
    }
}
