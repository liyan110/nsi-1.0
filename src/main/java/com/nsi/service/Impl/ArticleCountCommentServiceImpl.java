package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.ArticleMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Article;
import com.nsi.service.ArticleCountCommonService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = {NsiOperationException.class, Exception.class})
@Service
public class ArticleCountCommentServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleCountCommonService {

    @Override
    public void addWatchNum(Article article) {
        int reader = article.getArticleReader() == null ? 0 : article.getArticleReader();

        article.setArticleReader(++reader);

        this.updateById(article);
    }

}
