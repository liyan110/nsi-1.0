package com.nsi.service.Impl;

import com.nsi.common.ServerResponse;
import com.nsi.dao.CommentsMapper;
import com.nsi.pojo.Comments;
import com.nsi.service.ICommentsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author: Luo Zhen
 * @date: 2018/11/28 10:14
 * @description:
 */
@Service
public class CommentsServiceImpl implements ICommentsService {

    @Autowired
    private CommentsMapper commentsMapper;

    @Override
    public ServerResponse saveComments(Comments comments) {
        if (StringUtils.isBlank(comments.getCommentName()) || comments.getGuestId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        comments.setCreateTime(new Date());
        int resultCount = commentsMapper.insert(comments);
        if (resultCount <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        }
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @Override
    public ServerResponse selectByGuestId(Integer guestId) {
        if (guestId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        List<Comments> commentsList = commentsMapper.selectByGuestId(guestId);
        return ServerResponse.createBySuccess(commentsList);
    }

    @Override
    public ServerResponse modifyConmments(Comments comments) {
        if (comments.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        int count = commentsMapper.updateByPrimaryKeySelective(comments);
        if (count <= 0) {
            return ServerResponse.createByErrorMessage("修改失败");
        }
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    @Override
    public ServerResponse getCommentsByType(Integer guestId, String type) {
        List<Comments> commentsList = commentsMapper.findCommentsByGuestIdAndType(guestId, type);
        return ServerResponse.createBySuccess(commentsList);
    }
}
