package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.EventCollectMapper;
import com.nsi.pojo.EventCollect;
import com.nsi.service.EventCollectService;
import com.nsi.vo.EventCollectOrderVo;
import com.nsi.vo.EventOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动用户报名表 服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-23
 */
@Service
public class EventCollectServiceImpl extends ServiceImpl<EventCollectMapper, EventCollect> implements EventCollectService {

    @Autowired
    private EventCollectMapper eventCollectMapper;

    @Override
    public List List_byEventId(Integer EventId, Integer orderStatus, String searchKey) {
        List<EventCollectOrderVo> list = eventCollectMapper.List_byEventId(EventId, orderStatus, searchKey);
        return list;
    }

    @Override
    public List List_byPhone(String phone, Integer activeId) {
        List<EventCollectOrderVo> list = eventCollectMapper.List_byPhone(phone, activeId);
        return list;
    }

    @Override
    public List<EventCollectOrderVo> findEventList(Integer verify, Integer status, Integer artiveId, String searchKey) {
        List<EventCollectOrderVo> eventOrderList = eventCollectMapper.findEventOrderList(verify, status, artiveId, searchKey);
        return eventOrderList;
    }

    @Override
    public List<EventOrderVo> findByActiveIdOrEventCollectAndOrder(Integer acticeId, String searchKey) {
        return eventCollectMapper.findEventCollectAndOrderList(acticeId, searchKey);

    }

    @Override
    public List<EventOrderVo> findByActivePayList(Integer activeId, String searchKey) {
        return eventCollectMapper.findByActivePayList(activeId, searchKey);
    }

    @Override
    public int findByActivePayListCount(Integer activityId) {
        return eventCollectMapper.findByActivePayListCount(activityId);
    }

    @Override
    public int findByActiveCheckInCount(Integer activityId) {
        return eventCollectMapper.findByActiveCheckInCount(activityId);
    }

}
