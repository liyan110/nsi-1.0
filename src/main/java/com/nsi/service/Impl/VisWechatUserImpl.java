package com.nsi.service.Impl;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.LogMapper;
import com.nsi.pojo.Log;
import com.nsi.service.IVisWechatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Li Yan
 * @create 2018-10-13
 **/
@Service
public class VisWechatUserImpl implements IVisWechatUserService{


    @Autowired
    private LogMapper logMapper;

    /**
     * 活动发起人-插入
     */
    @Override
    public ServerResponse VisWechatUserLaunch_insert(Log log) {

        log.setSign("WechatUserLaunch");
        log.setIndex02(new Date());

        int Count =logMapper.insert(log);
        if (Count==1){
            return ServerResponse.createBySuccessMessage("插入成功");
        }else {
            return ServerResponse.createByErrorMessage("插入失败");
        }
    }

    /**
     * 活动发起人-检查是否报名过
     */
    @Override
    public ServerResponse VisWechatUserLaunch_check(String OpenId) {
        List<Log> list = null;
        list=logMapper.findBy_sign_index01("WechatUserLaunch",OpenId);

        if (list.size()>=1){
//            return ServerResponse.createByErrorMessage("检查不通过，用户已报名");
            return  ServerResponse.createBySuccess("检查不通过，用户已报名,返回用户信息",list);
        }else {
            return ServerResponse.createByErrorMessage("检查通过，用户未报名");
        }
    }

    /**
     * 活动参与人-检查是否砍过价
     */
    @Override
    public ServerResponse VisWechatUserShare_check(String OpenId,String LaunchOpenId) {
        List<Log> list = null;
//        list=logMapper.findBy_sign_index01("WechatUserShare",OpenId);
        list =logMapper.findBy_sign_index01_index09("WechatUserShare",OpenId,LaunchOpenId);
        if (list.size()>=1){
            return ServerResponse.createByErrorMessage("检查不通过，用户已参与");
        }else {
            return ServerResponse.createBySuccessMessage("检查通过，用户未参与");
        }
    }


    /**
     * 活动发起人-获取优惠金额和列表
     */
    @Override
    public ServerResponse VisWechatUserLaunch_list(String OpenId) {
        List<Log> list = null;
        list=logMapper.findBy_sign_index09("WechatUserShare",OpenId);
        PageInfo pageInfo = new PageInfo(list);
        return ServerResponse.createBySuccess("返回成功",pageInfo);

    }


    /**
     * 活动发起人-插入
     */
    @Override
    public ServerResponse VisWechatUserShare_insert(Log log,String LaunchOpenId) {

        log.setSign("WechatUserShare");
        log.setIndex02(new Date());
        log.setIndex09(LaunchOpenId);
        int Count =logMapper.insert(log);
        if (Count==1){
            return ServerResponse.createBySuccessMessage("插入成功");
        }else {
            return ServerResponse.createByErrorMessage("插入失败");
        }
    }




}
