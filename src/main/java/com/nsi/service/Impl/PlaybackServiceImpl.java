package com.nsi.service.Impl;


import com.nsi.common.ServerResponse;
import com.nsi.dao.PropertiesMapper;
import com.nsi.pojo.Properties;
import com.nsi.service.IPlaybackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PlaybackServiceImpl implements IPlaybackService {

    @Autowired
    private PropertiesMapper propertiesMapper;

    @Override
    public ServerResponse search(Integer courseId) {
        Properties properties = propertiesMapper.selectByCourseId_playback(courseId);
        String liveUrl=properties.getPlaybackUrl();
        System.out.println("liveUrl:"+liveUrl);
        return ServerResponse.createBySuccessMessage(liveUrl);
    }
}

