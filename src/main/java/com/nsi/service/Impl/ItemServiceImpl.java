package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ItemCatMapper;
import com.nsi.dao.ItemMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Item;
import com.nsi.pojo.ItemCat;
import com.nsi.service.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 业务层
 *
 * @author Luo Zhen
 * @create 2019-01-02 13:29
 */
@Service
public class ItemServiceImpl implements IItemService {

    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemCatMapper itemCatMapper;


    @Override
    @Transactional(rollbackFor = NsiOperationException.class)
    public ServerResponse saveItem(Item item) throws NsiOperationException {

        item.setStatus(ResponseCode.CHECK.getCode());
        item.setCreateTime(new Date());
        item.setUpdateTime(item.getCreateTime());

        try {
            int resultCount = itemMapper.insert(item);
            if (resultCount <= 0) {
                throw new NsiOperationException("添加失败");
            }
            return ServerResponse.createBySuccessMessage("添加成功");
        } catch (Exception e) {
            throw new NsiOperationException("create Item error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse findItemById(Integer itemId) {
        if (itemId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        Item item = itemMapper.findItemById(itemId);
        return ServerResponse.createBySuccess(item);
    }

    @Override
    @Transactional(rollbackFor = NsiOperationException.class)
    public ServerResponse modifyItem(Item item) throws NsiOperationException {
        if (item.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        item.setUpdateTime(new Date());
        try {
            // 1.修改item信息
            itemMapper.updateByPrimaryKeySelective(item);

            // 2.根据此id 查询出最新item 并同步到item_cat表中
            ServerResponse response = this.findItemById(item.getId());
            Item currentItem = (Item) response.getData();

            int count = itemCatMapper.selectByItemName(item.getName());
            // 3.判断 修改状态 = 1 并且 item_cat 表中不能有 itemName 同名重复数据
            if (item.getStatus() == 1 && count == 0) {
                // 4. 没有则 新增
                this.saveOn(item);
            } else {
                // 5. 有则 同步修改
                this.updateOn(currentItem);
            }
            return ServerResponse.createBySuccessMessage("修改成功");
        } catch (Exception e) {
            throw new NsiOperationException("modify item error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse findItemList(int pageNum, int pageSize, String searchKey) throws NsiOperationException {
        PageHelper.startPage(pageNum, pageSize);
        List<Item> itemList = itemMapper.findItemList(searchKey);
        PageInfo pageInfo = new PageInfo(itemList);
        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    @Transactional(rollbackFor = NsiOperationException.class)
    public ServerResponse removeItem(Integer itemId) {
        if (itemId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        try {
            itemMapper.deleteByPrimaryKey(itemId);
            return ServerResponse.createBySuccessMessage("删除成功");
        } catch (Exception e) {
            throw new NsiOperationException("remove itemCat error:" + e.getMessage());
        }
    }

    @Override
    public ServerResponse findItemList() {
        List<Item> itemList = itemMapper.findItemList(null);
        return ServerResponse.createBySuccess(itemList);
    }

    private void saveOn(Item item) {
        ItemCat itemCat = new ItemCat();
        itemCat.setParentId(0);
        itemCat.setName(item.getName());
        itemCat.setItemAmount(item.getActualAmount());
        itemCat.setWithTime(item.getEstimatedTime());
        itemCat.setText(item.getProjectBrief());
        itemCat.setCreator(item.getCreator());
        itemCat.setCreateTime(new Date());
        itemCat.setUpdateTime(itemCat.getCreateTime());

        itemCatMapper.insert(itemCat);
    }

    private void updateOn(Item item) {
        ItemCat itemCat = new ItemCat();
        itemCat.setParentId(0);
        itemCat.setName(item.getName());
        itemCat.setItemAmount(item.getActualAmount());
        itemCat.setWithTime(item.getEstimatedTime());
        itemCat.setText(item.getProjectBrief());
        itemCat.setCreator(item.getCreator());

        itemCatMapper.updateByItemName(itemCat);

    }

}
