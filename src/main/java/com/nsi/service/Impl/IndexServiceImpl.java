package com.nsi.service.Impl;

import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.dao.OrderMapper;
import com.nsi.pojo.Order;
import com.nsi.service.IIndexService;
import com.nsi.vo.OrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Luo Zhen
 * @create 2019-03-04 16:37
 */
@Service
public class IndexServiceImpl implements IIndexService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public ServerResponse getDaysStatistics() {
        Map<String, Object> resultMap = new HashMap<>();
        int bookCount = orderMapper.findOrderListByTodayAndType(Const.ProductTypeEnum.BOOKSTORE.getValue());
        int courseCount = orderMapper.findOrderListByTodayAndType(Const.ProductTypeEnum.MEETING.getValue());
        int cartCount = orderMapper.findOrderListByTodayAndType(Const.ProductTypeEnum.SHOPPING.getValue());

        resultMap.put("courseCount", courseCount);
        resultMap.put("bookCount", bookCount);
        resultMap.put("cartCount", cartCount);

        List<Order> orderList = orderMapper.findOrderListByToday();
        int totalPrice = this.totalAmountByOrder(orderList);

        resultMap.put("totalPrice", totalPrice);
        return ServerResponse.createBySuccess(resultMap);
    }

    @Override
    public ServerResponse getWeekStatistics() {
        Map<String, Object> resultMap = new HashMap<>();
        int bookCount = orderMapper.findOrderListByWeekAndType(Const.ProductTypeEnum.BOOKSTORE.getValue());
        int courseCount = orderMapper.findOrderListByWeekAndType(Const.ProductTypeEnum.MEETING.getValue());
        int cartCount = orderMapper.findOrderListByTodayAndType(Const.ProductTypeEnum.SHOPPING.getValue());

        resultMap.put("courseCount", courseCount);
        resultMap.put("bookCount", bookCount);
        resultMap.put("cartCount", cartCount);

        List<Order> orderList = orderMapper.findOrderListByWeek();
        int totalPrice = this.totalAmountByOrder(orderList);

        resultMap.put("totalPrice", totalPrice);
        return ServerResponse.createBySuccess(resultMap);
    }

    @Override
    public ServerResponse getMonthStatistics() {
        Map<String, Object> resultMap = new HashMap<>();
        int bookCount = orderMapper.findOrderListByMonthAndType(Const.ProductTypeEnum.BOOKSTORE.getValue());
        int courseCount = orderMapper.findOrderListByMonthAndType(Const.ProductTypeEnum.MEETING.getValue());
        int cartCount = orderMapper.findOrderListByTodayAndType(Const.ProductTypeEnum.SHOPPING.getValue());

        resultMap.put("courseCount", courseCount);
        resultMap.put("bookCount", bookCount);
        resultMap.put("cartCount", cartCount);

        List<Order> orderList = orderMapper.findOrderListByMonth();
        int totalPrice = this.totalAmountByOrder(orderList);

        resultMap.put("totalPrice", totalPrice);
        return ServerResponse.createBySuccess(resultMap);
    }

    @Override
    public ServerResponse getSevenDaysListStatistics() {
        List<OrderStatisticsVo> statisticsVosList = new ArrayList<>();

        List<String> lastSevenDays = this.getLastWeekList();
        for (int i = 0; i < lastSevenDays.size(); i++) {
            OrderStatisticsVo statisticsVo = new OrderStatisticsVo();
            String days = lastSevenDays.get(i);
            int bookCount = orderMapper.findOrderListByDateAndType(days, Const.ProductTypeEnum.BOOKSTORE.getValue());
            int courseCount = orderMapper.findOrderListByDateAndType(days, Const.ProductTypeEnum.MEETING.getValue());
            int cartCount = orderMapper.findOrderListByDateAndType(days, Const.ProductTypeEnum.SHOPPING.getValue());
            List<Order> orderList = orderMapper.findOrderListByDateList(days);
            int totalPrice = this.totalAmountByOrder(orderList);

            statisticsVo.setDate(days);
            statisticsVo.setCourseCount(courseCount);
            statisticsVo.setBookCount(bookCount);
            statisticsVo.setCartCount(cartCount);
            statisticsVo.setTotalPrice(totalPrice);
            statisticsVosList.add(statisticsVo);
        }
        return ServerResponse.createBySuccess(statisticsVosList);
    }

    @Override
    public ServerResponse getYearListStatistics(String year) {
        List<Order> orderList = orderMapper.findOrderListBySumAndYear(year);
        int total_price = 0;
        for (Order order : orderList) {
            total_price = total_price + order.getTotalPrice();
        }

        int bookCount = orderMapper.findOrderListCountByYearAndType(year, Const.ProductTypeEnum.BOOKSTORE.getValue());
        int cartCount = orderMapper.findOrderListCountByYearAndType(year, Const.ProductTypeEnum.SHOPPING.getValue());
        int courseCount = orderMapper.findOrderListCountByYearAndType(year, Const.ProductTypeEnum.MEETING.getValue());

        int resultCount = bookCount + cartCount;

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put(year, total_price);
        resultMap.put("书店", resultCount);
        resultMap.put("课程", courseCount);
        return ServerResponse.createBySuccess(resultMap);
    }

    /**
     * 根据当前时间计算出前7天每天的日期
     *
     * @return
     */
    private List<String> getLastWeekList() {
        List<String> weekList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i > -7; i--) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, i);
            String days = sdf.format(cal.getTime());
            weekList.add(days);
        }
        return weekList;
    }

    private int totalAmountByOrder(List<Order> orderList) {
        int totalPrice = 0;
        for (Order order : orderList) {
            totalPrice = totalPrice + order.getTotalPrice();
        }
        return totalPrice;
    }
}
