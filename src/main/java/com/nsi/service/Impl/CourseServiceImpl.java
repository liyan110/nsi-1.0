package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.CourseMapper;
import com.nsi.dao.Course_userMapper;
import com.nsi.pojo.Course;
import com.nsi.pojo.Course_user;
import com.nsi.service.ICourseService;
import com.old.nsi_class.DB.Course_DB;
import com.old.nsi_class.common.properties.Properties;
import com.old.nsi_class.model.Properties_model;
import com.old.nsi_class.model.Teacher_model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Li Yan
 * @create 2018-10-24
 **/
@Service
public class CourseServiceImpl implements ICourseService {


    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private Course_userMapper course_userMapper;


    @Override
    public ServerResponse Course_list(String Id, String CourseSubject) {

        List<Course> list = courseMapper.selectBy_Id_CourseSubject(Id, CourseSubject);
        return ServerResponse.createBySuccess("返回成功", list);
    }

    @Override
    public ServerResponse MyCourse(String UserMail) {

//        List<Course_user> list = course_userMapper.selectBy_ClassId_UserMail(null,UserMail);

        List<Course> list = courseMapper.selectBy_UserMail(UserMail);

        return ServerResponse.createBySuccess("返回成功", list);
    }

    @Override
    public ServerResponse Insert_Course(Course course) {

//        添加发布时间
        java.util.Date currentTime = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        course.setCourserelease(formatter.format(currentTime));

        int i = courseMapper.insert(course);
        if (i > 0) {
            return ServerResponse.createBySuccessMessage("添加成功");
        }
        return ServerResponse.createByErrorMessage("添加失败");
    }

    @Override
    public ServerResponse alter_Course(Course course) {

        int i = courseMapper.updateByPrimaryKeySelective(course);
        if (i > 0) {
            return ServerResponse.createBySuccessMessage("修改成功");
        }
        return ServerResponse.createByErrorMessage("修改失败");
    }

    @Override
    public ServerResponse Delete_Course(int Class_Id) {
        int i = courseMapper.deleteByPrimaryKey(Class_Id);
        if (i > 0) {
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }

    @Override
    public ServerResponse Admin_Course_list() {
        List<Course> list = courseMapper.selectBy_Id_CourseSubject(null, null);

        return ServerResponse.createBySuccess("查询成功", list);
    }


    @Override
    public ServerResponse LiveCourseSet(String TeacherId, String CourseId) {

        String sql = "UPDATE nsi_properties SET teacherId='" + TeacherId + "',courseId='" + CourseId + "' WHERE id='1';";
        int i = Properties.commonMethod(sql);
        if (i > 0) {
            return ServerResponse.createBySuccessMessage("设置成功");
        } else {
            return ServerResponse.createByErrorMessage("设置失败");
        }
    }

    @Override
    public Map ShowLiveCourse() {
        Map<String, Object> modelMap = new HashMap<>();
        List<Properties_model> Properties_list = Properties.searchProperties();
        String TeacherId = Properties_list.get(0).getTeacherId();
        String CourseId = Properties_list.get(0).getCourseId();

        List<Course> courseList = courseMapper.selectBy_Id_CourseSubject(CourseId, null);
        String sqlTeacher = "SELECT * FROM nsi_teacher WHERE Id='" + TeacherId + "';";
        List<Teacher_model> teacherList = Course_DB.searchTeacher(sqlTeacher);
        modelMap.put("code", 0);
        modelMap.put("msg", "返回成功");
        modelMap.put("courseList", courseList);
        modelMap.put("teacherList", teacherList);
        return modelMap;
    }

    @Override
    public ServerResponse AdminSearch_Course_User(String UserMail, String ClassId, String pageNum, String OnePageNum) {


        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(OnePageNum));

        List<Course_user> list = course_userMapper.selectBy_ClassId_UserMail(ClassId, UserMail);

        PageInfo pageInfo = new PageInfo(list);

        return ServerResponse.createBySuccess("返回成功", (int) pageInfo.getTotal(), pageInfo.getList());
    }


}
