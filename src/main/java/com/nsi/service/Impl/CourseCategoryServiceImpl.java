package com.nsi.service.Impl;

import com.nsi.common.ResponseCode;
import com.nsi.dao.CourseCategoryMapper;
import com.nsi.pojo.CourseCategory;
import com.nsi.service.ICourseCategoryService;
import com.nsi.vo.CourseItemVo;
import com.nsi.vo.CourseListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 课程详情业务
 *
 * @author Luo Zhen
 * @create 2019-02-13 10:31
 */
@Service
public class CourseCategoryServiceImpl implements ICourseCategoryService {

    @Autowired
    private CourseCategoryMapper courseCategoryMapper;

    @Override
    public void saveCourseCategory(CourseCategory category) {
        courseCategoryMapper.insert(category);
    }

    @Override
    public CourseCategory findCourseCategoryById(Integer courseListId) {
        return courseCategoryMapper.selectByPrimaryKey(courseListId);
    }

    @Override
    public void deleteCourseCategory(Integer courseListId) {
        courseCategoryMapper.deleteByPrimaryKey(courseListId);
    }

    @Override
    public void updateCourseCategory(CourseCategory category) {
        courseCategoryMapper.updateByPrimaryKeySelective(category);
    }

    @Override
    public List<CourseCategory> findAll(String status) {
        return courseCategoryMapper.findAll(status);
    }

    @Override
    public List<CourseCategory> findCourseCategoryByListId(Integer listId) {
        return courseCategoryMapper.findCourseCategoryByListId(listId, String.valueOf(ResponseCode.SUCCESS.getCode()));
    }

    @Override
    public void deleteCourseCategoryByListId(Integer courseListId) {
        courseCategoryMapper.deleteByListId(courseListId);
    }

    @Override
    public CourseCategory findCourseCategoryByCourseId(Integer courseId) {
        return courseCategoryMapper.findCourseCategoryByCourseId(courseId);
    }

    @Override
    public List<CourseItemVo> selectByListId(Integer listId, Integer status) {
        return courseCategoryMapper.selectByListId(listId, status);
    }
}
