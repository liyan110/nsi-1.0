package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.EventMeetingMapper;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.EventMeeting;
import com.nsi.service.EventMeetingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-14
 */
@Service
@Transactional
public class EventMeetingServiceImpl implements EventMeetingService {

    @Autowired
    private EventMeetingMapper eventMeetingMapper;

    @Override
    public ServerResponse saveMeeting(EventMeeting meet) {
        if (StringUtils.isEmpty(meet.getTitle())) {
            throw new NsiOperationException("活动标题不能为空");
        }
        if (meet.getStartTime() == null || meet.getEndTime() == null) {
            throw new NsiOperationException("活动开始/结束日期不能为空");
        }
        if (StringUtils.isEmpty(meet.getCreatePeople())) {
            throw new NsiOperationException("活动类型不能为空");
        }
        if (meet.getIsTicket() == null || meet.getIsInvoice() == null) {
            throw new NsiOperationException("发票或电子票不能为空");
        }

        meet.setCreateTime(new Date());
        Integer autoId = eventMeetingMapper.insert(meet);
        return ServerResponse.createBySuccess(autoId);
    }

    @Override
    public ServerResponse updateMeeting(EventMeeting eventMeeting) {
        if (eventMeeting.getId() == null) {
            throw new NsiOperationException("id not empty");
        }
        Integer autoId = eventMeetingMapper.updateById(eventMeeting);
        return ServerResponse.createBySuccess(autoId);
    }

    @Override
    public ServerResponse getMeetingList(int pageNum, int pageSize, String type, String startTime) {
        PageHelper.startPage(pageNum, pageSize);
        EntityWrapper<EventMeeting> wrapper = new EntityWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(type), "type", type);
        if ("DESC".equalsIgnoreCase(startTime)) {
            wrapper.orderBy("start_time", false);
        } else {
            wrapper.orderBy("start_time", true);
        }
        List<EventMeeting> lists = eventMeetingMapper.selectList(wrapper);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse deleteMeeting(Integer meetId) {
        if (meetId == null) {
            throw new NsiOperationException("id not empty");
        }
        eventMeetingMapper.deleteById(meetId);
        return ServerResponse.createBySuccess();
    }

    @Override
    public ServerResponse findEventMeetingList(String phone) {
        if (StringUtils.isEmpty(phone)) {
            throw new NsiOperationException("参数不合法");
        }
        // todo 返回个人参会详情(已购买)

        return null;
    }

    @Override
    public ServerResponse findOne(Integer enentId) {
        return ServerResponse.createBySuccess(eventMeetingMapper.selectById(enentId));
    }
}
