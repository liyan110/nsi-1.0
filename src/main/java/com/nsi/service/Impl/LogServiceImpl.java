package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ActivityMapper;
import com.nsi.dao.LogMapper;
import com.nsi.pojo.Activity;
import com.nsi.pojo.Log;
import com.nsi.service.ILogService;
import com.nsi.util.DateTimeUtil;
import com.nsi.vo.UserPayInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Luo Zhen
 * @create 2018-08-14
 */
@Service
@Slf4j
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements ILogService {

    private static final String H5_LOG = "H5log";

    @Autowired
    private LogMapper logMapper;
    @Autowired
    private ActivityMapper activityMapper;

    /**
     * 根据推荐人，返回详情
     *
     * @param keyword
     * @return
     */
    @Override
    public ServerResponse getList(String keyword) {
        List<Log> list = logMapper.getList(keyword);
        if (CollectionUtils.isEmpty(list)) {
            log.info("数据为空！");
        }
        return ServerResponse.createBySuccess(list);

    }

//    /**
//     * 根据邮箱查询活动表详情(微信/支付宝)
//     *
//     * @return
//     */
//    @Override
//    public List<UserPayInfoVo> getPayUserInfoList(List<Log> listLog) {
//        List<UserPayInfoVo> userPayInfoVoList = new ArrayList<>();
//        for (Log log : listLog) {
//            UserPayInfoVo userPayInfoVo = assertUserPayInfoVo(log);
//            userPayInfoVoList.add(userPayInfoVo);
//        }
//        return userPayInfoVoList;
//    }

    @Override
    public ServerResponse saveLog(Log log) {
        log.setSign(H5_LOG);
        logMapper.insert(log);
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @Override
    public ServerResponse getH5LogList(String index03, String index04, String index05, String index06) {

        Log log = new Log();
        log.setIndex03(index03);
        log.setIndex04(index04);
        log.setIndex05(index05);
        log.setIndex06(index06);
        log.setSign(H5_LOG);
        logMapper.insert(log);


        Map<String, Object> map = new HashMap<>();
        int resultCount = logMapper.selectBySign(H5_LOG);
        int count03 = logMapper.selectByIndex03Count(index03, H5_LOG);
        int count04 = logMapper.selectByIndex04Count(index04, H5_LOG);
        int count05 = logMapper.selectByIndex05Count(index05, H5_LOG);
        int count06 = logMapper.selectByIndex06Count(index06, H5_LOG);
        int sum = logMapper.selectByIndex05AndIndex06(index05, index06, H5_LOG);

        NumberFormat numberFormat = NumberFormat.getInstance();

        String count03_100 = numberFormat.format((float) count03 / (float) resultCount * 100);
        String count04_100 = numberFormat.format((float) count04 / (float) resultCount * 100);
        String count05_100 = numberFormat.format((float) count05 / (float) resultCount * 100);
        String count06_100 = numberFormat.format((float) count06 / (float) resultCount * 100);


        map.put("resultCount", resultCount);
        map.put("count03", count03);
        map.put("count04", count04);
        map.put("count05", count05);
        map.put("count06", count06);
        map.put("count03_100", count03_100);
        map.put("count04_100", count04_100);
        map.put("count05_100", count05_100);
        map.put("count06_100", count06_100);
        map.put("sum", sum);


        return ServerResponse.createBySuccess(map);
    }

//    /**
//     * 组装支付模型
//     *
//     * @param log
//     * @return
//     */
//    private UserPayInfoVo assertUserPayInfoVo(Log log) {
//        UserPayInfoVo userPayInfoVo = new UserPayInfoVo();
//        userPayInfoVo.setId(log.getId());
//        userPayInfoVo.setPayment(log.getSign());
//        userPayInfoVo.setMail(log.getIndex06());
//        Activity activity = activityMapper.queryByUserName(log.getIndex06());
//        if (activity != null) {
//            userPayInfoVo.setUsername(activity.getContent3());
//            userPayInfoVo.setPhone(activity.getContent6());
//            userPayInfoVo.setInstitution(activity.getContent4());
//            userPayInfoVo.setPosition(activity.getContent5());
//        }
//        userPayInfoVo.setActivity(log.getIndex08());
//        userPayInfoVo.setTotal_fee(log.getIndex07());
//        userPayInfoVo.setCreate_time(DateTimeUtil.dateToStr(log.getIndex02()));
//        return userPayInfoVo;
//    }

}
