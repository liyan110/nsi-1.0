package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.nsi.common.ServerResponse;
import com.nsi.dao.SysExceptionLogMapper;
import com.nsi.pojo.EventApplyHead;
import com.nsi.pojo.SysExceptionLog;
import com.nsi.service.ISysExceptionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Li Yan
 * @create 2019-10-21
 **/
@Service
public class SysExceptionLogServiceImpl implements ISysExceptionLogService{

    @Autowired
    private SysExceptionLogMapper sysExceptionLogMapper;

    @Override
    public ServerResponse list() {
        EntityWrapper<SysExceptionLog> wrapper=new EntityWrapper<>();
        wrapper.orderBy("id",false);
        List<SysExceptionLog> list= sysExceptionLogMapper.selectList(wrapper);

        return ServerResponse.createBySuccess("success",list);
    }

    @Override
    public ServerResponse delete(int id) {

        int i=sysExceptionLogMapper.deleteById(id);
        return ServerResponse.createBySuccessMessage("success");
    }
}
