package com.nsi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ResponseCode;
import com.nsi.common.ServerResponse;
import com.nsi.dao.RecuitmentInfoMapper;
import com.nsi.dao.RecuitmentMapper;
import com.nsi.pojo.Recuitment;
import com.nsi.pojo.RecuitmentInfo;
import com.nsi.service.IRecuitmentService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author: Luo Zhen
 * @date: 2018/11/23 10:43
 * @description:
 */
@Service
public class RecuitmentServiceImpl implements IRecuitmentService {

    @Autowired
    private RecuitmentMapper recuitmentMapper;
    @Autowired
    private RecuitmentInfoMapper recuitmentInfoMapper;

    @Override
    public ServerResponse addRecuitment(Recuitment recuitment) {
        if (StringUtils.isBlank(recuitment.getName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        recuitment.setLoadTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int rowCount = recuitmentMapper.insert(recuitment);
        int recuitmentId = recuitment.getId();
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        } else {
            return ServerResponse.createBySuccess(recuitmentId);
        }
    }


    @Override
    public ServerResponse addRecuitmentInfo(RecuitmentInfo recuitmentInfo) {
        if (StringUtils.isBlank(recuitmentInfo.getRecuitmentId()) ||
                StringUtils.isBlank(recuitmentInfo.getRecuitmentName())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        recuitmentInfo.setLoadTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int rowCount = recuitmentInfoMapper.insert(recuitmentInfo);
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        }
        return ServerResponse.createBySuccess("添加成功");
    }

    @Override
    public Map<String, Object> getRecuitmentInfoList(int pageNum, int pageSize, String searchKey) {
        PageHelper.startPage(pageNum, pageSize);
        Map<String, Object> resultMap = new HashMap<>();
        List<RecuitmentInfo> recumentInfoList = recuitmentInfoMapper.selectBySearchKey(searchKey);
        List<Recuitment> recumentList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(recumentInfoList)) {
            for (RecuitmentInfo item : recumentInfoList) {
                Recuitment recuitment = recuitmentMapper.selectByPrimaryKey(Integer.parseInt(item.getRecuitmentId()));
                if (recuitment != null) {
                    recumentList.add(recuitment);
                }
            }
            PageInfo pageInfo = new PageInfo(recumentInfoList);
            resultMap.put("code", ResponseCode.SUCCESS.getCode());
            resultMap.put("count", pageInfo.getTotal());
            resultMap.put("recumentInfoList", pageInfo.getList());
            resultMap.put("recumentList", recumentList);
        } else {
            resultMap.put("code", ResponseCode.ERROR.getCode());
            resultMap.put("errMsg", "招聘库职位信息不存在");
        }
        return resultMap;
    }

    @Override
    public ServerResponse selectAllById(Integer recuitmentId) {
        if (recuitmentId == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        RecuitmentInfo info = recuitmentInfoMapper.selectByPrimaryKey(recuitmentId);
        return ServerResponse.createBySuccess(info);
    }
}
