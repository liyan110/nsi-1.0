package com.nsi.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.dao.ExperienceUserMapper;
import com.nsi.pojo.ExperienceUser;
import com.nsi.service.ExperienceUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author luo Zhen
 * @since 2021-03-17
 */
@Service
public class ExperienceUserServiceImpl extends ServiceImpl<ExperienceUserMapper, ExperienceUser> implements ExperienceUserService {

}
