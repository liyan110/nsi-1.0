package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.nsi.common.Const;
import com.nsi.common.ServerResponse;
import com.nsi.dao.ConfigureMapper;
import com.nsi.pojo.Configure;
import com.nsi.service.IConfigureService;
import com.nsi.util.DateTimeUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-07
 */
@Service
public class ConfigureServiceImpl extends ServiceImpl<ConfigureMapper, Configure> implements IConfigureService {

    @Autowired
    ConfigureMapper configureMapper;

    /**
     * 修改方法
     *
     * @param configure
     * @return
     */
    @Override
    public ServerResponse<String> update(Configure configure) {
        if (configure.getId() == null) {
            return ServerResponse.createByErrorMessage("id不能为空");
        }
        configureMapper.updateById(configure);
        return ServerResponse.createBySuccessMessage("修改成功");
    }

    /**
     * 返回信息
     *
     * @param type
     * @return
     */
    @Override
    public ServerResponse list(String type) {
        List<Configure> list = this.selectList(
                new EntityWrapper<Configure>()
                        .eq("type", type));
        int rowCount = list.size();
        return ServerResponse.createBySuccess("返回成功", rowCount, list);

    }

    /**
     * 修改模板
     *
     * @param templateHtml
     * @return
     */
    @Override
    public ServerResponse updateTemplate(String templateHtml) {
        if (StringUtils.isBlank(templateHtml)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        String htmlStr = StringEscapeUtils.unescapeHtml4(templateHtml);
        int rowCount = configureMapper.updateByTemplateHtml(htmlStr, Const.ARTICLE_TYPE);
        if (rowCount > 0) {
            return ServerResponse.createBySuccessMessage("模板修改成功");
        }
        return ServerResponse.createByErrorMessage("模板修改失败");
    }

    @Override
    public ServerResponse insertConfigure(Configure configure) {
        if (StringUtils.isBlank(configure.getType()) || StringUtils.isBlank(configure.getContent01())) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        configure.setContent10(new SimpleDateFormat(DateTimeUtil.STANDARD_FORMAT).format(new Date()));
        int rowCount = configureMapper.insert(configure);
        if (rowCount <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        }
        return ServerResponse.createBySuccessMessage("添加成功");
    }

    @Override
    public ServerResponse getConfigureList(String typeName) {
        if (StringUtils.isBlank(typeName)) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        List<Configure> configureList = this.selectList(
                new EntityWrapper<Configure>()
                        .eq("type", typeName));
        return ServerResponse.createBySuccess(configureList);
    }

    @Override
    public ServerResponse modifyConfigure(Configure configure) {
        if (configure.getId() == null) {
            return ServerResponse.createByErrorMessage("参数不合法");
        }
        configureMapper.updateById(configure);
        return ServerResponse.createByErrorMessage("修改失败");
    }

}
