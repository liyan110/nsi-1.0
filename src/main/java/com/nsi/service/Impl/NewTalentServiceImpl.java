package com.nsi.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.dao.NewTalentMapper;
import com.nsi.pojo.NewTalent;
import com.nsi.service.NewTalentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-11-26
 */
@Service
public class NewTalentServiceImpl extends ServiceImpl<NewTalentMapper, NewTalent> implements NewTalentService {

    @Autowired
    private NewTalentMapper newTalentMapper;

    @Override
    public ServerResponse findList(int pageNum, int pageSize, String searchKey, Integer isCheck) {
        PageHelper.startPage(pageNum, pageSize);
        List<NewTalent> lists = this.queryByCondition(searchKey, isCheck);
        PageInfo pageInfo = new PageInfo(lists);
        return ServerResponse.createBySuccess(pageInfo);
    }

    private List<NewTalent> queryByCondition(String searchKey, Integer isCheck) {
        EntityWrapper<NewTalent> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotEmpty(searchKey)) {
            wrapper.like("username", searchKey)
                    .or()
                    .eq("telphone", searchKey)
                    .or()
                    .eq("user_mail", searchKey);
        }
        if (isCheck != null) {
            wrapper.eq("is_check", isCheck);
        }
        wrapper.orderBy("id", false);
        List<NewTalent> lists = newTalentMapper.selectList(wrapper);
        return lists;
    }

    @Override
    public ServerResponse queryList(String searchKey) {
        List<NewTalent> lists = this.queryByCondition(searchKey, null);
        return ServerResponse.createBySuccess(lists);
    }
}
