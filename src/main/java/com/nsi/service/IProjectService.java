package com.nsi.service;

import com.baomidou.mybatisplus.service.IService;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Projects;

/**
 * @author: Luo Zhen
 * @date: 2018/9/26 10:29
 * @description:
 */
public interface IProjectService extends IService<Projects> {

    /**
     * 添加项目库
     *
     * @param projects
     * @return
     * @throws NsiOperationException
     */
    void addProject(Projects projects) throws NsiOperationException;

    /**
     * 修改项目库
     *
     * @param projects
     * @return
     * @throws NsiOperationException
     */
    void modifyProject(Projects projects) throws NsiOperationException;

    /**
     * 返回关键字分页详情
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getProjectList(String searchKey, int pageNum, int pageSize);


    /**
     * 根据提交者邮箱 返回详情
     *
     * @param userMail
     * @return
     */
    ServerResponse getProjectByUserMail(String userMail);

}
