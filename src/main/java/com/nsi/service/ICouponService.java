package com.nsi.service;

import com.github.pagehelper.PageInfo;
import com.nsi.pojo.Coupon;

public interface ICouponService {

    void save(Coupon coupon);

    void modify(Coupon coupon);

    void delete(Integer couponId);

    PageInfo findALl(int pageNum, int pageSize);
}
