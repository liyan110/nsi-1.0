package com.nsi.service;

import com.nsi.common.ServerResponse;

public interface ISchoolEighteenService {
    /**
     * 详情List(分页,排序)
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse searchList(String searchKey, int pageNum, int pageSize, String verifyCode);


    /**
     * 根据Id 返回详情
     *
     * @param schoolId
     * @return
     */
    ServerResponse getDetail(Integer schoolId);
}
