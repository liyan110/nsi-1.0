package com.nsi.service;

import com.github.pagehelper.PageInfo;
import com.nsi.common.ServerResponse;
import com.nsi.exception.NsiOperationException;
import com.nsi.pojo.Coordinate;
import com.nsi.pojo.School;
import com.nsi.pojo.SchoolCertification;

/**
 * @author Luo Zhen
 * @create 2018-08-17
 */
public interface ISchoolService {

    /**
     * 展示学校了广告牌
     *
     * @param keyword
     * @return
     */
    ServerResponse showSchoolBoards(String keyword);

    /**
     * 高级搜索(分页)
     *
     * @param properties
     * @param area
     * @param system
     * @param course
     * @param fondTime
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<PageInfo> searchPower(String[] properties, String[] area, String[] system, String[] course, String fondTime, int pageNum, int pageSize);

    /**
     * 详情List(分页,排序)
     *
     * @param searchKey
     * @param orderBy
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse searchList(String searchKey, String orderBy, int pageNum, int pageSize, String verifyCode);

    /**
     * 关键字返回学校名字
     *
     * @param keyword
     * @return
     */
    ServerResponse getSchoolName(String keyword);

    /**
     * 根据Id 返回详情
     *
     * @param schoolId
     * @return
     */
    ServerResponse getDetail(Integer schoolId);

    /**
     * 添加方法
     *
     * @param school
     * @param certification
     * @return
     */
    ServerResponse addSchoolInfo(School school, SchoolCertification certification) throws NsiOperationException;

    /**
     * 验证学校名
     *
     * @param schoolName
     * @return
     */
    ServerResponse checkValidedByName(String schoolName);


    /**
     * 可视化(返回学校制定字段)
     *
     * @return
     */
    ServerResponse getVisualizationSchoolList();

    /**
     * 可视化(添加学校经纬度)
     *
     * @param coordinate
     * @return
     */
    ServerResponse saveOn(Coordinate coordinate);

    /**
     * 后台返回学校详情带分页
     *
     * @param searchKey
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getSchoolList(String searchKey, int pageNum, int pageSize, String verifyCode);

    /**
     * 后台 根据id 删除学校信息和学校审核信息
     *
     * @param schoolId
     * @return
     */
    ServerResponse removeSchoolList(Integer schoolId) throws NsiOperationException;

    /**
     * 后台 修改学校信息和学校审核信息
     *
     * @param school
     * @param schoolCertification
     * @return
     */
    ServerResponse modifySchoolCategory(School school, SchoolCertification schoolCertification);

    /**
     * 学校库可视化——省份学校数量查询
     *
     * @param option
     * @return
     */
    ServerResponse getSchoolNum(String option);


    ServerResponse getTimeList(String beginTime, String endTime);

    /**
     * 前台高级搜索(第二页)
     *
     * @param area
     * @param system
     * @param properties
     * @param authority
     * @param evaluation
     * @param organization
     * @param course
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<PageInfo> searchPowerList(
            String[] area, String[] system, String[] properties,
            String[] authority, String[] evaluation, String[] organization,
            String[] course, int pageNum, int pageSize);

    ServerResponse getSchoolItemList();

}
