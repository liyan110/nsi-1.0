package com.nsi.common;

/**
 * Created by geely
 */
public enum ResponseCode {

    CHECK(0, "审核中"),
    PASS(1, "已审核"),
    REFUSE(2, "审核拒绝"),
    SUCCESS(0, "SUCCESS"),
    ERROR(1, "ERROR"),
    TOKEN_IS_INVALID(3, "Token 无效"),
    INNER_ERROR(99, "内部系统错误"),
    SERVER_ERROR(500, "未知错误"),
    ILLEGAL_ARGUMENT(2, "参数不合法"),
    IS_PUBLIC(1, "公开"),
    FAILED(-1, "审核失败"),
    NO_AUTHENTICATION(0, "无认证"),
    YES_AUTHENTICATION(1, "有认证"),
    UPLOAD_ERROR(10, "上传失败"),
    DELETION_FAILED(20, "删除失败");
    private final int code;
    private final String desc;


    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ResponseCode stateOf(int code) {
        for (ResponseCode stateEnum : values()) {
            if (stateEnum.getCode() == code) {
                return stateEnum;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
