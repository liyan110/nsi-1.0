package com.nsi.common;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * @author Luo Zhen
 */
public class Const {

    /**
     * 用户存储session信息
     */
    public static final String USERNAME = "username";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String SECRET_KEY = "secretKey_";
    public static final String ALL_PROVINCE = "provinceList";
    public static final long EXPIRE_TIME = 86400000;


    /**
     * 文章置顶参数
     */
    public static final Integer NUM_DEFAULT_VALUE = 0;
    public static final Integer CANCEL_TOP = 1;
    public static final Integer TOP_VALUE = 2;

    public static final Integer SHOW_STATUS = 0;
    public static final Integer HIDE_STATUS = 1;

    public static final Integer PARENT_ID = 0;
    public static final Integer CHILDREN_ID = 1;

    public static final String ACTIVITY_TYPE_VIS2019 = "vis2019";

    public static final Integer CITY_WIDE_SIZE = 4;

    public static final String ARTICLE_TYPE = "官网文章模板";
    public static final String SCHOOL_TYPE = "四库全书学校模板";

    public static final String REGISTER_TYPE_SIKU = "四库全书小程序注册";
    public static final String REGISTER_TYPE_SIKUQUANSHU = "四库全书官网";
    public static final String REGISTER_MANAGER = "后台管理";

    public static final String[] CHINA_AREAS = {"北京", "上海", "天津", "重庆", "浙江", "江苏", "广东", "福建", "湖南", "湖北", "辽宁",
            "吉林", "黑龙江", "河北", "河南", "山东", "陕西", "甘肃", "新疆", "青海", "山西", "四川",
            "贵州", "安徽", "江西", "云南", "内蒙古", "西藏", "广西", "宁夏", "海南", "香港", "澳门", "台湾"};

    public static final String TEST_ACCOUNT_POSTION = "新学说课堂临时账号";

    /**
     * 官网首页跳转参数
     */
    public static final String BOARDNAME = "官网的首页跳转";

    /**
     * 支付宝回调状态
     */
    public interface AlipayCallback {
        String TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";
        String RESPONSE_SUCCESS = "success";
        String RESPONSE_FAILED = "failed";
    }


    /**
     * 升序降序用到得参数
     */
    public interface SchoolListOrderBy {
        Set<String> SCHOOL_ASC_DESC =
                Sets.newHashSet("Load_Time-asc",
                        "Load_Time-desc",
                        "Founded_time-asc",
                        "Founded_time-desc",
                        "convert(School_name using gbk)-asc",
                        "convert(School_name using gbk)-desc");
    }


    public interface imageUrl {
        //文字图片活动
        String image_one = "C:\\upImage\\event\\H5paper\\100.jpg";
        String image_two = "C:\\upImage\\event\\H5paper\\80.jpg";
        String image_three = "C:\\upImage\\event\\H5paper\\60.jpg";
        String image_four = "C:\\upImage\\event\\H5paper\\40.jpg";
        String image_five = "C:\\upImage\\event\\H5paper\\20.jpg";
        String image_six = "C:\\upImage\\event\\H5paper\\0.jpg";

        //VIS图片活动
        String TICKET_IMAGE_VIS2019_GB = "C:\\upImage\\event\\HRticketQR\\vis2020-gb.jpg";
        String TICKET_IMAGE_VIS2019_ZX = "C:\\upImage\\event\\HRticketQR\\vis2020-zx.jpg";


    }

    public interface PayInfo {
        String WX_PAY_TYPE = "微信支付";
        String WX_PAY_TRANSACTION_NO = "微信订单号：";
        String WX_PAY_ORDER_NO = "商户订单号：";
        String WX_PAY_SUCCESS = "已支付";

        String ALI_PAY_TYPE = "支付宝支付";
        String ALI_PAY_TRANSACTION_NO = "支付宝交易凭证号：";
        String ALI_PAY_ORDER_NO = "商户订单号：";
        String ALI_PAY_SUCCESS = "已支付";
    }

    public enum ProductTypeEnum {
        BOOKSTORE("书店"),
        MEETING("课程"),
        SHOPPING("购物车");

        private String value;

        ProductTypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CheckStatusEnum {
        SIGN_IN("已签到", 0),
        NOT_SIGN_IN("未签到", 1);

        private String value;
        private int code;

        CheckStatusEnum(String value, int code) {
            this.value = value;
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        public static CheckStatusEnum codeOf(int code) {
            for (CheckStatusEnum checkStatusEnum : values()) {
                if (checkStatusEnum.getCode() == code) {
                    return checkStatusEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }
    }

    public enum CourseStatusEnum {
        PUT_AWAY("正常", 0),
        SOLD_OUT("下架", 1);
        private String value;
        private int code;

        CourseStatusEnum(String value, int code) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum GoodsStatusEnum {
        PUT_AWAY("上架", 1),
        SOLD_OUT("下架", 0);
        private String value;
        private int code;

        GoodsStatusEnum(String value, int code) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum OrderStatusEnum {
        CANCELED(0, "已取消"),
        NO_PAY(1, "未支付"),
        PAID(2, "已付款"),
        SHIPPED(4, "已发货"),
        ORDER_SUCCESS(5, "订单完成");

        private String value;
        private int code;

        OrderStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        public static OrderStatusEnum codeOf(int code) {
            for (OrderStatusEnum orderStatusEnum : values()) {
                if (orderStatusEnum.getCode() == code) {
                    return orderStatusEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }

    }

}
