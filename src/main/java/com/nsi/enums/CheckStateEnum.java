package com.nsi.enums;

import lombok.Getter;

@Getter
public enum CheckStateEnum {

    IN_REVIEW(0, "审核中"),
    IN_APPROVE(1, "审核通过"),
    IN_UPAPPROVE(2, "审核不通过"),
    ;

    private Integer code;
    private String msg;


    CheckStateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


}
