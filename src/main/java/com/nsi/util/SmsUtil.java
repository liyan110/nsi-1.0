package com.nsi.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.nsi.exception.NsiOperationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Luo Zhen
 * @create 2019-05-23 9:37
 */
@Component
@Slf4j
public class SmsUtil {

    static final String product = "Dysmsapi";
    static final String domain = "dysmsapi.aliyuncs.com";
    public static final String ACCESS_KEY_ID = "LTAImYDS3LZdwdxJ";
    public static final String ACCESS_KEY_SECRET = "o9jiE62nevfu3716mae1w2XaS65mwY";

    public static final String SIGN_NAME = "新学说";


    /**
     * 发送短信
     *
     * @param mobile
     * @param template_code
     * @return
     * @throws ClientException
     */
    public SendSmsResponse sendSms(String mobile, String template_code) {
        return this.sendSms(mobile, template_code, null);
    }


    /**
     * 发送短信
     *
     * @param mobile
     * @param template_code
     * @param param
     * @return
     */
    public SendSmsResponse sendSms(String mobile, String template_code, String param) {
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_ID,
                ACCESS_KEY_SECRET);
        SendSmsResponse sendSmsResponse = null;
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            //组装请求对象
            SendSmsRequest request = new SendSmsRequest();
            //使用post提交
            request.setMethod(MethodType.POST);
            //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为国际区号+号码，如“85200000000”
            request.setPhoneNumbers(mobile);
            //必填:短信签名-可在短信控制台中找到
            request.setSignName(SIGN_NAME);
            //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
            request.setTemplateCode(template_code);
            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
            request.setTemplateParam(param);
            //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
            //request.setSmsUpExtendCode("90997");
            //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            request.setOutId("yourOutId");
            IAcsClient acsClient = new DefaultAcsClient(profile);

            //请求失败这里会抛ClientException异常
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (Exception e) {
            log.error("============[阿里云短信发送失败]==========");
            throw new NsiOperationException(e.getMessage());
        }
        return sendSmsResponse;
    }
}
