package com.nsi.util;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luo Zhen
 * Redis 工具类
 */
@Slf4j
public class RedisUtils {

    /**
     * 存储数据
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean addRedis(String key, String value) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        //查看服务是否运行
        jedis.set(key, value);
        if (jedis.exists(key)) {
            jedis.close();
            return true;
        }
        jedis.close();
        return false;
    }

    /**
     * 根据指定时间存储热数据
     *
     * @param key   键
     * @param time  毫秒
     * @param value 值
     * @return
     */
    public static boolean expireSet(String key, long time, String value) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        //查看服务是否运行
        jedis.psetex(key, time, value);
        if (jedis.exists(key)) {
            jedis.close();
            return true;
        }
        jedis.close();
        return false;
    }

    /**
     * 根据指定时间存储Map集合
     *
     * @param key
     * @return
     */
    public static boolean expireStr(String key, String value) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        //查看服务是否运行
        jedis.set(key, value);
        jedis.expire(key, 7200);
        if (jedis.exists(key)) {
            jedis.close();
            return true;
        }
        jedis.close();
        return false;
    }

    /**
     * 修改
     *
     * @param key
     * @param value
     */
    public static void alterRedis(String key, String value) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        //查看服务是否运行
        jedis.set(key, value);
        jedis.close();
    }

    /**
     * 保持有效期 修改
     *
     * @param key
     * @param value
     */
    public static void alterTimeRedis(String key, String value) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        long time = jedis.ttl(key) * 1000;
        System.out.println("time:" + time);
        jedis.psetex(key, time, value);
        jedis.close();
    }

    /**
     * 查询
     *
     * @param key
     * @return
     */
    public static String searchRedis(String key) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123"); // 设置密码
        //查看服务是否运行
//        System.out.println("服务正在运行: " + jedis.ping());
        if (jedis.exists(key)) {
            String str = jedis.get(key);
            jedis.close();
            return str;
        }
        jedis.close();
        return null;
    }

    /**
     * 根据键自增1
     *
     * @return
     */
    public static int incrment(String key) {
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123");
        long count = 0L;
        if (jedis.get(key) == null) {
            jedis.set(key, "0");
        } else {
            count = jedis.incr(key).longValue();
        }
        jedis.close();
        return (int) count;
    }

    /**
     * 根据键自增1
     *
     * @param key
     * @param time
     * @return
     */
    public static int incrment(String key, int time) {
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123");
        long count = 0L;
        if (jedis.get(key) == null) {
            jedis.set(key, "0");
            jedis.expire(key, time);
        } else {
            count = jedis.incr(key).longValue();
        }
        jedis.close();
        return (int) count;
    }

    /**
     * 根据键 删除
     *
     * @param key
     * @return
     */
    public static boolean delete(String key) {
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("XinxueshuoRedis123");
        jedis.del(key);
        jedis.close();
        return true;
    }

    /**
     * redis 参数类型转换
     *
     * @param params
     * @param paramsType
     * @return
     */
    public static <T> List constructParameter(String params, Class<?> paramsType) {
        ObjectMapper mapper = new ObjectMapper();
        JavaType javaType = mapper.getTypeFactory().constructParametricType(ArrayList.class, paramsType);
        List<T> lists = null;
        try {
            lists = mapper.readValue(params, javaType);
        } catch (IOException e) {
            log.error("[Redis] 转换失败：{}", e.getMessage());
        }
        return lists;
    }


}
