package com.nsi.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 文件工具
 *
 * @author Luo Zhen
 * @create 2019-10-23 16:52
 */
@Slf4j
public class FileUtil {

    public static String writeHtml(int id, String articleHTML, String path) {
        //文件名
        String fileName = id + ".html";
        //如果目录不存在则创建
        File uploadDir = new File(path);
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
        try (
                FileOutputStream out = new FileOutputStream(new File(path + fileName));
        ) {
            out.write(articleHTML.getBytes("UTF-8"));
            out.flush();
        } catch (FileNotFoundException e) {
            log.error("【文件上传错误】msg:{}", e.getMessage());
        } catch (IOException e) {
            log.error("【文件上传错误】msg:{}", e.getMessage());
        }
        return fileName;
    }
}
