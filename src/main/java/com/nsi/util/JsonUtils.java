package com.nsi.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Luo Zhen
 * @create 2020-01-09 17:19
 */
public class JsonUtils {

    /**
     * 获取json对象
     *
     * @param responseJson
     * @return
     */
    public static JsonObject getJsonObject(String responseJson) {
        JsonParser jp = new JsonParser();
        JsonObject jsonObject = jp.parse(responseJson).getAsJsonObject();
        return jsonObject;
    }

    public static String toJson(Object object) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        return gson.toJson(object);
    }
}
