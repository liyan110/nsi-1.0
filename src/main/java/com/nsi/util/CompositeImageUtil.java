package com.nsi.util;

import com.nsi.exception.NsiOperationException;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Coordinate;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 图片合成工具类
 */
@Slf4j
public class CompositeImageUtil {

    private static Graphics2D g;
    private static Font font = new Font("微软雅黑", Font.BOLD, 45); // 添加字体的属性设置


    // 合成图片的x,y坐标
    private static int x_width_picture = 230;
    private static int y_height_picture = 490;

    // 合成文字的y坐标
    private static int y_height_text = 290;


    /**
     * 图片合成功能
     *
     * @param bigImgUrl 大图地址
     * @param qrImgUrl  二维码地址
     * @return
     */
    public static BufferedImage compositeImage(String bigImgUrl, String qrImgUrl) {
        URL bigImage = null;
        URL qrImage = null;
        BufferedImage bufferedImage = null;
        try {
            bigImage = new URL(bigImgUrl);
            qrImage = new URL(qrImgUrl);
            bufferedImage = Thumbnails.of(bigImage)
                    .watermark(new Coordinate(x_width_picture, y_height_picture), ImageIO.read(qrImage), 1f)
                    .scale(1f)
                    .asBufferedImage();
        } catch (MalformedURLException e) {
            log.error("【图片合成】,{}", e.getMessage());
            throw new NsiOperationException("大图或二维码地址无效");
        } catch (IOException e) {
            log.error("【图片合成】,{}", e.getMessage());
            throw new NsiOperationException("二维码图片无法获取");
        }
        return bufferedImage;
    }

    /**
     * 文字合成功能
     *
     * @param bufferedImage
     * @param text
     * @return
     */
    public static BufferedImage compositeText(BufferedImage bufferedImage, String text) {
        int xCoordinate = getXCoordinate(text);
        BufferedImage bufferedImgText = modifyImage(bufferedImage, text, xCoordinate, y_height_text);
        return bufferedImgText;
    }

    /**
     * 写出图片
     *
     * @param fileName 文件地址
     * @param img      文件流
     */
    public static void writeImage(String fileName, BufferedImage img) {
        if (fileName != null && img != null) {
            try {
                File outputfile = new File(fileName);
                ImageIO.write(img, "jpg", outputfile);
            } catch (IOException e) {
                log.info("[图片合成-写出图片异常]", e.getMessage());
            }
        }
    }

    /**
     * 设置画笔属性和文字坐标
     *
     * @param img
     * @param content
     * @param x
     * @param y
     * @return
     */
    private static BufferedImage modifyImage(BufferedImage img, Object content, int x, int y) {
        try {
            int w = img.getWidth();
            int h = img.getHeight();
            g = img.createGraphics();
            g.setBackground(Color.WHITE);
            g.setColor(new Color(166, 120, 62));//设置字体颜色
            if (font != null) {
                g.setFont(font);
            }
            if (content != null) {
                g.translate(w / 2, h / 2);
                g.drawString(content.toString(), x, y);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (g != null) {
                g.dispose();
            }
        }
        return img;
    }


    /**
     * 根据名字计算x坐标
     *
     * @param username
     * @return
     */
    private static int getXCoordinate(String username) {
        int x = 0;
        if (CompositeImageUtil.isChinese(username)) {
            x = -(50 * username.length() / 2);
        } else {
            x = -(23 * username.length() / 2);
        }
        return x;
    }


    public static boolean isChinese(String str) {
        if (str == null) {
            return false;
        }
        for (char c : str.toCharArray()) {
            // 如果是中文
            if (CompositeImageUtil.isChinese(c)) {
                return true;
            }
        }
        return false;
    }


    public static boolean isChinese(char c) {
        return c >= 0x4E00 && c <= 0x9FA5;
    }

}
