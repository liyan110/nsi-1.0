package com.nsi.util;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;
import java.util.Properties;


public class MailUtil {

    private static final String SMTP_HOST = "smtpdm.aliyun.com";
    private static final String SOCKET_PORT = "465";
    private static final String SMTP_MAIL_USER = "service@mail.html9.top";
    private static final String SMTP_MAIL_PASSWORD = "Xinxueshuo123Mail";
    private static final String SMTP_SOCKET_CLASS = "javax.net.ssl.SSLSocketFactory";
    private static final String SMTP_AUTH = "true";

    /**
     * 封装邮箱参数
     *
     * @return
     */
    private static Properties getProperties() {
        final Properties props = new Properties();
        // 表示SMTP发送邮件，需要进行身份验证
        props.put("mail.smtp.auth", SMTP_AUTH);
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.socketFactory.class", SMTP_SOCKET_CLASS);
        props.put("mail.smtp.socketFactory.port", SOCKET_PORT);
        props.put("mail.smtp.port", SOCKET_PORT);
        // 发件人的账号
        props.put("mail.user", SMTP_MAIL_USER);
        // 访问SMTP服务时需要提供的密码
        props.put("mail.password", SMTP_MAIL_PASSWORD);
        return props;
    }

    /**
     * 构建授权信息
     *
     * @return
     */
    private static Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SMTP_MAIL_USER, SMTP_MAIL_PASSWORD);
            }
        };
    }


    /**
     * 新学说联合-用户注册
     * 用户注册方法-新方法
     *
     * @param toWho 发送方
     * @param code  随机码
     */
    public static void sendMailRegisterCode(String toWho, String code) throws Exception, MessagingException {

        final Properties props = getProperties();
        Authenticator authenticator = getAuthenticator();

        // 使用环境属性和授权信息，创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        //创建邮件对象
        Message message = new MimeMessage(mailSession);
        //发件人
        message.setFrom(new InternetAddress("service@mail.html9.top"));
        //收件人
        message.setRecipient(RecipientType.TO, new InternetAddress(toWho));
        //主题
        message.setSubject("新学说的激活邮件");
        message.setContent("<blockquote class=\"box\" style=\"height:1000px;width:670px;border:1px solid #d9d9d9;margin:0 auto;border-top: 4px solid #215089;\">\n\t\t<div class=\"top-bar\" style=\"width:600px;height:30px;line-height: 30px;background-color: #215089;color:white;text-align: center;margin:0 auto;margin-top: 30px;\">\u65B0\u5B66\u8BF4\u7528\u6237\u8D26\u53F7\u7684\u6FC0\u6D3B</div>\n\t\t<p style=\"margin-left:35px;font-size:14px;\">" + toWho + "\u5C0A\u656C\u7684\u7528\u6237\u60A8\u597D\uFF1A</p>\n\t\t<p style=\"margin-left:35px;font-size:14px;\">\u6B22\u8FCE\u60A8\u6CE8\u518C\u65B0\u5B66\u8BF4\u5E73\u53F0\u3002</p>\n\t\t<p class=\"number\" style=\"font-size: 20px;font-weight: bold;text-align: center;\">\u60A8\u7684\u9A8C\u8BC1\u7801:" + code + "</p>\n\t\t<p class=\"explain\" style=\"color:#000;font-size:14px;font-weight: bold;margin-left:35px;\">\u91CD\u8981\u8BF4\u660E\uFF1A</p>\n\t\t<p class=\"explain-content\" style=\"margin-left:35px;font-size:14px;\">\u60A8\u7684\u8D26\u53F7\u53EF\u4EE5\u5728\u65B0\u5B66\u8BF4\u56DB\u5E93\u5168\u4E66\u548C\u65B0\u5B66\u8BF4\u56FD\u9645\u6559\u80B2\u5B66\u9662\u5171\u540C\u4F7F\u7528\uFF0C\u671B\u8BF7\u77E5\u6653\uFF01</p>\n\t\t<div class=\"about-box\" style=\"width:600px;height:170px;background-color: #f9f9f9;margin:0 auto;color:#8a6d3b;margin-top: 30px;\">\n\t\t\t<p class=\"about\" style=\"font-weight: bold;font-size:14px;padding-top:8px;text-indent:0;\">\u5173\u4E8E\u65B0\u5B66\u8BF4</p>\n\t\t\t<p style=\"color:#999;margin-right:33px;text-indent: 25px;font-size: 12px;line-height: 18px;\">\u65B0\u5B66\u8BF4(NewSchool Insight)\u662F\u7531\u56FD\u9645\u5B66\u6821\u8D44\u6DF1\u4E13\u5BB6\u5171\u540C\u6253\u9020\u7684\u56FD\u9645\u5B66\u6821\u670D\u52A1\u5E73\u53F0\u3002 \u65D7\u4E0B\u6709\u65B0\u5B66\u8BF4\u4F20\u5A92\u548C\u65B0\u5B66\u8BF4\u56FD\u9645\u6559\u80B2\u7814\u7A76\u9662\u4E24\u5927\u54C1\u724C\u3002\u4E3A\u56FD\u9645\u5B66\u6821\u63D0\u4F9B\u884C\u4E1A\u6700\u65B0\u8D44\u8BAF\uFF0C\u4F1A\u8BAE\u7EC4\u7EC7\uFF0C \u6D77\u5916\u8003\u5BDF\uFF1B\u7814\u7A76\u62A5\u544A\uFF0C\u884C\u4E1A\u6570\u636E\u5E93\uFF1B\u5B66\u6821\u6295\u5EFA\uFF0C\u8FD0\u8425\u7BA1\u7406\uFF0C\u54C1\u724C\u7BA1\u7406\uFF0C\u4EBA\u624D\u57F9\u517B\u7B49\u4E13\u4E1A\u54A8\u8BE2\u670D\u52A1\u3002 </p>\n\t\t\t<p style=\"color:#999;margin-right:33px;text-indent: 25px;font-size: 12px;line-height: 18px;\">\u81EA\u6210\u7ACB\u4EE5\u6765\u4E00\u76F4\u81F4\u529B\u4E8E\u4E2D\u56FD\u56FD\u9645\u5B66\u6821\u884C\u4E1A\u7814\u7A76\uFF0C\u5BF9\u4E8E\u4E2D\u56FD\u56FD\u9645\u5B66\u6821\u6709\u7740\u6DF1\u523B\u7684\u4E86\u89E3\u3002\u5230\u76EE\u524D\u4E3A\u6B62\uFF0C\u65B0\u5B66\u8BF4\u5DF2\u5728\u81EA\u6709\u5A92\u4F53\u4E0A\u53D1\u8868\u4E86\u8FD1\u5343\u7BC7\u884C\u4E1A\u539F\u521B\u6587\u7AE0\uFF0C\u5341\u591A\u672C\u6DF1\u5EA6\u884C\u4E1A\u62A5\u544A\uFF0C\u5E76\u4E3A\u591A\u5BB6\u5B66\u6821\u63D0\u4F9B\u4E86\u8BE6\u7EC6\u7684\u5E02\u573A\u3001\u8FD0\u8425\u3001\u5EFA\u8BBE\u65B9\u6848\uFF0C\u53D7\u5230\u4E1A\u5185\u4EBA\u58EB\u548C\u5355\u4F4D\u7684\u9AD8\u5EA6\u8BA4\u53EF\u3002</p>\n\t\t</div>\n\t\t<div class=\"line\" style=\"border: 1px dashed #ccc;width: 600px;margin:20px auto;\"></div>\n\t\t<div class=\"help-box\" style=\"width:600px;height:200px;background-color: #f9f9f9;margin:0 auto;color:#8a6d3b;\">\n\t\t\t<ul class=\"clearfix\" style=\"display:block;clear:both;height:0;content:'';margin-top: 15px;margin-left:35px;\">\n\t\t\t\t<li class=\"title\" style=\"font-weight: bold;padding-top: 10px;list-style: none;\">\u65B0\u5B66\u8BF4\u53EF\u4EE5\u5E2E\u60A8\u505A\u4EC0\u4E48\uFF1F</li>\n\t\t\t\t<li style=\"line-height: 30px;font-size: 14px;list-style: none;\">\u6559\u80B2\u5708\u7684\u54A8\u8BE2\u52A8\u6001</li>\n\t\t\t\t<li style=\"line-height: 30px;font-size: 14px;list-style: none;\">\u4E13\u4E1A\u7684\u7814\u7A76\u6210\u679C</li>\n\t\t\t\t<li style=\"line-height: 30px;font-size: 14px;list-style: none;\">\u56FD\u9645\u5B66\u6821\u7684\u54A8\u8BE2\u670D\u52A1</li>\n\t\t\t\t<li style=\"line-height: 30px;font-size: 14px;list-style: none;\">\u5B66\u6821\u6570\u636E\u5E93\u6700\u8FD1\u66F4\u65B0</li>\n\t\t\t\t<li style=\"line-height: 30px;font-size: 14px;list-style: none;\">\u6570\u636E\u53EF\u89C6\u5316\u5206\u5E03\u56FE</li>\n\t\t\t</ul>\n\t\t</div>\n\t\t<div class=\"line\" style=\"border: 1px dashed #ccc;width: 600px;margin:20px auto;\"></div>\n\t\t<div class=\"bottom-box\" style=\"width:600px;height:220px;background-color: #f9f9f9;margin:0 auto;color:#8a6d3b;\">\n\t\t\t<p class=\"top\" style=\"font-size: 12px;color:#999;line-height: 12px;text-align: center;padding-top: 10px;\">\u672C\u90AE\u4EF6\u7531\u7CFB\u7EDF\u81EA\u52A8\u53D1\u9001\uFF0C\u8BF7\u52FF\u76F4\u63A5\u56DE\u590D\uFF01\u5982\u6709\u4EFB\u4F55\u7591\u95EE\uFF0C\u8BF7\u8054\u7CFB\u6211\u4EEC\u7684\u5BA2\u670D\u4EBA\u5458\u3002</p>\n\t\t\t<p class=\"phone\" style=\"font-size: 12px;color:#999;line-height: 12px;text-align: center;\">\n\t\t\t\t<span style=\"font-size: 12px;color:#999;margin-right: 30px;\">\u8054\u7CFB\u7535\u8BDD\uFF1A 010-52468286 </span> \n\t\t\t\t<span style=\"font-size: 12px;color:#999;margin-right: 30px;\">\u624B\u673A\uFF1A15010927730</span>\n\t\t\t</p>\n\t\t\t<p class=\"wechat-number\" style=\"font-size: 12px;color:#999;line-height: 12px;text-align: center;font-weight: bold;font-size:14px;line-height: 5px;\">\u5FAE\u4FE1\u516C\u4F17\u53F7</p>\n\t\t\t<p style=\"font-size: 12px;color:#999;line-height: 12px;text-align: center;\">\n\t\t\t\t<img alt=\"\u5FAE\u4FE1\u4E8C\u7EF4\u7801\" src=\"http://data.xinxueshuo.cn/nsi/assets/img/wechat_QR.png\" style=\"width:100px;height:100px;\">\n\t\t\t</p>\n\t\t\t<p class=\"follow\" style=\"font-size: 12px;color:#999;line-height: 12px;text-align: center;font-size:14px;line-height: 5px;\">\u5FAE\u4FE1\u5173\u6CE8\u6211\u4EEC</p>\n\t\t</div>\n\t</blockquote>", "text/html;charset=UTF-8");
        //发送激活邮件
        Transport.send(message);
    }

    /**
     * 通用 通知邮件发送 ：通知用户 或 管理员
     *
     * @param toWho
     * @param Template
     * @throws Exception
     * @throws MessagingException
     */
    public static void SendNotifyMail(String toWho, String Template) throws Exception, MessagingException {
        final Properties props = getProperties();
        //构建授权信息
        Authenticator authenticator = getAuthenticator();
        //创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        //创建邮件对象
        Message message = new MimeMessage(mailSession);
        //发件人
        message.setFrom(new InternetAddress("service@mail.html9.top"));
        //收件人
        message.setRecipient(RecipientType.TO, new InternetAddress(toWho));
        //主题
        message.setSubject("新学说通知邮件：" + Template);
        //正文
        message.setContent("<blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\">\n<div style=\"font-size:14px; color:#666;border:1px solid #d9d9d9; border-top:4px solid #20a56e; height:auto; padding:12px 35px 28px ;width:600px;margin:40px auto;font-family:microsoft yahei;word-wrap:break-word\">\n<p style=\"line-height:20px;\">\n<span style=\"color:#000;font-size:1.2em;font-weight:bold;\">" + toWho + "</span>\uff0c\u6b22\u8fce\u60a8\u4f7f\u7528\u65b0\u5b66\u8bf4\u6570\u636e\u7cfb\u7edf\u3002\n</p>\n<p style=\"line-height:20px;\">\u4ee5\u4e0b\u662f\u901a\u77e5\u6d88\u606f</p>\n"
                + "<p style=\"line-height:20px;\"> <h3><br/> <a href=\"http://data.xinxueshuo.cn\">" + Template + "</a></h3></p>"
                + "\n<dl style=\"background:#f9f9f9; border:1px solid #dcdcdc; padding:12px; color:#8a6d3b; margin:25px 0;\">\n<dt style=\"line-height:30px; font-weight:bold; margin-left:12px\">\u65b0\u5b66\u8bf4\u53ef\u4ee5\u5e2e"
                + "\u60a8\u505a\u4ec0\u4e48\uff1f</dt>\n<dd style=\"line-height:30px; margin-left:12px \">\u6559\u80b2\u5708\u7684\u54a8\u8be2\u52a8\u6001</dd>\n<dd style=\"line-height:30px; margin-left:12px \">\u4e13\u4e1a\u7684\u7814"
                + "\u7a76\u6210\u679c</dd>\n<dd style=\"line-height:30px;  margin-left:12px\">\u56fd\u9645\u5b66\u6821\u54a8\u8be2\u670d\u52a1</dd></dl>\n<p style=\"color:#999; font-size:12px; line-height:12px; padding-top:24px;"
                + "border-top:1px solid #dcdcdc;\">\u672c\u90ae\u4ef6\u7531\u7cfb\u7edf\u81ea\u52a8\u53d1\u9001\uff0c\u8bf7\u52ff\u76f4\u63a5\u56de\u590d\uff01\u5982\u6709\u4efb\u4f55\u7591\u95ee\uff0c\u8bf7\u8054\u7cfb\u6211\u4eec\u7684\u5ba2"
                + "\u670d\u4eba\u5458\u3002</p>\n<p style=\"color:#999; font-size:12px; line-height:12px;\">\u65b0\u5b66\u8bf4 \u8054\u7cfb\u7535\u8bdd\uff1a<span style=\"border-bottom:1px dashed #ccc;z-index:1\" t=\"7\" onclick=\"return false;\" >010-52468286</span>"
                + "\n</p></div>\n</blockquote>", "text/html;charset=UTF-8");

        //		3、发送邮件
        Transport.send(message);
    }

    /**
     * 忘记密码邮件验证
     */
    public static void ForgetPWsendMail(String toWho) throws Exception, javax.mail.MessagingException {

        final Properties props = getProperties();
        Authenticator authenticator = getAuthenticator();
        //创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        //创建邮件对象
        Message message = new MimeMessage(mailSession);
        //发件人
        message.setFrom(new InternetAddress("service@mail.html9.top"));
        //收件人
        message.setRecipient(RecipientType.TO, new InternetAddress(toWho));
        //主题
        message.setSubject("新学说的验证邮件");
        //基于时间和邮件地址字符长度的加密算法
        Calendar time = Calendar.getInstance();
        System.out.println("ForgetPWsendMail:cc加密算法：hour:" + time.get(Calendar.HOUR_OF_DAY));
        int time_hour = time.get(Calendar.HOUR_OF_DAY);
        int cc = toWho.length() * time_hour + 987654;
        //正文
//		message.setContent("<h3>欢迎注册新学说数据系统</h3> <h3>请点击下面的链接 激活账号 <br /><a href=\"http://47.92.84.36:8080/nsi-0.8/register?registerCode="+code+"&UserMail="+toWho+"\">http://47.92.84.36:8080/nsi-0.8/register?registerCode="+code+"&UserMail="+toWho+"</a></h3>", "text/html;charset=UTF-8");
        message.setContent("<blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\">\n<div style=\"font-size:14px; color:#666;border:1px solid #d9d9d9; border-top:4px solid #20a56e; height:auto; padding:12px 35px 28px ;width:600px;margin:40px auto;font-family:microsoft yahei;word-wrap:break-word\">\n<p style=\"line-height:20px;\">\n<span style=\"color:#000;font-size:1.2em;font-weight:bold;\">" + toWho + "</span>\uFF0C\u6B22\u8FCE\u60A8\u6CE8\u518C\u65B0\u5B66\u8BF4\u6570\u636E\u7CFB\u7EDF\u3002\n</p>\n<p style=\"line-height:20px;\">\u8BF7\u8BBF\u95EE\u4EE5\u4E0B\u94FE\u63A5\u5B8C\u6210\u60A8\u7684\u90AE\u7BB1\u6FC0\u6D3B\u9A8C\u8BC1\u3002</p>\n<p style=\"line-height:20px;\">"
                + "<h3>验证码：" + cc + "</h3>"
                + "</p>\n<p style=\"line-height:20px;\">\u5982\u679C\u65E0\u6CD5\u6253\u5F00\u94FE\u63A5\uFF0C\u8BF7\u590D\u5236\u4E0A\u9762\u7684\u94FE\u63A5\u7C98\u8D34\u5230\u6D4F\u89C8\u5668\u7684\u5730\u5740\u680F\u3002 </p>\n<dl style=\"background:#f9f9f9; border:1px solid #dcdcdc; padding:12px; color:#8a6d3b; margin:25px 0;\">\n<dt style=\"line-height:30px; font-weight:bold; margin-left:12px\">\u65B0\u5B66\u8BF4\u53EF\u4EE5\u5E2E\u60A8\u505A\u4EC0\u4E48\uFF1F</dt>\n<dd style=\"line-height:30px; margin-left:12px \">\u6559\u80B2\u5708\u7684\u54A8\u8BE2\u52A8\u6001</dd>\n<dd style=\"line-height:30px; margin-left:12px \">\u4E13\u4E1A\u7684\u7814\u7A76\u6210\u679C</dd>\n<dd style=\"line-height:30px;  margin-left:12px\">\u56FD\u9645\u5B66\u6821\u54A8\u8BE2\u670D\u52A1</dd></dl>\n<p style=\"color:#999; font-size:12px; line-height:12px; padding-top:24px; border-top:1px solid #dcdcdc;\">\u672C\u90AE\u4EF6\u7531\u7CFB\u7EDF\u81EA\u52A8\u53D1\u9001\uFF0C\u8BF7\u52FF\u76F4\u63A5\u56DE\u590D\uFF01\u5982\u6709\u4EFB\u4F55\u7591\u95EE\uFF0C\u8BF7\u8054\u7CFB\u6211\u4EEC\u7684\u5BA2\u670D\u4EBA\u5458\u3002</p>\n<p style=\"color:#999; font-size:12px; line-height:12px;\">\u8054\u7CFB\u7535\u8BDD\uFF1A<span style=\"border-bottom:1px dashed #ccc;z-index:1\" t=\"7\" onclick=\"return false;\" >010-52468286</span>\n</p></div>\n</blockquote>", "text/html;charset=UTF-8");

        //		3、发送激活邮件
        Transport.send(message);

    }

}
