package com.nsi.util;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

@Slf4j
public class ImageUtil {

    private Font font = new Font("微软雅黑", Font.BOLD, 55); // 添加字体的属性设置

    private Graphics2D g = null;

    /**
     * 导入本地图片到缓冲区
     */
    public BufferedImage loadImageLocal(String imgName) {
        try {
            return ImageIO.read(new File(imgName));
        } catch (IOException e) {
            log.info("[图片合成-写入图片异常]", e.getMessage());
        }
        return null;
    }

    /**
     * 生成新图片到本地
     */
    public void writeImageLocal(String newImage, BufferedImage img) {
        if (newImage != null && img != null) {
            try {
                File outputfile = new File(newImage);
                ImageIO.write(img, "jpg", outputfile);
            } catch (IOException e) {
                log.info("[图片合成-写出图片异常]", e.getMessage());
            }
        }
    }

    /**
     * 合并另个图片
     *
     * @param b
     * @param d
     * @return
     */
    public BufferedImage modifyImagetogeter(BufferedImage b, BufferedImage d, int x, int y) {
        try {
            g = d.createGraphics();
            g.drawImage(b, x, y, 250, 250, null);
            g.dispose();
        } catch (Exception e) {
            log.info("[图片合成-合成图片异常]", e.getMessage());
        }
        return d;
    }


    /**
     * 修改图片,返回修改后的图片缓冲区（只输出一行文本）
     */
    public BufferedImage modifyImage(BufferedImage img, Object content, int x, int y) {

        try {
            int w = img.getWidth();
            int h = img.getHeight();
            g = img.createGraphics();
            g.setBackground(Color.WHITE);
            g.setColor(new Color(80, 80, 80));//设置字体颜色
            if (font != null)
                g.setFont(font);
            if (content != null) {
                g.translate(w / 2, h / 2);
                g.drawString(content.toString(), x, y);
            }
        } catch (Exception e) {
            log.info("[图片合成-合成图片异常]", e.getMessage());
        } finally {
            if (g != null) {
                g.dispose();
            }
        }
        return img;
    }

    /**
     * 下载图片工具类
     *
     * @param urlString 图片链接
     * @param filename  图片名字
     * @param savePath  保存路径·
     * @throws Exception
     */
    public static void download(String urlString, String filename, String savePath) throws Exception {
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        //设置请求超时为5s
        con.setConnectTimeout(5 * 1000);
        // 输入流
        InputStream is = con.getInputStream();
        // 1K的数据缓冲
        byte[] bs = new byte[1024 * 10];
        // 读取到的数据长度
        int len;
        // 输出的文件流
        File sf = new File(savePath);
        if (!sf.exists()) {
            sf.mkdirs();
        }
        OutputStream os = new FileOutputStream(sf.getPath() + "\\" + filename);
        // 开始读取
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        // 完毕，关闭所有链接
        os.close();
        is.close();
    }

}
