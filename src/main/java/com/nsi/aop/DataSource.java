package com.nsi.aop;

import java.lang.annotation.*;

/**
 * 数据源注解
 *
 * @author Luo Zhen
 * @create 2019-02-18 17:37
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface DataSource {

    String value() default "";
}
