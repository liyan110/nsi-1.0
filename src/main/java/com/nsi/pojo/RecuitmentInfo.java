package com.nsi.pojo;

public class RecuitmentInfo {
    private Integer id;

    private String recuitmentId;

    private String recuitmentName;

    private String type;

    private String position;

    private String seniority;

    private String headcount;

    private String jobDescription;

    private String jobRequirement;

    private String jobWeal;

    private String salary01;

    private String salary02;

    private String areas;

    private String areas02;

    private String areas03;

    private String loadPeople;

    private String loadTime;

    private String batchinputSign;

    private String verifysign;

    public RecuitmentInfo(Integer id, String recuitmentId, String recuitmentName, String type, String position, String seniority, String headcount, String jobDescription, String jobRequirement, String jobWeal, String salary01, String salary02, String areas, String areas02, String areas03, String loadPeople, String loadTime, String batchinputSign, String verifysign) {
        this.id = id;
        this.recuitmentId = recuitmentId;
        this.recuitmentName = recuitmentName;
        this.type = type;
        this.position = position;
        this.seniority = seniority;
        this.headcount = headcount;
        this.jobDescription = jobDescription;
        this.jobRequirement = jobRequirement;
        this.jobWeal = jobWeal;
        this.salary01 = salary01;
        this.salary02 = salary02;
        this.areas = areas;
        this.areas02 = areas02;
        this.areas03 = areas03;
        this.loadPeople = loadPeople;
        this.loadTime = loadTime;
        this.batchinputSign = batchinputSign;
        this.verifysign = verifysign;
    }

    public RecuitmentInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRecuitmentId() {
        return recuitmentId;
    }

    public void setRecuitmentId(String recuitmentId) {
        this.recuitmentId = recuitmentId == null ? null : recuitmentId.trim();
    }

    public String getRecuitmentName() {
        return recuitmentName;
    }

    public void setRecuitmentName(String recuitmentName) {
        this.recuitmentName = recuitmentName == null ? null : recuitmentName.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getSeniority() {
        return seniority;
    }

    public void setSeniority(String seniority) {
        this.seniority = seniority == null ? null : seniority.trim();
    }

    public String getHeadcount() {
        return headcount;
    }

    public void setHeadcount(String headcount) {
        this.headcount = headcount == null ? null : headcount.trim();
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription == null ? null : jobDescription.trim();
    }

    public String getJobRequirement() {
        return jobRequirement;
    }

    public void setJobRequirement(String jobRequirement) {
        this.jobRequirement = jobRequirement == null ? null : jobRequirement.trim();
    }

    public String getJobWeal() {
        return jobWeal;
    }

    public void setJobWeal(String jobWeal) {
        this.jobWeal = jobWeal == null ? null : jobWeal.trim();
    }

    public String getSalary01() {
        return salary01;
    }

    public void setSalary01(String salary01) {
        this.salary01 = salary01 == null ? null : salary01.trim();
    }

    public String getSalary02() {
        return salary02;
    }

    public void setSalary02(String salary02) {
        this.salary02 = salary02 == null ? null : salary02.trim();
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas == null ? null : areas.trim();
    }

    public String getAreas02() {
        return areas02;
    }

    public void setAreas02(String areas02) {
        this.areas02 = areas02 == null ? null : areas02.trim();
    }

    public String getAreas03() {
        return areas03;
    }

    public void setAreas03(String areas03) {
        this.areas03 = areas03 == null ? null : areas03.trim();
    }

    public String getLoadPeople() {
        return loadPeople;
    }

    public void setLoadPeople(String loadPeople) {
        this.loadPeople = loadPeople == null ? null : loadPeople.trim();
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime == null ? null : loadTime.trim();
    }

    public String getBatchinputSign() {
        return batchinputSign;
    }

    public void setBatchinputSign(String batchinputSign) {
        this.batchinputSign = batchinputSign == null ? null : batchinputSign.trim();
    }

    public String getVerifysign() {
        return verifysign;
    }

    public void setVerifysign(String verifysign) {
        this.verifysign = verifysign == null ? null : verifysign.trim();
    }
}