package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 优惠用户信息表
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-21
 */
@TableName("nsi_discounts")
@Data
public class Discounts {

    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户姓名
     */
    private String username;

    /**
     * 手机号
     */
    private String telphone;

    /**
     * 公司名称
     */
    private String company;

    /**
     * 优惠类型
     */
    private String type;

    /**
     * 状态 0-未优惠 1-已优惠
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
