package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@Data
@TableName("nsi_title_form")
public class TitleForm implements Serializable {


    private static final long serialVersionUID = 377547090526908762L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动id
     */
    @TableField("active_id")
    private Integer activeId;

    /**
     * 姓名标题项
     */
    private String name;

    /**
     * 职位标题项
     */
    private String position;

    /**
     * 公司标题项
     */
    private String company;

    /**
     * 电话标题项
     */
    private String telphone;

    /**
     * 邮箱标题项
     */
    @TableField("user_mail")
    private String userMail;

    /**
     * 备份选项-1
     */
    @TableField("option_1")
    private String option1;

    @TableField("value_1")
    private String value1;

    @TableField("type_1")
    private String type1;

    /**
     * 备份选项-2
     */
    @TableField("option_2")
    private String option2;

    @TableField("value_2")
    private String value2;

    @TableField("type_2")
    private String type2;

    /**
     * 备份选项-3
     */
    @TableField("option_3")
    private String option3;

    @TableField("value_3")
    private String value3;

    @TableField("type_3")
    private String type3;

    /**
     * 备份选项-4
     */
    @TableField("option_4")
    private String option4;

    @TableField("value_4")
    private String value4;

    @TableField("type_4")
    private String type4;

    /**
     * 备份选项-5
     */
    @TableField("option_5")
    private String option5;

    @TableField("value_5")
    private String value5;

    @TableField("type_5")
    private String type5;

    /**
     * 备份选项-6
     */
    @TableField("option_6")
    private String option6;

    @TableField("value_6")
    private String value6;

    @TableField("type_6")
    private String type6;

    /**
     * 备份选项-7
     */
    @TableField("option_7")
    private String option7;

    @TableField("value_7")
    private String value7;

    @TableField("type_7")
    private String type7;

    /**
     * 备份选项-8
     */
    @TableField("option_8")
    private String option8;

    @TableField("value_8")
    private String value8;

    @TableField("type_8")
    private String type8;

    /**
     * 备份选项-9
     */
    @TableField("option_9")
    private String option9;

    @TableField("value_9")
    private String value9;

    @TableField("type_9")
    private String type9;

    /**
     * 备份选项-10
     */
    @TableField("option_10")
    private String option10;

    @TableField("value_10")
    private String value10;

    @TableField("type_10")
    private String type10;

    @TableField("create_time")
    private Date createTime;

    /**
     * 截至日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField("expriy_time")
    private Date expriyTime;

}
