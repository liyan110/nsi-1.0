package com.nsi.pojo;

import java.util.Date;

public class Comments {

    // 逻辑id
    private Integer id;
    // 评论人姓名
    private String commentName;
    // 评论类型
    private String commentType;
    // 评论内容
    private String commentText;
    // 不用传
    private Date createTime;
    // 评论对象id
    private Integer guestId;

    public Comments(Integer id, String commentName, String commentType, String commentText, Date createTime, Integer guestId) {
        this.id = id;
        this.commentName = commentName;
        this.commentType = commentType;
        this.commentText = commentText;
        this.createTime = createTime;
        this.guestId = guestId;
    }

    public Comments() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommentName() {
        return commentName;
    }

    public void setCommentName(String commentName) {
        this.commentName = commentName == null ? null : commentName.trim();
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType == null ? null : commentType.trim();
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText == null ? null : commentText.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGuestId() {
        return guestId;
    }

    public void setGuestId(Integer guestId) {
        this.guestId = guestId;
    }
}