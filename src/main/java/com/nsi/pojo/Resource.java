package com.nsi.pojo;

import java.util.Date;

/**
 * @author Luo Zhen
 * @create 2018-08-03
 */
public class Resource {
    private Integer id;

    private String type;

    private String imageUrl;

    private String fileName;

    private String fileUrl;

    private String year;

    private Date createTime;

    public Resource(Integer id, String type, String imageUrl, String fileName, String fileUrl, String year, Date createTime) {
        this.id = id;
        this.type = type;
        this.imageUrl = imageUrl;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.year = year;
        this.createTime = createTime;
    }

    public Resource() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}