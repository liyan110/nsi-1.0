package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 社区关注
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@Data
@TableName("community_follow")
public class CommunityFollow implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 微信openId
     */
    @TableField("open_id")
    private String openId;

    /**
     * 微信头像
     */
    @TableField("wechat_portrait")
    private String wechatPortrait;

    /**
     * 关注对象id
     */
    @TableField("follower_id")
    private String followerId;

    /**
     * 用户昵称
     */
    @TableField("follower_nickname")
    private String followerNickname;

    /**
     * 微信头像
     */
    @TableField("follower_portrait")
    private String followerPortrait;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
