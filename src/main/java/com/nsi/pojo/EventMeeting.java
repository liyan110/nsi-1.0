package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-14
 */
@TableName("nsi_event_meeting")
@Data
public class EventMeeting {


    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 富文本-PC
     */
    @TableField("pc_desc")
    private String pcDesc;

    /**
     * 富文本-H5
     */
    @TableField("h5_desc")
    private String h5Desc;

    /**
     * 活动地址
     */
    @TableField("activity_address")
    private String activityAddress;

    /**
     * 立即报名url
     */
    @TableField("apply_url")
    private String applyUrl;

    /**
     * 个人中心url
     */
    @TableField("person_center_url")
    private String personCenterUrl;

    /**
     * 活动开始日期
     */
    @TableField("start_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 活动结束日期
     */
    @TableField("end_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 微信分享标题
     */
    @TableField("wechat_share_title")
    private String wechatShareTitle;

    /**
     * 微信分享描述
     */
    @TableField("wechat_share_desc")
    private String wechatShareDesc;

    /**
     * 微信分享logo
     */
    @TableField("wechat_share_logo")
    private String wechatShareLogo;

    /**
     * 活动发起人
     */
    @TableField("create_people")
    private String createPeople;

    /**
     * 证书模板id
     */
    @TableField("template_id")
    private Integer templateId;

    /**
     * 是否需要电子票 0-true 1-false
     */
    @TableField("is_ticket")
    private Integer isTicket;

    /**
     * 是否需要发票 0-true 1-false
     */
    @TableField("is_invoice")
    private Integer isInvoice;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
