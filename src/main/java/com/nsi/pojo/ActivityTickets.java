package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-26
 */
@Data
@TableName("nsi_activity_tickets")
public class ActivityTickets implements Serializable {


    private static final long serialVersionUID = 3924084040066352797L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动id
     */
    @TableField("active_id")
    private Integer activeId;

    /**
     * 票名称
     */
    @TableField("ticket_name")
    private String ticketName;

    /**
     * 门票简称
     */
    @TableField("short_name")
    private String shortName;

    /**
     * 票价格
     */
    @TableField("ticket_price")
    private Integer ticketPrice;

    /**
     * 门票状态(0-正常显示 1-关闭)
     */
    @TableField("status")
    private Integer status;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

}
