package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("nsi_relation")
public class Relation implements Serializable {

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    private String type;

    private Integer c01;

    @TableField("c_name")
    private String cName;

    private Integer t02;
    @TableField("t_name")
    private String tName;

    private Date createtime;

}