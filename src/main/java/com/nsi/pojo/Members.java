package com.nsi.pojo;

import java.util.Date;

public class Members {
    private Integer id;

    private String name;

    private String institution;

    private String phone;

    private String duty;

    private String wechatId;

    private String mail;

    private String memberType;

    private Integer states;

    private Date createTime;

    public Members(Integer id, String name, String institution, String phone, String duty, String wechatId, String mail, String memberType, Integer states, Date createTime) {
        this.id = id;
        this.name = name;
        this.institution = institution;
        this.phone = phone;
        this.duty = duty;
        this.wechatId = wechatId;
        this.mail = mail;
        this.memberType = memberType;
        this.states = states;
        this.createTime = createTime;
    }

    public Members() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution == null ? null : institution.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty == null ? null : duty.trim();
    }

    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId == null ? null : wechatId.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType == null ? null : memberType.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStates() {
        return states;
    }

    public void setStates(Integer states) {
        this.states = states;
    }
}