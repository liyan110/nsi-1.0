package com.nsi.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;

    private String username;

    private String password;

    private Integer memberSign;

    private String userTurename;

    private String userOrganization;

    private String userPosition;

    private String userPhone;

    private String userRegistercode;

    private Integer userScore;

    private String loadTime;

    private String wechatid;

    private String userPortrait;

    private String registerType;

    private String unionId;

    private Date updateTime;

}