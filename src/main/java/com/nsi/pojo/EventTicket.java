package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-09-03
 **/
@Data
@TableName("nsi_event_ticket")
public class EventTicket {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    @TableField("event_id")
    private int eventId;
    private String info01;
    private String info02;
    private String info03;
    private String priceString01;
    private String priceString02;
    private String priceString03;
    private String priceString04;
    private String priceString05;
    private String priceString06;
    private String priceString07;
    private String priceString08;
    private String priceString09;
    private String priceString10;

    private String ticketType;

    private Date creatTime;

}
