package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 社区-标准评论表
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
@TableName("community_comment")
@Data
public class CommunityComment implements Serializable{


    private static final long serialVersionUID = -224202732350283517L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 评论人_微信id
     */
    @TableField("wechat_id")
    private String wechatId;

    /**
     * 评论人头像
     */
    private String portrait;

    /**
     * 评论人昵称
     */
    private String nickname;

    /**
     * 被评论对象id 帖子id
     */
    @TableField("object_id")
    private Integer objectId;

    /**
     * 评论内容（富文本）
     */
    private String content;

    /**
     * 审核标记（0待审核，1已通过）
     */
    @TableField("verify_sign")
    private Integer verifySign;

    /**
     * 权重
     */
    private Integer level;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 审核时间
     */
    @TableField("verify_time")
    private Date verifyTime;

    /**
     * 子评论数
     */
    @TableField("son_comments_num")
    private Integer sonCommentsNum;

    /**
     * 分数
     */
    @TableField("score")
    private Integer score;

    /**
     * 评论类型 1-火龙果 2-四库全书
     */
    @TableField("comment_type")
    private Integer commentType;

}
