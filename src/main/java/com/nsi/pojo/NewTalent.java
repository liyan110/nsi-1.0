package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-11-26
 */
@Data
@TableName("nsi_new_talent")
public class NewTalent implements Serializable {

    private static final long serialVersionUID = -9034547049050422945L;
    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户姓名
     */
    private String username;

    /**
     * 性别
     */
    private String sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 电话
     */
    private String telphone;

    /**
     * 用户邮箱
     */
    @TableField("user_mail")
    private String userMail;

    /**
     * 最高学历
     */
    @TableField("high_education")
    private String highEducation;

    /**
     * 专业
     */
    private String profession;

    /**
     * 工作地点
     */
    private String workplace;

    /**
     * 工作经验时间
     */
    private String expreience;

    /**
     * 期望工作地点
     */
    @TableField("expected_work")
    private String expectedWork;

    /**
     * 期望职位
     */
    private String position;

    /**
     * 期望年薪
     */
    private String salary;

    /**
     * 入职时间
     */
    @TableField("entry_time")
    private String entryTime;

    /**
     * 工作经验
     */
    @TableField("work_experience")
    private String workExperience;

    /**
     * 教育背景
     */
    private String education;

    /**
     * 培训经历
     */
    private String training;

    /**
     * 简历附件
     */
    private String resume;

    /**
     * 资格证书
     */
    private String certificate;

    /**
     * 标签
     */
    private String tag;

    /**
     * 其他信息
     */
    private String other;

    /**
     * 是否公开 0-是 1-否
     */
    @TableField("is_public")
    private Integer isPublic;

    /**
     * 是否审核 0-通过 1-未通过
     */
    @TableField("is_check")
    private Integer isCheck;

    /**
     * 内部描述(公司)
     */
    @TableField("internal_desc")
    private String internalDesc;

    /**
     * 提交人邮箱
     */
    private String submitter;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

}
