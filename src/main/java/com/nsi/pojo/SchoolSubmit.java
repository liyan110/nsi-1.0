package com.nsi.pojo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 学校提交人信息数据库表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-10-15
 */
@Data
@TableName("nsi_school_submit")
public class SchoolSubmit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 提交人姓名
     */
    private String name;

    /**
     * 提交人公司
     */
    private String company;


    /**
     * 提交人手机号
     */
    private String telphone;

    /**
     * 学校id
     */
    private Integer schoolId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
