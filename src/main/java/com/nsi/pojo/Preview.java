package com.nsi.pojo;

import java.util.Date;

public class Preview {
    private Integer id;

    private String userId;

    private String fileCategory;

    private String fileName;

    private String fileUrl;

    private Date createTime;

    public Preview(Integer id, String userId, String fileCategory, String fileName, String fileUrl, Date createTime) {
        this.id = id;
        this.userId = userId;
        this.fileCategory = fileCategory;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.createTime = createTime;
    }

    public Preview() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getFileCategory() {
        return fileCategory;
    }

    public void setFileCategory(String fileCategory) {
        this.fileCategory = fileCategory == null ? null : fileCategory.trim();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}