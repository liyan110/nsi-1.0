package com.nsi.pojo;

public class Course {
    private Integer id;

    private String channelnumber;

    private String channelname;

    private String secretkey;

    private String teacherid;

    private String html01;

    private String html02;

    private String html03;

    private String html04;

    private String coursesubject;

    private String coursename;

    private String coursestate;

    private String coursedescription;

    private String classbegins;

    private String coursepraise;

    private String courseprice;

    private String courserelease;

    private String coursepeople;

    private String coverimage;

    private String courseimage;

    public Course(Integer id, String channelnumber, String channelname, String secretkey, String teacherid, String html01, String html02, String html03, String html04, String coursesubject, String coursename, String coursestate, String coursedescription, String classbegins, String coursepraise, String courseprice, String courserelease, String coursepeople, String coverimage, String courseimage) {
        this.id = id;
        this.channelnumber = channelnumber;
        this.channelname = channelname;
        this.secretkey = secretkey;
        this.teacherid = teacherid;
        this.html01 = html01;
        this.html02 = html02;
        this.html03 = html03;
        this.html04 = html04;
        this.coursesubject = coursesubject;
        this.coursename = coursename;
        this.coursestate = coursestate;
        this.coursedescription = coursedescription;
        this.classbegins = classbegins;
        this.coursepraise = coursepraise;
        this.courseprice = courseprice;
        this.courserelease = courserelease;
        this.coursepeople = coursepeople;
        this.coverimage = coverimage;
        this.courseimage = courseimage;
    }

    public Course() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannelnumber() {
        return channelnumber;
    }

    public void setChannelnumber(String channelnumber) {
        this.channelnumber = channelnumber == null ? null : channelnumber.trim();
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname == null ? null : channelname.trim();
    }

    public String getSecretkey() {
        return secretkey;
    }

    public void setSecretkey(String secretkey) {
        this.secretkey = secretkey == null ? null : secretkey.trim();
    }

    public String getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(String teacherid) {
        this.teacherid = teacherid == null ? null : teacherid.trim();
    }

    public String getHtml01() {
        return html01;
    }

    public void setHtml01(String html01) {
        this.html01 = html01 == null ? null : html01.trim();
    }

    public String getHtml02() {
        return html02;
    }

    public void setHtml02(String html02) {
        this.html02 = html02 == null ? null : html02.trim();
    }

    public String getHtml03() {
        return html03;
    }

    public void setHtml03(String html03) {
        this.html03 = html03 == null ? null : html03.trim();
    }

    public String getHtml04() {
        return html04;
    }

    public void setHtml04(String html04) {
        this.html04 = html04 == null ? null : html04.trim();
    }

    public String getCoursesubject() {
        return coursesubject;
    }

    public void setCoursesubject(String coursesubject) {
        this.coursesubject = coursesubject == null ? null : coursesubject.trim();
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename == null ? null : coursename.trim();
    }

    public String getCoursestate() {
        return coursestate;
    }

    public void setCoursestate(String coursestate) {
        this.coursestate = coursestate == null ? null : coursestate.trim();
    }

    public String getCoursedescription() {
        return coursedescription;
    }

    public void setCoursedescription(String coursedescription) {
        this.coursedescription = coursedescription == null ? null : coursedescription.trim();
    }

    public String getClassbegins() {
        return classbegins;
    }

    public void setClassbegins(String classbegins) {
        this.classbegins = classbegins == null ? null : classbegins.trim();
    }

    public String getCoursepraise() {
        return coursepraise;
    }

    public void setCoursepraise(String coursepraise) {
        this.coursepraise = coursepraise == null ? null : coursepraise.trim();
    }

    public String getCourseprice() {
        return courseprice;
    }

    public void setCourseprice(String courseprice) {
        this.courseprice = courseprice == null ? null : courseprice.trim();
    }

    public String getCourserelease() {
        return courserelease;
    }

    public void setCourserelease(String courserelease) {
        this.courserelease = courserelease == null ? null : courserelease.trim();
    }

    public String getCoursepeople() {
        return coursepeople;
    }

    public void setCoursepeople(String coursepeople) {
        this.coursepeople = coursepeople == null ? null : coursepeople.trim();
    }

    public String getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage == null ? null : coverimage.trim();
    }

    public String getCourseimage() {
        return courseimage;
    }

    public void setCourseimage(String courseimage) {
        this.courseimage = courseimage == null ? null : courseimage.trim();
    }
}