package com.nsi.pojo.transformObject;

import lombok.Data;

/**
 * @author Li Yan
 * @create 2020-01-10
 **/
@Data
public class WechatSubmitPagesParm {
    private String path;
    private String query;


}
