package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author luo Zhen
 * @since 2021-03-17
 */
@TableName("nsi_experience_user")
@Data
public class ExperienceUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String username;

    /**
     * 手机号
     */
    private String telphone;

    /**
     * 公司名称
     */
    private String company;

    /**
     * 职位
     */
    private String position;

    /**
     * 是否联系(0-未联系 1-已联系)
     */
    @TableField("is_relation")
    private Integer isRelation;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
}
