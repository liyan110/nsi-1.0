package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("nsi_school_visit")
public class SchoolVisit implements Serializable {

    private static final long serialVersionUID = 8958185053178021431L;
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("name")
    private String name;

    @TableField("attend")
    private String attend;

    @TableField("telphone")
    private String telphone;

    @TableField("remark")
    private String remark;

    @TableField("open_id")
    private String openId;

    @TableField("fill_desc")
    private Integer fillDesc;

    @TableField("create_time")
    private Date createTime;

    @TableField("school_id")
    private Integer schoolId;

    @TableField(exist = false)
    private String schoolName;

    @TableField(exist = false)
    private String icon;

}