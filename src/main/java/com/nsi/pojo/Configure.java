package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;


/**
 * @author Luo Zhen
 */
@Data
@TableName("nsi_configure")
public class Configure implements Serializable {
    private static final long serialVersionUID = -435928650371465471L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    private String type;

    private String content01;

    private String content02;

    private String content03;

    private String content04;

    private String content05;

    private String content06;

    private String content07;

    private String content08;

    private String content09;

    private String content10;

    private String textcontent01;

    private String textcontent02;

    private String textcontent03;


}