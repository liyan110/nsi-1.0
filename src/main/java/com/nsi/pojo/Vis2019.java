package com.nsi.pojo;

import lombok.Data;
import lombok.Setter;

import java.util.Date;

@Data
@Setter
public class Vis2019 {
    private Integer id;

    private String name;

    private String company;

    private String position;

    private String mail;

    private String phone;

    private String option01;

    private String option02;

    private String ispublic;

    private String attendno;

    private String type;

    private Date creattime;

}