package com.nsi.pojo;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName("nsi_article")
@Data
public class Article implements Serializable {

    private static final long serialVersionUID = -5681246579392894808L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String title;

    private String summary;

    @TableField("article_content")
    private String articleContent;

    private Integer visible;

    @TableField("cover_image")
    private String coverImage;

    @TableField("sift_type")
    private String siftType;

    @TableField("article_cat")
    private String articleCat;

    @TableField("article_writer")
    private String articleWriter;

    @TableField("article_reader")
    private Integer articleReader;

    @TableField("article_url")
    private String articleUrl;

    @TableField("article_source_title")
    private String articleSourceTitle;

    @TableField("article_source_adress")
    private String articleSourceAdress;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;
}