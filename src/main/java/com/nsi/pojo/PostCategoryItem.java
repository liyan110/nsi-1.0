package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 帖子详情表
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@TableName("nsi_post_category_item")
@Data
public class PostCategoryItem implements Serializable {


    private static final long serialVersionUID = 4756838460893562545L;
    @TableId(value = "item_id", type = IdType.AUTO)
    private Integer itemId;

    /**
     * 帖子标题
     */
    private String title;

    /**
     * 帖子内容
     */
    private String content;

    /**
     * 文章摘要
     */
    @TableField("summary_desc")
    private String summaryDesc;

    /**
     * 帖子小图
     */
    @TableField("post_icon")
    private String postIcon;

    /**
     * 帖子类型
     */
    @TableField("post_type")
    private String postType;

    /**
     * 评论数
     */
    @TableField("comment_num")
    private Integer commentNum;

    /**
     * 分享数
     */
    @TableField("share_num")
    private Integer shareNum;

    /**
     * 观看数
     */
    @TableField("watch_num")
    private Integer watchNum;

    /**
     * 收藏数
     */
    @TableField("collect_num")
    private Integer collectNum;

    /**
     * 标签
     */
    private String tag;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 类目名称
     */
    @TableField(exist = false)
    private List<String> categoryName;


    /**
     * 微信用户id
     */
    @TableField("open_id")
    private String openId;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 用户等级（-1-禁止,1-基本,2-完善,3-完善且认证,9-管理员）
     */
    @TableField("grade_sign")
    private Integer gradeSign;

    /**
     * 是否审核
     */
    @TableField("is_check")
    private Integer isCheck;

    /**
     * 附件-1
     */
    @TableField("attach_one")
    private String attachOne;

    /**
     * 附件-2
     */
    @TableField("attach_two")
    private String attachTwo;

    /**
     * 附件-3
     */
    @TableField("attach_three")
    private String attachThree;

    /**
     * 置顶字段
     */
    @TableField("is_top")
    private Integer isTop;


}
