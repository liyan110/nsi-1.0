package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author luo Zhen
 * @since 2020-04-01
 */
@Data
@TableName("nsi_electronic_ticket")
public class ElectronicTicket implements Serializable {


    private static final long serialVersionUID = 7906549583326395006L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动id
     */
    @TableField("active_id")
    private Integer activeId;

    /**
     * 门票类型
     */
    private String type;

    /**
     * 大图文件名字
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 合成门票大图地址
     */
    @TableField("img_url")
    private String imgUrl;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
