package com.nsi.pojo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Coupon {

    private Integer id;

    private String name;

    private String imgUrl;

    private Integer monthActivityType;

    private Integer couponType;

    private Integer withAmount;

    private Integer usedAmount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}