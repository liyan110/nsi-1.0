package com.nsi.pojo;

import java.util.Date;

public class ForeignTeacher {
    private Integer id;

    private String imgAddr;

    private String teacherName;

    private String teacherNationality;

    private String idCard;

    private String story;

    private String informant;

    private String reportingUnit;

    private String email;

    private String phone;

    private Integer enableStatus;

    private Date createTime;

    public ForeignTeacher(Integer id, String imgAddr, String teacherName, String teacherNationality, String idCard, String story, String informant, String reportingUnit, String email, String phone, Integer enableStatus, Date createTime) {
        this.id = id;
        this.imgAddr = imgAddr;
        this.teacherName = teacherName;
        this.teacherNationality = teacherNationality;
        this.idCard = idCard;
        this.story = story;
        this.informant = informant;
        this.reportingUnit = reportingUnit;
        this.email = email;
        this.phone = phone;
        this.enableStatus = enableStatus;
        this.createTime = createTime;
    }

    public ForeignTeacher() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgAddr() {
        return imgAddr;
    }

    public void setImgAddr(String imgAddr) {
        this.imgAddr = imgAddr == null ? null : imgAddr.trim();
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    public String getTeacherNationality() {
        return teacherNationality;
    }

    public void setTeacherNationality(String teacherNationality) {
        this.teacherNationality = teacherNationality == null ? null : teacherNationality.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story == null ? null : story.trim();
    }

    public String getInformant() {
        return informant;
    }

    public void setInformant(String informant) {
        this.informant = informant == null ? null : informant.trim();
    }

    public String getReportingUnit() {
        return reportingUnit;
    }

    public void setReportingUnit(String reportingUnit) {
        this.reportingUnit = reportingUnit == null ? null : reportingUnit.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Integer enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}