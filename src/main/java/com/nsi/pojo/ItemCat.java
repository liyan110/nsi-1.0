package com.nsi.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class ItemCat {
    private Integer id;

    private Integer parentId;

    private String name;

    private Integer itemAmount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date withTime;

    private Integer amountCased;

    private String invoiceNum;

    private String text;

    private Integer receivingTotal;

    private Integer balance;

    private String creator;

    private Date createTime;

    private Date updateTime;

    public ItemCat(Integer id, Integer parentId, String name, Integer itemAmount, Date withTime, Integer amountCased, String invoiceNum, String text, Integer receivingTotal, Integer balance, String creator, Date createTime, Date updateTime) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.itemAmount = itemAmount;
        this.withTime = withTime;
        this.amountCased = amountCased;
        this.invoiceNum = invoiceNum;
        this.text = text;
        this.receivingTotal = receivingTotal;
        this.balance = balance;
        this.creator = creator;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public ItemCat() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Integer itemAmount) {
        this.itemAmount = itemAmount;
    }

    public Date getWithTime() {
        return withTime;
    }

    public void setWithTime(Date withTime) {
        this.withTime = withTime;
    }

    public Integer getAmountCased() {
        return amountCased;
    }

    public void setAmountCased(Integer amountCased) {
        this.amountCased = amountCased;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum == null ? null : invoiceNum.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public Integer getReceivingTotal() {
        return receivingTotal;
    }

    public void setReceivingTotal(Integer receivingTotal) {
        this.receivingTotal = receivingTotal;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}