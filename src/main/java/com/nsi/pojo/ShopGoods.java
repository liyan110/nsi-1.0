package com.nsi.pojo;

import lombok.Data;

import java.util.Date;
@Data
public class ShopGoods {
    private Integer id;

    private String goodsName;

    private String goodsDescribe;

    private String goodsAuthor;

    private String goodsPress;

    private String goodsPubdate;

    private String goodsInfo;

    private String goodsType;

    private Integer goodsStock;

    private Date goodsCreattime;

    private Integer goodsLevel;

    private String goodsLabel;

    private String goodsSeries;

    private String goodsState;

    private Integer goodsPrice;

    private Integer goodsSales;

    private Integer goodsVisitcount;

    private String goodsImg;

    private String goodsShareImg;

}