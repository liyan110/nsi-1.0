package com.nsi.pojo;

import java.util.Date;

public class Boards {
    private Integer id;

    private String boardname;

    private String sid;

    private Date updateTime;

    public Boards(Integer id, String boardname, String sid, Date updateTime) {
        this.id = id;
        this.boardname = boardname;
        this.sid = sid;
        this.updateTime = updateTime;
    }

    public Boards() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBoardname() {
        return boardname;
    }

    public void setBoardname(String boardname) {
        this.boardname = boardname == null ? null : boardname.trim();
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid == null ? null : sid.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}