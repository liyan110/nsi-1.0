package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 社区消息
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-26
 */
@Data
@TableName("community_message")
public class CommunityMessage extends Model<CommunityMessage> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息类型
     */
    @TableField("message_type")
    private String messageType;

    /**
     * 消息人姓名
     */
    @TableField("message_name")
    private String messageName;

    /**
     * 消息人头像
     */
    @TableField("message_portrait")
    private String messagePortrait;

    /**
     * 消息人微信id
     */
    @TableField("message_wechat_id")
    private String messageWechatId;

    /**
     * 消息 内容
     */
    @TableField("message_content")
    private String messageContent;

    /**
     * 对象id
     */
    @TableField("object_id")
    private String objectId;

    /**
     * 对象标题
     */
    @TableField("object_title")
    private String objectTitle;

    /**
     * 对象 所属用户微信id
     */
    @TableField("object_user_id")
    private String objectUserId;

    /**
     * 消息接收人 微信id
     */
    @TableField("receiver_wechat_id")
    private String receiverWechatId;


    /**
     * 消息点击 跳转id
     */
    @TableField("jump_id")
    private String jumpId;

    /**
     * 是否被查看（0未查看 1已查看）
     */
    @TableField("check_state")
    private Integer checkState;


    /**
     * 消息查看时间
     */
    @TableField("check_time")
    private Date checkTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}
