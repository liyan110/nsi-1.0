package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 帖子类目表
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@TableName("nsi_post_category")
@Data
public class PostCategory implements Serializable {


    private static final long serialVersionUID = 8392251396585704334L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 类目名称
     */
    @TableField("category_name")
    private String categoryName;

    /**
     * 背景大图
     */
    @TableField("background_img")
    private String backgroundImg;

    /**
     * 类目图标
     */
    private String icon;

    /**
     * 父类id
     */
    @TableField("parent_id")
    private Integer parentId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

}
