package com.nsi.pojo;

public class CourseUser {
    private Integer id;

    private String classid;

    private String usermail;

    public CourseUser(Integer id, String classid, String usermail) {
        this.id = id;
        this.classid = classid;
        this.usermail = usermail;
    }

    public CourseUser() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid == null ? null : classid.trim();
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail == null ? null : usermail.trim();
    }
}