package com.nsi.pojo;

public class Recuitment {
    private Integer id;

    private String name;

    private String briefIntro;

    private String intro;

    private String logo;

    private String province;

    private String city;

    private String site;

    private String linkName;

    private String linkPhone;

    private String linkMail;

    private String loadPeople;

    private String loadTime;

    public Recuitment(Integer id, String name, String briefIntro, String intro, String logo, String province, String city, String site, String linkName, String linkPhone, String linkMail, String loadPeople, String loadTime) {
        this.id = id;
        this.name = name;
        this.briefIntro = briefIntro;
        this.intro = intro;
        this.logo = logo;
        this.province = province;
        this.city = city;
        this.site = site;
        this.linkName = linkName;
        this.linkPhone = linkPhone;
        this.linkMail = linkMail;
        this.loadPeople = loadPeople;
        this.loadTime = loadTime;
    }

    public Recuitment() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getBriefIntro() {
        return briefIntro;
    }

    public void setBriefIntro(String briefIntro) {
        this.briefIntro = briefIntro == null ? null : briefIntro.trim();
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName == null ? null : linkName.trim();
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone == null ? null : linkPhone.trim();
    }

    public String getLinkMail() {
        return linkMail;
    }

    public void setLinkMail(String linkMail) {
        this.linkMail = linkMail == null ? null : linkMail.trim();
    }

    public String getLoadPeople() {
        return loadPeople;
    }

    public void setLoadPeople(String loadPeople) {
        this.loadPeople = loadPeople == null ? null : loadPeople.trim();
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime == null ? null : loadTime.trim();
    }
}