package com.nsi.pojo;

/**
 * @author Luo Zhen
 * @create 2018-07-30
 */
public class OldArticles {

    private Integer id;

    private String title;

    private String content;

    private String path;

    private Long createTime;

    public OldArticles(Integer id, String title, String content, String path, Long createTime) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.path = path;
        this.createTime = createTime;
    }

    public OldArticles() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}