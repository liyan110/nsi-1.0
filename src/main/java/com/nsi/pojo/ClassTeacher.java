package com.nsi.pojo;

public class ClassTeacher {
    private Integer id;

    private String teachername;

    private String teacherdescription;

    private String teachercourse;

    private String teacherimage;

    private String html01;

    private String loadPeople;

    private String loadTime;

    public ClassTeacher(Integer id, String teachername, String teacherdescription, String teachercourse, String teacherimage, String html01, String loadPeople, String loadTime) {
        this.id = id;
        this.teachername = teachername;
        this.teacherdescription = teacherdescription;
        this.teachercourse = teachercourse;
        this.teacherimage = teacherimage;
        this.html01 = html01;
        this.loadPeople = loadPeople;
        this.loadTime = loadTime;
    }

    public ClassTeacher() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeachername() {
        return teachername;
    }

    public void setTeachername(String teachername) {
        this.teachername = teachername == null ? null : teachername.trim();
    }

    public String getTeacherdescription() {
        return teacherdescription;
    }

    public void setTeacherdescription(String teacherdescription) {
        this.teacherdescription = teacherdescription == null ? null : teacherdescription.trim();
    }

    public String getTeachercourse() {
        return teachercourse;
    }

    public void setTeachercourse(String teachercourse) {
        this.teachercourse = teachercourse == null ? null : teachercourse.trim();
    }

    public String getTeacherimage() {
        return teacherimage;
    }

    public void setTeacherimage(String teacherimage) {
        this.teacherimage = teacherimage == null ? null : teacherimage.trim();
    }

    public String getHtml01() {
        return html01;
    }

    public void setHtml01(String html01) {
        this.html01 = html01 == null ? null : html01.trim();
    }

    public String getLoadPeople() {
        return loadPeople;
    }

    public void setLoadPeople(String loadPeople) {
        this.loadPeople = loadPeople == null ? null : loadPeople.trim();
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime == null ? null : loadTime.trim();
    }
}