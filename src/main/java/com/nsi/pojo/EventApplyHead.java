package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-08-16
 **/
@Data
@TableName("nsi_event_apply_head")
public class EventApplyHead {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    @TableField("event_id")
    private int eventId;
    private String baseContent01;
    private String baseContent02;
    private String baseContent03;
    private String baseContent04;
    private String baseContent05;

    private String dynamicContent01;
    private String dynamicContent02;
    private String dynamicContent03;
    private String dynamicContent04;
    private String dynamicContent05;

    private String attach_content01;
    private String attach_content02;
    private String attach_content03;
    private String attach_content04;
    private String attach_content05;

    private Date creat_time;

}
