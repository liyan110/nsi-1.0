package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动用户报名表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-23
 */
@Data
@TableName("nsi_event_collect")
public class EventCollect implements Serializable {


    private static final long serialVersionUID = -8378652679815537515L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 职位
     */
    private String position;

    /**
     * 公司
     */
    private String company;

    /**
     * 电话
     */
    private String telphone;

    /**
     * 邮箱
     */
    @TableField("user_mail")
    private String userMail;

    /**
     * 备份选项-1
     */
    @TableField("option_1")
    private String option1;

    /**
     * 备份选项-2
     */
    @TableField("option_2")
    private String option2;

    /**
     * 备份选项-3
     */
    @TableField("option_3")
    private String option3;

    /**
     * 备份选项-4
     */
    @TableField("option_4")
    private String option4;

    /**
     * 备份选项-5
     */
    @TableField("option_5")
    private String option5;

    /**
     * 备份选项-6
     */
    @TableField("option_6")
    private String option6;

    /**
     * 备份选项-7
     */
    @TableField("option_7")
    private String option7;

    /**
     * 备份选项-8
     */
    @TableField("option_8")
    private String option8;

    /**
     * 备份选项-9
     */
    @TableField("option_9")
    private String option9;

    /**
     * 备份选项-10
     */
    @TableField("option_10")
    private String option10;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 审核标记（0 审核通过 1 审核中）
     */
    @TableField("verify")
    private Integer verify;

    /**
     * 签到标记(0-已签到 1-未签到)
     */
    @TableField("check_in")
    private Integer checkIn;

    /**
     * 邀请人
     */
    private String inviter;

    /**
     * 支付类型(空-默认支付 1-线下支付)
     */
    @TableField("pay_type")
    private Integer payType;
    /**
     * 活动id
     */
    @TableField("active_id")
    private Integer activeId;


}
