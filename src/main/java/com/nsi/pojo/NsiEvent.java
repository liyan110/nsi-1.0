package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 活动定义表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@Data
@TableName("nsi_event")
public class NsiEvent implements Serializable {

    private static final long serialVersionUID = 3566162030319405385L;

    /**
     * 活动id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动名称
     */
    @TableField("event_name")
    private String eventName;

    /**
     * 活动摘要
     */
    @TableField("event_summary")
    private String eventSummary;

    /**
     * 活动开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("event_start_time")
    private Date eventStartTime;

    /**
     * 活动地点
     */
    @TableField("event_place")
    private String eventPlace;

    /**
     * 活动类型 缩写
     */
    private String type;

    /**
     * 开启支付模块（0 关闭、1 开启）
     */
    @TableField("open_pay")
    private Integer openPay;

    /**
     * 开启电子门票（0关闭、1开启）
     */
    @TableField("open_e_ticket")
    private Integer openETicket;

    /**
     * 开启发票功能（0 关闭、1 开启）
     */
    @TableField("open_invoice")
    private Integer openInvoice;

    /**
     * 开启审核功能 (0-关闭,1-开启)
     */
    @TableField("open_verify")
    private Integer openVerify;

    /**
     * 开启授权功能 (0-关闭,1-开启)
     */
    @TableField("open_authorize")
    private Integer openAuthorize;

    /**
     * 票务说明信息01
     */
    @TableField("ticket_info01")
    private String ticketInfo01;

    /**
     * 票务说明信息02
     */
    @TableField("ticket_info02")
    private String ticketInfo02;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 备注信息
     */
    private String notes;

    @TableField(exist = false)
    private List<String> notesList;
}
