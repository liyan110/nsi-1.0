package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-16
 */
@TableName("nsi_entry_audit")
@Data
public class EntryAudit implements Serializable {

    private static final long serialVersionUID = -1200174318563723683L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 报名表id
     */
    @TableField("entry_id")
    private Integer entryId;

    @TableField(exist = false)
    private String applicationName;

    @TableField(exist = false)
    private String company;

    /**
     * 邀请人
     */
    private String inviter;

    /**
     * 是否审核 0-未审核 1-已审核
     */
    @TableField("is_check")
    private Integer isCheck;

    /**
     * 申请时间
     */
    @TableField("enrolment_time")
    private Date enrolmentTime;

    /**
     * 审核时间
     */
    @TableField("audit_time")
    private Date auditTime;

    @TableField(exist = false)
    private String checkMsg;

}
