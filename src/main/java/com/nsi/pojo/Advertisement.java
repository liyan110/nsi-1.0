package com.nsi.pojo;

public class Advertisement {
    private Integer id;

    private String typename;

    private String adtext;

    private String imgurl;

    private String clickurl;

    private String createtime;

    private String sort;

    public Advertisement(Integer id, String typename, String adtext, String imgurl, String clickurl, String createtime, String sort) {
        this.id = id;
        this.typename = typename;
        this.adtext = adtext;
        this.imgurl = imgurl;
        this.clickurl = clickurl;
        this.createtime = createtime;
        this.sort = sort;
    }

    public Advertisement() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename == null ? null : typename.trim();
    }

    public String getAdtext() {
        return adtext;
    }

    public void setAdtext(String adtext) {
        this.adtext = adtext == null ? null : adtext.trim();
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl == null ? null : imgurl.trim();
    }

    public String getClickurl() {
        return clickurl;
    }

    public void setClickurl(String clickurl) {
        this.clickurl = clickurl == null ? null : clickurl.trim();
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }
}