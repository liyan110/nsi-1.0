package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 社区用户
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-13
 */
@TableName("community_user")
@Data
public class CommunityUser implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String truename;

    /**
     * 用户邮箱
     */
    private String usermail;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 用户等级（-1-禁止,1-基本,2-完善,3-完善且认证,9-管理员）
     */
    @TableField("grade_sign")
    private Integer gradeSign;

    /**
     * 个性签名
     */
    private String slogan;

    /**
     * 认证类型
     */
    @TableField("auth_type")
    private String authType;

    /**
     * 个人简介(富文本)
     */
    @TableField("resume")
    private String resume;

    /**
     * 组织 公司
     */
    private String organization;

    /**
     * 职位
     */
    private String position;

    /**
     * 身份证号
     */
    @TableField("idCard_num")
    private String idCardNum;

    /**
     * 密码
     */
    private String password;

    /**
     * 类型
     */
    private String type;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间（上次登录）
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 微信昵称
     */
    @TableField("wechat_nickname")
    private String wechatNickname;

    /**
     * 微信openId
     */
    @TableField("open_id")
    private String openId;

    /**
     * 微信unionID
     */
    @TableField("union_id")
    private String unionId;

    /**
     * 微信头像
     */
    @TableField("wechat_portrait")
    private String wechatPortrait;

    /**
     * 用户积分
     */
    private Integer score;
}
