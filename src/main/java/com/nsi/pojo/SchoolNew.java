package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * <p> 新学校库实体类
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-10
 */
@TableName("nsi_new_school")
@Data
public class SchoolNew implements Serializable {

    private static final long serialVersionUID = 7119673557630082856L;
    /**
     * 逻辑ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 学校名字
     */
    @TableField("school_name")
    private String schoolName;

    /**
     * 学校英文名字
     */
    @TableField("school_english_name")
    private String schoolEnglishName;

    /**
     * 学校性质
     */
    @TableField("school_properties")
    private String schoolProperties;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String town;

    /**
     * 地址
     */
    private String address;

    /**
     * 成立时间
     */
    @TableField("founding_time")
    private Integer foundingTime;

    /**
     * 运营状态
     */
    @TableField("operation_state")
    private String operationState;

    /**
     * 学制
     */
    @TableField("school_system")
    private String schoolSystem;

    /**
     * 幼儿园-学费
     */
    @TableField("one_tuition")
    private Integer oneTuition;

    /**
     * 小学-学费
     */
    @TableField("two_tuition")
    private Integer twoTuition;

    /**
     * 初中-学费
     */
    @TableField("third_tuition")
    private Integer thirdTuition;

    /**
     * 高中-学费
     */
    @TableField("four_tuition")
    private Integer fourTuition;

    /**
     * 官网
     */
    private String website;

    /**
     * 电话
     */
    private String telephone;

    /**
     * 国际课程成立时间
     */
    @TableField("inter_course_founded_time")
    private Integer interCourseFoundedTime;

    /**
     * 课程
     */
    private String course;

    /**
     * 认证
     */
    private String authentication;

    /**
     * 学生总人数
     */
    private Integer students;

    @TableField("student_num_one")
    private Integer studentNumOne;

    @TableField("student_num_two")
    private Integer studentNumTwo;

    @TableField("student_num_third")
    private Integer studentNumThird;

    @TableField("student_num_four")
    private Integer studentNumFour;

    /**
     * 学生容量
     */
    @TableField("student_capacity")
    private Integer studentCapacity;

    /**
     * 毕业班人数
     */
    @TableField("graduated_stu_num")
    private Integer graduatedStuNum;

    /**
     * 学生主要国籍
     */
    @TableField("stu_dominant_nationality")
    private String stuDominantNationality;

    /**
     * 员工数量
     */
    @TableField("staff_num")
    private Integer staffNum;

    /**
     * 教师数量
     */
    @TableField("teacher_num")
    private Integer teacherNum;

    /**
     * 外籍教师数量
     */
    @TableField("foreign_teacher_num")
    private Integer foreignTeacherNum;

    /**
     * 师生比
     */
    @TableField("teacher_stu_ratio")
    private String teacherStuRatio;

    /**
     * 占地面积
     */
    @TableField("covered_area")
    private String coveredArea;

    /**
     * 建筑面积
     */
    @TableField("built_area")
    private String builtArea;

    /**
     * 硬件设施
     */
    private String hardware;

    /**
     * 投资信息
     */
    private String investment;

    /**
     * 备注
     */
    private String remark;

    /**
     * 提交人
     */
    private String submitter;

    /**
     * 提交时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 学校logo
     */
    @TableField("school_logo")
    private String schoolLogo;

    /**
     * 大图一
     */
    @TableField("school_show_one")
    private String schoolShowOne;

    /**
     * 大图二
     */
    @TableField("school_show_two")
    private String schoolShowTwo;

    /**
     * 大图三
     */
    @TableField("school_show_third")
    private String schoolShowThird;

    /**
     * 大图四
     */
    @TableField("school_show_four")
    private String schoolShowFour;

    /**
     * 大图五
     */
    @TableField("school_show_five")
    private String schoolShowFive;

    /**
     * 学校简介
     */
    @TableField("school_desc")
    private String schoolDesc;

    /**
     * 住宿情况
     */
    private String accommodation;

    /**
     * 招生信息
     */
    @TableField("student_enrollment")
    private String studentEnrollment;

    /**
     * 留学生留学国家
     */
    @TableField("stude_abroad_countries")
    private String studeAbroadCountries;

    private String prospects;

    /**
     * 申请费
     */
    @TableField("filing_fee")
    private String filingFee;

    /**
     * 办学理念
     */
    @TableField("school_management")
    private String schoolManagement;

    /**
     * 办学特色
     */
    @TableField("school_characteristics")
    private String schoolCharacteristics;

    /**
     * 课程体系
     */
    @TableField("course_system")
    private String courseSystem;

    /**
     * 课程体系图片
     */
    @TableField("course_img")
    private String courseImg;


    /**
     * 学生国籍数
     */
    @TableField("nationality_of_students")
    private Integer nationalityOfStudents;

    /**
     * 班级规模
     */
    @TableField("class_size")
    private String classSize;

    /**
     * 授课形式
     */
    @TableField("teaching_form")
    private String teachingForm;

    /**
     * 新学说分析
     */
    @TableField("company_analysis")
    private String companyAnalysis;


    @TableField(exist = false)
    private Map studentEnrollmentVo;
    @TableField(exist = false)
    private Map schoolCharacteristicsVo;

    /**
     * 0: 审核中 1: 审核通过
     */
    @TableField("verify_sign")
    private Integer verifySign;

    /**
     * 0:正常显示 1：隐藏
     */
    @TableField("status")
    private Integer status;

    /**
     * 数据年份
     */
    @TableField("year_of_data")
    private Integer yearOfData;

    /**
     * 提交人姓名
     */
    @TableField(exist = false)
    private String submitName;

    /**
     * 提交人公司
     */
    @TableField(exist = false)
    private String company;

    /**
     * 提交人手机号
     */
    @TableField(exist = false)
    private String telphone;

    /**
     * 是否跳转诺汇系统(0-不跳转 1-跳转)
     */
    @TableField("is_use_nuohui")
    private Integer isUseNuohui;


}
