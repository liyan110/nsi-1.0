package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.Setter;

import java.util.Date;

/**
 * <p>
 * 展位表
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-02
 */
@TableName("nsi_exhibitor")
@Data
public class Exhibitor {


    /**
     * 展商id
     */
    private Integer id;

    /**
     * 展商名字
     */
    @TableField("exhibitor_name")
    private String exhibitorName;

    /**
     * 展位号
     */
    @TableField("booth_num")
    private String boothNum;

    /**
     * 类别
     */
    private String type;

    /**
     * logo图片
     */
    @TableField("logo_icon")
    private String logoIcon;

    /**
     * 简介
     */
    private String intro;

    /**
     * 详细介绍(富文本)
     */
    @TableField("text_desc")
    private String textDesc;

    /**
     * 点赞值
     */
    @TableField("thumb_value")
    private Integer thumbValue;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
