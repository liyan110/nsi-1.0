package com.nsi.pojo;

public class School {
    private Integer id;

    private String schoolName;

    private String schoolEnglishname;

    private String schoolProperties;

    private String areas;

    private String areas02;

    private String areas03;

    private Integer foundedTime;

    private String operationstate;

    private String schoolSystem;

    private String tuition01;

    private String tuition02;

    private String tuition03;

    private String tuition04;

    private String tuitionhigh;

    private String website;

    private String telephone;

    private Integer interCourseFoundedTime;

    private String course;

    private String authentication;

    private String courseEvaluation;

    private Integer studentNumAll;

    private Integer studentNum01;

    private Integer studentNum02;

    private Integer studentNum03;

    private Integer studentNum04;

    private Integer studentCapacity;

    private Integer graduatedStuNum;

    private String stuDominantNationality;

    private String stuYearInvestment;

    private String clubNum;

    private String presidentCountry;

    private Integer staffNum;

    private Integer teacherNum;

    private Integer foreignTeacherNum;

    private String teacherYearInvestment;

    private String teacherRetention;

    private String teacherSalary;

    private String teacherStuRatio;

    private String coveredArea;

    private String builtArea;

    private String hardware;

    private String investment;

    private String remark;

    private String recentModifier;

    private String loadPeople;

    private String loadTime;

    private String un09;

    private String un10;

    private String schoolLogo;

    private String schoolShow;

    private String img02;

    private String img03;

    private String img04;

    private String img05;

    private String batchinputSign;

    private String verifysign;

    private String evaluate;

    private SchoolCertification schoolCertification;

    public School(Integer id, String schoolName, String schoolEnglishname, String schoolProperties, String areas, String areas02, String areas03, Integer foundedTime, String operationstate, String schoolSystem, String tuition01, String tuition02, String tuition03, String tuition04, String tuitionhigh, String website, String telephone, Integer interCourseFoundedTime, String course, String authentication, String courseEvaluation, Integer studentNumAll, Integer studentNum01, Integer studentNum02, Integer studentNum03, Integer studentNum04, Integer studentCapacity, Integer graduatedStuNum, String stuDominantNationality, String stuYearInvestment, String clubNum, String presidentCountry, Integer staffNum, Integer teacherNum, Integer foreignTeacherNum, String teacherYearInvestment, String teacherRetention, String teacherSalary, String teacherStuRatio, String coveredArea, String builtArea, String hardware, String investment, String remark, String recentModifier, String loadPeople, String loadTime, String un09, String un10, String schoolLogo, String schoolShow, String img02, String img03, String img04, String img05, String batchinputSign, String verifysign, String evaluate, SchoolCertification schoolCertification) {
        this.id = id;
        this.schoolName = schoolName;
        this.schoolEnglishname = schoolEnglishname;
        this.schoolProperties = schoolProperties;
        this.areas = areas;
        this.areas02 = areas02;
        this.areas03 = areas03;
        this.foundedTime = foundedTime;
        this.operationstate = operationstate;
        this.schoolSystem = schoolSystem;
        this.tuition01 = tuition01;
        this.tuition02 = tuition02;
        this.tuition03 = tuition03;
        this.tuition04 = tuition04;
        this.tuitionhigh = tuitionhigh;
        this.website = website;
        this.telephone = telephone;
        this.interCourseFoundedTime = interCourseFoundedTime;
        this.course = course;
        this.authentication = authentication;
        this.courseEvaluation = courseEvaluation;
        this.studentNumAll = studentNumAll;
        this.studentNum01 = studentNum01;
        this.studentNum02 = studentNum02;
        this.studentNum03 = studentNum03;
        this.studentNum04 = studentNum04;
        this.studentCapacity = studentCapacity;
        this.graduatedStuNum = graduatedStuNum;
        this.stuDominantNationality = stuDominantNationality;
        this.stuYearInvestment = stuYearInvestment;
        this.clubNum = clubNum;
        this.presidentCountry = presidentCountry;
        this.staffNum = staffNum;
        this.teacherNum = teacherNum;
        this.foreignTeacherNum = foreignTeacherNum;
        this.teacherYearInvestment = teacherYearInvestment;
        this.teacherRetention = teacherRetention;
        this.teacherSalary = teacherSalary;
        this.teacherStuRatio = teacherStuRatio;
        this.coveredArea = coveredArea;
        this.builtArea = builtArea;
        this.hardware = hardware;
        this.investment = investment;
        this.remark = remark;
        this.recentModifier = recentModifier;
        this.loadPeople = loadPeople;
        this.loadTime = loadTime;
        this.un09 = un09;
        this.un10 = un10;
        this.schoolLogo = schoolLogo;
        this.schoolShow = schoolShow;
        this.img02 = img02;
        this.img03 = img03;
        this.img04 = img04;
        this.img05 = img05;
        this.batchinputSign = batchinputSign;
        this.verifysign = verifysign;
        this.evaluate = evaluate;
        this.schoolCertification = schoolCertification;
    }

    public School() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName == null ? null : schoolName.trim();
    }

    public String getSchoolEnglishname() {
        return schoolEnglishname;
    }

    public void setSchoolEnglishname(String schoolEnglishname) {
        this.schoolEnglishname = schoolEnglishname == null ? null : schoolEnglishname.trim();
    }

    public String getSchoolProperties() {
        return schoolProperties;
    }

    public void setSchoolProperties(String schoolProperties) {
        this.schoolProperties = schoolProperties == null ? null : schoolProperties.trim();
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas == null ? null : areas.trim();
    }

    public String getAreas02() {
        return areas02;
    }

    public void setAreas02(String areas02) {
        this.areas02 = areas02 == null ? null : areas02.trim();
    }

    public String getAreas03() {
        return areas03;
    }

    public void setAreas03(String areas03) {
        this.areas03 = areas03 == null ? null : areas03.trim();
    }

    public Integer getFoundedTime() {
        return foundedTime;
    }

    public void setFoundedTime(Integer foundedTime) {
        this.foundedTime = foundedTime;
    }

    public String getOperationstate() {
        return operationstate;
    }

    public void setOperationstate(String operationstate) {
        this.operationstate = operationstate == null ? null : operationstate.trim();
    }

    public String getSchoolSystem() {
        return schoolSystem;
    }

    public void setSchoolSystem(String schoolSystem) {
        this.schoolSystem = schoolSystem == null ? null : schoolSystem.trim();
    }

    public String getTuition01() {
        return tuition01;
    }

    public void setTuition01(String tuition01) {
        this.tuition01 = tuition01 == null ? null : tuition01.trim();
    }

    public String getTuition02() {
        return tuition02;
    }

    public void setTuition02(String tuition02) {
        this.tuition02 = tuition02 == null ? null : tuition02.trim();
    }

    public String getTuition03() {
        return tuition03;
    }

    public void setTuition03(String tuition03) {
        this.tuition03 = tuition03 == null ? null : tuition03.trim();
    }

    public String getTuition04() {
        return tuition04;
    }

    public void setTuition04(String tuition04) {
        this.tuition04 = tuition04 == null ? null : tuition04.trim();
    }

    public String getTuitionhigh() {
        return tuitionhigh;
    }

    public void setTuitionhigh(String tuitionhigh) {
        this.tuitionhigh = tuitionhigh == null ? null : tuitionhigh.trim();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website == null ? null : website.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public Integer getInterCourseFoundedTime() {
        return interCourseFoundedTime;
    }

    public void setInterCourseFoundedTime(Integer interCourseFoundedTime) {
        this.interCourseFoundedTime = interCourseFoundedTime;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course == null ? null : course.trim();
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication == null ? null : authentication.trim();
    }

    public String getCourseEvaluation() {
        return courseEvaluation;
    }

    public void setCourseEvaluation(String courseEvaluation) {
        this.courseEvaluation = courseEvaluation == null ? null : courseEvaluation.trim();
    }

    public Integer getStudentNumAll() {
        return studentNumAll;
    }

    public void setStudentNumAll(Integer studentNumAll) {
        this.studentNumAll = studentNumAll;
    }

    public Integer getStudentNum01() {
        return studentNum01;
    }

    public void setStudentNum01(Integer studentNum01) {
        this.studentNum01 = studentNum01;
    }

    public Integer getStudentNum02() {
        return studentNum02;
    }

    public void setStudentNum02(Integer studentNum02) {
        this.studentNum02 = studentNum02;
    }

    public Integer getStudentNum03() {
        return studentNum03;
    }

    public void setStudentNum03(Integer studentNum03) {
        this.studentNum03 = studentNum03;
    }

    public Integer getStudentNum04() {
        return studentNum04;
    }

    public void setStudentNum04(Integer studentNum04) {
        this.studentNum04 = studentNum04;
    }

    public Integer getStudentCapacity() {
        return studentCapacity;
    }

    public void setStudentCapacity(Integer studentCapacity) {
        this.studentCapacity = studentCapacity;
    }

    public Integer getGraduatedStuNum() {
        return graduatedStuNum;
    }

    public void setGraduatedStuNum(Integer graduatedStuNum) {
        this.graduatedStuNum = graduatedStuNum;
    }

    public String getStuDominantNationality() {
        return stuDominantNationality;
    }

    public void setStuDominantNationality(String stuDominantNationality) {
        this.stuDominantNationality = stuDominantNationality == null ? null : stuDominantNationality.trim();
    }

    public String getStuYearInvestment() {
        return stuYearInvestment;
    }

    public void setStuYearInvestment(String stuYearInvestment) {
        this.stuYearInvestment = stuYearInvestment == null ? null : stuYearInvestment.trim();
    }

    public String getClubNum() {
        return clubNum;
    }

    public void setClubNum(String clubNum) {
        this.clubNum = clubNum == null ? null : clubNum.trim();
    }

    public String getPresidentCountry() {
        return presidentCountry;
    }

    public void setPresidentCountry(String presidentCountry) {
        this.presidentCountry = presidentCountry == null ? null : presidentCountry.trim();
    }

    public Integer getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(Integer staffNum) {
        this.staffNum = staffNum;
    }

    public Integer getTeacherNum() {
        return teacherNum;
    }

    public void setTeacherNum(Integer teacherNum) {
        this.teacherNum = teacherNum;
    }

    public Integer getForeignTeacherNum() {
        return foreignTeacherNum;
    }

    public void setForeignTeacherNum(Integer foreignTeacherNum) {
        this.foreignTeacherNum = foreignTeacherNum;
    }

    public String getTeacherYearInvestment() {
        return teacherYearInvestment;
    }

    public void setTeacherYearInvestment(String teacherYearInvestment) {
        this.teacherYearInvestment = teacherYearInvestment == null ? null : teacherYearInvestment.trim();
    }

    public String getTeacherRetention() {
        return teacherRetention;
    }

    public void setTeacherRetention(String teacherRetention) {
        this.teacherRetention = teacherRetention == null ? null : teacherRetention.trim();
    }

    public String getTeacherSalary() {
        return teacherSalary;
    }

    public void setTeacherSalary(String teacherSalary) {
        this.teacherSalary = teacherSalary == null ? null : teacherSalary.trim();
    }

    public String getTeacherStuRatio() {
        return teacherStuRatio;
    }

    public void setTeacherStuRatio(String teacherStuRatio) {
        this.teacherStuRatio = teacherStuRatio == null ? null : teacherStuRatio.trim();
    }

    public String getCoveredArea() {
        return coveredArea;
    }

    public void setCoveredArea(String coveredArea) {
        this.coveredArea = coveredArea == null ? null : coveredArea.trim();
    }

    public String getBuiltArea() {
        return builtArea;
    }

    public void setBuiltArea(String builtArea) {
        this.builtArea = builtArea == null ? null : builtArea.trim();
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware == null ? null : hardware.trim();
    }

    public String getInvestment() {
        return investment;
    }

    public void setInvestment(String investment) {
        this.investment = investment == null ? null : investment.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getRecentModifier() {
        return recentModifier;
    }

    public void setRecentModifier(String recentModifier) {
        this.recentModifier = recentModifier == null ? null : recentModifier.trim();
    }

    public String getLoadPeople() {
        return loadPeople;
    }

    public void setLoadPeople(String loadPeople) {
        this.loadPeople = loadPeople == null ? null : loadPeople.trim();
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime == null ? null : loadTime.trim();
    }

    public String getUn09() {
        return un09;
    }

    public void setUn09(String un09) {
        this.un09 = un09 == null ? null : un09.trim();
    }

    public String getUn10() {
        return un10;
    }

    public void setUn10(String un10) {
        this.un10 = un10 == null ? null : un10.trim();
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo == null ? null : schoolLogo.trim();
    }

    public String getSchoolShow() {
        return schoolShow;
    }

    public void setSchoolShow(String schoolShow) {
        this.schoolShow = schoolShow == null ? null : schoolShow.trim();
    }

    public String getImg02() {
        return img02;
    }

    public void setImg02(String img02) {
        this.img02 = img02 == null ? null : img02.trim();
    }

    public String getImg03() {
        return img03;
    }

    public void setImg03(String img03) {
        this.img03 = img03 == null ? null : img03.trim();
    }

    public String getImg04() {
        return img04;
    }

    public void setImg04(String img04) {
        this.img04 = img04 == null ? null : img04.trim();
    }

    public String getImg05() {
        return img05;
    }

    public void setImg05(String img05) {
        this.img05 = img05 == null ? null : img05.trim();
    }

    public String getBatchinputSign() {
        return batchinputSign;
    }

    public void setBatchinputSign(String batchinputSign) {
        this.batchinputSign = batchinputSign == null ? null : batchinputSign.trim();
    }

    public String getVerifysign() {
        return verifysign;
    }

    public void setVerifysign(String verifysign) {
        this.verifysign = verifysign == null ? null : verifysign.trim();
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate == null ? null : evaluate.trim();
    }

    public SchoolCertification getSchoolCertification() {
        return schoolCertification;
    }

    public void setSchoolCertification(SchoolCertification schoolCertification) {
        this.schoolCertification = schoolCertification;
    }
}