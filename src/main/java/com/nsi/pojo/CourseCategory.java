package com.nsi.pojo;

public class CourseCategory {
    private Integer id;

    private Integer courseId;

    private Integer listId;

    private String courseName;

    private String courseTitle;

    private Integer price;

    private String duration;

    private String courseInstructor;

    private Integer status;

    private Integer pattern;

    private String courseAddress;

    public CourseCategory(Integer id, Integer courseId, Integer listId, String courseName, String courseTitle, Integer price, String duration, String courseInstructor, Integer status, Integer pattern, String courseAddress) {
        this.id = id;
        this.courseId = courseId;
        this.listId = listId;
        this.courseName = courseName;
        this.courseTitle = courseTitle;
        this.price = price;
        this.duration = duration;
        this.courseInstructor = courseInstructor;
        this.status = status;
        this.pattern = pattern;
        this.courseAddress = courseAddress;
    }

    public CourseCategory() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle == null ? null : courseTitle.trim();
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration == null ? null : duration.trim();
    }

    public String getCourseInstructor() {
        return courseInstructor;
    }

    public void setCourseInstructor(String courseInstructor) {
        this.courseInstructor = courseInstructor == null ? null : courseInstructor.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPattern() {
        return pattern;
    }

    public void setPattern(Integer pattern) {
        this.pattern = pattern;
    }

    public String getCourseAddress() {
        return courseAddress;
    }

    public void setCourseAddress(String courseAddress) {
        this.courseAddress = courseAddress == null ? null : courseAddress.trim();
    }
}