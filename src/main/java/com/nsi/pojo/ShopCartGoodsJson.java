package com.nsi.pojo;

import lombok.Data;

/**
 * @author Li Yan
 * @create 2019-03-05
 **/
@Data
public class ShopCartGoodsJson {
    private String goodsId;

    private String goodsName;

    private String goodsPrice;

    private String goodsNum;

    private String goodsImg;

}
