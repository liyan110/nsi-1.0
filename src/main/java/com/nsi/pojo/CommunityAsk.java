package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 提问互动-提问表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@TableName("community_ask")
public class CommunityAsk extends Model<CommunityAsk> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息人姓名
     */
    @TableField("message_name")
    private String messageName;

    /**
     * 消息人头像
     */
    @TableField("message_portrait")
    private String messagePortrait;

    /**
     * 消息人微信id
     */
    @TableField("message_wechat_id")
    private String messageWechatId;

    /**
     * 消息 内容
     */
    @TableField("message_content")
    private String messageContent;

    /**
     * 对象用户微信id
     */
    @TableField("object_wechat_id")
    private String objectWechatId;

    /**
     * 是否被查看（0未查看 1已查看）
     */
    @TableField("check_state")
    private Integer checkState;

    /**
     * 消息查看时间
     */
    @TableField("check_time")
    private Date checkTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }
    public String getMessagePortrait() {
        return messagePortrait;
    }

    public void setMessagePortrait(String messagePortrait) {
        this.messagePortrait = messagePortrait;
    }
    public String getMessageWechatId() {
        return messageWechatId;
    }

    public void setMessageWechatId(String messageWechatId) {
        this.messageWechatId = messageWechatId;
    }
    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getObjectWechatId() {
        return objectWechatId;
    }

    public void setObjectWechatId(String objectWechatId) {
        this.objectWechatId = objectWechatId;
    }

    public Integer getCheckState() {
        return checkState;
    }

    public void setCheckState(Integer checkState) {
        this.checkState = checkState;
    }
    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CommunityAsk{" +
        "id=" + id +
        ", messageName=" + messageName +
        ", messagePortrait=" + messagePortrait +
        ", messageWechatId=" + messageWechatId +
        ", messageContent=" + messageContent +
        ", objectWechatId=" + objectWechatId +
        ", checkState=" + checkState +
        ", checkTime=" + checkTime +
        ", createTime=" + createTime +
        "}";
    }
}
