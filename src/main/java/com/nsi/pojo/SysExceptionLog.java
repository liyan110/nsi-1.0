package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.nsi.aop.DataSource;
import lombok.Data;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-10-21
 **/
@Data
@TableName("nsi_sys_exception_log")
public class SysExceptionLog {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("api_url")
    private String api_url;

    @TableField("request_data")
    private String request_data;

    @TableField("err_msg")
    private String err_msg;

    @TableField("html_url")
    private String html_url;

    private String env;

    @TableField("create_time")
    private Date create_time;

}
