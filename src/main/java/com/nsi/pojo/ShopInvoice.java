package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("nsi_invoice")
public class ShopInvoice implements Serializable {

    private static final long serialVersionUID = 2904539092557671840L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "user_invoice_name")
    private String userInvoiceName;

    @TableField(value = "user_invoice_num")
    private String userInvoiceNum;

    @TableField(value = "user_billing_type")
    private String userBillingType;

    @TableField(value = "usermail")
    private String usermail;

    @TableField(value = "username")
    private String username;

    @TableField(value = "user_order_num")
    private String userOrderNum;

    @TableField(value = "user_bank_addr")
    private String userBankAddr;

    @TableField(value = "user_reserve_phone")
    private String userReservePhone;

    @TableField(value = "account_num")
    private String accountNum;

    @TableField(value = "email_address")
    private String emailAddress;

    @TableField(value = "manage_name")
    private String manageName;

    @TableField(value = "manage_project_name")
    private String manageProjectName;

    @TableField(value = "manage_payment_method")
    private String managePaymentMethod;

    @TableField(value = "manage_money")
    private String manageMoney;

    @TableField(value = "manage_state")
    private String manageState;

    @TableField(value = "finance_name")
    private String financeName;

    @TableField(value = "finance_state")
    private String financeState;

    @TableField(value = "invoice_type")
    private Integer invoiceType;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "wechat_id")
    private String wechatId;
}