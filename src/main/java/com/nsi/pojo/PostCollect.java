package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Li Yan1
 * @since 2019-12-31
 */
@TableName("nsi_post_collect")
@Data
public class PostCollect implements Serializable {


    private static final long serialVersionUID = 995740245181910394L;
    /**
     * 逻辑主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户身份标记
     */
    @TableField("open_id")
    private String openId;

    /**
     * 帖子Id
     */
    @TableField("item_id")
    private Integer itemId;


}
