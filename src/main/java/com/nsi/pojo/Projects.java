package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("nsi_project_data")
public class Projects {

    @TableId
    @TableField("Id")
    private Integer id;
    @TableField("SubjectName")
    private String subjectname;
    @TableField("Areas")
    private String areas;
    @TableField("Areas02")
    private String areas02;
    @TableField("Areas03")
    private String areas03;
    @TableField("Company")
    private String company;
    @TableField("SubjectLabel")
    private String subjectlabel;
    @TableField("Name")
    private String name;
    @TableField("Phone")
    private String phone;
    @TableField("Mail")
    private String mail;
    @TableField("SubjectIntroduction")
    private String subjectintroduction;
    @TableField("DetailInstitution")
    private String detailinstitution;
    @TableField("Requirement")
    private String requirement;
    @TableField("UserMail")
    private String usermail;
    @TableField("Load_time")
    private String loadTime;
    @TableField("Subject_logo")
    private String subjectLogo;
    @TableField("Subject_Show")
    private String subjectShow;

}