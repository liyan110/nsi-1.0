package com.nsi.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private Integer id;

    private String teachername;

    private String teacherdescription;

    private String teachercourse;

    private String teacherimage;

    private String html01;

    private String loadPeople;

    private String loadTime;

}