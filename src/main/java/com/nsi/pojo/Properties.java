package com.nsi.pojo;

public class Properties {
    private Integer id;

    private String teacherid;

    private String courseid;

    private String courseidPlayback;

    private String playbackUrl;

    public Properties(Integer id, String teacherid, String courseid, String courseidPlayback, String playbackUrl) {
        this.id = id;
        this.teacherid = teacherid;
        this.courseid = courseid;
        this.courseidPlayback = courseidPlayback;
        this.playbackUrl = playbackUrl;
    }

    public Properties() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(String teacherid) {
        this.teacherid = teacherid == null ? null : teacherid.trim();
    }

    public String getCourseid() {
        return courseid;
    }

    public void setCourseid(String courseid) {
        this.courseid = courseid == null ? null : courseid.trim();
    }

    public String getCourseidPlayback() {
        return courseidPlayback;
    }

    public void setCourseidPlayback(String courseidPlayback) {
        this.courseidPlayback = courseidPlayback == null ? null : courseidPlayback.trim();
    }

    public String getPlaybackUrl() {
        return playbackUrl;
    }

    public void setPlaybackUrl(String playbackUrl) {
        this.playbackUrl = playbackUrl == null ? null : playbackUrl.trim();
    }
}