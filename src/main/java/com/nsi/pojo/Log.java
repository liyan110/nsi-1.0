package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("nsi_log")
public class Log implements Serializable {
    private static final long serialVersionUID = 8850221672528578276L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String sign;

    private String index01;

    private Date index02;

    private String index03;

    private String index04;

    private String index05;

    private String index06;

    private String index07;

    private String index08;

    private String index09;

    private String index10;

}