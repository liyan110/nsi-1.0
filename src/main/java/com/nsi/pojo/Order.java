package com.nsi.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Order implements Serializable {

    private Integer id;

    private Long orderNo;

    private String wechatId;

    private String unionId;

    private String username;

    private Integer goodsId;

    private String productName;

    private String productType;

    private Integer payment;

    private Integer quantity;

    private Integer totalPrice;

    private Integer status;

    private Date paymentTime;

    private String buyerMessage;

    private String shippingCode;

    private Integer addressId;

    private Date createTime;

    private Date updateTime;

}