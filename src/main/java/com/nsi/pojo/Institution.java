package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName("nsi_institution_data")
@Data
public class Institution implements Serializable {


    private static final long serialVersionUID = 6826801463004909046L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 机构名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 成立时间
     */
    @TableField("found_time")
    private Integer foundTime;

    /**
     * 地区
     */
    @TableField(value = "areas")
    private String areas;
    @TableField(value = "areas02")
    private String areas02;
    @TableField(value = "areas03")
    private String areas03;

    /**
     * 机构类型
     */
    @TableField(value = "type")
    private String type;

    /**
     * 标签
     */
    @TableField(value = "label")
    private String label;

    /**
     * 网站
     */
    @TableField("web_site")
    private String webSite;

    /**
     * 一句话服务
     */
    @TableField("service")
    private String service;

    @TableField("contact_position")
    private String contactPosition;

    @TableField("contact_name")
    private String contactName;

    @TableField("contact_phone")
    private String contactPhone;

    @TableField("contact_mail")
    private String contactMail;

    /**
     * 介绍
     */
    @TableField("introduction")
    private String introduction;

    /**
     * 投资信息
     */
    @TableField("investment")
    private String investment;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 服务过的学校
     */
    @TableField("served_school")
    private String servedSchool;

    @TableField("institution_logo")
    private String institutionLogo;

    @TableField("institution_show")
    private String institutionShow;

    @TableField("img02")
    private String img02;

    @TableField("img03")
    private String img03;
    @TableField("img04")
    private String img04;
    @TableField("img05")
    private String img05;

    @TableField("recent_modifier")
    private String recentModifier;

    @TableField("load_people")
    private String loadPeople;

    @TableField("load_time")
    private Date loadTime;

    @TableField("batchInput_sign")
    private String batchinputSign;

    /**
     * 审核标记
     */
    @TableField("verify_sign")
    private Integer verifySign;

    /**
     * 用户评价
     */
    @TableField("evaluate")
    private String evaluate;

}