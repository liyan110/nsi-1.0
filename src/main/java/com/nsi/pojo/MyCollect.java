package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 四库全书-我的收藏表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-09-02
 */
@Data
@TableName("nsi_my_collect")
public class MyCollect implements Serializable {


    private static final long serialVersionUID = 4059255846787831154L;
    /**
     * 逻辑id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 微信标识
     */
    @TableField("open_id")
    private String openId;

    /**
     * 收藏学校id/机构id
     */
    @TableField("c_id")
    private Integer cId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 收藏类型 1-学校库 2-机构库
     */
    @TableField("collect_type")
    private Integer collectType;
}
