package com.nsi.pojo;

public class Activity {
    private Integer id;

    private String title1;

    private String content1;

    private String title2;

    private String content2;

    private String title3;

    private String content3;

    private String title4;

    private String content4;

    private String title5;

    private String content5;

    private String title6;

    private String content6;

    private String title7;

    private String content7;

    private String title8;

    private String content8;

    private String title9;

    private String content9;

    private String title10;

    private String content10;

    private String title11;

    private String content11;

    private String title12;

    private String content12;

    private String title13;

    private String content13;

    private String title14;

    private String content14;

    private String title15;

    private String content15;

    private String title16;

    private String content16;

    private String title17;

    private String content17;

    private String title18;

    private String content18;

    private String title19;

    private String content19;

    private String title20;

    private String content20;

    private String deadline;

    private String loadTime;

    public Activity(Integer id, String title1, String content1, String title2, String content2, String title3, String content3, String title4, String content4, String title5, String content5, String title6, String content6, String title7, String content7, String title8, String content8, String title9, String content9, String title10, String content10, String title11, String content11, String title12, String content12, String title13, String content13, String title14, String content14, String title15, String content15, String title16, String content16, String title17, String content17, String title18, String content18, String title19, String content19, String title20, String content20, String deadline, String loadTime) {
        this.id = id;
        this.title1 = title1;
        this.content1 = content1;
        this.title2 = title2;
        this.content2 = content2;
        this.title3 = title3;
        this.content3 = content3;
        this.title4 = title4;
        this.content4 = content4;
        this.title5 = title5;
        this.content5 = content5;
        this.title6 = title6;
        this.content6 = content6;
        this.title7 = title7;
        this.content7 = content7;
        this.title8 = title8;
        this.content8 = content8;
        this.title9 = title9;
        this.content9 = content9;
        this.title10 = title10;
        this.content10 = content10;
        this.title11 = title11;
        this.content11 = content11;
        this.title12 = title12;
        this.content12 = content12;
        this.title13 = title13;
        this.content13 = content13;
        this.title14 = title14;
        this.content14 = content14;
        this.title15 = title15;
        this.content15 = content15;
        this.title16 = title16;
        this.content16 = content16;
        this.title17 = title17;
        this.content17 = content17;
        this.title18 = title18;
        this.content18 = content18;
        this.title19 = title19;
        this.content19 = content19;
        this.title20 = title20;
        this.content20 = content20;
        this.deadline = deadline;
        this.loadTime = loadTime;
    }

    public Activity() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1 == null ? null : title1.trim();
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1 == null ? null : content1.trim();
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2 == null ? null : title2.trim();
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2 == null ? null : content2.trim();
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3 == null ? null : title3.trim();
    }

    public String getContent3() {
        return content3;
    }

    public void setContent3(String content3) {
        this.content3 = content3 == null ? null : content3.trim();
    }

    public String getTitle4() {
        return title4;
    }

    public void setTitle4(String title4) {
        this.title4 = title4 == null ? null : title4.trim();
    }

    public String getContent4() {
        return content4;
    }

    public void setContent4(String content4) {
        this.content4 = content4 == null ? null : content4.trim();
    }

    public String getTitle5() {
        return title5;
    }

    public void setTitle5(String title5) {
        this.title5 = title5 == null ? null : title5.trim();
    }

    public String getContent5() {
        return content5;
    }

    public void setContent5(String content5) {
        this.content5 = content5 == null ? null : content5.trim();
    }

    public String getTitle6() {
        return title6;
    }

    public void setTitle6(String title6) {
        this.title6 = title6 == null ? null : title6.trim();
    }

    public String getContent6() {
        return content6;
    }

    public void setContent6(String content6) {
        this.content6 = content6 == null ? null : content6.trim();
    }

    public String getTitle7() {
        return title7;
    }

    public void setTitle7(String title7) {
        this.title7 = title7 == null ? null : title7.trim();
    }

    public String getContent7() {
        return content7;
    }

    public void setContent7(String content7) {
        this.content7 = content7 == null ? null : content7.trim();
    }

    public String getTitle8() {
        return title8;
    }

    public void setTitle8(String title8) {
        this.title8 = title8 == null ? null : title8.trim();
    }

    public String getContent8() {
        return content8;
    }

    public void setContent8(String content8) {
        this.content8 = content8 == null ? null : content8.trim();
    }

    public String getTitle9() {
        return title9;
    }

    public void setTitle9(String title9) {
        this.title9 = title9 == null ? null : title9.trim();
    }

    public String getContent9() {
        return content9;
    }

    public void setContent9(String content9) {
        this.content9 = content9 == null ? null : content9.trim();
    }

    public String getTitle10() {
        return title10;
    }

    public void setTitle10(String title10) {
        this.title10 = title10 == null ? null : title10.trim();
    }

    public String getContent10() {
        return content10;
    }

    public void setContent10(String content10) {
        this.content10 = content10 == null ? null : content10.trim();
    }

    public String getTitle11() {
        return title11;
    }

    public void setTitle11(String title11) {
        this.title11 = title11 == null ? null : title11.trim();
    }

    public String getContent11() {
        return content11;
    }

    public void setContent11(String content11) {
        this.content11 = content11 == null ? null : content11.trim();
    }

    public String getTitle12() {
        return title12;
    }

    public void setTitle12(String title12) {
        this.title12 = title12 == null ? null : title12.trim();
    }

    public String getContent12() {
        return content12;
    }

    public void setContent12(String content12) {
        this.content12 = content12 == null ? null : content12.trim();
    }

    public String getTitle13() {
        return title13;
    }

    public void setTitle13(String title13) {
        this.title13 = title13 == null ? null : title13.trim();
    }

    public String getContent13() {
        return content13;
    }

    public void setContent13(String content13) {
        this.content13 = content13 == null ? null : content13.trim();
    }

    public String getTitle14() {
        return title14;
    }

    public void setTitle14(String title14) {
        this.title14 = title14 == null ? null : title14.trim();
    }

    public String getContent14() {
        return content14;
    }

    public void setContent14(String content14) {
        this.content14 = content14 == null ? null : content14.trim();
    }

    public String getTitle15() {
        return title15;
    }

    public void setTitle15(String title15) {
        this.title15 = title15 == null ? null : title15.trim();
    }

    public String getContent15() {
        return content15;
    }

    public void setContent15(String content15) {
        this.content15 = content15 == null ? null : content15.trim();
    }

    public String getTitle16() {
        return title16;
    }

    public void setTitle16(String title16) {
        this.title16 = title16 == null ? null : title16.trim();
    }

    public String getContent16() {
        return content16;
    }

    public void setContent16(String content16) {
        this.content16 = content16 == null ? null : content16.trim();
    }

    public String getTitle17() {
        return title17;
    }

    public void setTitle17(String title17) {
        this.title17 = title17 == null ? null : title17.trim();
    }

    public String getContent17() {
        return content17;
    }

    public void setContent17(String content17) {
        this.content17 = content17 == null ? null : content17.trim();
    }

    public String getTitle18() {
        return title18;
    }

    public void setTitle18(String title18) {
        this.title18 = title18 == null ? null : title18.trim();
    }

    public String getContent18() {
        return content18;
    }

    public void setContent18(String content18) {
        this.content18 = content18 == null ? null : content18.trim();
    }

    public String getTitle19() {
        return title19;
    }

    public void setTitle19(String title19) {
        this.title19 = title19 == null ? null : title19.trim();
    }

    public String getContent19() {
        return content19;
    }

    public void setContent19(String content19) {
        this.content19 = content19 == null ? null : content19.trim();
    }

    public String getTitle20() {
        return title20;
    }

    public void setTitle20(String title20) {
        this.title20 = title20 == null ? null : title20.trim();
    }

    public String getContent20() {
        return content20;
    }

    public void setContent20(String content20) {
        this.content20 = content20 == null ? null : content20.trim();
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline == null ? null : deadline.trim();
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime == null ? null : loadTime.trim();
    }
}