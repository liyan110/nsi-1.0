package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Luo Zhen
 */
@Data
@TableName("nsi_course_list")
public class CourseList implements Serializable {

    private static final long serialVersionUID = 933973615256789935L;

    @TableId(value = "list_id", type = IdType.AUTO)
    private Integer listId;

    @TableField("list_img")
    private String listImg;

    @TableField("list_title")
    private String listTitle;

    private String syllabus;

    @TableField("list_price")
    private Integer listPrice;

    @TableField("list_theme")
    private String listTheme;

    private String lecturer;

    private Integer status;

    private Integer pattern;

    @TableField("list_description")
    private String listDescription;

    @TableField("list_type")
    private String listType;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


}