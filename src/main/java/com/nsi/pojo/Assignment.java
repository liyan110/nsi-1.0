package com.nsi.pojo;

public class Assignment {
    private Integer id;

    private Integer courseId;

    private String usermail;

    private String assignmentContent;

    private String attachmentNameOne;

    private String attachmentUrlOne;

    private String attachmentNameTwo;

    private String attachmentUrlTwo;

    private String attachmentNameThree;

    private String attachmentUrlThree;

    private String teacherId;

    private Integer verify;

    private Long createTime;

    public Assignment(Integer id, Integer courseId, String usermail, String assignmentContent, String attachmentNameOne, String attachmentUrlOne, String attachmentNameTwo, String attachmentUrlTwo, String attachmentNameThree, String attachmentUrlThree, String teacherId, Integer verify, Long createTime) {
        this.id = id;
        this.courseId = courseId;
        this.usermail = usermail;
        this.assignmentContent = assignmentContent;
        this.attachmentNameOne = attachmentNameOne;
        this.attachmentUrlOne = attachmentUrlOne;
        this.attachmentNameTwo = attachmentNameTwo;
        this.attachmentUrlTwo = attachmentUrlTwo;
        this.attachmentNameThree = attachmentNameThree;
        this.attachmentUrlThree = attachmentUrlThree;
        this.teacherId = teacherId;
        this.verify = verify;
        this.createTime = createTime;
    }

    public Assignment() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail == null ? null : usermail.trim();
    }

    public String getAssignmentContent() {
        return assignmentContent;
    }

    public void setAssignmentContent(String assignmentContent) {
        this.assignmentContent = assignmentContent == null ? null : assignmentContent.trim();
    }

    public String getAttachmentNameOne() {
        return attachmentNameOne;
    }

    public void setAttachmentNameOne(String attachmentNameOne) {
        this.attachmentNameOne = attachmentNameOne == null ? null : attachmentNameOne.trim();
    }

    public String getAttachmentUrlOne() {
        return attachmentUrlOne;
    }

    public void setAttachmentUrlOne(String attachmentUrlOne) {
        this.attachmentUrlOne = attachmentUrlOne == null ? null : attachmentUrlOne.trim();
    }

    public String getAttachmentNameTwo() {
        return attachmentNameTwo;
    }

    public void setAttachmentNameTwo(String attachmentNameTwo) {
        this.attachmentNameTwo = attachmentNameTwo == null ? null : attachmentNameTwo.trim();
    }

    public String getAttachmentUrlTwo() {
        return attachmentUrlTwo;
    }

    public void setAttachmentUrlTwo(String attachmentUrlTwo) {
        this.attachmentUrlTwo = attachmentUrlTwo == null ? null : attachmentUrlTwo.trim();
    }

    public String getAttachmentNameThree() {
        return attachmentNameThree;
    }

    public void setAttachmentNameThree(String attachmentNameThree) {
        this.attachmentNameThree = attachmentNameThree == null ? null : attachmentNameThree.trim();
    }

    public String getAttachmentUrlThree() {
        return attachmentUrlThree;
    }

    public void setAttachmentUrlThree(String attachmentUrlThree) {
        this.attachmentUrlThree = attachmentUrlThree == null ? null : attachmentUrlThree.trim();
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId == null ? null : teacherId.trim();
    }

    public Integer getVerify() {
        return verify;
    }

    public void setVerify(Integer verify) {
        this.verify = verify;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}