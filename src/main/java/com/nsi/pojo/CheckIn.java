package com.nsi.pojo;

import java.util.Date;

public class CheckIn {
    private Integer id;

    private String name;

    private String type;

    private String company;

    private String position;

    private String phone;

    private String email;

    private String token;

    private String activity;

    private Date createTime;

    private Date checkinTime;

    public CheckIn(Integer id, String name, String type, String company, String position, String phone, String email, String token, String activity, Date createTime, Date checkinTime) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.company = company;
        this.position = position;
        this.phone = phone;
        this.email = email;
        this.token = token;
        this.activity = activity;
        this.createTime = createTime;
        this.checkinTime = checkinTime;
    }

    public CheckIn() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity == null ? null : activity.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(Date checkinTime) {
        this.checkinTime = checkinTime;
    }
}