package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

@TableName("nsi_shop_address")
@Data
public class ShopAddress implements Serializable {

    private static final long serialVersionUID = 3975040288476945505L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("wechatId")
    private String wechatid;

    @TableField("userMail")
    private String usermail;

    @TableField("receiveName")
    private String receivename;

    @TableField("receiveArea01")
    private String receivearea01;

    @TableField("receiveArea02")
    private String receivearea02;

    @TableField("receiveArea03")
    private String receivearea03;

    @TableField("receivePhone")
    private String receivephone;

    @TableField("postCode")
    private String postcode;

    @TableField("union_id")
    private String unionId;

}