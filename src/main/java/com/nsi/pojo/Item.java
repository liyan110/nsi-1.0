package com.nsi.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Item {
    private Integer id;

    private String name;

    private String principal;

    private String customerName;

    private String customerPhone;

    private String customerUnit;

    private String customerLocaltion;

    private String customerPrincipal;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date estimatedTime;

    private String projectBrief;

    private Integer status;

    private Integer actualAmount;

    private String visitPeople;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date visitTime;

    private String interviewResults;

    private String creator;

    private String possibility;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Date createTime;

    private Date updateTime;

    public Item(Integer id, String name, String principal, String customerName, String customerPhone, String customerUnit, String customerLocaltion, String customerPrincipal, Date estimatedTime, String projectBrief, Integer status, Integer actualAmount, String visitPeople, Date visitTime, String interviewResults, String creator, String possibility, Date endTime, Date createTime, Date updateTime) {
        this.id = id;
        this.name = name;
        this.principal = principal;
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.customerUnit = customerUnit;
        this.customerLocaltion = customerLocaltion;
        this.customerPrincipal = customerPrincipal;
        this.estimatedTime = estimatedTime;
        this.projectBrief = projectBrief;
        this.status = status;
        this.actualAmount = actualAmount;
        this.visitPeople = visitPeople;
        this.visitTime = visitTime;
        this.interviewResults = interviewResults;
        this.creator = creator;
        this.possibility = possibility;
        this.endTime = endTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Item() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone == null ? null : customerPhone.trim();
    }

    public String getCustomerUnit() {
        return customerUnit;
    }

    public void setCustomerUnit(String customerUnit) {
        this.customerUnit = customerUnit == null ? null : customerUnit.trim();
    }

    public String getCustomerLocaltion() {
        return customerLocaltion;
    }

    public void setCustomerLocaltion(String customerLocaltion) {
        this.customerLocaltion = customerLocaltion == null ? null : customerLocaltion.trim();
    }

    public String getCustomerPrincipal() {
        return customerPrincipal;
    }

    public void setCustomerPrincipal(String customerPrincipal) {
        this.customerPrincipal = customerPrincipal == null ? null : customerPrincipal.trim();
    }

    public Date getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Date estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getProjectBrief() {
        return projectBrief;
    }

    public void setProjectBrief(String projectBrief) {
        this.projectBrief = projectBrief == null ? null : projectBrief.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(Integer actualAmount) {
        this.actualAmount = actualAmount;
    }

    public String getVisitPeople() {
        return visitPeople;
    }

    public void setVisitPeople(String visitPeople) {
        this.visitPeople = visitPeople == null ? null : visitPeople.trim();
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getInterviewResults() {
        return interviewResults;
    }

    public void setInterviewResults(String interviewResults) {
        this.interviewResults = interviewResults == null ? null : interviewResults.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPossibility() {
        return possibility;
    }

    public void setPossibility(String possibility) {
        this.possibility = possibility;
    }
}