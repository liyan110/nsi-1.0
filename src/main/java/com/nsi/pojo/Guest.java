package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("nsi_guest")
public class Guest implements Serializable {

    private static final long serialVersionUID = 5879057926832276955L;

    @TableId(value = "guest_id", type = IdType.AUTO)
    private Integer guestId;

    @TableField(value = "guest_name")
    private String guestName;

    @TableField(value = "guest_phone")
    private String guestPhone;

    @TableField(value = "guest_post")
    private String guestPost;

    @TableField(value = "guest_company")
    private String guestCompany;

    @TableField(value = "guest_address")
    private String guestAddress;

    @TableField(value = "guest_birthday")
    private String guestBirthday;

    @TableField(value = "birthday_month")
    private String birthdayMonth;

    @TableField(value = "guest_wechat")
    private String guestWechat;

    @TableField(value = "id_card")
    private String idCard;

    @TableField(value = "card_number")
    private String cardNumber;

    @TableField(value = "banks")
    private String banks;

    @TableField(value = "guest_email")
    private String guestEmail;

    @TableField(value = "guest_profile")
    private String guestProfile;

    @TableField(value = "img_addr")
    private String imgAddr;

    @TableField(value = "participants")
    private String participants;

    @TableField(value = "submitter")
    private String submitter;

    @TableField(value = "create_time")
    private Date createTime;

}