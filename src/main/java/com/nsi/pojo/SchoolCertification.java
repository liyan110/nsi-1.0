package com.nsi.pojo;

public class SchoolCertification {
    private Integer id;

    private Integer schoolId;

    private String certificateAuthority;

    private String studentEvaluation;

    private String thirdOrganizations;

    private String courseAuthority;

    private String anyCertification;

    public SchoolCertification(Integer id, Integer schoolId, String certificateAuthority, String studentEvaluation, String thirdOrganizations, String courseAuthority, String anyCertification) {
        this.id = id;
        this.schoolId = schoolId;
        this.certificateAuthority = certificateAuthority;
        this.studentEvaluation = studentEvaluation;
        this.thirdOrganizations = thirdOrganizations;
        this.courseAuthority = courseAuthority;
        this.anyCertification = anyCertification;
    }

    public SchoolCertification() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getCertificateAuthority() {
        return certificateAuthority;
    }

    public void setCertificateAuthority(String certificateAuthority) {
        this.certificateAuthority = certificateAuthority == null ? null : certificateAuthority.trim();
    }

    public String getStudentEvaluation() {
        return studentEvaluation;
    }

    public void setStudentEvaluation(String studentEvaluation) {
        this.studentEvaluation = studentEvaluation == null ? null : studentEvaluation.trim();
    }

    public String getThirdOrganizations() {
        return thirdOrganizations;
    }

    public void setThirdOrganizations(String thirdOrganizations) {
        this.thirdOrganizations = thirdOrganizations == null ? null : thirdOrganizations.trim();
    }

    public String getCourseAuthority() {
        return courseAuthority;
    }

    public void setCourseAuthority(String courseAuthority) {
        this.courseAuthority = courseAuthority == null ? null : courseAuthority.trim();
    }

    public String getAnyCertification() {
        return anyCertification;
    }

    public void setAnyCertification(String anyCertification) {
        this.anyCertification = anyCertification == null ? null : anyCertification.trim();
    }
}