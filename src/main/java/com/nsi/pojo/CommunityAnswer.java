package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 提问互动-回复表
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@Data
@TableName("community_answer")
public class CommunityAnswer extends Model<CommunityAnswer> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 提问id
     */
    @TableField("ask_id")
    private Integer askId;

    /**
     * 消息人姓名
     */
    @TableField("message_name")
    private String messageName;

    /**
     * 消息人头像
     */
    @TableField("message_portrait")
    private String messagePortrait;

    /**
     * 消息人微信id
     */
    @TableField("message_wechat_id")
    private String messageWechatId;

    /**
     * 消息 内容
     */
    @TableField("message_content")
    private String messageContent;

    /**
     * 是否被查看（0未查看 1已查看）
     */
    @TableField("check_state")
    private Integer checkState;

    /**
     * 消息查看时间
     */
    @TableField("check_time")
    private Date checkTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}
