package com.nsi.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Talent {
    private Integer id;

    private String image;

    private String name;

    private String sex;

    private String phone;

    private String mail;

    private String education;

    private String major;

    private String workPlace;

    private String workYear;

    private String expectWorkPlace;

    private String expectWorkPosition;

    private String expectSalary;

    private String entryTime;

    private String workExperience;

    private String educationBackground;

    private String trainingBackground;

    private String isPublic;

    private String verifySign;

    private String havaTalent;

    private String userMail;

    private String other;

    private Date createTime;

}