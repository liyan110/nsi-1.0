package com.nsi.pojo;

import java.util.Date;

/**
 * @author Luo Zhen
 * @create 2018-08-27
 */
public class Coordinate {
    private Integer id;

    private Integer schoolId;

    private String longitude;

    private String latitude;

    private Date createTime;

    public Coordinate(Integer id, Integer schoolId, String longitude, String latitude, Date createTime) {
        this.id = id;
        this.schoolId = schoolId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.createTime = createTime;
    }

    public Coordinate() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}