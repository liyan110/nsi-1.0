package com.nsi.pojo;

public class ClassComment {
    private Integer id;

    private String commentatormail;

    private String commentatorname;

    private String commentatorportrait;

    private String classify;

    private String objectid;

    private String releasetime;

    private String thumbsUp;

    private String content;

    private String verifysign;

    private String identity;

    public ClassComment(Integer id, String commentatormail, String commentatorname, String commentatorportrait, String classify, String objectid, String releasetime, String thumbsUp, String content, String verifysign, String identity) {
        this.id = id;
        this.commentatormail = commentatormail;
        this.commentatorname = commentatorname;
        this.commentatorportrait = commentatorportrait;
        this.classify = classify;
        this.objectid = objectid;
        this.releasetime = releasetime;
        this.thumbsUp = thumbsUp;
        this.content = content;
        this.verifysign = verifysign;
        this.identity = identity;
    }

    public ClassComment() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommentatormail() {
        return commentatormail;
    }

    public void setCommentatormail(String commentatormail) {
        this.commentatormail = commentatormail == null ? null : commentatormail.trim();
    }

    public String getCommentatorname() {
        return commentatorname;
    }

    public void setCommentatorname(String commentatorname) {
        this.commentatorname = commentatorname == null ? null : commentatorname.trim();
    }

    public String getCommentatorportrait() {
        return commentatorportrait;
    }

    public void setCommentatorportrait(String commentatorportrait) {
        this.commentatorportrait = commentatorportrait == null ? null : commentatorportrait.trim();
    }

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify == null ? null : classify.trim();
    }

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid == null ? null : objectid.trim();
    }

    public String getReleasetime() {
        return releasetime;
    }

    public void setReleasetime(String releasetime) {
        this.releasetime = releasetime == null ? null : releasetime.trim();
    }

    public String getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(String thumbsUp) {
        this.thumbsUp = thumbsUp == null ? null : thumbsUp.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getVerifysign() {
        return verifysign;
    }

    public void setVerifysign(String verifysign) {
        this.verifysign = verifysign == null ? null : verifysign.trim();
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity == null ? null : identity.trim();
    }
}