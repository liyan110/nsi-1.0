package com.nsi.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Postil {

    private Integer id;

    private String type;

    private String content;

    private String createPeople;

    private Date createTime;

    private Integer schoolId;

}