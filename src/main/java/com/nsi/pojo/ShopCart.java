package com.nsi.pojo;

import lombok.Data;

@Data
public class ShopCart {
    private Integer id;

    private String userid;

    private String price;

    private String state;

    private String goodsjson;

}