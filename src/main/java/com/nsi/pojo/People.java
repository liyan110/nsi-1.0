package com.nsi.pojo;

public class People {
    private Integer peopleId;

    private String peopleName;

    private String peopleMember;

    private String peopleDuetime;

    private String peopleWork;

    private String peoplePosition;

    private String peoplePhone;

    private String peopleMail;

    private String peopleTag;

    private String peopleTelephone;

    private String peopleWechat;

    private String peopleLoadpeople;

    private String peopleLoadtime;

    private String peopleAddress;

    private String peopleIntroduction;

    private String peopleRemark;

    private String peopleImgurl;

    private String verifysign;

    public People(Integer peopleId, String peopleName, String peopleMember, String peopleDuetime, String peopleWork, String peoplePosition, String peoplePhone, String peopleMail, String peopleTag, String peopleTelephone, String peopleWechat, String peopleLoadpeople, String peopleLoadtime, String peopleAddress, String peopleIntroduction, String peopleRemark, String peopleImgurl, String verifysign) {
        this.peopleId = peopleId;
        this.peopleName = peopleName;
        this.peopleMember = peopleMember;
        this.peopleDuetime = peopleDuetime;
        this.peopleWork = peopleWork;
        this.peoplePosition = peoplePosition;
        this.peoplePhone = peoplePhone;
        this.peopleMail = peopleMail;
        this.peopleTag = peopleTag;
        this.peopleTelephone = peopleTelephone;
        this.peopleWechat = peopleWechat;
        this.peopleLoadpeople = peopleLoadpeople;
        this.peopleLoadtime = peopleLoadtime;
        this.peopleAddress = peopleAddress;
        this.peopleIntroduction = peopleIntroduction;
        this.peopleRemark = peopleRemark;
        this.peopleImgurl = peopleImgurl;
        this.verifysign = verifysign;
    }

    public People() {
        super();
    }

    public Integer getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Integer peopleId) {
        this.peopleId = peopleId;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName == null ? null : peopleName.trim();
    }

    public String getPeopleMember() {
        return peopleMember;
    }

    public void setPeopleMember(String peopleMember) {
        this.peopleMember = peopleMember == null ? null : peopleMember.trim();
    }

    public String getPeopleDuetime() {
        return peopleDuetime;
    }

    public void setPeopleDuetime(String peopleDuetime) {
        this.peopleDuetime = peopleDuetime == null ? null : peopleDuetime.trim();
    }

    public String getPeopleWork() {
        return peopleWork;
    }

    public void setPeopleWork(String peopleWork) {
        this.peopleWork = peopleWork == null ? null : peopleWork.trim();
    }

    public String getPeoplePosition() {
        return peoplePosition;
    }

    public void setPeoplePosition(String peoplePosition) {
        this.peoplePosition = peoplePosition == null ? null : peoplePosition.trim();
    }

    public String getPeoplePhone() {
        return peoplePhone;
    }

    public void setPeoplePhone(String peoplePhone) {
        this.peoplePhone = peoplePhone == null ? null : peoplePhone.trim();
    }

    public String getPeopleMail() {
        return peopleMail;
    }

    public void setPeopleMail(String peopleMail) {
        this.peopleMail = peopleMail == null ? null : peopleMail.trim();
    }

    public String getPeopleTelephone() {
        return peopleTelephone;
    }

    public void setPeopleTelephone(String peopleTelephone) {
        this.peopleTelephone = peopleTelephone == null ? null : peopleTelephone.trim();
    }

    public String getPeopleWechat() {
        return peopleWechat;
    }

    public void setPeopleWechat(String peopleWechat) {
        this.peopleWechat = peopleWechat == null ? null : peopleWechat.trim();
    }

    public String getPeopleLoadpeople() {
        return peopleLoadpeople;
    }

    public void setPeopleLoadpeople(String peopleLoadpeople) {
        this.peopleLoadpeople = peopleLoadpeople == null ? null : peopleLoadpeople.trim();
    }

    public String getPeopleLoadtime() {
        return peopleLoadtime;
    }

    public void setPeopleLoadtime(String peopleLoadtime) {
        this.peopleLoadtime = peopleLoadtime == null ? null : peopleLoadtime.trim();
    }

    public String getPeopleAddress() {
        return peopleAddress;
    }

    public void setPeopleAddress(String peopleAddress) {
        this.peopleAddress = peopleAddress == null ? null : peopleAddress.trim();
    }

    public String getPeopleIntroduction() {
        return peopleIntroduction;
    }

    public void setPeopleIntroduction(String peopleIntroduction) {
        this.peopleIntroduction = peopleIntroduction == null ? null : peopleIntroduction.trim();
    }

    public String getPeopleRemark() {
        return peopleRemark;
    }

    public void setPeopleRemark(String peopleRemark) {
        this.peopleRemark = peopleRemark == null ? null : peopleRemark.trim();
    }

    public String getPeopleImgurl() {
        return peopleImgurl;
    }

    public void setPeopleImgurl(String peopleImgurl) {
        this.peopleImgurl = peopleImgurl == null ? null : peopleImgurl.trim();
    }

    public String getPeopleTag() {
        return peopleTag;
    }

    public void setPeopleTag(String peopleTag) {
        this.peopleTag = peopleTag;
    }

    public String getVerifysign() {
        return verifysign;
    }

    public void setVerifysign(String verifysign) {
        this.verifysign = verifysign == null ? null : verifysign.trim();
    }
}