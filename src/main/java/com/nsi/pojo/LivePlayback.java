package com.nsi.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
@Data
@TableName("nsi_post_live_playback")
public class LivePlayback implements Serializable {

    private static final long serialVersionUID = 6447657301131910960L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播课程id
     */
    @TableField("course_id")
    private Integer courseId;

    /**
     * 课程类型
     */
    @TableField("course_type")
    private String courseType;

    /**
     * 嘉宾名称
     */
    private String guest;

    /**
     * 视频封面
     */
    @TableField("img_url")
    private String imgUrl;

    /**
     * 视频回放地址
     */
    @TableField("mv_address")
    private String mvAddress;

    /**
     * 视频标题
     */
    @TableField("vidio_titile")
    private String vidioTitile;

    /**
     * 视频介绍(富文本)
     */
    @TableField("vidio_info")
    private String vidioInfo;

    /**
     * 视频观看次数
     */
    @TableField("watch_num")
    private Integer watchNum;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
