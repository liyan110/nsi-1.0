package com.nsi.pojo;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 社区-子评论表
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-17
 */
@TableName("community_comment_son")
public class CommunityCommentSon extends Model<CommunityCommentSon> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 评论人_微信id
     */
    @TableField("wechat_id")
    private String wechatId;

    /**
     * 评论人头像
     */
    private String portrait;

    /**
     * 评论人昵称
     */
    private String nickname;

    /**
     * 被评论对象id 帖子id
     */
    @TableField("object_id")
    private Integer objectId;

    /**
     * 评论内容（富文本）
     */
    private String content;

    /**
     * 审核标记（0待审核，1已通过）
     */
    @TableField("verify_sign")
    private Integer verifySign;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 审核时间
     */
    @TableField("verify_time")
    private Date verifyTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }
    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Integer getVerifySign() {
        return verifySign;
    }

    public void setVerifySign(Integer verifySign) {
        this.verifySign = verifySign;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CommunityCommentSon{" +
        "id=" + id +
        ", wechatId=" + wechatId +
        ", portrait=" + portrait +
        ", nickname=" + nickname +
        ", objectId=" + objectId +
        ", content=" + content +
        ", verifySign=" + verifySign +
        ", createTime=" + createTime +
        ", verifyTime=" + verifyTime +
        "}";
    }
}
