package com.nsi.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Li Yan
 * @create 2019-08-14
 **/
@Data
@TableName("nsi_event_course")
public class EventCourse {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer eventId;
    private String subject;
    private String teacherName;
    private String courseName;
    private String classHour;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date classTime;
}
