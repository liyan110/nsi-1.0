package com.nsi.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * @author Luo Zhen
 */
@Slf4j
@WebFilter(filterName = "char", urlPatterns = "/*")
public class CharFilter implements Filter {

    /**
     * Default constructor.
     */
    public CharFilter() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    }

    /**
     * 过滤器
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        //检查旧接口
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (httpRequest.getQueryString() != null) {
            if (httpRequest.getQueryString().indexOf("whereFrom") != -1) {
                log.info("[旧版功能打印]打印接口{},参数{}", httpRequest.getRequestURI(), httpRequest.getQueryString());
            }
        }
        //修改编码
        request.setCharacterEncoding("UTF-8");
        chain.doFilter(request, response);

    }

    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}
