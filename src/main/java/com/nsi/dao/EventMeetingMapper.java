package com.nsi.dao;


import com.nsi.aop.DataSource;
import com.nsi.pojo.EventMeeting;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-14
 */
@DataSource("dataSource1")
public interface EventMeetingMapper extends BaseMapper<EventMeeting> {

}
