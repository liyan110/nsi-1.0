package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityCommentSon;

/**
 * <p>
 * 社区-子评论表 Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-17
 */
@DataSource("dataSource1")
public interface CommunityCommentSonMapper extends BaseMapper<CommunityCommentSon> {

}
