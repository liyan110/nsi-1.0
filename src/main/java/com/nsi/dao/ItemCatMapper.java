package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ItemCat;

import java.util.List;
@DataSource("dataSource1")
public interface ItemCatMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ItemCat record);

    int insertSelective(ItemCat record);

    ItemCat selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ItemCat record);

    int updateByPrimaryKey(ItemCat record);

    List<ItemCat> selectByParentId(Integer parentId);

    void deleteByParentId(Integer itemId);

    int selectByItemName(String name);

    void updateByItemName(ItemCat itemCat);
}