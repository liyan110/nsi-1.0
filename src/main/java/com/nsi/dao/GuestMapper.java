package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Guest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface GuestMapper extends BaseMapper<Guest> {

    List<Guest> findByName(@Param("name") String name);
}