package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.MyCollect;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.vo.InstitutionCollectVo;
import com.nsi.vo.SchoolCollectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 四库全书-我的收藏表 Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-09-02
 */
@DataSource("dataSource1")
public interface MyCollectMapper extends BaseMapper<MyCollect> {

    List<SchoolCollectVo> findBySchoolCollect(@Param("openId") String openId, @Param("collectType") int collectType);

    List<InstitutionCollectVo> findByInstitutionCollect(@Param("openId") String openId, @Param("collectType") int collectType);
}
