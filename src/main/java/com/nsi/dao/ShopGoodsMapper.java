package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ShopGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface ShopGoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShopGoods record);

    int insertSelective(ShopGoods record);

    ShopGoods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShopGoods record);

    int updateByPrimaryKey(ShopGoods record);

    List<ShopGoods> selectByTypeState(@Param("type") String type, @Param("state") String state, @Param("searchKey")String searchKey);

    List<ShopGoods> goodsRecommend(@Param("goodsId") int goodsId);
}