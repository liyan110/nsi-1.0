package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Projects;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface ProjectsMapper extends BaseMapper<Projects> {

    List<Projects> findProjectByUserMail(String userMail);
}