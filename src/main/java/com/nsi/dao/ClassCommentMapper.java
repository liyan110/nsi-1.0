package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ClassComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface ClassCommentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClassComment record);

    int insertSelective(ClassComment record);

    ClassComment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClassComment record);

    int updateByPrimaryKey(ClassComment record);

//    自定义
    List<ClassComment> selectByCommentatorMail(@Param("CommentatorMail")String CommentatorMail);

    List<ClassComment> selectByObjectId(@Param("objectId")String objectId);

    List<ClassComment> selectByObjectIdVerify(@Param("objectId")String objectId);

    int CommentVerify(String CommentId);
}