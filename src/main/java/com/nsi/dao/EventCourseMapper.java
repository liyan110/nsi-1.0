package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.EventCourse;

/**
 * @author Li Yan
 * @create 2019-08-14
 **/
@DataSource("dataSource1")
public interface EventCourseMapper extends BaseMapper<EventCourse> {

}
