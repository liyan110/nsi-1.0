package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.SchoolCertification;
import org.apache.ibatis.annotations.Param;
@DataSource("dataSource1")
public interface SchoolCertificationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SchoolCertification record);

    int insertSelective(SchoolCertification record);

    SchoolCertification selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SchoolCertification record);

    int updateByPrimaryKey(SchoolCertification record);

    int selectAll(Integer schoolId);

    int deleteBySchoolId(Integer schoolId);

    int modifyBySchoolId(@Param("currentSchool") SchoolCertification currentSchool, @Param("schoolId") Integer schoolId);

    SchoolCertification selectSchoolBySchoolId(Integer schoolId);
}