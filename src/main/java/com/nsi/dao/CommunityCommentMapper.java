package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityComment;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 社区-标准评论表 Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-16
 */
@DataSource("dataSource1")
public interface CommunityCommentMapper extends BaseMapper<CommunityComment> {

    @Update("UPDATE community_comment SET son_comments_num=son_comments_num+1 WHERE id = #{id}")
    int Add_son_comments_num(int id);

    @Update("UPDATE community_comment SET son_comments_num=son_comments_num-1 WHERE id = #{id}")
    int Reduce_son_comments_num(int id);

}
