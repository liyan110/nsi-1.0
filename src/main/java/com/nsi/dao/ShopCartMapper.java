package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ShopCart;

@DataSource("dataSource1")
public interface ShopCartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShopCart record);

    int insertSelective(ShopCart record);

    ShopCart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShopCart record);

    int updateByPrimaryKey(ShopCart record);
//    自定义
    ShopCart selectByUserId(String userId);
}