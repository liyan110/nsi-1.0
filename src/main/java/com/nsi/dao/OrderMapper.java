package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
//    自定义
    Order getOrderByWechatIdAndUnionId(String wechatId,String unionId);

    Order selectByWechatId(@Param("wechatId") String wechatId);

    Order selectByWechatIdAndUnionId(@Param("wechatId") String wechatId,@Param("unionId") String unionId);

    int updateByWechatId(@Param("wechatId") String wechatId, @Param("code") int code);

    int updateByOrderNo(@Param("orderNo") String order_no, @Param("code") int code);

    List<Order> findOrderListByWechatIdCount(@Param("wechatId") String wechatId);
    //    查询课程订单
    List<Order> findOrderListByWechatId(@Param("wechatId") String wechatId,@Param("unionId") String unionId, @Param("productType") String productType);

    Order findOrderByOrderNo(@Param("orderNo") String orderNo);

    Order findOrderByOrderNoAndType(@Param("orderNo") String orderNo);

    int updateByWechatIdAndOrderNo(@Param("wechatId") String wechatId, @Param("orderNo") String orderNo);

    List<Order> findOrderListByStatusAndType(@Param("status") String status, @Param("productType") String productType, @Param("searchKey") String searchKey);

    int deleteByOrderNo(Long orderNo);

    int updateByOrderNoSelective(Order record);

    int selectOneByWechatIdAndStatus(@Param("wechatId") String wechatId,@Param("unionId") String unionId, @Param("status") int status);

    int findOrderListByTodayAndType(@Param("productType") String productType);

    List<Order> findOrderListByToday();

    int findOrderListByWeekAndType(@Param("productType") String productType);

    List<Order> findOrderListByWeek();

    int findOrderListByMonthAndType(@Param("productType") String productType);

    List<Order> findOrderListByMonth();

    int findOrderListByDateAndType(@Param("time") String time, @Param("productType") String productType);

    List<Order> findOrderListByDateList(@Param("time") String time);

    List<Order> findOrderListBySumAndYear(@Param("year") String year);

    int findOrderListCountByYearAndType(@Param("year") String year, @Param("type") String type);

    int findOrderNoAndStatus(@Param("orderNo") String orderNo);

    int findGoodIDAndStatusAndProduct_type(@Param("goods_id") String goods_id,@Param("product_type") String product_type);

    Order findOrderStatuswechatId(@Param("wechatId") String wechatId);

}