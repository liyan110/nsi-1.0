package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.PostCategory;

/**
 * <p>
 * 帖子类目表 Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@DataSource("dataSource1")
public interface PostCategoryMapper extends BaseMapper<PostCategory> {

}
