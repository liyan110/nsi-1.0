package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.School;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface SchoolEighteenMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(School record);

    int insertSelective(School record);

    School selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(School record);

    int updateByPrimaryKey(School record);

    List<School> getList(@Param("searchKey") String searchKey, @Param("verifyCode") String verifyCode);
}