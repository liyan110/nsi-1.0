package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.OldArticles;

import java.util.List;

/**
 * @author Luo Zhen
 */
@DataSource("dataSource1")
public interface OldArticlesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OldArticles record);

    int insertSelective(OldArticles record);

    OldArticles selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OldArticles record);

    int updateByPrimaryKey(OldArticles record);

    List<OldArticles> getList();
}