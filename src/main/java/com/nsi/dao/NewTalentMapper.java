package com.nsi.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.NewTalent;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-11-26
 */
@DataSource("dataSource1")
public interface NewTalentMapper extends BaseMapper<NewTalent> {

}
