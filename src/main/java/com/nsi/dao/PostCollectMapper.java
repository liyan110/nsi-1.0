package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.PostCollect;
import com.nsi.vo.nsi_shop_bar.PostCollectVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@DataSource("dataSource1")
public interface PostCollectMapper extends BaseMapper<PostCollect> {

    /**
     * 我的收藏列表
     *
     * @param openId
     * @return
     */
    List<PostCollectVO> findCollectList(@Param("openId") String openId);

    /**
     * 我的收藏数量
     *
     * @param openId
     * @return
     */
    int findCollectCount(@Param("openId") String openId);

}
