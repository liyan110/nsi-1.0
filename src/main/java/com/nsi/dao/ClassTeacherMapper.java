package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ClassTeacher;
@DataSource("dataSource2")
public interface ClassTeacherMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClassTeacher record);

    int insertSelective(ClassTeacher record);

    ClassTeacher selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClassTeacher record);

    int updateByPrimaryKey(ClassTeacher record);
}