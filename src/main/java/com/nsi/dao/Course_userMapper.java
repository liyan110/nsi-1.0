package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Course_user;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface Course_userMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Course_user record);

    int insertSelective(Course_user record);

    Course_user selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Course_user record);

    int updateByPrimaryKey(Course_user record);

//    自定义
    List<Course_user> selectBy_ClassId_UserMail(@Param("ClassId") String ClassId, @Param("UserMail") String UserMail);

    Course_user findCourseByClassIdAndUserMail(@Param("classId") String classId, @Param("usermail") String usermail);


}
