package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.ShopAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface ShopAddressMapper extends BaseMapper<ShopAddress> {

    int updateByPrimaryKey(ShopAddress record);

    int updateByWechatId(ShopAddress record);

    List<ShopAddress> selectByWechatIdOrUnionId(@Param("wechatId") String wechatId, @Param("unionId") String unionId);

    ShopAddress selectByWechatId(@Param("wechatId") String wechatId);
}