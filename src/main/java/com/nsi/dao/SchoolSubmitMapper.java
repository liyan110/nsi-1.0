package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.SchoolSubmit;
import com.nsi.vo.SchoolSubmitVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学校提交人信息数据库表 Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-10-15
 */
@DataSource("dataSource1")
public interface SchoolSubmitMapper extends BaseMapper<SchoolSubmit> {

    List<SchoolSubmitVo> findListByVerify(@Param("verify") String verify);
}
