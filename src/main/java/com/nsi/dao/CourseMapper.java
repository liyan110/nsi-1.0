package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface CourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);

    List<Course> selectBy_Id_CourseSubject(@Param("id") String id, @Param("CourseSubject") String CourseSubject);

    List<Course> selectBy_UserMail(String UserMail);

}
