package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Vis2019;
import com.nsi.vo.ActiveOrderVo;
import com.nsi.vo.ActiveReviewVo;

import org.apache.ibatis.annotations.Param;


import java.util.List;

@DataSource("dataSource1")
public interface Vis2019Mapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Vis2019 record);

    int insertSelective(Vis2019 record);

    Vis2019 selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Vis2019 record);

    int updateByPrimaryKey(Vis2019 record);

    List<Vis2019> selectByList();

    List<Vis2019> selectByType(String type);

    List<ActiveOrderVo> vis_list_byPhone(String phone);

    List<ActiveReviewVo> vis_orderList(String type);


    List<ActiveOrderVo> checkVis_payList(@Param("type")String type,@Param("searchKey") String searchKey);

    int visOrderCount_All(String type);

    int visOrderCount_Checked(String type);

}