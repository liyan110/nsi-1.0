package com.nsi.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Discounts;

/**
 * <p>
 * 优惠用户信息表 Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-08-21
 */
@DataSource("dataSource1")
public interface DiscountsMapper extends BaseMapper<Discounts> {

}
