package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Postil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface PostilMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Postil record);

    int insertSelective(Postil record);

    Postil selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Postil record);

    int updateByPrimaryKey(Postil record);

    List<Postil> findBySchoolId(@Param("schoolId") Integer schoolId);
}