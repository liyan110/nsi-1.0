package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Comments;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface CommentsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Comments record);

    int insertSelective(Comments record);

    Comments selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Comments record);

    int updateByPrimaryKey(Comments record);

    List<Comments> selectByGuestId(Integer guestId);

    List<Comments> findCommentsByGuestIdAndType(@Param("guestId") Integer guestId, @Param("type") String type);
}