package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.School;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface SchoolMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(School record);

    int insertSelective(School record);

    School selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(School record);

    int updateByPrimaryKey(School record);

    List<School> getBoardsByNameAndId(@Param("boardName") String boardName);

    List<School> searchPower(@Param("listProperties") List<String> listProperties,
                             @Param("listAreas") List<String> listAreas,
                             @Param("listSystem") List<String> listSystem,
                             @Param("listCourse") List<String> listCourse,
                             @Param("fondTime") String fondTime);

    List<School> getList(@Param("searchKey") String searchKey, @Param("verifyCode") String verifyCode);

    List<String> getSchoolName(@Param("keyword") String keyword);

    int checkBySchoolName(String schoolName);

    int selectByVerifySchoolInfo(String verifySign);

    List<School> selectByNoVerifyList(String verifySign);

    int updateByVerifySign(@Param("schoolId") Integer schoolId, @Param("verifySign") String verifySign);

    int getEchartSchoolNum(@Param("chinaArea") String chinaArea, @Param("option") String option);

    List<School> getTimeList(@Param("beginTime") String beginTime, @Param("endTime") String endTime);

    List<School> searchPowerList(
            @Param("listAreas") List<String> listAreas, @Param("listSystem") List<String> listSystem,
            @Param("listProperties") List<String> listProperties, @Param("listAuthority") List<String> listAuthority,
            @Param("listEvaluation") List<String> listEvaluation, @Param("listOrganization") List<String> listOrganization, @Param("listCourse") List<String> listCourse);















}