package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Coupon;

import java.util.List;

@DataSource("dataSource1")
public interface CouponMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Coupon record);

    int insertSelective(Coupon record);

    Coupon selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Coupon record);

    int updateByPrimaryKey(Coupon record);

    List<Coupon> findAll();

}