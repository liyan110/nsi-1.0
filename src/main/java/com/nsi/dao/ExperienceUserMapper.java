package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ExperienceUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2021-03-17
 */
@DataSource("dataSource1")
public interface ExperienceUserMapper extends BaseMapper<ExperienceUser> {

}
