package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Recuitment;
@DataSource("dataSource1")
public interface RecuitmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Recuitment record);

    int insertSelective(Recuitment record);

    Recuitment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Recuitment record);

    int updateByPrimaryKey(Recuitment record);
}