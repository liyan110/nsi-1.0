package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.RecuitmentInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface RecuitmentInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecuitmentInfo record);

    int insertSelective(RecuitmentInfo record);

    RecuitmentInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecuitmentInfo record);

    int updateByPrimaryKey(RecuitmentInfo record);

    List<RecuitmentInfo> selectBySearchKey(@Param("searchKey") String searchKey);
}