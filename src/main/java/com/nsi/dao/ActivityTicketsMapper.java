package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.ActivityTickets;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-26
 */
@DataSource("dataSource1")
public interface ActivityTicketsMapper extends BaseMapper<ActivityTickets> {

}
