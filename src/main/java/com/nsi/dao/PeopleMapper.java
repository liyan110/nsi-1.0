package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.People;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface PeopleMapper {
    int deleteByPrimaryKey(Integer peopleId);

    int insert(People record);

    int insertSelective(People record);

    People selectByPrimaryKey(Integer peopleId);

    int updateByPrimaryKeySelective(People record);

    int updateByPrimaryKey(People record);

    int checkUsername(String str);

    int checkEmail(String str);

    int checkPhone(String str);

    List<People> queryPeopleList(@Param("searchKey") String searchKey);

    List<People> queryPeopleListBySearchKey(@Param("searchKey") String searchKey);

    List<People> queryPeopleListByPeopleNum(@Param("searchKey") String searchKey);

    People findAll(Integer peopleId);
}