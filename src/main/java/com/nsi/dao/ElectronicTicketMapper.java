package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.ElectronicTicket;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-04-01
 */
@DataSource("dataSource1")
public interface ElectronicTicketMapper extends BaseMapper<ElectronicTicket> {

}
