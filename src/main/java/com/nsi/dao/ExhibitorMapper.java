package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Exhibitor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 展位表 Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-02
 */
@DataSource("dataSource1")
public interface ExhibitorMapper extends BaseMapper<Exhibitor> {

}
