package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Item;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface ItemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Item record);

    int insertSelective(Item record);

    Item selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Item record);

    int updateByPrimaryKey(Item record);

    Item findItemById(Integer itemId);

    List<Item> findItemList(@Param("searchKey") String searchKey);

    void updateStatusById(@Param("itemId") Integer itemId, @Param("status") Integer status);
}