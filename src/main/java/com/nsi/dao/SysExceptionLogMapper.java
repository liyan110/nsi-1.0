package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.SysExceptionLog;

/**
 * @author Li Yan
 * @create 2019-10-21
 **/
@DataSource("dataSource1")
public interface SysExceptionLogMapper extends BaseMapper<SysExceptionLog> {

}
