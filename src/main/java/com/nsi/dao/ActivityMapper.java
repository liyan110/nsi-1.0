package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface ActivityMapper{
    int deleteByPrimaryKey(Integer id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

    int updateByUserMail(@Param("userMail") String userMail, @Param("status") String status);

    List<Activity> getActivityList();

    List<Activity> selectByTitleAndContent(String title1);

    Activity queryByUserName(@Param("content8") String content8);
}