package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Members;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface MembersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Members record);

    int insertSelective(Members record);

    Members selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Members record);

    int updateByPrimaryKey(Members record);

    List<Members> queryMembers(@Param("searchKey") Integer searchKey);
}