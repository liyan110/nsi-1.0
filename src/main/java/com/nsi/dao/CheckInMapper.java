package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.CheckIn;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface CheckInMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CheckIn record);

    int insertSelective(CheckIn record);

    CheckIn selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CheckIn record);

    int updateCheckIn(CheckIn record);

    int updateByPrimaryKey(CheckIn record);

    int checkInByName(String name);

    List<CheckIn> selectGetList(@Param("keyword") String keyword);

    int selectCount();

    int deleteByCheckIds(@Param("checkIds") List<String> checkIds);

    List<CheckIn> selectByCheckInTime(@Param("keyword") String keyword);

    List<CheckIn> selectByNoCheckInTime(@Param("keyword") String keyword);

    CheckIn selectByToken(String token);

    int updateCheckInTime(String token);
}