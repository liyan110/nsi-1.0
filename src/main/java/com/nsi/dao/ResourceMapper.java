package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Luo Zhen
 * @create 2018-08-03
 */
@DataSource("dataSource1")
public interface ResourceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Resource record);

    int insertSelective(Resource record);

    Resource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Resource record);

    int updateByPrimaryKey(Resource record);

    List<Resource> getList(@Param("type") String type, @Param("year") String year);
}