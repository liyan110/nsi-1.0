package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityUser;

/**
 * <p>
 * 社区用户 Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-13
 */
@DataSource("dataSource1")
public interface CommunityUserMapper extends BaseMapper<CommunityUser> {

}
