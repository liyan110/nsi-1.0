package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Role;

import java.util.List;
@DataSource("dataSource1")
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> selectAll();

    Role findRoleByUserMail(String userMail);

}