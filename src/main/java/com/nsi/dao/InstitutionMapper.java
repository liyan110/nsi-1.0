package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Institution;

@DataSource("dataSource1")
public interface InstitutionMapper extends BaseMapper<Institution> {


}