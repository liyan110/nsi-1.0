package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Teacher;
@DataSource("dataSource2")
public interface TeacherMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Teacher record);

    int insertSelective(Teacher record);

    Teacher selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Teacher record);

    int updateByPrimaryKey(Teacher record);
}