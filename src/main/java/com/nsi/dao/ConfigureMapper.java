package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Configure;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface ConfigureMapper extends BaseMapper<Configure> {

    List<Configure> selectByType(String type);

    Configure findByType(@Param("type") String type);

    int updateByTemplateHtml(@Param("templateHtml") String templateHtml, @Param("type") String type);

    List<Configure> getCongfiureList(@Param("typeName") String typeName);

}