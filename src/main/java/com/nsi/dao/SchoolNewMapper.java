package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.SchoolNew;
import com.nsi.vo.SchoolStatistics;
import com.nsi.vo.SchoolTeacherStatistics;
import com.nsi.vo.SchoolTuitionSta;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-10
 */
@DataSource("dataSource1")
public interface SchoolNewMapper extends BaseMapper<SchoolNew> {

    List<SchoolNew> findAll(@Param("searchKey") String searchKey, @Param("verifySign") Integer verifySign);

    List<SchoolNew> searchPowerList(@Param("listAreas") List<String> listAreas,
                                    @Param("listSystem") List<String> listSystem,
                                    @Param("listProperties") List<String> listProperties,
                                    @Param("listCourse") List<String> listCourse);

    List<SchoolNew> findNewSchoolItem();

    List<SchoolNew> findCityWideBytown(@Param("id") int id);

    List<SchoolNew> findCityWideByprovince(@Param("id") int id);

    List<SchoolNew> findCityWideByrand();

    int checkBySchoolName(String schoolName);

    List<Map<String, Integer>> getSchoolRatio();

    List<SchoolStatistics> getAllSchoolRatio();

    int getCourseRatio(@Param("course") String course);

    int getGongSchoolRatioCount(@Param("province") String province, @Param("course") String course);

    int getMinSchoolRatioCount(@Param("province") String province, @Param("course") String course);

    int getWiSchoolRatioCount(@Param("province") String province, @Param("course") String course);

    List<Map<String, Object>> getOperatorStateCount();

    List<SchoolTeacherStatistics> getTeacherNumCount();

    int getGongSystemRatioCount(String system);

    int getMinSystemRatioCount(String system);

    int getWiSystemRatioCount(String system);

    List<Map<String, Object>> getTenYearSchoolNum(Integer time);

    List<Map<String, Object>> getAllProvince();

    List<SchoolTuitionSta> getAverageTuition();

    List<SchoolTuitionSta> getAverageTuitionByProvince(@Param("province") String province);

    int getGongSchoolAuthCount(@Param("province") String province, @Param("auth") String auth);

    int getMinSchoolAuthCount(@Param("province") String province, @Param("auth") String auth);

    int getWiSchoolAuthCount(@Param("province") String province, @Param("auth") String auth);
}

