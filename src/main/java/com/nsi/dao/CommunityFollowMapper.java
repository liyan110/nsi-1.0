package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityFollow;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社区关注 Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-31
 */
@DataSource("dataSource1")
public interface CommunityFollowMapper extends BaseMapper<CommunityFollow> {

}
