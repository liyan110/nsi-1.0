package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Advertisement;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface AdvertisementMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Advertisement record);

    int insertSelective(Advertisement record);

    Advertisement selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Advertisement record);

    int updateByPrimaryKey(Advertisement record);
//    通过广告类型名查询
    List<Advertisement> selectByTypeName(@Param("typeName") String typeName);
}