package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> selectAllList(@Param("siftType") String siftType, @Param("keyword") String keyword);

    int getCount();

    int selectRowCount();

    List<Article> findArticleBySiftTypeAndArticleCat(@Param("siftType") String siftType, @Param("articleCat") String articleCat);

    List<Integer> getId();

    List<Article> queryByArticle();

    Article findArticleByTitle(String title);

    List<Article> selectBySearchKey(@Param("searchKey") String searchKey);

    List<Article> findTitleOrSummaryOrContentLike(@Param("searchKey") String searchKey);
}