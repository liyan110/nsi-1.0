package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.ShopInvoice;
import com.nsi.vo.InvoiceOrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface ShopInvoiceMapper extends BaseMapper<ShopInvoice> {

    List<InvoiceOrderVo> InvoiceOrderList(@Param("SearchKey") String SearchKey, @Param("manageState") String manageState, @Param("financeState") String financeState);

    int selectCountByOrderNo(@Param("orderNo") String orderNo);
}