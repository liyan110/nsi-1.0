package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.ForeignTeacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource1")
public interface ForeignTeacherMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ForeignTeacher record);

    int insertSelective(ForeignTeacher record);

    ForeignTeacher selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ForeignTeacher record);

    int updateByPrimaryKey(ForeignTeacher record);

    List<ForeignTeacher> querySchoolBlackList(@Param("searchKey") String searchKey, @Param("enableCode") int enableCode);

}