package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityAnswer;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提问互动-回复表 Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-09
 */
@DataSource("dataSource1")
public interface CommunityAnswerMapper extends BaseMapper<CommunityAnswer> {

}
