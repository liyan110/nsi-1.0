package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.NewTalent;
import com.nsi.pojo.Talent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface TalentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Talent record);

    int insertSelective(Talent record);

    Talent selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Talent record);

    int updateByPrimaryKey(Talent record);

    int checkedByName(String name);

    List<Talent> selectBySearchKey(@Param("searchKey") String searchKey, @Param("isPublic") String isPublic, @Param("verifySign") String verifySign);

    List<Talent> selectByUserMail(@Param("userMail") String userMail);

    Talent selectById(@Param("talentId") Integer talentId);

    Talent checkByHavaTalent(String userMail);

    List<Talent> selectAllList(@Param("pageNum") int pageNum, @Param("pageSize") int pageSize);

    int ckeckByUpfile(String userMail);

    int updateByUserMail(Talent record);

    int updateImageByUserMail(String userMail);

    int selectRowCount();

    int setlectByVerifyTalentInfo(@Param("verifyCode") String verifyCode);
}