package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Relation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface RelationMapper extends BaseMapper<Relation> {

    List<Relation> selectByType(@Param("type") String type, @Param("searchId") String searchId);

}