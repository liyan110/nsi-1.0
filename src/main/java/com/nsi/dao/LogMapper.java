package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.Log;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Luo Zhen
 */
@DataSource(value = "dataSource1")
public interface LogMapper extends BaseMapper<Log> {


    List<Log> getList(@Param("keyword") String keyword);

    List<Log> searchLogList(@Param("pageNum") int pageNum, @Param("pageSize") int pageSize);

    int count();

    int getCount(@Param("type") String type);

    int findByAttach(@Param("attach") String attach);

    List<Log> findBy_sign_index01(@Param("sign") String sign, @Param("OpenId") String OpenId);

    List<Log> findBy_sign_index01_index09(@Param("sign") String sign, @Param("OpenId") String OpenId, @Param("LaunchOpenId") String LaunchOpenId);

    List<Log> findBy_sign_index09(@Param("sign") String sign, @Param("OpenId") String OpenId);

    Log selectByIndex05(@Param("sign") String sign, @Param("openid") String openid);

    List<Log> selectByIndex07(@Param("sign") String sign, @Param("camp") String camp);

    List<Log> selectByIndex07All(@Param("sign") String sign);

    int updateByIndex05(@Param("sign") String sign, @Param("openid") String openid, @Param("number") String number);

    int selectByIndex03Count(@Param("index") String index, @Param("h5log") String h5log);

    int selectByIndex04Count(@Param("index") String index, @Param("h5log") String h5log);

    int selectByIndex05Count(@Param("index") String index, @Param("h5log") String h5log);

    int selectByIndex06Count(@Param("index") String index, @Param("h5log") String h5log);

    int selectByIndex05AndIndex06(@Param("index05") String index05, @Param("index06") String index06, @Param("h5log") String h5log);

    int selectBySign(@Param("sign") String sign);


}