package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.NsiEvent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 活动定义表 Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@DataSource("dataSource1")
public interface NsiEventMapper extends BaseMapper<NsiEvent> {

}
