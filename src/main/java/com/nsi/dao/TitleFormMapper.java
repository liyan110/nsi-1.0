package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.TitleForm;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-20
 */
@DataSource("dataSource1")
public interface TitleFormMapper extends BaseMapper<TitleForm> {

}
