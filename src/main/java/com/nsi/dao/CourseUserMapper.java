package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.CourseUser;
import org.apache.ibatis.annotations.Param;
@DataSource("dataSource2")
public interface CourseUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CourseUser record);

    int insertSelective(CourseUser record);

    CourseUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseUser record);

    int updateByPrimaryKey(CourseUser record);

    int selectByCourseIdAndUserMail(@Param("classId") String classId, @Param("userMail") String userMail);
}