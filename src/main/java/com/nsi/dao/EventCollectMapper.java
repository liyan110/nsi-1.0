package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.EventCollect;
import com.nsi.vo.EventCollectOrderVo;
import com.nsi.vo.EventOrderVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 活动用户报名表 Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-03-23
 */
@DataSource("dataSource1")
public interface EventCollectMapper extends BaseMapper<EventCollect> {

    List List_byEventId(@Param("eventId") Integer EventId, @Param("orderStatus") Integer orderStatus, @Param("searchKey") String searchKey);

    List List_byPhone(@Param("phone") String phone, @Param("activeId") Integer activeId);

    List<EventCollectOrderVo> findEventOrderList(@Param("verif") Integer verify,
                                                 @Param("status") Integer status,
                                                 @Param("activeId") Integer activeId,
                                                 @Param("searchKey") String searchKey);

    List<EventOrderVo> findEventCollectAndOrderList(@Param("acticeId") Integer acticeId, @Param("searchKey") String searchKey);

    List<EventOrderVo> findByActivePayList(@Param("activityId") Integer activeId, @Param("searchKey") String searchKey);

    int findByActivePayListCount(@Param("activityId") Integer activityId);

    int findByActiveCheckInCount(@Param("activityId") Integer activityId);
}
