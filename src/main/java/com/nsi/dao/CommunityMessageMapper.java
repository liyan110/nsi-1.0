package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.CommunityMessage;

/**
 * <p>
 * 社区消息 Mapper 接口
 * </p>
 *
 * @author Li Yan
 * @since 2019-12-26
 */
@DataSource("dataSource1")
public interface CommunityMessageMapper extends BaseMapper<CommunityMessage> {

}
