package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Boards;
import org.apache.ibatis.annotations.Param;
@DataSource("dataSource1")
public interface BoardsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Boards record);

    int insertSelective(Boards record);

    Boards selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Boards record);

    int updateByPrimaryKey(Boards record);

    String findByBoardname(String boardName);

    int updateSidByBoardName(@Param("sid") String sid, @Param("boardName") String boardName);
}