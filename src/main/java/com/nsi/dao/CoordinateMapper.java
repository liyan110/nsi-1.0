package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Coordinate;
@DataSource("dataSource1")
public interface CoordinateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Coordinate record);

    int insertSelective(Coordinate record);

    Coordinate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Coordinate record);

    int updateByPrimaryKey(Coordinate record);

    Coordinate selectBySchoolId(Integer schoolId);

    int selectCountBySchoolId(Integer schoolId);
}