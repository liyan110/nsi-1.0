package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Assignment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@DataSource("dataSource2")
public interface AssignmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Assignment record);

    int insertSelective(Assignment record);

    Assignment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Assignment record);

    int updateByPrimaryKey(Assignment record);

    Assignment findAssignmentByUserMail(String usermail);

    Assignment findAssignmentByCourseId(Integer courseId);

    Assignment findInfoByTeacherId(Integer courseId);

    List<Assignment> findAssignmentListByCourseId(Integer courseId);

    List<Assignment> findAll(@Param("courseId") String courseId, @Param("validCode") int validCode);

    List<Assignment> findOneByVerify(@Param("courseId") String courseId);

    int setAssignmentStatus(@Param("assignmentId") Integer assignmentId);
}