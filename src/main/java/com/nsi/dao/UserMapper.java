package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //根据用户邮箱，修改用户等级
    int updateByUserName(@Param("UserName") String UserName, @Param("Member_sign") int Member_sign);

    User selectByUserName(String userName);

    //后台搜索用户
    List<User> selectByAdminSearch(@Param("searchKey") String searchKey);

    int selectByVerifyCode(@Param("Usermail") String Usermail, @Param("VerifyCode") String VerifyCode);

    int checkByUserName(String userName);

    User selectByNameAndPwd(@Param("userName") String userName, @Param("passWord") String passWord);

    //使用手机号登录
    User selectByPhoneAndPwd(@Param("User_phone") String User_phone, @Param("passWord") String passWord);

    User selectByWechatId(String OpenId);

    int updateByWechatBinding(@Param("UserName") String UserName, @Param("OpenId") String OpenId);

    int updateByPassword(@Param("UserName") String UserName, @Param("Password") String Password);

    void modifyTestPassword(@Param("userId") Integer userId, @Param("newPassword") String newPassword);

    //    删除账号
    int deleteByUserName(@Param("UserName") String UserName);

    User validByWechatId(@Param("wechatId") String wechatId);

    User validByWechatIdAndUnionId(@Param("wechatId") String wechatId, @Param("unionId") String unionId);

    int updateByWechatId(@Param("wechatId") String wechatId, @Param("unionId") String unionId);

    User selectByUnionId(@Param("unionId") String unionId);

    int updateByWechatBindingUnionId(@Param("username") String username, @Param("unionId") String unionId);
}