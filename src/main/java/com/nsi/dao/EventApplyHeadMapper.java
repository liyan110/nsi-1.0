package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.EventApplyHead;

/**
 * @author Li Yan
 * @create 2019-08-16
 **/
@DataSource("dataSource1")
public interface EventApplyHeadMapper extends BaseMapper<EventApplyHead>{

}
