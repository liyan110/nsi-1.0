package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.SchoolVisit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource1")
public interface SchoolVisitMapper extends BaseMapper<SchoolVisit> {

    List<SchoolVisit> selectVisitList(@Param("schoolId") Integer schoolId, @Param("openId") String openId);

}