package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.CourseCategory;
import com.nsi.vo.CourseItemVo;
import com.nsi.vo.CourseListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource2")
public interface CourseCategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CourseCategory record);

    int insertSelective(CourseCategory record);

    CourseCategory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseCategory record);

    int updateByPrimaryKey(CourseCategory record);

    List<CourseCategory> findAll(String status);

    List<CourseCategory> findCourseCategoryByListId(@Param("listId") Integer listId, @Param("status") String status);

    void deleteByListId(Integer listId);

    CourseCategory findCourseCategoryByCourseId(Integer courseId);

    List<CourseItemVo> selectByListId(@Param("listId") Integer listId, @Param("status") Integer status);
}