package com.nsi.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.PostCategoryItem;

/**
 * <p>
 * 帖子详情表 Mapper 接口
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-12-16
 */
@DataSource("dataSource1")
public interface PostCategoryItemMapper extends BaseMapper<PostCategoryItem> {

}
