package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.LivePlayback;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author luo Zhen
 * @since 2020-02-25
 */
@DataSource("dataSource1")
public interface LivePlaybackMapper extends BaseMapper<LivePlayback> {

}
