package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Properties;
@DataSource("dataSource2")
public interface PropertiesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Properties record);

    int insertSelective(Properties record);

    Properties selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Properties record);

    int updateByPrimaryKey(Properties record);

    Properties selectByCourseId_playback(Integer courseId_playback);
}