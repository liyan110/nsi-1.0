package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.EntryAudit;
import com.nsi.vo.ActiveOrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author Luo Zhen
 * @since 2019-09-16
 */
@DataSource("dataSource1")
public interface EntryAuditMapper extends BaseMapper<EntryAudit> {

    List<ActiveOrderVo> checkTicketApproved(@Param("telphone") String telphone);

    List<EntryAudit> selectEntryAuditList(@Param("entryAudit") EntryAudit entryAudit);


    List<ActiveOrderVo> getEntryListBySearchKey(@Param("searchKey") String searchKey);

    int EntryListCount_All();

    int EntryListCount_Checked();

}
