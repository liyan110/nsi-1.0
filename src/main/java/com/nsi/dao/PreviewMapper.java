package com.nsi.dao;

import com.nsi.aop.DataSource;
import com.nsi.pojo.Preview;

import java.util.List;
@DataSource("dataSource1")
public interface PreviewMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Preview record);

    int insertSelective(Preview record);

    Preview selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Preview record);

    int updateByPrimaryKey(Preview record);

    List<Preview> getList();
}