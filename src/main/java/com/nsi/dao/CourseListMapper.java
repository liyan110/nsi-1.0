package com.nsi.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.nsi.aop.DataSource;
import com.nsi.pojo.CourseList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@DataSource("dataSource2")
public interface CourseListMapper extends BaseMapper<CourseList> {

    List<CourseList> findAll(@Param("status") String status);

    List<CourseList> CourseRecommend(@Param("id") int id);

    List<CourseList> findCourseListItem();
}